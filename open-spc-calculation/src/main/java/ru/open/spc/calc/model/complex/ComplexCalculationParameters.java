package ru.open.spc.calc.model.complex;

import lombok.Data;

@Data public class ComplexCalculationParameters extends AbstractComplexCalculationParameters {

    private Double strike1From;
    private Double strike1To;
    private Double strike1Step;

}
