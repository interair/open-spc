package ru.open.spc.calc.kernel.enrichers;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.dao.util.DateUtil;

import java.time.LocalDateTime;

import static ru.open.spc.calc.util.HeaderParams.UI_DESCRIPTION;

@MessageEndpoint
public class DatesEnricher {

    @Transformer(inputChannel = "prepareDatesCalculationChannel", outputChannel = "productRouterChannel")
    public CalculationParameters enrichInitVariable(final CalculationParameters param, @Header(UI_DESCRIPTION) DescriptionWriter descriptionWriter) {
        initVariables((SimpleCalculationParameters) param.getCalculationParams(), descriptionWriter);
        return param;
    }

    private void initVariables(SimpleCalculationParameters param, DescriptionWriter desc) {
        final double daysCountBtwnNowExpOpt = DateUtil.countDaysBetweenABS(LocalDateTime.now(), param.getOptExpDate());
        param.setDaysCountToExpOpt(daysCountBtwnNowExpOpt);
        desc.write("days.count.to.expire.option", daysCountBtwnNowExpOpt);

        final double daysCountBtwnStartProductEndProduct = DateUtil.countDaysBetween(param.getDepositStartDate(), param.getProductEndDate());
        param.setDaysCountToEndProduct(daysCountBtwnStartProductEndProduct);
        desc.write("days.count.to.expire.product", daysCountBtwnStartProductEndProduct);
    }
}
