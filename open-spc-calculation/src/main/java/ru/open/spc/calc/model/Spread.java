package ru.open.spc.calc.model;

public enum Spread {
    BID, ASK, NONE
}
