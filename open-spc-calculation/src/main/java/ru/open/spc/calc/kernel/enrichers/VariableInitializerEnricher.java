package ru.open.spc.calc.kernel.enrichers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.kernel.services.ApproximationCalculationService;
import ru.open.spc.calc.model.ApproximatedPrice;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.complex.flex.PriceType;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.model.simple.flex.PriceTypeParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.model.enums.ProductCode;

import java.time.LocalDate;

import static java.util.EnumSet.of;
import static ru.open.spc.calc.kernel.services.ApproximationCalculationService.writeCorrectionLog;
import static ru.open.spc.calc.util.HeaderParams.CALCULATION_BASE;
import static ru.open.spc.calc.util.HeaderParams.UI_DESCRIPTION;
import static ru.open.spc.calc.util.MathUtil.fromPercent;

@MessageEndpoint
public class VariableInitializerEnricher {

	private final static Logger LOG = LoggerFactory.getLogger(VariableInitializerEnricher.class);

	@Autowired private ApproximationCalculationService approximationCalculating;

	@Transformer(inputChannel = "variableInitializerEnricherChannel", outputChannel = "prepareDatesCalculationChannel")
	public CalculationParameters enrichInitVariable(final CalculationParameters param,
                                                    @Header(UI_DESCRIPTION) DescriptionWriter descriptionWriter,
                                                    @Header(CALCULATION_BASE) CalculationBase calculationBase) {
		try {
			initVariables((SimpleCalculationParameters) param.getCalculationParams(), param.getProduct().getCode(), descriptionWriter, calculationBase);
		} catch (Exception e) {
			LOG.error("Ooopss... can't enrich parameters", e);
		}
		return param;
	}

	private void initVariables(SimpleCalculationParameters param, ProductCode productCode, DescriptionWriter desc, CalculationBase calculationBase) {
		if (calculationBase == CalculationBase.FLEX || checkApplyCorrection(productCode)) {   //TODO: think about splitting it
			correctPriceWithAppx(param, desc, calculationBase);
		}
		correctPriceWithPointPrice(param);
		convertPriceToRub(param, desc);
		desc.write("asset.price", param.getTotalAssetPrice());
		calculateStrike1Correction(param, desc);
		desc.write("volatilitySpread.value", param.getVolatilitySpread());
	}

    private boolean checkApplyCorrection(final ProductCode productCode) {
        return of(ProductCode.BaseProtect, ProductCode.SmartDeposit).contains(productCode);
    }

    private void correctPriceWithAppx(SimpleCalculationParameters param, DescriptionWriter desc, CalculationBase calculationBase) {
		param.setTotalAssetPrice(priceCorrection(param, desc, calculationBase));
	}

	private double priceCorrection(SimpleCalculationParameters param, DescriptionWriter desc, CalculationBase calculationBase) {
		final ApproximatedPrice appr = approximationCalculating.applyBAApproximation(param.getAsset(), LocalDate.now(), param.getTotalAssetPrice(), calculationBase);
		writeCorrectionLog(appr, desc);
		return appr.getTotal();
	}

	private void convertPriceToRub(SimpleCalculationParameters param, DescriptionWriter desc) {
        if (param.getAssetUnity() == AssetUnity.USD) {
            param.setTotalAssetPriceInRub(param.getTotalAssetPriceInRub() * param.getDollarExchangeRate());
            desc.write("asset.price.in.rub", param.getTotalAssetPriceInRub())
                .write("usd", param.getDollarExchangeRate());
        }
	}

	private void calculateStrike1Correction(SimpleCalculationParameters param, DescriptionWriter desc) {
        if (param.getStrike1() == null) {param.setStrike1(0d);}
        double withStrike1;
        PriceType priceType;

        if (param instanceof PriceTypeParameters) {
            PriceTypeParameters typeParameters = (PriceTypeParameters) param;
            priceType = typeParameters.getPriceType();
        } else {
            priceType = PriceType.PERCENT;
        }

        switch (priceType) {
            case VALUE:
                withStrike1 = param.getStrike1();
                break;
            default:
                withStrike1 = param.getTotalAssetPrice() * (1 + fromPercent(param.getStrike1()));
                break;
        }

        param.setTotalAssetPriceWithStrike1Coef(withStrike1);
        if (param.getStrike1() != 0) {
            desc.write("asset.price.str1", priceType, param.getStrike1(), withStrike1);
        }
	}

	private void correctPriceWithPointPrice(SimpleCalculationParameters param) {
		param.setTotalAssetPriceInRub(param.getTotalAssetPrice() * param.getItemCost());
		LOG.debug("itemCost() = {}, totalAssetPrice() = {}, totalAssetPriceInRub() = {}",
			param.getItemCost(), param.getTotalAssetPrice(), param.getTotalAssetPriceInRub());
	}
}
