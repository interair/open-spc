package ru.open.spc.calc.kernel.result.factory;

import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.model.result.PricingResult;

public interface Creator<T extends PricingResult> {

	T createResult(ResultBuilder rb);
}
