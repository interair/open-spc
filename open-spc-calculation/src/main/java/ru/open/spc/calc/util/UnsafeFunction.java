package ru.open.spc.calc.util;

public interface UnsafeFunction<K, V, T extends Throwable> {

    V apply(K key) throws T;
}
