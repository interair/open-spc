package ru.open.spc.calc.kernel.activators;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import ru.open.auto.call.*;
import ru.open.spc.calc.kernel.result.factory.ResultFactory;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.calc.model.simple.AutoCallParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.dao.util.DateUtil;
import ru.open.spc.dao.util.RoundUtil;
import ru.open.spc.model.AutoCallAsset;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.model.result.PricingResult;
import ru.open.spc.services.soap.auto.call.AutoCallClient;

import javax.xml.bind.JAXBElement;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static ru.open.spc.calc.util.HeaderParams.UI_CALLBACK;
import static ru.open.spc.calc.util.HeaderParams.UI_DESCRIPTION;
import static ru.open.spc.calc.util.MathUtil.fromPercent;

@MessageEndpoint
@Slf4j
public class AutoCallServiceEndpoint implements CalculationService {

	@Autowired
	private AutoCallClient autoCallClient;
	@Autowired
	private ResultFactory resultFactory;

	private ObjectFactory objectFactory = new ObjectFactory();
	private static final Double DAYS_YEAR = 365d;

	@Override
	@ServiceActivator(inputChannel = "autoCallChannel")
	public void calculate(Message<CalculationParameters> message) {

		final MessageHeaders headers = message.getHeaders();
		final CalculationParameters payload = message.getPayload();
		final DescriptionWriter dsc = (DescriptionWriter) headers.get(UI_DESCRIPTION);
		final UiCallback<PricingResult> uiCallback = (UiCallback<PricingResult>) headers.get(UI_CALLBACK);
		List<PricingResult> result;
		try {
			result = doCalculating(dsc, payload);
		} catch (Exception e) {
			log.error("Ooops....", e);
			log.error(dsc.getResult());
			dsc.write("api.call.error", e);
			result = DateUtil.getEmptyRes();
		}
		uiCallback.addResults(result);

	}

	private List<PricingResult> doCalculating(DescriptionWriter dsc, CalculationParameters payload) {
		AutoCallParameters calculationParams = (AutoCallParameters) payload.getCalculationParams();
		Integer countAssets = calculationParams.getCountAssets();
		GetPricingData getPricingData = new GetPricingData();
		getPricingData.setInterest(fromPercent(calculationParams.getInterestRate()));
		getPricingData.setCoupon(RoundUtil.roundHUP(fromPercent(calculationParams.getClientIncome()) * (calculationParams.getCountOfDays()/DAYS_YEAR)
            / calculationParams.getCoupon().doubleValue(), 5));
		getPricingData.setInitInvest(calculationParams.getInvestingSum());
		getPricingData.setBottomBarrierRate(fromPercent(calculationParams.getBottomBarrierRate()));
		getPricingData.setTopBarrierRate(fromPercent(calculationParams.getTopBarrierRate()));
		getPricingData.setAdditionalCoupon(fromPercent(calculationParams.getAdditionalCoupon()));
		getPricingData.setSpotPriceRef(convertToArraySpotPriceRef(calculationParams.getAutoCallAssets(), countAssets));
		getPricingData.setCorr(convertToArrayArray(calculationParams.getCorrelations(), countAssets));
		getPricingData.setShipmentBarrierRate(fromPercent(calculationParams.getShipmentBarrierRate()));
		getPricingData.setTimePassed(calculationParams.getTimePassed().doubleValue() / DAYS_YEAR);


		getPricingData.setCurrentAssetVals(convertToArrayAssetVal(calculationParams.getAutoCallAssets(), countAssets));

		ArrayOfdouble sigmas = new ArrayOfdouble();
		sigmas.getDouble().addAll(calculationParams.getAutoCallAssets().stream().map(a -> a.getSigma()).limit(countAssets).collect(Collectors.toList()));
		getPricingData.setSigmas(objectFactory.createGetPricingDataSigmas(sigmas));

		ArrayOfdouble triggers = new ArrayOfdouble();
		triggers.getDouble().addAll(calculateTriggerPoints(calculationParams.getCountOfDays(), calculationParams.getCoupon()));
		getPricingData.setTriggerPoints(objectFactory.createGetPricingDataTriggerPoints(triggers));
		dsc.write("api.call");
		dsc.writeRaw(getPricingData.toString());
		GetPricingDataResponse calculate = autoCallClient.calculate(getPricingData);
        dsc.writeEmptyLine();dsc.writeEmptyLine();
        dsc.write("api.call.result");
        dsc.writeRaw(calculate.toString());
        log.info("result {}", calculate);
		final ResultBuilder builder = new ResultBuilder().descResult(dsc.getResult()).product(payload.getProduct())
				.interestRate(calculationParams.getInterestRate()).direction(Direction.RISING)
				.priceDate(LocalDateTime.now()).timeout(calculationParams.getTimeout())
				.productStartDate(calculationParams.getDepositStartDate())
				.productStopDate(calculationParams.getDepositStartDate().plusDays(calculationParams.getCountOfDays()))
				.maximumEarning(calculate.getGetPricingDataResult().getPrice());
		return Collections.singletonList(resultFactory.createPricingResult(builder));
	}

    private Collection<? extends Double> calculateTriggerPoints(Long countOfDays, Integer coupon) {
        double total = countOfDays / DAYS_YEAR;
        double part = total / coupon;
        List<Double> doubles = new ArrayList<>();
        for (int i = 1; i < coupon + 1; i++) {
            doubles.add(part * i);
        }

        return doubles;
    }

	private JAXBElement<ArrayOfdouble> convertToArrayAssetVal(List<AutoCallAsset> assets, Integer countAssets) {
		ArrayOfdouble arrayOfdouble = new ArrayOfdouble();
		arrayOfdouble.getDouble().addAll(assets.stream().map(a -> a.getRelativePrice()).limit(countAssets).collect(Collectors.toList()));

		return objectFactory.createGetPricingDataCurrentAssetVals(arrayOfdouble);
	}

	private JAXBElement<ArrayOfdouble> convertToArraySpotPriceRef(List<AutoCallAsset> assets, Integer countAssets) {
		ArrayOfdouble arrayOfdouble = new ArrayOfdouble();
		arrayOfdouble.getDouble().addAll(assets.stream().map(a -> a.getInitRelativePrice()).limit(countAssets).collect(Collectors.toList()));

		return objectFactory.createGetPricingDataSpotPriceRef(arrayOfdouble);
	}

	private JAXBElement<ArrayOfArrayOfdouble> convertToArrayArray(Double[][] assets, Integer countAssets) {
		ArrayOfArrayOfdouble arrayOfdouble = new ArrayOfArrayOfdouble();
		Double[][] limited = Arrays.copyOfRange(assets, 0, countAssets);

        StringBuilder stringBuilder = new StringBuilder();

        for (Double[] doubles : limited) {
            stringBuilder.append(Arrays.toString(doubles));
        }
        log.info("source Map {}", stringBuilder);

        for (int i = 0; i < limited.length; i++) {
            Double[] doubles = limited[i];
            for (int j = i; j < doubles.length; j++) {
                limited[j][i] = doubles[j];

            }
        }

        stringBuilder = new StringBuilder();

        for (Double[] doubles : limited) {
            stringBuilder.append(Arrays.toString(doubles));
        }

        log.info("result Map {}", stringBuilder);

        for (Double[] asset : limited) {
            List<Double> doubles = Arrays.asList(Arrays.copyOfRange(asset, 0, countAssets));
            ArrayOfdouble ad = new ArrayOfdouble();
            ad.getDouble().addAll(doubles);
            arrayOfdouble.getArrayOfdouble().add(ad);
        }

        return objectFactory.createGetPricingDataCorr(arrayOfdouble);
	}
}
