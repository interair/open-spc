package ru.open.spc.calc.kernel.activators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.model.Series;
import ru.open.spc.model.Underlying;
import ru.open.spc.model.result.PricingResult;

import java.util.Collections;
import java.util.List;

import static ru.open.spc.calc.kernel.SPCalculationCommon.toRub;
import static ru.open.spc.calc.model.ResultBuilder.buildFromParams;
import static ru.open.spc.calc.model.Spread.BID;
import static ru.open.spc.calc.util.HeaderParams.*;
import static ru.open.spc.calc.util.MathUtil.fromPercent;
import static ru.open.spc.calc.util.MathUtil.toPercent;
import static ru.open.spc.dao.util.RoundUtil.roundDn;
import static ru.open.spc.model.enums.OptionType.PUT;

@MessageEndpoint
public class FixedCouponCalculationService extends AbstractCalculationService {

	private final static Logger LOG = LoggerFactory.getLogger(FixedCouponCalculationService.class);

	private final static double TO_OPTIONS_CONST = 0.25;
	private final static double REWARD_CONST = 2d / 3d;

	@SuppressWarnings("unchecked")
	@ServiceActivator(inputChannel = "fixedCouponChannel")
	public void calculate(final Message<CalculationParameters> message) {
        wrapCalculate(message);
	}

    @Override
    protected List<PricingResult> doCalculating(MessageHeaders headers, final DescriptionWriter dscSource,
                                                CalculationParameters payload) throws Exception {

        final DescriptionWriter dsc = dscSource.clone();
        final SimpleCalculationParameters param = (SimpleCalculationParameters) payload.getCalculationParams();
        final List<Series> series = (List<Series>) headers.get(SERIES);
        final CalculationBase calculationBase = (CalculationBase) headers.get(CALCULATION_BASE);
        final List<Underlying> underlying = (List<Underlying>) headers.get(FUTURES);


        final PriceCalculationFunction priceCalculationFunction = new PriceCalculationFunction(param, calculationBase).desc(dsc).interval(BID).underlyings(underlying)
                .optionType(PUT).serieses(series).strike(param.getTotalAssetPriceWithStrike1Coef());

        dsc.write("strike.price", param.getTotalAssetPriceWithStrike1Coef());
        series.forEach(s -> dsc.write("series", s.getName(), s.getRealPrice()));
        final double optPrice = priceCalculationFunction.apply(param.getOptionPrice());
        final double optPriceInRub = toRub(optPrice, param);
        dsc.write("calculated.price", PUT, optPrice, optPriceInRub);
        final long optionsNumber = calculateOptionsCount(param.getTotalAssetPriceWithStrike1Coef(), param.getInvestingSum());
        dsc.write("options.number", optionsNumber);
        final double optReward = calculateOptReward(optionsNumber, optPriceInRub);
        dsc.write("options.reward", optReward);
        final double sumToOptions = calculateSumToOptions(optionsNumber, param.getTotalAssetPriceWithStrike1Coef());
        dsc.write("sum.to.options", sumToOptions);
        final double depoSum = calculateToDepo(sumToOptions, param.getInvestingSum());
        dsc.write("depoSum", depoSum);
        final double depoReward = calculateDepoReward(param.getDaysCountToEndProduct(), param.getInterestRate(), depoSum);
        dsc.write("depoReward", depoReward);
        final double totalReward = calculateTotalReward(depoReward, optReward);
        final double retailTotal = calculateTotalRetail(totalReward, param.getInvestingSum(), param.getDaysCountToEndProduct());
        dsc.write("retailTotal", retailTotal);

        final double result = retailTotal * REWARD_CONST;
        LOG.debug("result = {}", result);
        final ResultBuilder builder = buildFromParams(param)
            .product(payload.getProduct()).sumToOptions(sumToOptions).desc(dsc)
            .strike1(param.getTotalAssetPriceWithStrike1Coef()).maximumEarning(result).optionPrice(optPrice);
        return Collections.singletonList(getResultFactory().createPricingResult(builder));
	}

	private long calculateOptionsCount(double optLeftPrice, Double investingSum) {
		return roundDn(investingSum / optLeftPrice);
	}

	private double calculateOptReward(long optionsNumber, double optPriceInRub) {
		return optPriceInRub * optionsNumber;
	}

	private double calculateSumToOptions(long optionsNumber, double strikePrice) {
		return optionsNumber * strikePrice * TO_OPTIONS_CONST;
	}

	private double calculateToDepo(double sumToOptions, double investingSum) {
		return investingSum - sumToOptions;
	}

	private double calculateDepoReward(double daysCountBtwnStartProductEndProduct, double interestRate, double depoSum) {
		double tBtwnStartProductStartEnd = daysCountBtwnStartProductEndProduct / DAYS_PER_YEAR;
		return depoSum * tBtwnStartProductStartEnd * fromPercent(interestRate);
	}

	private double calculateTotalReward(double depoReward, double optReward) {
		return depoReward + optReward;
	}

	private double calculateTotalRetail(double totalReward, Double investingSum, double daysCountBtwnStartProductEndProduct) {
		return toPercent(totalReward * DAYS_PER_YEAR / investingSum / daysCountBtwnStartProductEndProduct);
	}

}
