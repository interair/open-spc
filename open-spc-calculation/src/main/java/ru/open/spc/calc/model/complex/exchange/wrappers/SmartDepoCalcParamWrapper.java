package ru.open.spc.calc.model.complex.exchange.wrappers;

import ru.open.spc.calc.model.Orientation;
import ru.open.spc.calc.model.complex.AbstractComplexCalculationParameters;
import ru.open.spc.calc.model.complex.ComplexParamContext;
import ru.open.spc.calc.model.complex.ParamWrapper;
import ru.open.spc.calc.model.complex.exchange.BasicProtectionComplexParameters;
import ru.open.spc.calc.model.simple.exchange.SmartDepositSimpleParameters;
import ru.open.spc.model.Asset;
import ru.open.spc.model.enums.AssetUnity;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class SmartDepoCalcParamWrapper extends SmartDepositSimpleParameters implements ParamWrapper {

    private ComplexParamContext context;
    private BasicProtectionComplexParameters parameter;

    public SmartDepoCalcParamWrapper(BasicProtectionComplexParameters parameter, ComplexParamContext context) {
        this.context = context;
        this.parameter = parameter;
    }

    public SmartDepoCalcParamWrapper() {
    }

    public Orientation getOrientation() {
        return context.getOrientation();
    }

    public LocalDateTime getOptExpDate() {
        return context.getOptExpDate();
    }

    public LocalDate getProductEndDate() {
        return context.getProductEndDate();
    }

    public Double getCoeffAssetProtection() {
        return context.getCoeffAssetProtection();
    }

    public Double getInterestRate() {
        return context.getInterestRate();
    }

    public Double getItemCost() {
        return parameter.getItemCost();
    }

    public Double getStrikeMin() {
        return context.getStrike1();
    }

    public Double getStrikeMax() {
        return context.getStrike2();
    }

    public Double getUnderlyingAssetPrice() {
        return parameter.getUnderlyingAssetPrice();
    }

    public Integer getUnderlyingAssetMultiplicator() {
        return parameter.getUnderlyingAssetMultiplicator();
    }

    public Double getInvestingSum() {
        return parameter.getInvestingSum();
    }

    public AssetUnity getAssetUnity() {
        return parameter.getAssetUnity();
    }

    public Asset getAsset() {
        return parameter.getAsset();
    }

    public LocalDate getDepositStartDate() {
        return parameter.getDepositStartDate();
    }

    public Double getDollarExchangeRate() {
        return parameter.getDollarExchangeRate();
    }

    public Double getVolatilitySpread() {
        if (context.getVolatilitySpread() != null) {
            return context.getVolatilitySpread();
        } else {
            return super.getVolatilitySpread();
        }
    }

    public String getBaseTicker() {
        return parameter.getBaseTicker();
    }

    @Override
    public AbstractComplexCalculationParameters getParameter() {
        return parameter;
    }

    @Override
    public ComplexParamContext getContext() {
        return context;
    }

    @Override
    public void setParameter(AbstractComplexCalculationParameters parameter) {
        this.parameter = (BasicProtectionComplexParameters) parameter;
    }

    @Override
    public void setContext(ComplexParamContext context) {
        this.context = context;
    }
}
