package ru.open.spc.calc.kernel.result.factory;

import org.springframework.stereotype.Service;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.model.result.SMPricingResult;

@Service
public class SmartDepoResultCreator extends PricingResultCreator<SMPricingResult> {
    @Override
    public SMPricingResult createResult(ResultBuilder rb) {
        final SMPricingResult pricingResult = fill(rb, new SMPricingResult());
        pricingResult.setDirection(rb.getDirection());
        pricingResult.setKzk(rb.getCoeffAssetProtection());
        pricingResult.setStrike2Price(rb.getStrike2());
        pricingResult.setMaximumEarning(rb.getMaximumEarning());
        return pricingResult;
    }
}
