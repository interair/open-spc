package ru.open.spc.calc.kernel.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.Filter;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.dao.util.DateUtil;
import ru.open.spc.model.Option;

import static ru.open.spc.calc.util.HeaderParams.NEAREST_STRIKE_PAIR;
import static ru.open.spc.calc.util.HeaderParams.UI_CALLBACK;

@MessageEndpoint
public class EmptyNearestStrikesFilter {

	private final static Logger LOG = LoggerFactory.getLogger(EmptyNearestStrikesFilter.class);

	@Filter(inputChannel = "filterOptionsChannel", outputChannel = "baseCalculationChannel")
	public boolean isCalculatingPossible(@Header(NEAREST_STRIKE_PAIR) Pair<Option, Option> strikes,
										 @Header(UI_CALLBACK) UiCallback callback) {
		boolean filled = strikes.isFilled();    //TODO: add exchange
		if (!filled) {
			callback.addResults(DateUtil.getEmptyRes());
			LOG.debug("!strikes.ifFilled()");
		}
		return filled;
	}
}
