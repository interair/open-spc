package ru.open.spc.calc.kernel.splitters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Splitter;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.complex.AbstractComplexCalculationParameters;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.model.Asset;

import java.util.ArrayList;
import java.util.List;

import static ru.open.spc.calc.util.HeaderParams.UI_CALLBACK;

@MessageEndpoint
public class AssetSplitter {

    private final static Logger LOG = LoggerFactory.getLogger(AssetSplitter.class);
	@Autowired private AssetManager assetManager;

	@Splitter(inputChannel = "assetSplitterChannel", outputChannel = "complexParametersRouterChannel")
	public List<CalculationParameters> processMessage(final CalculationParameters param, @Header(UI_CALLBACK) UiCallback callback) {
		final List<CalculationParameters> cps = new ArrayList<>();
		final List<Asset> allFor = assetManager.allFor(param.getProduct().getId());
        allFor.stream().filter(asset -> asset.getAssetPrice() != null).forEach(asset -> {
            try {
                CalculationParameters copyParam = new CalculationParameters();
                BeanUtils.copyProperties(param, copyParam);
                AbstractComplexCalculationParameters copyCalcParam = null;
                copyCalcParam = (AbstractComplexCalculationParameters) copyParam.getCalculationParams().getClass().newInstance();
                BeanUtils.copyProperties(copyParam.getCalculationParams(), copyCalcParam);
                enrichAsset(copyCalcParam, asset);
                copyParam.setCalculationParams(copyCalcParam);
                cps.add(copyParam);
            } catch (InstantiationException | IllegalAccessException e) {
                LOG.error("can't copy calc param", e);
            }
        });
        callback.setPhaseCount(cps.size());
		return cps;
	}

	private void enrichAsset(AbstractComplexCalculationParameters copyCalcParam, Asset asset) {
		copyCalcParam.setAsset(asset);
		copyCalcParam.setAssetUnity(asset.getUnity());
		copyCalcParam.setBaseTicker(asset.getBaseTicker());
		copyCalcParam.setUnderlyingAssetMultiplicator(asset.getMultiplier());
		copyCalcParam.setItemCost(asset.getPointPrice());
		copyCalcParam.setInvestingSum(asset.getMinimumPrice());
		copyCalcParam.setUnderlyingAssetPrice(asset.getAssetPrice().getRealPrice());
	}
}
