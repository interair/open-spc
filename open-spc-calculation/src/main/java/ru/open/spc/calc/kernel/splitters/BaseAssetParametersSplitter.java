package ru.open.spc.calc.kernel.splitters;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Splitter;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.complex.ComplexParamContext;
import ru.open.spc.calc.model.complex.exchange.BasicProtectionComplexParameters;
import ru.open.spc.calc.model.complex.flex.DateStepHolder;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.model.enums.Direction;

import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static ru.open.spc.calc.util.HeaderParams.CALCULATION_BASE;
import static ru.open.spc.calc.util.HeaderParams.UI_CALLBACK;
import static ru.open.spc.calc.util.Util.getArray;
import static ru.open.spc.dao.util.DateUtil.getNextDate;
import static ru.open.spc.dao.util.DateUtil.setHours;

@MessageEndpoint
public class BaseAssetParametersSplitter extends ComplexParametersSplitter {

	@Splitter(inputChannel = "splitBaseAssetComplexChannel", outputChannel = "prepareSimpleCalculationChannel")
	public List<CalculationParameters> processMessage(final CalculationParameters param,
                                                      @Header(UI_CALLBACK) UiCallback callback,
                                                      @Header(CALCULATION_BASE) CalculationBase calculationBase) {
        final BasicProtectionComplexParameters calculationParams = (BasicProtectionComplexParameters) param.getCalculationParams();
        final List<CalculationParameters> cps = new ArrayList<>();
        List<LocalDate> dates;
        switch (calculationBase) {
            case FLEX:
                DateStepHolder flexParameters = (DateStepHolder) calculationParams;
                final Duration dateStepDuration = flexParameters.getDateStepDuration();
                dates = findAllAvailableFlexSeries(calculationParams, dateStepDuration);
                break;
            default:
                dates = filterDates(findAllAvailableSeries(calculationParams));
                break;
        }
        final List<Double> kzks = getArray(calculationParams.getCoeffAssetProtectionFrom(),
				calculationParams.getCoeffAssetProtectionTo(),
				calculationParams.getCoeffAssetProtectionStep());
        final List<Double> rates = getArray(calculationParams.getInterestRateFrom(),
				calculationParams.getInterestRateTo(), calculationParams.getInterestRateStep());
        final List<Double> strikes1 = getArray(calculationParams.getStrike1From(),
            calculationParams.getStrike1To(), calculationParams.getStrike1Step());
		for (LocalDate date : dates) {
			for (Direction direction : Direction.values()) {
				for (Double kzk : kzks) {
					for (Double rate : rates) {
						for (Double strike1 : strikes1) {
							final Double spread = calculateSpread(date, calculationParams.getAsset());
                            final ComplexParamContext context = new ComplexParamContext.Builder()
									.optExpDate(setHours(date, 18, 45))
									.productEndDate(getNextDate(date))
									.coeffAssetProtection(kzk)
									.interestRate(rate)
									.direction(direction)
									.strike1(strike1)
									.volatilitySpread(spread)
                                    .calculationBase(calculationBase)
									.interval(checkForInterval(calculationParams))
									.create();
							cps.add(createSimpleCalculation(param, context));
						}
					}
				}
			}
		}
        callback.setTotalStepsAmount(cps.size());
		return cps;
	}

    protected boolean checkForInterval(BasicProtectionComplexParameters calculationParams) {
		return (calculationParams.getStrike2From() != 0 || calculationParams.getStrike2To() != 0);
	}
}
