package ru.open.spc.calc.util;

public final class HeaderParams {
    public static final String CALCULATION_BASE = "CalculationBase";
    public static final String CALCULATION_TYPE = "CalculationType";
	public static final String OPTIONS = "options";
	public static final String SERIES = "series";
    public static final String FUTURES = "futures";
	public static final String NEAREST_STRIKE_PAIR = "nearestStrikePair";
	public static final String UI_CALLBACK = "uiCallback";
    public static final String UI_DESCRIPTION = "uiDescription";
    public static final String SOURCE_PARAM = "sourceParam";
    public static final String OPTION_TYPE = "optionType";
    public static final String PUT_OPTIONS = "putOptions";
    public static final String CALL_OPTIONS = "callOptions";

}
