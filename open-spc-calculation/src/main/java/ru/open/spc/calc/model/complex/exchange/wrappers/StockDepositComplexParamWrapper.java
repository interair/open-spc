package ru.open.spc.calc.model.complex.exchange.wrappers;

import ru.open.spc.calc.model.complex.AbstractComplexCalculationParameters;
import ru.open.spc.calc.model.complex.ComplexParamContext;
import ru.open.spc.calc.model.complex.ParamWrapper;
import ru.open.spc.calc.model.simple.exchange.StocksDepositSimpleParameters;
import ru.open.spc.model.Asset;
import ru.open.spc.model.enums.AssetUnity;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class StockDepositComplexParamWrapper extends StocksDepositSimpleParameters implements ParamWrapper {

	private AbstractComplexCalculationParameters parameter;
	private ComplexParamContext context;

    public StockDepositComplexParamWrapper() {}

    public StockDepositComplexParamWrapper(AbstractComplexCalculationParameters parameter, ComplexParamContext context) {
		this.parameter = parameter;
		this.context = context;
	}

	@Override
	public LocalDateTime getOptExpDate() {
		return context.getOptExpDate();
	}

	@Override
	public Double getInterestRate() {
		return context.getInterestRate();
	}

	@Override
	public Double getStrike1() {
		return context.getStrike1();
	}

    @Override
    public Long getStockNum() {
        return context.getStockNum();
    }

    @Override
    public Double getRepoDiscount() {
        return context.getRepoDiscount();
    }

    @Override
	public Double getVolatilitySpread() {
		return context.getVolatilitySpread();
	}

    @Override
    public Double getRepoRate() {
        return context.getRepoRate();
    }

    @Override
	public Double getItemCost() {
		return parameter.getItemCost();
	}

	@Override
	public Double getUnderlyingAssetPrice() {
		return parameter.getUnderlyingAssetPrice();
	}

	@Override
	public Integer getUnderlyingAssetMultiplicator() {
		return parameter.getUnderlyingAssetMultiplicator();
	}

	@Override
	public Double getInvestingSum() {
		return parameter.getInvestingSum();
	}

	@Override
	public AssetUnity getAssetUnity() {
		return parameter.getAssetUnity();
	}

	@Override
	public Asset getAsset() {
		return parameter.getAsset();
	}

	@Override
	public LocalDate getDepositStartDate() {
		return parameter.getDepositStartDate();
	}

	@Override
	public LocalDate getProductEndDate() {
		return context.getProductEndDate();
	}

	@Override
	public Double getDollarExchangeRate() {
		return parameter.getDollarExchangeRate();
	}

	@Override
	public String getBaseTicker() {
		return parameter.getBaseTicker();
	}

    @Override
    public Boolean getVariability() {
        return true;
    }

    public AbstractComplexCalculationParameters getParameter() {
        return parameter;
    }

    public void setParameter(AbstractComplexCalculationParameters parameter) {
        this.parameter = parameter;
    }

    public ComplexParamContext getContext() {
        return context;
    }

    public void setContext(ComplexParamContext context) {
        this.context = context;
    }
}
