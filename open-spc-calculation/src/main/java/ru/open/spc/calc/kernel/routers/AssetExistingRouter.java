package ru.open.spc.calc.kernel.routers;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Router;
import ru.open.spc.calc.model.AbstractCalculationParameters;
import ru.open.spc.calc.model.BaseParameters;
import ru.open.spc.calc.model.CalculationParameters;

@MessageEndpoint
public class AssetExistingRouter {

	@Router(inputChannel = "complexCalculationChannel")
	public String routeByAssetExisting(CalculationParameters param) {
		BaseParameters calculationParams = param.getCalculationParams();
		if (calculationParams instanceof AbstractCalculationParameters) {
			if (((AbstractCalculationParameters) calculationParams).getAsset() == null) {
				return "assetSplitterChannel";
			}
		}
		return "complexParametersRouterChannel";
	}
}
