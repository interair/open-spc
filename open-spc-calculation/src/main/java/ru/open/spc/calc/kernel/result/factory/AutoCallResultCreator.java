package ru.open.spc.calc.kernel.result.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.dao.manager.CurrencyManager;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.model.result.ACPricingResult;

@Service
public class AutoCallResultCreator implements Creator<ACPricingResult> {

    @Autowired
    CurrencyManager currencyManager;

	public ACPricingResult createResult(ResultBuilder rb) {
		ACPricingResult result = new ACPricingResult();
		if (rb.getId() != null) {
			result.setId(rb.getId());
		}
        result.setAsset(rb.getAsset());
        result.setStrikePrice(0d);
		result.setProduct(rb.getProduct());
		result.setPriceDate(rb.getPriceDate());
		result.setProductStartDate(rb.getProductStartDate());
		result.setProductStopDate(rb.getProductStopDate());
		result.setInterestRate(rb.getInterestRate());
		result.setDescription(rb.getDescResult());
		result.setCurrency(currencyManager.getBy(AssetUnity.RUB.name()));
		result.setTimeout(rb.getTimeout());
		result.setAssetPrice(rb.getAssetPrice());
		result.setOptionPrice(rb.getOptionPrice());
		result.setPercentByLoanDeal(0d);
		result.setDirection(rb.getDirection());
		result.setInterval(0);
		result.setMaximumEarning(rb.getMaximumEarning());
		return result;
	}
}