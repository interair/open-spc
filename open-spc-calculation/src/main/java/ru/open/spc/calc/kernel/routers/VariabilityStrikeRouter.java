package ru.open.spc.calc.kernel.routers;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Router;
import ru.open.spc.calc.model.CalculationParameters;

@MessageEndpoint
public class VariabilityStrikeRouter {

    @Router(inputChannel = "complexCalculationChannel")
    public String routeByVariability(CalculationParameters param) {
        if (param.getCalculationParams().getVariability()) {
            return "strikeSplitterByBaseChannel";
        } else {
            return "fixStrikeSplitterChannel";
        }
    }
}
