package ru.open.spc.calc.kernel.splitters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Splitter;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.complex.ComplexCalculationParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.model.Option;
import ru.open.spc.model.enums.OptionType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ru.open.spc.calc.kernel.SPCalculationCommon.sortByStrike;
import static ru.open.spc.calc.util.HeaderParams.SOURCE_PARAM;
import static ru.open.spc.calc.util.MathUtil.fromPercent;

//TODO: ASK
@MessageEndpoint
public class StockDepositStrikeSplitter extends AbstractStrikeSplitter {

    @Autowired private CalculationManager calculationManager;

    @Splitter(inputChannel = "stockDepositStrikeSplitterChannel", outputChannel = "seriesEnricherRouterChannel")
    public List<CalculationParameters> processMessage(final CalculationParameters param,
                                                      @Header(SOURCE_PARAM)ComplexCalculationParameters complexParameters) throws Exception {
        final List<Double> strikes = getStrikes((SimpleCalculationParameters) param.getCalculationParams(),
            complexParameters.getStrike1From(), complexParameters.getStrike1To());
        if (strikes.isEmpty()) {
            return Collections.singletonList(param);
        }
        return cloneMessages(param, strikes);
    }

    private List<Double> getStrikes(SimpleCalculationParameters params, double strike1From, double strike1To) {
        final List<Option> strikesBy = calculationManager.getStrikesBy(params.getAsset().getOuterId(), params.getOptExpDate().toLocalDate(), OptionType.PUT);
        sortByStrike(strikesBy, true);
        final Double strike1FromInCur = (1 + fromPercent(strike1From)) * params.getTotalAssetPrice();
        final Double strike1ToInCur = (1 + fromPercent(strike1To)) * params.getTotalAssetPrice();
        List<Double> strikes = new ArrayList<>();
        for (Option option : strikesBy) {
            if (option.getStrikePrice().compareTo(strike1FromInCur) > -1) {
                if (option.getStrikePrice().compareTo(strike1ToInCur) < 1) {
                    strikes.add(option.getStrikePrice());
                } else if (strikes.isEmpty()){
                    strikes.add(option.getStrikePrice());
                    break;
                }
            }
        }
        return strikes;
    }
}
