package ru.open.spc.calc.kernel.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.Filter;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.dao.util.DateUtil;
import ru.open.spc.model.Option;

import java.util.List;

import static ru.open.spc.calc.util.HeaderParams.*;

@MessageEndpoint
public class EmptyOptionsFilter {

	private final static Logger LOG = LoggerFactory.getLogger(EmptyOptionsFilter.class);

	@Filter(inputChannel = "filterOptionsChannel", outputChannel = "resolveStrikesChannel")
	public boolean isCalculatingPossible(@Header(OPTIONS) List<Option> options,
										 @Header(UI_CALLBACK) UiCallback callback,
                                         @Header(CALCULATION_BASE) CalculationBase calculationBase) {
		boolean empty = options.isEmpty() && calculationBase == CalculationBase.EXCHANGE;
		if (empty) {
			callback.addResults(DateUtil.getEmptyRes());
			LOG.debug("options.isEmpty()");
		}
		return !empty;
	}
}
