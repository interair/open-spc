package ru.open.spc.calc.kernel.result.factory;

import org.springframework.stereotype.Service;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.model.result.SDPricingResult;

@Service
public class StockDepoPricingResultCreator extends PricingResultCreator<SDPricingResult> {
    @Override
    public SDPricingResult createResult(ResultBuilder rb) {
        SDPricingResult pricingResult = fill(rb, new SDPricingResult());
        pricingResult.setCouponSize(rb.getCouponSize());
        pricingResult.setDirection(Direction.RISING);
        pricingResult.setInvestingVolume(null);
        pricingResult.setInvestingAmount(rb.getStockNum());
        return pricingResult;
    }
}
