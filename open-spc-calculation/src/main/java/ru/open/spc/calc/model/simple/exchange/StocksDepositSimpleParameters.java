package ru.open.spc.calc.model.simple.exchange;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data public class StocksDepositSimpleParameters extends FixStrike1AwareSimpleParameters {

	private Double repoDiscount; // Дисконт по репо %
	private Double repoRate; //Ставка репо %
    private Long stockNum;

}
