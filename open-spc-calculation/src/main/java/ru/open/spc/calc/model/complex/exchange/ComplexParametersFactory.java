package ru.open.spc.calc.model.complex.exchange;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.complex.AbstractComplexCalculationParameters;
import ru.open.spc.calc.model.complex.ComplexParamContext;
import ru.open.spc.calc.model.complex.exchange.wrappers.*;
import ru.open.spc.calc.model.complex.flex.BasicProtectionComplexFlexParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.model.enums.ProductCode;

@Service
public class ComplexParametersFactory {
	
	public CalculationParameters create(CalculationParameters param, ComplexParamContext context) {
		final ProductCode code = param.getProduct().getCode();
		final AbstractComplexCalculationParameters calculationParams = (AbstractComplexCalculationParameters) param.getCalculationParams();
		SimpleCalculationParameters simpleCalculationParameters = null;
		switch(code) {
			case BaseProtect:
                switch (context.getCalculationBase()) {
                    case EXCHANGE:
                        final BasicProtectionComplexParameters basicProtectionParams = (BasicProtectionComplexParameters) calculationParams;
                        simpleCalculationParameters = new BasicProtectionCalcParamWrapper(basicProtectionParams, context);
                    break;
                    case FLEX:
                        final BasicProtectionComplexFlexParameters basicProtectionFlexParams = (BasicProtectionComplexFlexParameters) calculationParams;
                        simpleCalculationParameters = new BasicProtectionCalcFlexParamWrapper(basicProtectionFlexParams, context);
                        break;
                }
				break;
			case FixedCoupon:
				simpleCalculationParameters = new FixedCouponComplexCalcParamWrapper(calculationParams, context);
				break;
			case StocksDeposit:
				simpleCalculationParameters = new StockDepositComplexParamWrapper(calculationParams, context);
				break;
            case SmartDeposit:
                final BasicProtectionComplexParameters bProtectionParams = (BasicProtectionComplexParameters) calculationParams;
                simpleCalculationParameters = new SmartDepoCalcParamWrapper(bProtectionParams, context);
		}
		CalculationParameters copyParam = new CalculationParameters();
		BeanUtils.copyProperties(param, copyParam);
		copyParam.setCalculationParams(simpleCalculationParameters);
		return copyParam;
		
	}
}
