package ru.open.spc.calc.model.simple.exchange;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;

@EqualsAndHashCode(callSuper = false)
@Data public class FixStrike1AwareSimpleParameters extends SimpleCalculationParameters {

    private Double fixStrike1; //Фиксированное пороговое значение

    public FixStrike1AwareSimpleParameters() { }

}