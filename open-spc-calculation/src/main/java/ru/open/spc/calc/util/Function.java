package ru.open.spc.calc.util;

public interface Function<K, V> extends UnsafeFunction<K, V, RuntimeException> {
}
