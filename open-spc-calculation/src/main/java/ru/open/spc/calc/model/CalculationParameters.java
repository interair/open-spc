package ru.open.spc.calc.model;

import lombok.Data;
import ru.open.spc.model.Product;

import java.io.Serializable;

@SuppressWarnings("serial")
@Data public class CalculationParameters implements Serializable {

	private Product product;
	private BaseParameters calculationParams;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CalculationParameters other = (CalculationParameters) obj;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		return true;
	}

}
