package ru.open.spc.calc.model.complex.exchange.wrappers;

import ru.open.spc.calc.model.complex.AbstractComplexCalculationParameters;
import ru.open.spc.calc.model.complex.ComplexParamContext;
import ru.open.spc.calc.model.complex.ParamWrapper;
import ru.open.spc.calc.model.complex.flex.BasicProtectionComplexFlexParameters;
import ru.open.spc.calc.model.complex.flex.PriceType;
import ru.open.spc.calc.model.simple.flex.BasicProtectionFlexSimpleParameters;
import ru.open.spc.model.Asset;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.model.enums.Direction;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class BasicProtectionCalcFlexParamWrapper extends BasicProtectionFlexSimpleParameters implements ParamWrapper {

	private BasicProtectionComplexFlexParameters parameter;
	private ComplexParamContext context;

	public BasicProtectionCalcFlexParamWrapper(BasicProtectionComplexFlexParameters parameter, ComplexParamContext context) {
		this.parameter = parameter;
		this.context = context;
	}

	public Direction getDirection() {
		return context.getDirection();
	}

	public LocalDateTime getOptExpDate() {
		return context.getOptExpDate();
	}

	public LocalDate getProductEndDate() {
		return context.getProductEndDate();
	}

	public Double getCoeffAssetProtection() {
		return context.getCoeffAssetProtection();
	}

	public Double getInterestRate() {
		return context.getInterestRate();
	}

	public Double getItemCost() {
		return parameter.getItemCost();
	}

	public Double getStrike1() {
		return context.getStrike1();
	}

	public Double getStrike2() {
		return context.getStrike2();
	}

	public Double getUnderlyingAssetPrice() {
		return parameter.getUnderlyingAssetPrice();
	}

	public Integer getUnderlyingAssetMultiplicator() {
		return parameter.getUnderlyingAssetMultiplicator();
	}

	public Double getInvestingSum() {
		return parameter.getInvestingSum();
	}

	public AssetUnity getAssetUnity() {
		return parameter.getAssetUnity();
	}

	public Asset getAsset() {
		return parameter.getAsset();
	}

	public LocalDate getDepositStartDate() {
		return parameter.getDepositStartDate();
	}

	public Double getDollarExchangeRate() {
		return parameter.getDollarExchangeRate();
	}

	public Double getVolatilitySpread() {
		if (context.getVolatilitySpread() != null) {
			return context.getVolatilitySpread();
		} else {
			return super.getVolatilitySpread();
		}
	}

	public String getBaseTicker() {
		return parameter.getBaseTicker();
	}

	public Double getStrike2From() {
		return parameter.getStrike2From();
	}

	public Double getStrike2To() {
		return parameter.getStrike2To();
	}

	public Double getStrike2Step() {
		return parameter.getStrike2Step();
	}

	@Override
	public Boolean getVariability() {
		return true;
	}

	public Boolean getInterval() {
		return context.isInterval();
	}

	public BasicProtectionComplexFlexParameters getParameter() {
		return parameter;
	}

	public void setParameter(AbstractComplexCalculationParameters parameter) {
		this.parameter = (BasicProtectionComplexFlexParameters) parameter;
	}

	@Override
	public PriceType getPriceType() {
		return PriceType.PERCENT;
	}

	public ComplexParamContext getContext() {
		return context;
	}

	public void setContext(ComplexParamContext context) {
		this.context = context;
	}

	public Boolean getAllOptions() {
		return false;
	}
}