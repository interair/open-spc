package ru.open.spc.calc.kernel.enrichers;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import ru.open.spc.calc.model.CalculationParameters;

import static ru.open.spc.calc.util.HeaderParams.SOURCE_PARAM;

@MessageEndpoint
public class HeaderEnricher {

    @Transformer(inputChannel = "processCalculationChannel", outputChannel = "calculationTypeRouterChannel")
    public Message<CalculationParameters> enrichInitVariable(Message<CalculationParameters> message) {
        return MessageBuilder.fromMessage(message)
            .setHeader(SOURCE_PARAM, message.getPayload().getCalculationParams())
            .build();
    }
}
