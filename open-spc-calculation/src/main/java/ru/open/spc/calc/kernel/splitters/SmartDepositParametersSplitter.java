package ru.open.spc.calc.kernel.splitters;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Splitter;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.Orientation;
import ru.open.spc.calc.model.complex.ComplexParamContext;
import ru.open.spc.calc.model.complex.exchange.BasicProtectionComplexParameters;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.dao.util.DateUtil;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ru.open.spc.calc.util.HeaderParams.CALCULATION_BASE;
import static ru.open.spc.calc.util.HeaderParams.UI_CALLBACK;
import static ru.open.spc.calc.util.MathUtil.fromPercent;
import static ru.open.spc.calc.util.MathUtil.isBetween;
import static ru.open.spc.calc.util.Util.getArray;
import static ru.open.spc.dao.util.DateUtil.getNextDate;

@MessageEndpoint
public class SmartDepositParametersSplitter extends ComplexParametersSplitter {

    @Override
    @Splitter(inputChannel = "splitSmartDepoComplexChannel", outputChannel = "prepareSimpleCalculationChannel")
    public List<CalculationParameters> processMessage(final CalculationParameters param,
                                                      @SuppressWarnings("rawtypes") @Header(UI_CALLBACK) UiCallback callback,
                                                      @Header(CALCULATION_BASE) CalculationBase calculationBase) {
        final BasicProtectionComplexParameters calculationParams = (BasicProtectionComplexParameters) param.getCalculationParams();
        final List<CalculationParameters> cps = new ArrayList<>();
        List<LocalDate> dates = resolveDates(calculationBase, calculationParams);
        final List<Double> kzks = getArray(calculationParams.getCoeffAssetProtectionFrom(),
            calculationParams.getCoeffAssetProtectionTo(),
            calculationParams.getCoeffAssetProtectionStep());
        final List<Double> rates = getArray(calculationParams.getInterestRateFrom(),
            calculationParams.getInterestRateTo(), calculationParams.getInterestRateStep());

        for (LocalDate date : dates) {
            List<Double> realStrikes = getRealStrikes(calculationParams, date);
            final List<Double> strikesMin = getStrikesMin(calculationParams.getStrike1From(),
                calculationParams.getStrike1To(), calculationParams.getAsset().getAssetPrice().getRealPrice(), realStrikes);
            final List<Double> strikesMax = getStrikesMax(calculationParams.getStrike2From(),
                calculationParams.getStrike2To(), calculationParams.getAsset().getAssetPrice().getRealPrice(), realStrikes);

            for (Orientation orientation : Orientation.values()) {
                for (Double kzk : kzks) {
                    for (Double rate : rates) {
                        for (Double strikeMax : strikesMax) {
                            for (Double strikeMin : findLessThan(strikeMax, strikesMin)) {
                                final Double spread = calculateSpread(date, calculationParams.getAsset());
                                final ComplexParamContext context = new ComplexParamContext.Builder()
                                    .optExpDate(DateUtil.setHours(date, 18, 45))
                                    .productEndDate(getNextDate(date))
                                    .coeffAssetProtection(kzk)
                                    .interestRate(rate)
                                    .orientation(orientation)
                                    .strike1(strikeMin)
                                    .strike2(strikeMax)
                                    .volatilitySpread(spread)
                                    .calculationBase(calculationBase)
                                    .create();
                                cps.add(createSimpleCalculation(param, context));
                            }
                        }
                    }
                }
            }
        }

        callback.setTotalStepsAmount(cps.size());
        return cps;
    }

    @SuppressWarnings("serial")
    private List<Double> findLessThan(Double strikeMax, final List<Double> strikesMin) {
        final int indexFrom = 0; int indexTo;
        if ((indexTo = Collections.binarySearch(strikesMin, strikeMax)) > 0) {
            indexTo = indexTo - 1;
            if (indexFrom == indexTo) {
                return new ArrayList<Double>() {{
                    add(strikesMin.get(indexFrom));
                }};
            }
            return strikesMin.subList(indexFrom, indexTo);
        }

        List<Double> strikes = new ArrayList<>();
        for (Double strike : strikesMin) {
            if (strike < strikeMax) {
                strikes.add(strike);
            } else {
                break;
            }
        }
        return strikes;
    }

    private List<Double> getStrikesMax(Double strikeMaxFromPrcnt, Double strikeMaxToPrcnt, Double assetPrice, final List<Double> realStrikes) {
        double strikeMaxFrom = (1 +  fromPercent(strikeMaxFromPrcnt)) * assetPrice, strikeMaxTo = (1 + fromPercent(strikeMaxToPrcnt)) * assetPrice;
        int indexFrom, indexTo;
        if ((indexFrom = Collections.binarySearch(realStrikes, strikeMaxFrom)) >= 0 && (indexTo = Collections.binarySearch(realStrikes, strikeMaxTo)) > 0) {
            return realStrikes.subList(indexFrom, indexTo);
        }

        List<Double> strikes = new ArrayList<>();
        Double nearestStrike = null;
        for (Double strike : realStrikes) {
            if (isBetween(strike, strikeMaxFrom, strikeMaxTo)) {
                strikes.add(strike);
            } else {
                if (strike > strikeMaxTo) {
                    nearestStrike = strike;
                    break;
                }
            }
        }
        if (strikes.isEmpty() && nearestStrike != null) {
            strikes.add(nearestStrike);
        }
        return strikes;
    }

    private List<Double> getStrikesMin(Double strikeMinFromPcnt, Double strikeMinToPcnt, Double assetPrice, final List<Double> realStrikes) {
        int indexFrom, indexTo;
        double strikeMinFrom = (1 +  fromPercent(strikeMinFromPcnt)) * assetPrice, strikeMinTo = (1 +  fromPercent(strikeMinToPcnt)) * assetPrice;
        if ((indexFrom = Collections.binarySearch(realStrikes, strikeMinFrom)) >= 0 && (indexTo = Collections.binarySearch(realStrikes, strikeMinTo)) > 0) {
            return realStrikes.subList(indexFrom, indexTo);
        }

        List<Double> strikes = new ArrayList<>();
        Double nearestStrike = null;
        for (Double strike : realStrikes) {
            if (isBetween(strike, strikeMinFrom, strikeMinTo)) {
                strikes.add(strike);
            } else {
                if (nearestStrike == null && strike < strikeMinFrom) {
                    nearestStrike = strike;
                }
                if (nearestStrike != null && strike < strikeMinFrom && strike > nearestStrike) {
                    nearestStrike = strike;
                }
                if (strike > strikeMinTo) {
                    break;
                }
            }
        }
        if (strikes.isEmpty() && nearestStrike != null) {
            strikes.add(nearestStrike);
        }
        return strikes;
    }

    private List<Double> getRealStrikes(BasicProtectionComplexParameters calculationParams, LocalDate date) {
        Long assetId = calculationParams.getAsset().getOuterId();
        return getCalculationManager().getAllStrikesOnDate(assetId, date);
    }

}
