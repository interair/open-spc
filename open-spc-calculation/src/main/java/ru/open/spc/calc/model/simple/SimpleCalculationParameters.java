package ru.open.spc.calc.model.simple;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.calc.model.simple.exchange.AbstractExchangeCalculationParameters;
import ru.open.spc.dao.util.DateUtil;

import java.time.LocalDateTime;

@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = false)
@Data public class SimpleCalculationParameters extends AbstractExchangeCalculationParameters {

    private LocalDateTime optExpDate;
    private Double interestRate;
    private Double volatilitySpread;
    private Double strike1;
    @JsonIgnore
    private Double totalAssetPrice; //P * mult * strike1
    @JsonIgnore
    private Double totalAssetPriceInRub; //P * mult * L * Dollar
    @JsonIgnore
    private Double totalAssetPriceWithStrike1Coef; //P * mult * strike1
    @JsonIgnore
    private Double daysCountToExpOpt; //Дней до экспирации
    @JsonIgnore
    private Double daysCountToEndProduct; //Дней до окончания СП
    private Double optionPrice;

    public void setOptExpDate(LocalDateTime optExpDate) {
        this.optExpDate = DateUtil.setHours(optExpDate, 18, 45);
    }

}
