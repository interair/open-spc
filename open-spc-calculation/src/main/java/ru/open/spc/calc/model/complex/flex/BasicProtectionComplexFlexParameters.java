package ru.open.spc.calc.model.complex.flex;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.calc.model.complex.exchange.BasicProtectionComplexParameters;

import java.time.Duration;

@EqualsAndHashCode(callSuper = true)
@Data public class BasicProtectionComplexFlexParameters extends BasicProtectionComplexParameters implements DateStepHolder {

    private Duration dateStepDuration = Duration.ofDays(1);
}
