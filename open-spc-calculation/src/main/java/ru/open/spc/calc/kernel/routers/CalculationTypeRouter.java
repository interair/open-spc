package ru.open.spc.calc.kernel.routers;

import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.model.CalculationType;

import java.util.Map;

import static ru.open.spc.calc.util.HeaderParams.CALCULATION_TYPE;

public class CalculationTypeRouter {

	private Map<CalculationType, String> routers;

	public String routeByComplexity(@Header(CALCULATION_TYPE) CalculationType calculationType) {
		return routers.get(calculationType);
	}

	public void setRouters(Map<CalculationType, String> routers) {
		this.routers = routers;
	}
}
