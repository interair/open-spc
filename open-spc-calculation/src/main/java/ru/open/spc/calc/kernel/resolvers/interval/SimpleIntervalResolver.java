package ru.open.spc.calc.kernel.resolvers.interval;

import org.springframework.stereotype.Service;
import ru.open.spc.calc.kernel.SPCalculationCommon;
import ru.open.spc.calc.model.complex.flex.PriceType;
import ru.open.spc.calc.model.simple.exchange.BasicProtectionSimpleParameters;
import ru.open.spc.calc.model.simple.flex.PriceTypeParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.model.Option;
import ru.open.spc.model.enums.OptionType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Double.*;
import static java.lang.Math.abs;
import static ru.open.spc.calc.util.MathUtil.fromPercent;
import static ru.open.spc.model.enums.OptionType.CALL;
import static ru.open.spc.model.enums.OptionType.PUT;

@Service
public class SimpleIntervalResolver implements IntervalResolverStrategy {

	private static final double PRECISION = 0.001d;

	@Override
	public List<Option> resolve(BasicProtectionSimpleParameters param,
								DescriptionWriter dsc, List<Option> options,
								OptionType optionType,
								Pair<Option, Option> strikes) {

		double strike2 = 0;
		PriceType priceType;

		if (param instanceof PriceTypeParameters) {
			PriceTypeParameters typeParameters = (PriceTypeParameters) param;
			priceType = typeParameters.getPriceType();
		} else {
			priceType = PriceType.PERCENT;
		}
		switch (priceType) {
			case VALUE:
				strike2 = param.getStrike2();
				break;
			default:
				final Double strike2Percent = param.getStrike2();
				if (abs(strike2Percent) > PRECISION)
				strike2 = param.getTotalAssetPrice() * (1 + fromPercent(optionType == CALL ? strike2Percent : -strike2Percent));
				break;
		}
		final boolean rangeSet = abs(strike2) > PRECISION;

		double bounds = getBounds(optionType, strikes);

		if (!rangeSet) {
			return Collections.singletonList(findNearestIntervalOption(options, bounds, optionType));
		}

		List<Option> inOpts = new ArrayList<>();

		inOpts.addAll(findIntervalOptions(options, bounds,
				optionType, min(strike2, bounds), max(strike2, bounds)));

		if (!param.getShowAllOptions() && inOpts.size() > 0) {
			SPCalculationCommon.sortOptions(inOpts);
			switch (optionType) {
				case CALL:
					return Collections.singletonList(inOpts.get(inOpts.size() - 1));
				case PUT:
					return Collections.singletonList(inOpts.get(0));

			}
		}
		return new ArrayList<>(inOpts);
	}

	protected Option findNearestIntervalOption(List<Option> options, Double optPrice, OptionType type) {
		Double interval = type == CALL ? MAX_VALUE : - MAX_VALUE;
		Option intervalOption = null;
		for (Option option : options) {
			Double curr = option.getStrikePrice() - optPrice;
			if ((type == CALL && curr > 0 && curr < interval) ||
				(type == PUT && curr < 0 && curr > interval)) {
				intervalOption = option;
				interval = curr;
			}
		}
		return intervalOption;
	}

	protected List<Option> findIntervalOptions(List<Option> options, Double intStrike, OptionType type, Double min, Double max) {
		List<Option> result = new ArrayList<>();
		for (Option option : options) {
			Double curr = option.getStrikePrice();
			if (curr >= min && curr <= max) {
				if ((type == PUT && curr < intStrike) || (type == CALL && curr > intStrike)) {
					result.add(option);
				}
			}
		}
		return result;
	}

	protected double getBounds(OptionType optionType, Pair<Option, Option> strikes) {
		return optionType == PUT ?
			strikes.getLeft().getStrikePrice() : strikes.getRight().getStrikePrice();
	}

}
