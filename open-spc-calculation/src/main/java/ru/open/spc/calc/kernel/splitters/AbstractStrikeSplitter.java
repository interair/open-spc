package ru.open.spc.calc.kernel.splitters;

import org.springframework.beans.BeanUtils;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractStrikeSplitter {

    List<CalculationParameters> cloneMessages(final CalculationParameters param, final List<Double> strikes) throws Exception {
        final List<CalculationParameters> cps = new ArrayList<>();
        for(Double strike : strikes) {
            final CalculationParameters copyParam = new CalculationParameters();
            BeanUtils.copyProperties(param, copyParam);
            final SimpleCalculationParameters calculationParams = (SimpleCalculationParameters) copyParam.getCalculationParams();
            final SimpleCalculationParameters copyCalculationParams = calculationParams.getClass().newInstance();
            BeanUtils.copyProperties(calculationParams, copyCalculationParams);
            copyParam.setCalculationParams(copyCalculationParams);
            copyCalculationParams.setTotalAssetPriceWithStrike1Coef(strike);
            cps.add(copyParam);
        }
        return cps;
    }
}
