package ru.open.spc.calc.model.simple;

import lombok.Data;
import ru.open.spc.calc.model.BaseParameters;
import ru.open.spc.model.AutoCallAsset;

import java.util.ArrayList;
import java.util.List;

@Data
//TODO: remove values from class to XML!
public class AutoCallParameters extends BaseParameters {

	public AutoCallParameters() {
		setInvestingSum(100d);
		reset();
	}

	public void reset() {
		for (int i = 0; i < correlations.length; i++) {
			Double[] correlation = correlations[i];
			for (int j = 0; j < correlation.length; j++) {
				correlation[j] = 0.25;
				if (i == j) {
					correlation[j] = 1d;
				}
			}
		}
		autoCallAssets.clear();
		for (int i = 0; i < 6; i++) {
			autoCallAssets.add(new AutoCallAsset());
		}
	}

	private Double interestRate = 11.8;
	private Long countOfDays = 365L;
	private Double clientIncome = 20d;
	private Double topBarrierRate = 100d;
	private Double bottomBarrierRate = 80d;
	private Integer coupon = 4;
	private Double additionalCoupon = 0d;
	private Double shipmentBarrierRate = 80d;
	private Integer timePassed = 0;

	private Integer countAssets = 4;

	private List<AutoCallAsset> autoCallAssets = new ArrayList<>();
	private Double[][] correlations = new Double[4][4];

	private AutoCallAsset getAutoCallAssets(int i) {
		return autoCallAssets.get(i);
	}

	public AutoCallAsset getAutoCallAssets0() {
		return getAutoCallAssets(0);
	}

	public AutoCallAsset getAutoCallAssets1() {
		return getAutoCallAssets(1);
	}

	public AutoCallAsset getAutoCallAssets2() {
		return getAutoCallAssets(2);
	}

	public AutoCallAsset getAutoCallAssets3() {
		return getAutoCallAssets(3);
	}

	public Double getCorrelation00() {
		return correlations[0][0];
	}

	public void setCorrelation00(Double correlation00) {
		this.correlations[0][0] = correlation00;
	}

	public Double getCorrelation01() {
		return correlations[0][1];
	}

	public void setCorrelation01(Double correlations) {
		this.correlations[0][1] = correlations;
	}

	public Double getCorrelation02() {
		return correlations[0][2];
	}

	public void setCorrelation02(Double correlations) {
		this.correlations[0][2] = correlations;
	}

	public Double getCorrelation03() {
		return correlations[0][3];
	}

	public void setCorrelation03(Double correlations) {
		this.correlations[0][3] = correlations;
	}

	public Double getCorrelation11() {
		return correlations[1][1];
	}

	public void setCorrelation11(Double correlations) {
		this.correlations[1][1] = correlations;
	}

	public Double getCorrelation12() {
		return correlations[1][2];
	}

	public void setCorrelation12(Double correlations) {
		this.correlations[1][2] = correlations;
	}

	public Double getCorrelation13() {
		return correlations[1][3];
	}

	public void setCorrelation13(Double correlations) {
		this.correlations[1][3] = correlations;
	}

	public Double getCorrelation22() {
		return correlations[2][2];
	}

	public void setCorrelation22(Double correlations) {
		this.correlations[2][2] = correlations;
	}

	public Double getCorrelation23() {
		return correlations[2][3];
	}

	public void setCorrelation23(Double correlations) {
		this.correlations[2][3] = correlations;
	}

	public Double getCorrelation33() {
		return correlations[3][3];
	}

	public void setCorrelation33(Double correlations) {
		this.correlations[3][3] = correlations;
	}

	//Init

	//triggerPoints
	private Double triggerPoint1 = 0.25d;
	private Double triggerPoint2 = 0.5d;
	private Double triggerPoint3 = 0.75;
	private Double triggerPoint4 = 1d;

	public List<Double> getTriggerPoints() {
		List<Double> assets = new ArrayList<>();
		assets.add(triggerPoint1);
		assets.add(triggerPoint2);
		assets.add(triggerPoint3);
		assets.add(triggerPoint4);
		return assets;
	}

	@Override
	public String toString() {
		return "InterestRate=" + interestRate + "\n" +
				"CountOfDays=" + countOfDays + "\n" +
				"ClientIncome=" + clientIncome + "\n" +
				"TopBarrierRate=" + topBarrierRate + "\n" +
				"BottomBarrierRate=" + bottomBarrierRate + "\n" +
				"Coupon=" + coupon + "\n" +
				"AdditionalCoupon=" + additionalCoupon + "\n" +
				"ShipmentBarrierRate=" + shipmentBarrierRate + "\n" +
				"Trigger Points=" + getTriggerPoints() + "\n" +
				"";
	}
}
