package ru.open.spc.calc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.Asset;
import ru.open.spc.model.enums.AssetUnity;

@SuppressWarnings("serial")
@EqualsAndHashCode(of = {"productEndDate", "asset", "investingSum"})
@Data public abstract class AbstractCalculationParameters extends BaseParameters {

	@JsonIgnore
	private Double dollarExchangeRate;
	@JsonIgnore
	private Asset asset;
	@JsonIgnore
	private AssetUnity assetUnity;
	@JsonIgnore
	private Double underlyingAssetPrice;
	@JsonIgnore
	private Integer underlyingAssetMultiplicator;
	@JsonIgnore
	private Double itemCost;
	@JsonIgnore
	private String baseTicker;

	public void cleanAssetFields() {
		super.cleanAssetFields();
		setUnderlyingAssetPrice(0d);
		setAssetUnity(null);
		setUnderlyingAssetMultiplicator(0);
		setItemCost(0d);
		setBaseTicker(null);
	}

}