package ru.open.spc.calc.model.simple.flex;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.calc.model.complex.flex.PriceType;
import ru.open.spc.calc.model.simple.exchange.StocksDepositSimpleParameters;

@EqualsAndHashCode(callSuper = false)
@Data public class StocksDepositFLexSimpleParameters extends StocksDepositSimpleParameters implements IVParameters, PriceTypeParameters {

    private Double optionIV;
    private PriceType priceType;

}
