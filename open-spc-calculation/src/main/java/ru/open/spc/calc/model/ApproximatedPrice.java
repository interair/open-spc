package ru.open.spc.calc.model;

public class ApproximatedPrice {

	private final double totalValue;
	private final double valuePerDividends;
	private final double valuePerRate;

	public ApproximatedPrice(double total, double valuePerDividends, double valuePerRate) {
		this.totalValue = total;
		this.valuePerDividends = valuePerDividends;
		this.valuePerRate = valuePerRate;
	}

	public Double getTotal() {
		return totalValue - valuePerDividends;
	}

	public Double getTotalValue() {
		return totalValue;
	}

	public Double getValuePerDividends() {
		return valuePerDividends;
	}

	public Double getValuePerRate() {
		return valuePerRate;
	}

	@Override
	public String toString() {
		return "ApproximatedPrice{" + "totalValue=" + totalValue + ", valuePerDividends=" +
            valuePerDividends + ", valuePerRate=" + valuePerRate + '}';
	}
}
