package ru.open.spc.calc.model.simple.flex;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.calc.model.complex.flex.PriceType;
import ru.open.spc.calc.model.simple.exchange.BasicProtectionSimpleParameters;

@EqualsAndHashCode(callSuper = false)
@Data public class BasicProtectionFlexSimpleParameters extends BasicProtectionSimpleParameters
				implements IVParameters, IVIntervalParameters, PriceTypeParameters {

	private Double optionIV;
	private Double optionIntervalIV;
	private PriceType priceType;
	private Boolean showAllOptions;

}
