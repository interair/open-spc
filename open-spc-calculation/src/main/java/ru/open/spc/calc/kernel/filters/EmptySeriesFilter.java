package ru.open.spc.calc.kernel.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.Filter;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.dao.util.DateUtil;
import ru.open.spc.model.Series;

import java.util.List;

import static ru.open.spc.calc.util.HeaderParams.SERIES;
import static ru.open.spc.calc.util.HeaderParams.UI_CALLBACK;

@MessageEndpoint
public class EmptySeriesFilter {

	private final static Logger LOG = LoggerFactory.getLogger(EmptySeriesFilter.class);

	@Filter(inputChannel = "emptySeriesFilterChannel", outputChannel = "seriesFuturesFlexEnricherChannel")
	public boolean isCalculatingPossible(@Header(SERIES) List<Series> series,
										 @Header(UI_CALLBACK) UiCallback callback) {
		boolean empty = series.isEmpty();
		if (empty) {
			callback.addResults(DateUtil.getEmptyRes());
			LOG.debug("no series was found");
		}
		return !empty;
	}
}
