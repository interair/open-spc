package ru.open.spc.calc.kernel.enrichers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.model.Series;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ru.open.spc.calc.util.HeaderParams.SERIES;

@MessageEndpoint
public class SeriesFlexEnricher {

    @Autowired private CalculationManager calculationManager;

    @Transformer(inputChannel = "seriesFlexEnricherChannel", outputChannel = "emptySeriesFilterChannel")
    public Message<CalculationParameters> enrichOptions(Message<CalculationParameters> message) {
        final SimpleCalculationParameters param = (SimpleCalculationParameters)message.getPayload().getCalculationParams();
        final List<LocalDate> availableSeriesDates = new ArrayList<>(calculationManager.getAvailableSeriesDates(LocalDate.now(), param.getAsset().getOuterId()));
        final LocalDate expDate = param.getOptExpDate().toLocalDate();
        List<Series> series = new ArrayList<>();
        if (availableSeriesDates.contains(expDate)) {
            Series seriesBy = calculationManager.getSeriesBy(param.getAsset().getOuterId(), expDate);
            add(series, seriesBy);
        } else {
            Pair<LocalDate, LocalDate> dates = findClosestDates(availableSeriesDates, expDate);
            Series seriesLeft = calculationManager.getSeriesBy(param.getAsset().getOuterId(), dates.getLeft());
            add(series, seriesLeft);
            Series seriesRight = calculationManager.getSeriesBy(param.getAsset().getOuterId(), dates.getRight());
            add(series, seriesRight);
        }
        return MessageBuilder.fromMessage(message).setHeader(SERIES, series).build();
    }

    private void add(List<Series> series, Series s) {
        if (s != null) series.add(s);
    }

    private Pair<LocalDate, LocalDate> findClosestDates(List<LocalDate> availableSeriesDates, LocalDate expDate) {
        availableSeriesDates.add(expDate);
        Collections.sort(availableSeriesDates);
        final Pair<LocalDate, LocalDate> pair = new Pair<>();
        final int i = Collections.binarySearch(availableSeriesDates, expDate);
        if (i > 0) pair.setLeft(availableSeriesDates.get(i - 1));
        if (i +1 < availableSeriesDates.size()) pair.setRight(availableSeriesDates.get(i + 1));
        return pair;
    }
}
