package ru.open.spc.calc.kernel.activators;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import ru.open.spc.calc.kernel.result.factory.ResultFactory;
import ru.open.spc.calc.kernel.services.ApproximationCalculationService;
import ru.open.spc.calc.kernel.services.IVCalculationService;
import ru.open.spc.calc.model.ApproximatedPrice;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.Spread;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.model.simple.flex.IVIntervalParameters;
import ru.open.spc.calc.model.simple.flex.IVParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.dao.util.DateUtil;
import ru.open.spc.model.Option;
import ru.open.spc.model.Series;
import ru.open.spc.model.Underlying;
import ru.open.spc.model.enums.OptionType;
import ru.open.spc.model.result.PricingResult;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static ru.open.spc.calc.util.HeaderParams.UI_CALLBACK;
import static ru.open.spc.calc.util.HeaderParams.UI_DESCRIPTION;
import static ru.open.spc.calc.util.MathUtil.*;
import static ru.open.spc.calc.util.Util.checkForNotNull;

public abstract class AbstractCalculationService implements CalculationService {

    private final static Logger LOG = LoggerFactory.getLogger(AbstractCalculationService.class);
    final static double DAYS_PER_YEAR = 365d;

    @Autowired private ApproximationCalculationService approximationCalculating;
    @Autowired private ResultFactory resultFactory;
    @Autowired private IVCalculationService iVCalculationService;

    protected void wrapCalculate(Message<CalculationParameters> message) {
        final MessageHeaders headers = message.getHeaders();
        final CalculationParameters payload = message.getPayload();
        final DescriptionWriter dsc = (DescriptionWriter) headers.get(UI_DESCRIPTION);
        final UiCallback<PricingResult> uiCallback = (UiCallback<PricingResult>) headers.get(UI_CALLBACK);
        List<PricingResult> result;
        try {
            result = doCalculating(headers, dsc, payload);
        } catch (Exception e) {
            LOG.error("Ooops....", e);
            LOG.error(dsc.getResult());
            result = DateUtil.getEmptyRes();
        }
        uiCallback.addResults(result);
    }

    protected abstract List<PricingResult> doCalculating(MessageHeaders headers,
                                                         DescriptionWriter dsc,
                                                         CalculationParameters payload) throws Exception;

    @RequiredArgsConstructor
    protected class PriceCalculationFunction {

        private final SimpleCalculationParameters param;
        private final CalculationBase calculationBase;

        private DescriptionWriter desc;
        private List<Underlying> underlyings;
        private List<Series> serieses;
        private OptionType optionType;
        private Spread interval;
        private double strike;
        private boolean isInterval;
        private boolean conditions = true;

        public PriceCalculationFunction underlyings(List<Underlying> underlyings) {
            this.underlyings = underlyings;
            return this;
        }
        public PriceCalculationFunction serieses(List<Series> serieses) {
            this.serieses = serieses;
            return this;
        }
        public PriceCalculationFunction optionType(OptionType optionType) {
            this.optionType = optionType;
            return this;
        }
        public PriceCalculationFunction interval(Spread interval) {
            this.interval = interval;
            return this;
        }
        public PriceCalculationFunction strike(double strike) {
            this.strike = strike;
            return this;
        }
        public PriceCalculationFunction isInterval(boolean isInterval) {
            this.isInterval = isInterval;
            return this;
        }
        public PriceCalculationFunction conditions(boolean conditions) {
            this.conditions = conditions;
            return this;
        }
        public PriceCalculationFunction desc(DescriptionWriter desc) {
            this.desc = desc;
            return this;
        }

        public double apply(Double field) {
            if (checkForNotNull(field) && conditions && calculationBase == CalculationBase.EXCHANGE) {
                desc.write("price.set", field);
                return field;
            } else {
                return calculateOptPrices(calculationBase, param, desc, serieses, underlyings, optionType, interval, strike, isInterval);
            }
        }
    }

    private double calculateOptPrices(CalculationBase calculationBase,
                                      SimpleCalculationParameters param,
                                      DescriptionWriter desc,
                                      List<Series> series,
                                      List<Underlying> underlyings,
                                      OptionType optionType,
                                      Spread interval,
                                      double strike,
                                      boolean isInterval) {

        double strikePrice = strike;
        final double daysCount = param.getDaysCountToExpOpt();
        double forwardValue = 0;
        double iv = checkForFlexIVSetParam(param, isInterval);
        switch (calculationBase) {
            case EXCHANGE:
                strikePrice =  series.get(0) instanceof Option ? ((Option)  series.get(0)).getStrikePrice() : strike;
                forwardValue = approximationCalculating.applyNotLiquidPriceCorrection(param, null, series.get(0));
                break;
            case FLEX:
                final Integer multiplicator = param.getUnderlyingAssetMultiplicator();
                if (underlyings.size() == 2) {
                    final double aBefore = approximationCalculating.applyNotLiquidPriceCorrection(param, null, underlyings.get(0), param.getOptExpDate().toLocalDate());    //TODO: replace series on  futures
                    final double aAfter = approximationCalculating.applyNotLiquidPriceCorrection(param, null, underlyings.get(1), param.getOptExpDate().toLocalDate());
                    final double fv = approximationCalculating.calculateLiquidFeatures(         //fv0
                                                        param.getOptExpDate().toLocalDate(),
                                                        underlyings.get(0).getUnderlyingDate(),
                                                        underlyings.get(1).getUnderlyingDate(),
                                                        aBefore,
                                                        aAfter,
                                                        param.getAsset());
                    forwardValue = fv / multiplicator;

                } else if (underlyings.size() == 1) {
                    final LocalDate expDate = param.getOptExpDate().toLocalDate();
                    final LocalDate coeffFromDate = param.getAsset().getCoefficients().getFromDate();
                    final LocalDate underlyingDate = param.getAsset().getAssetPrice().getUnderlyingDate();
                    if (expDate.isAfter(coeffFromDate) || expDate.equals(coeffFromDate)) {
                        forwardValue = approximationCalculating.applyNotLiquidPriceCorrection(param, null, underlyings.get(0), param.getOptExpDate().toLocalDate())
                            / multiplicator;
                    } else if (expDate.isBefore(underlyingDate) || expDate.equals(underlyingDate)) {
                        final ApproximatedPrice approximatedPrice = approximationCalculating.applyBAApproximation(param.getAsset(), expDate, param.getAsset().getAssetPrice().getRealPrice() / multiplicator, CalculationBase.FLEX);
                        forwardValue = approximatedPrice.getTotal();
                    } else if (expDate.isBefore(coeffFromDate) && expDate.isAfter(underlyingDate)) {
                        forwardValue = underlyings.get(0).getRealPrice() / multiplicator;
                    }

                }
                break;
        }
        if (iv <= 0) {
            iv = calculateIV(series, strikePrice, param.getOptExpDate(), forwardValue, desc);
        }
        desc.writeWithDelimiter("iv", "; ", iv);
        final double correctedIv = correctIV(interval, iv, param.getVolatilitySpread(), desc);
        desc.write("option.price", strikePrice, correctedIv, daysCount, forwardValue, optionType);
        final double price = iVCalculationService.calculatePrice(strikePrice, correctedIv, daysCount, 0, forwardValue, optionType);
        LOG.debug("price = {}", price);
        return price;
    }

    protected double calculateIV(List<Series> series, double strikePrice,
                                 LocalDateTime optExpDate,
                                 double forwardValue, DescriptionWriter desc) {
        if (series.size() == 1) {
            desc.write("iv.price", forwardValue);
            return iVCalculationService.calculateIV(series.get(0), strikePrice, forwardValue);
        }
        if (series.size() == 2) {
            desc.write("iv.price", forwardValue);
            final double ivBfr = iVCalculationService.calculateIV(series.get(0), strikePrice, forwardValue);
            desc.writeWithDelimiter("iv.bfr", " ", ivBfr);
            final double ivAft = iVCalculationService.calculateIV(series.get(1), strikePrice, forwardValue);
            desc.writeWithDelimiter("iv.aft", " ", ivAft);
            final double leftRelation = calculateRelation(series.get(0).getExpireDate(), series.get(1).getExpireDate(), optExpDate);
            return ivAft* leftRelation +  ivBfr * (1 - leftRelation);
        }
        throw new IllegalArgumentException("series must be size 1 or 2, but actual size is " + series.size());
    }

    protected double checkForFlexIVSetParam(SimpleCalculationParameters param, boolean isInterval){
        if (isInterval) {
            if (param instanceof IVIntervalParameters) {   //FLEX
                IVIntervalParameters p = (IVIntervalParameters) param;
                if (p.getOptionIntervalIV() != null) {
                    return fromPercent(p.getOptionIntervalIV());
                }
            }
        } else {
            if (param instanceof IVParameters) {   //FLEX
                IVParameters p = (IVParameters) param;
                if (p.getOptionIV() != null) {
                    return fromPercent(p.getOptionIV());
                }
            }
        }

        return 0;
    }

    private double calculateRelation(LocalDate left, LocalDate right, LocalDateTime expDate) {
        final double totalDays = DateUtil.countDaysBetweenAbsIgnoreOneDay(left, right);
        final double leftPartDays = DateUtil.countDaysBetweenAbsIgnoreOneDay(left, expDate.toLocalDate());
        return  leftPartDays / totalDays;
    }

    protected double correctIV(Spread interval, double iv, Double volatilitySpread, DescriptionWriter desc) {
        switch (interval) {
            case BID:
                final double bidIV = findBid(iv, volatilitySpread);
                desc.write("bid.iv", bidIV);
                return bidIV;
            case ASK:
                final double askIV = findAsk(iv, volatilitySpread);
                desc.write("ask.iv", askIV);
                return askIV;
            default:
                desc.writeEmptyLine();
                return iv;
        }
    }

    public double correctPrice(SimpleCalculationParameters param, DescriptionWriter desc, Option value) {
        return approximationCalculating.applyNotLiquidPriceCorrection(param, desc, value);
    }

    public ResultFactory getResultFactory() {
        return resultFactory;
    }

}
