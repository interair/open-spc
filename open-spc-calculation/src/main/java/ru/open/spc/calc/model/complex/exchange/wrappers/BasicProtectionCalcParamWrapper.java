package ru.open.spc.calc.model.complex.exchange.wrappers;

import ru.open.spc.calc.model.complex.AbstractComplexCalculationParameters;
import ru.open.spc.calc.model.complex.ComplexParamContext;
import ru.open.spc.calc.model.complex.ParamWrapper;
import ru.open.spc.calc.model.complex.exchange.BasicProtectionComplexParameters;
import ru.open.spc.calc.model.simple.exchange.BasicProtectionSimpleParameters;
import ru.open.spc.model.Asset;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.model.enums.Direction;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class BasicProtectionCalcParamWrapper extends BasicProtectionSimpleParameters implements ParamWrapper {

	private BasicProtectionComplexParameters parameter;
	private ComplexParamContext context;

	public BasicProtectionCalcParamWrapper(BasicProtectionComplexParameters parameter, ComplexParamContext context) {
		this.parameter = parameter;
		this.context = context;
	}

	public Direction getDirection() {
		return context.getDirection();
	}

	public LocalDateTime getOptExpDate() {
		return context.getOptExpDate();
	}

	public LocalDate getProductEndDate() {
		return context.getProductEndDate();
	}

	public Double getCoeffAssetProtection() {
		return context.getCoeffAssetProtection();
	}

	public Double getInterestRate() {
		return context.getInterestRate();
	}

	public Double getItemCost() {
		return parameter.getItemCost();
	}

	public Double getStrike1() {
		return context.getStrike1();
	}

	public Double getStrike2() {
		return context.getStrike2();
	}

	public Double getUnderlyingAssetPrice() {
		return parameter.getUnderlyingAssetPrice();
	}

	public Integer getUnderlyingAssetMultiplicator() {
		return parameter.getUnderlyingAssetMultiplicator();
	}

	public Double getInvestingSum() {
		return parameter.getInvestingSum();
	}

	public AssetUnity getAssetUnity() {
		return parameter.getAssetUnity();
	}

	public Asset getAsset() {
		return parameter.getAsset();
	}

	public LocalDate getDepositStartDate() {
		return parameter.getDepositStartDate();
	}

	public Double getDollarExchangeRate() {
		return parameter.getDollarExchangeRate();
	}

	public Double getVolatilitySpread() {
		if (context.getVolatilitySpread() != null) {
			return context.getVolatilitySpread();
		} else {
			return super.getVolatilitySpread();
		}
	}

    public AssetUnity getVzkUnity () {
        return parameter.getVzkUnity();
    }
	public String getBaseTicker() {
		return parameter.getBaseTicker();
	}

	public Double getStrike2From() {
		return parameter.getStrike2From();
	}

	public Double getStrike2To() {
		return parameter.getStrike2To();
	}

	public Double getStrike2Step() {
		return parameter.getStrike2Step();
	}

	@Override
	public Boolean getVariability() {
		return true;
	}
	
	public Boolean getInterval() {
		return context.isInterval();
	}

	public BasicProtectionComplexParameters getParameter() {
	    return parameter;
	}

	public void setParameter(AbstractComplexCalculationParameters parameter) {
	    this.parameter = (BasicProtectionComplexParameters) parameter;
	}

	@Override
	public Boolean getShowAllOptions() {
		return false;
	}

	public ComplexParamContext getContext() {
	return context;
	}

	public void setContext(ComplexParamContext context) {
	this.context = context;
	}
}
