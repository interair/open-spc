package ru.open.spc.calc.kernel.splitters;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Splitter;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.complex.ComplexParamContext;
import ru.open.spc.calc.model.complex.exchange.StockDepositComplexParameters;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.dao.util.DateUtil;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static ru.open.spc.calc.util.HeaderParams.CALCULATION_BASE;
import static ru.open.spc.calc.util.HeaderParams.UI_CALLBACK;
import static ru.open.spc.calc.util.Util.getArray;
import static ru.open.spc.dao.util.DateUtil.getNextDate;

@MessageEndpoint
public class StockDepositParametersSplitter extends ComplexParametersSplitter {

    @Splitter(inputChannel = "splitStockDepositComplexChannel", outputChannel = "prepareSimpleCalculationChannel")
    public List<CalculationParameters> processMessage(final CalculationParameters param,
                                                      @SuppressWarnings("rawtypes") @Header(UI_CALLBACK) UiCallback callback,
                                                      @Header(CALCULATION_BASE) CalculationBase calculationBase) {
        final StockDepositComplexParameters calculationParams = (StockDepositComplexParameters) param.getCalculationParams();
        final List<CalculationParameters> cps = new ArrayList<>();
        List<LocalDate> dates = resolveDates(calculationBase, calculationParams);
        final List<Double> rates = getArray(calculationParams.getInterestRateFrom(),
            calculationParams.getInterestRateTo(), calculationParams.getInterestRateStep());
        for (LocalDate date : dates) {
            for (Double rate : rates) {
                final Double spread = calculateSpread(date, calculationParams.getAsset());
                final ComplexParamContext context = new ComplexParamContext.Builder()
                    .optExpDate(DateUtil.setHours(date, 18, 45))
                    .productEndDate(getNextDate(date))
                    .interestRate(rate)
                    .volatilitySpread(spread)
                    .stockNum(calculationParams.getStockNum())
                    .repoDiscount(calculationParams.getRepoDiscount())
                    .repoRate(calculationParams.getRepoRate())
                    .calculationBase(calculationBase)
                    .create();
                cps.add(createSimpleCalculation(param, context));
            }
        }
        callback.setTotalStepsAmount(cps.size());
        return cps;
    }

}
