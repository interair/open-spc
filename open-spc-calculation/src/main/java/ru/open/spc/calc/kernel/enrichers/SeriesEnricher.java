package ru.open.spc.calc.kernel.enrichers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.model.Series;

import java.util.Collections;

import static ru.open.spc.calc.util.HeaderParams.SERIES;

@MessageEndpoint
public class SeriesEnricher {

	@Autowired private CalculationManager calculationManager;

	@Transformer(inputChannel = "seriesEnricherChannel", outputChannel = "emptySeriesFilterChannel")
	public Message<CalculationParameters> enrichOptions(Message<CalculationParameters> message) {
        final SimpleCalculationParameters param = (SimpleCalculationParameters)message.getPayload().getCalculationParams();
        Series seriesBy = calculationManager.getSeriesBy(param.getAsset().getOuterId(), param.getOptExpDate().toLocalDate());
        final MessageBuilder<CalculationParameters> messageBuilder = MessageBuilder.fromMessage(message);
        if (seriesBy == null) {
            return messageBuilder.setHeader(SERIES, Collections.emptyList()).build();
        }  else {
            return messageBuilder.setHeader(SERIES, Collections.singletonList(seriesBy)).build();
        }
	}
}
