package ru.open.spc.calc.model.complex.flex;

import java.time.Duration;

public interface DateStepHolder {

    public default Long getDateStep() {
        return getDateStepDuration().toDays();
    }

    public Duration getDateStepDuration();

    public default void setDateStep(Long dateStep) {
        setDateStepDuration(Duration.ofDays(dateStep));
    }

    public void setDateStepDuration(Duration duration);
}
