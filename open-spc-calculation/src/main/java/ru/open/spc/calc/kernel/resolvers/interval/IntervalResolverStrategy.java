package ru.open.spc.calc.kernel.resolvers.interval;

import ru.open.spc.calc.model.simple.exchange.BasicProtectionSimpleParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.model.Option;
import ru.open.spc.model.enums.OptionType;

import java.util.List;

public interface IntervalResolverStrategy {
	List<Option> resolve(BasicProtectionSimpleParameters param,
						 DescriptionWriter dsc,
						 List<Option> options,
						 OptionType optionType,
						 Pair<Option, Option> strikes);
}
