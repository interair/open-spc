package ru.open.spc.calc.kernel.splitters;

import org.springframework.beans.BeanUtils;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Splitter;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.model.simple.exchange.FixStrike1AwareSimpleParameters;

import java.util.ArrayList;
import java.util.List;

@MessageEndpoint
public class FixStrikeSplitter {

	@Splitter(inputChannel = "strikeSplitterChannel", outputChannel = "seriesEnricherChannel")
	public List<CalculationParameters> processMessage(final CalculationParameters param) throws IllegalAccessException, InstantiationException {
		final List<CalculationParameters> cps = new ArrayList<>();
		cps.add(param);
		final FixStrike1AwareSimpleParameters parameters = (FixStrike1AwareSimpleParameters) param.getCalculationParams();
		if (!checkForEmpty(parameters.getFixStrike1())) {
            final CalculationParameters copyParam = new CalculationParameters();
			BeanUtils.copyProperties(param, copyParam);
			final SimpleCalculationParameters calculationParams = (SimpleCalculationParameters) copyParam.getCalculationParams();
            final SimpleCalculationParameters copyCalculationParams = (SimpleCalculationParameters) copyParam.getCalculationParams().getClass().newInstance();
            BeanUtils.copyProperties(calculationParams, copyCalculationParams);
            copyParam.setCalculationParams(copyCalculationParams);
            copyCalculationParams.setTotalAssetPriceWithStrike1Coef(parameters.getFixStrike1());
			cps.add(copyParam);
		}
		return cps;
	}

	private boolean checkForEmpty(Double param) {
		return param == null || param.equals(0d);
	}
}
