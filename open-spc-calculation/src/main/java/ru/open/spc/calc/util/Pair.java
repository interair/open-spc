package ru.open.spc.calc.util;

import lombok.Data;

import java.io.Serializable;

@SuppressWarnings("serial")
@Data public class Pair<Left, Right> implements Serializable {

	private Left left;
	private Right right;

	public Pair(Left left, Right right) {
		this.left = left;
		this.right = right;
	}

	public Pair() {
	}

	public Boolean isFilled() {
		return left != null && right != null;
	}

}
