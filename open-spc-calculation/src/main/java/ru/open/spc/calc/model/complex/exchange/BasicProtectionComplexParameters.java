package ru.open.spc.calc.model.complex.exchange;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.calc.model.complex.ComplexCalculationParameters;
import ru.open.spc.model.enums.AssetUnity;

@EqualsAndHashCode(callSuper = true)
@Data public class BasicProtectionComplexParameters extends ComplexCalculationParameters {
	
	private Double coeffAssetProtectionFrom;
	private Double coeffAssetProtectionTo;
	private Double coeffAssetProtectionStep;
	private Double strike2From;
	private Double strike2To;
	private Double strike2Step;
    private Boolean interval;

    private AssetUnity vzkUnity = AssetUnity.RUB;

}
