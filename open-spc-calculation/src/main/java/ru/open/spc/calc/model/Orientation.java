package ru.open.spc.calc.model;

import ru.open.spc.model.enums.StringCodeAware;

public enum Orientation implements StringCodeAware {
    INNER("inner"), OUTER("outer");

    private final String code;

    private Orientation(String code) {
        this.code = code;
    }

    @Override
    public String code() {
        return code;
    }

    public static Orientation fromCode(String code) {
        for (Orientation orientation : values()) {
            if (orientation.code.equals(code)) {
                return orientation;
            }
        }
        return null;
    }

}
