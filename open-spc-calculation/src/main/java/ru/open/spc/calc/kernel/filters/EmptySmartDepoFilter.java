package ru.open.spc.calc.kernel.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.Filter;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.dao.util.DateUtil;
import ru.open.spc.model.Option;

import static ru.open.spc.calc.util.HeaderParams.*;

@MessageEndpoint
public class EmptySmartDepoFilter {

    private final static Logger LOG = LoggerFactory.getLogger(EmptySmartDepoFilter.class);

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Filter(inputChannel = "filterStrikesSDChannel", outputChannel = "smartDepositChannel")
    public boolean isCalculatingPossible(@Header(PUT_OPTIONS)Pair<Option, Option> putOptions,
                                         @Header(CALL_OPTIONS) Pair<Option, Option> callOptions,
                                         @Header(UI_CALLBACK) UiCallback callback) {
        boolean full = putOptions.isFilled() && callOptions.isFilled()
                     && ((putOptions.getLeft().getStrikePrice() <= putOptions.getRight().getStrikePrice())
                     &&  (putOptions.getRight().getStrikePrice() <= callOptions.getLeft().getStrikePrice())
                     && (callOptions.getLeft().getStrikePrice() <= callOptions.getRight().getStrikePrice()));
        if (!full) {
            callback.addResults(DateUtil.getEmptyRes());
            LOG.debug("no series was found");
        }
        return full;
    }
}
