package ru.open.spc.calc.kernel.gateway;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.annotation.Async;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.model.CalculationType;

import static ru.open.spc.calc.util.HeaderParams.*;


@MessageEndpoint
public interface Calculation {

    @Async
	@Gateway(requestChannel = "processCalculationChannel", replyChannel = "nullChannel")
	void calculate(@Payload CalculationParameters param,
                   @Header(UI_CALLBACK) UiCallback<?> callback,
                   @Header(CALCULATION_BASE) CalculationBase calculationBase,
                   @Header(CALCULATION_TYPE) CalculationType calculationType);

    @Gateway(requestChannel = "processCalculationChannel", replyChannel = "nullChannel")
    void calculateSync(@Payload CalculationParameters param,
                   @Header(UI_CALLBACK) UiCallback<?> callback,
                   @Header(CALCULATION_BASE) CalculationBase calculationBase,
                   @Header(CALCULATION_TYPE) CalculationType calculationType);
}
