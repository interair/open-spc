package ru.open.spc.calc.kernel.splitters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Splitter;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.complex.ComplexCalculationParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.util.HeaderParams;
import ru.open.spc.dao.manager.CalculationManager;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static ru.open.spc.calc.util.MathUtil.fromPercent;
import static ru.open.spc.calc.util.Util.getArray;

@MessageEndpoint
public class FlexStrikeSplitter extends AbstractStrikeSplitter {

    @Autowired private CalculationManager calculationManager;

    @Splitter(inputChannel = "splitFlexStrikeComplexChannel", outputChannel = "seriesEnricherRouterChannel")
    public List<CalculationParameters> processMessage(final CalculationParameters param,
                                                      @Header(HeaderParams.SOURCE_PARAM)ComplexCalculationParameters complexParameters) throws Exception {
        final List<Double> strikes = getStrikes((SimpleCalculationParameters) param.getCalculationParams(),
            complexParameters.getStrike1From(), complexParameters.getStrike1To(), complexParameters.getStrike1Step());
        if (strikes.isEmpty()) {
            return Collections.singletonList(param);
        }
        return cloneMessages(param, strikes);
    }

    List<Double> getStrikes(SimpleCalculationParameters params, double strike1From, double strike1To, double step) {
        return getArray(strike1From, strike1To, step).stream().map(strike -> (1 + fromPercent(strike)) * params.getTotalAssetPrice())
            .collect(Collectors.toList());
    }
}
