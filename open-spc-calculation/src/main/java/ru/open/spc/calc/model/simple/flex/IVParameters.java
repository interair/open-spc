package ru.open.spc.calc.model.simple.flex;

public interface IVParameters {

    void setOptionIV(Double optionIV);
    Double getOptionIV();
}
