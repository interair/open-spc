package ru.open.spc.calc.model.complex;

import lombok.Data;
import ru.open.spc.calc.model.BaseParameters;
import ru.open.spc.model.AutoCallAsset;

import java.util.ArrayList;
import java.util.List;

@Data
public class AutoCallComplexCalculationParameters extends BaseParameters {

	private List<AutoCallAsset> autoCallAssets = new ArrayList<>();

	private Double interestRateFrom = 11.8d;
	private Double interestRateTo = 11.8d;
	private Double interestRateStep = 1d;

	private Double clientIncomeFrom = 20d;
	private Double clientIncomeTo = 20d;
	private Double clientIncomeStep = 1d;

	private Double topBarrierRateFrom = 100d;
	private Double topBarrierRateTo = 100d;
	private Double topBarrierRateStep = 1d;

	private Double bottomBarrierRateFrom = 100d;
	private Double bottomBarrierRateTo = 100d;
	private Double bottomBarrierRateStep = 1d;

	private Double shipmentBarrierRateFrom = 80d;
	private Double shipmentBarrierRateTo = 80d;
	private Double shipmentBarrierRateStep = 1d;

	private Integer couponFrom = 4;
	private Integer couponTo = 4;
	private Integer couponStep = 1;

	private Integer additionalCouponFrom = 0;
	private Integer additionalCouponTo = 0;
	private Integer additionalCouponStep = 1;

	private Double countOfYearsFrom = 1d;
	private Double countOfYearsTo = 1d;
	private Double countOfYearsStep = 1d;

}
