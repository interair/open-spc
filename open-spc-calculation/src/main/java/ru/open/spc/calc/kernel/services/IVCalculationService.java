package ru.open.spc.calc.kernel.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.open.spc.model.Series;
import ru.open.spc.model.SmileParams;
import ru.open.spc.model.enums.OptionType;

import static ru.open.spc.calc.util.MathUtil.normcdf;

@Service
public class IVCalculationService {

	private final static Logger LOG = LoggerFactory.getLogger(IVCalculationService.class);
    private final static double SECOND_IN_DAY_PART = 1d / 24d / 60d / 60d;
    private final static double DAYS_IN_YEAR = 365d;

	public double calculateIV(Series series, double strike, double realPrice) {
		LOG.debug("calculating iv {} {}", realPrice, series);
		SmileParams smileParams = series.getSmileParams();
		double x0, mb0, mr0, sv0 = 0d;
		double result = series.getSmileParams().getBottom();
		if (strike == 0d || realPrice == 0d) {
			return result;
		}
		x0 = Math.log(strike / realPrice) - smileParams.getLoc();
		if (smileParams.getScal() <= 0 || Math.abs(x0) == 0) {
			return result;
		} else {
			if (Math.log(Math.abs(x0) * smileParams.getScal()) == 0) {
				mb0 = 2d;
			} else {
				if (smileParams.getKurt() * Math.log(Math.abs(x0) * smileParams.getScal()) >= Math.log(Double.MAX_VALUE)) {
					mb0 = Double.MAX_VALUE;
				} else {
					mb0 = 1 + Math.pow((Math.abs(x0) * smileParams.getScal()), smileParams.getKurt());
				}
			}
		}
		if (x0 == 0) {
			mr0 = 1d;
		} else {
			if (Math.abs(smileParams.getSkew()) > 0) {
				return result;
			} else {
				mr0 = Math.sqrt(1 - Math.pow(Math.abs(smileParams.getSkew()), (1 / Math.abs(x0))) * Math.signum(x0) * Math.signum(smileParams.getSkew()));
				if (mb0 == Double.MAX_VALUE) {
					sv0 = 0d;
				} else {
					sv0 = Math.pow(mb0, (-mr0));
				}
			}
		}
		double totalResult = smileParams.getHeight() * (1 - sv0) + smileParams.getBottom();
		LOG.debug("iv = {}", totalResult);
		return totalResult;
	}

	public double calculatePrice(double strikePrice, double iv, double daysCount, double interestRate, double realPrice, OptionType type) {

		double d1, d2, dteYears, irDteYears, result;
		int optionType = type.code();

		if ((daysCount <= SECOND_IN_DAY_PART) || (iv == 0)) {
			result = (Math.abs(realPrice - strikePrice) + optionType * (realPrice - strikePrice)) / 2;
		} else {
			dteYears = daysCount / DAYS_IN_YEAR;
			d1 = Math.log(realPrice / strikePrice) / iv / Math.sqrt(dteYears) + iv * Math.sqrt(dteYears) / 2;
			d2 = d1 - iv * Math.sqrt(dteYears);
			if (interestRate == 0d) {
				irDteYears = 1d;
			} else {
				irDteYears = Math.pow(1 + interestRate, -dteYears);
			}
			result = irDteYears * optionType * (realPrice * normcdf(optionType * d1) - strikePrice * normcdf(optionType * d2));
		}
		return result;
	}
}
