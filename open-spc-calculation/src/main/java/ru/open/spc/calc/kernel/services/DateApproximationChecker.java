package ru.open.spc.calc.kernel.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.model.Asset;

import java.time.LocalDate;
import java.util.List;

@Service
public class DateApproximationChecker {

    private final static Logger LOG = LoggerFactory.getLogger(DateApproximationChecker.class);

    @Autowired
    private AssetManager service;

    @Autowired
    private CalculationManager calculationManager;

    @Scheduled(cron = "0 0 0 * * MON-FRI")
    public void check() {
        LOG.info("check started");
        final List<Asset> all = service.getByCoefficientFromDate(LocalDate.now());
        LOG.info("need to update {} assets", all.size());
        all.forEach(this::checkAndSet);
    }

    private void checkAndSet(Asset asset) {
        asset.getCoefficients().setFromDate(findNextDate(asset));
    }

    private LocalDate findNextDate(Asset asset) {
        final LocalDate nextFutureDate = calculationManager.getNextFutureDate(asset.getOuterId());
        LOG.debug("next date for asset {} = {}", asset, nextFutureDate);
        return nextFutureDate;
    }

    public void setAssetService(AssetManager service) {
        this.service = service;
    }

    public void setCalculationManager(CalculationManager calculationManager) {
        this.calculationManager = calculationManager;
    }
}
