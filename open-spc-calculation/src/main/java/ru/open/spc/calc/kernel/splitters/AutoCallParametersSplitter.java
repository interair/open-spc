package ru.open.spc.calc.kernel.splitters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Splitter;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import ru.open.spc.calc.kernel.enrichers.DescriptionEnricher;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.complex.AutoCallComplexCalculationParameters;
import ru.open.spc.calc.model.simple.AutoCallParameters;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.dao.manager.AutoCallAssetsRelationManager;
import ru.open.spc.dao.util.RoundUtil;
import ru.open.spc.model.AutoCallAsset;
import ru.open.spc.model.AutoCallAssetsRelation;

import java.util.ArrayList;
import java.util.List;

import static ru.open.spc.calc.util.HeaderParams.UI_CALLBACK;
import static ru.open.spc.calc.util.Util.getArray;

@MessageEndpoint
public class AutoCallParametersSplitter {

	@Autowired
	private DescriptionEnricher descriptionEnricher;

	@Autowired
	private AutoCallAssetsRelationManager autoCallAssetsRelationManager;

	@Splitter(inputChannel = "autoCallSplitterChannel", outputChannel = "autoCallSimpleChannel")
	public List<Message<CalculationParameters>> processMessage(Message<CalculationParameters> message) {
		CalculationParameters param = message.getPayload();
		UiCallback callback = (UiCallback) message.getHeaders().get(UI_CALLBACK);
		final List<Message<CalculationParameters>> cps = new ArrayList<>();

		final AutoCallComplexCalculationParameters calculationParams = (AutoCallComplexCalculationParameters) param.getCalculationParams();
		List<Double> clientIncomes = getArray(calculationParams.getClientIncomeFrom(), calculationParams.getClientIncomeTo(),
				calculationParams.getClientIncomeStep());

		List<Double> interestRates = getArray(calculationParams.getInterestRateFrom(), calculationParams.getInterestRateTo(),
				calculationParams.getInterestRateStep());

		List<Double> bottomBarrierRates = getArray(calculationParams.getBottomBarrierRateFrom(), calculationParams.getBottomBarrierRateTo(),
				calculationParams.getBottomBarrierRateStep());

		List<Double> shippementBarrierRates = getArray(calculationParams.getShipmentBarrierRateFrom(), calculationParams.getShipmentBarrierRateTo(),
				calculationParams.getBottomBarrierRateStep());

		List<Double> topBarrierRateSteps = getArray(calculationParams.getTopBarrierRateFrom(), calculationParams.getTopBarrierRateTo(),
				calculationParams.getTopBarrierRateStep());

		List<Double> additionalCoupons = getArray(calculationParams.getAdditionalCouponFrom(), calculationParams.getAdditionalCouponTo(),
				calculationParams.getAdditionalCouponStep());

		List<Double> countOfYears = getArray(calculationParams.getCountOfYearsFrom(), calculationParams.getCountOfYearsTo(),
				calculationParams.getCountOfYearsStep());

		for (Double clientIncome : clientIncomes) {
			for (Double interestRate : interestRates) {
				for (Double bottomBarrierRate : bottomBarrierRates) {
					for (Double shippementBarrierRate : shippementBarrierRates) {
						for (Double topBarrierRateStep : topBarrierRateSteps) {
							for (Double additionalCoupon : additionalCoupons) {
								for (Double countOfYear : countOfYears) {
									AutoCallParameters autoCallParameters = new AutoCallParameters();
									autoCallParameters.setBottomBarrierRate(bottomBarrierRate);
									autoCallParameters.setShipmentBarrierRate(shippementBarrierRate);
									autoCallParameters.setTopBarrierRate(topBarrierRateStep);

									autoCallParameters.setAdditionalCoupon(additionalCoupon);
									autoCallParameters.setClientIncome(clientIncome);
									autoCallParameters.setCountOfDays(RoundUtil.roundUP(countOfYear * 365L));
									autoCallParameters.setDepositStartDate(calculationParams.getDepositStartDate());
									autoCallParameters.setInterestRate(interestRate);

									List<AutoCallAsset> autoCallAssets = calculationParams.getAutoCallAssets();
									int size = autoCallAssets.size();
									autoCallParameters.setCountAssets(size);
									autoCallParameters.setAutoCallAssets(autoCallAssets);
									Double[][] corr = new Double[size][size];
									for (int i = 0; i < autoCallAssets.size(); i++) {
										for (int j = 0; j < autoCallAssets.size(); j++) {
											AutoCallAsset ai = autoCallAssets.get(i);
											AutoCallAsset aj = autoCallAssets.get(j);

											if (ai.getId() != null && aj.getId() != null) {
												AutoCallAssetsRelation relation = autoCallAssetsRelationManager.getRelation(ai.getId(), aj.getId());
												if (relation != null) {
													corr[i][j] = relation.getCoefficient();
												}
											}
										}
									}
									autoCallParameters.setCorrelations(corr);

									CalculationParameters copyParam = new CalculationParameters();
									BeanUtils.copyProperties(param, copyParam);
									copyParam.setCalculationParams(autoCallParameters);
									Message<CalculationParameters> m = MessageBuilder.withPayload(copyParam).copyHeaders(message.getHeaders()).build();
									cps.add(descriptionEnricher.enrichDescriptionWriter(m));
								}
							}
						}
					}
				}
			}
		}
		callback.setTotalStepsAmount(cps.size());
		return cps;
	}

}
