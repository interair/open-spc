package ru.open.spc.calc.model.simple.flex;

public interface IVIntervalParameters {

    void setOptionIntervalIV(Double optionIntervalIV);
    Double getOptionIntervalIV();
}
