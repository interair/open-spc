package ru.open.spc.calc.kernel.resolvers.interval;

import org.springframework.stereotype.Service;
import ru.open.spc.calc.model.complex.exchange.wrappers.BasicProtectionCalcParamWrapper;
import ru.open.spc.calc.model.simple.exchange.BasicProtectionSimpleParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.model.Option;
import ru.open.spc.model.enums.OptionType;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static ru.open.spc.calc.util.MathUtil.fromPercent;

@Service
public class ComplexIntervalResolver extends SimpleIntervalResolver {

	public List<Option> resolve(BasicProtectionSimpleParameters param,
								DescriptionWriter dsc,
								List<Option> options,
								OptionType optionType,
								Pair<Option, Option> strikes) {
		List<Option> inOpts = new ArrayList<>();
		BasicProtectionCalcParamWrapper wrparam = (BasicProtectionCalcParamWrapper) param;
		Double bounds = getBounds(optionType, strikes);

		if (wrparam.getStrike2From() != 0 || wrparam.getStrike2To() != 0) {
			inOpts = findMinMaxIntervalOptionsBounds(options, bounds, optionType,
				wrparam.getStrike2From(), wrparam.getStrike2To(),
				wrparam.getTotalAssetPrice());
		}
		return inOpts;
	}

	public List<Option> findMinMaxIntervalOptionsBounds(List<Option> options, Double intStrike, OptionType type,
														Double from, Double to, Double assetPrice) {
		double p1 = assetPrice * (1 + fromPercent(calcValByType(from, type)));
		double p2 = assetPrice * (1 + fromPercent(calcValByType(to, type)));
		double min = min(p1, p2);
		double max = max(p1, p2);
		return findIntervalOptions(options, intStrike, type, min, max);
	}

	private Double calcValByType(Double from, OptionType type) {
		return from * type.code();
	}
}
