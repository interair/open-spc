package ru.open.spc.calc.kernel.result.factory;

import org.springframework.stereotype.Service;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.model.result.FCPricingResult;

@Service
public class FixCouponPricingResultCreator extends PricingResultCreator<FCPricingResult> {

	@Override
	public FCPricingResult createResult(ResultBuilder rb) {
		final FCPricingResult pricingResult = fill(rb, new FCPricingResult());
		pricingResult.setMaximumEarning(rb.getMaximumEarning());
		pricingResult.setDirection(Direction.FALLING);
		return pricingResult;
	}

}
