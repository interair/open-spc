package ru.open.spc.calc.kernel.result.factory;

import org.springframework.stereotype.Service;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.model.enums.ProductCode;
import ru.open.spc.model.result.PricingResult;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class ResultFactory {

	@Resource(name = "productCreators")
	private Map<ProductCode, Creator<?>> creators;

	public PricingResult createPricingResult(ResultBuilder rb) {
		return creators.get(rb.getProduct().getCode()).createResult(rb);
	}
}
