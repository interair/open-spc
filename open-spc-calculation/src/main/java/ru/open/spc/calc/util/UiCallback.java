package ru.open.spc.calc.util;

import java.io.Serializable;
import java.util.List;

public interface UiCallback<T> extends Serializable {

	public void setTotalStepsAmount(final int totalStep);

	public int getIndicatorValue();

	public int getTotalSteps();

	public boolean isDone();

	public List<T> getResult();

	public void finish();

	public int getTotalPhases();

	public int getCurrentPhase();

	public void increment();

	public void setPhaseCount(final int count);

	public void incrementPhase();

	public void addResults(final List<T> doCalculating);
}
