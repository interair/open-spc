package ru.open.spc.calc.model.complex.flex;

import lombok.Data;
import ru.open.spc.calc.model.complex.exchange.StockDepositComplexParameters;

import java.time.Duration;

@Data public class StockDepositComplexFlexParameters extends StockDepositComplexParameters implements DateStepHolder {

    private Duration dateStepDuration = Duration.ofDays(1);

}
