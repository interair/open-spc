package ru.open.spc.calc.model.simple.flex;

import ru.open.spc.calc.model.complex.flex.PriceType;

public interface PriceTypeParameters {

    PriceType getPriceType();
    void setPriceType(PriceType priceType);
}
