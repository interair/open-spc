package ru.open.spc.calc.kernel.enrichers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.model.Underlying;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ru.open.spc.calc.util.HeaderParams.FUTURES;

@MessageEndpoint
public class FuturesFlexEnricher {

    @Autowired
    private CalculationManager calculationManager;

    @Transformer(inputChannel = "seriesFuturesFlexEnricherChannel", outputChannel = "fcSdRouterChannel")
    public Message<CalculationParameters> enrichFutures(Message<CalculationParameters> message) {
        final SimpleCalculationParameters param = (SimpleCalculationParameters)message.getPayload().getCalculationParams();
        final List<LocalDate> availableFuturesDates = new ArrayList<>(calculationManager.getAvailableFuturesDates(LocalDate.now(),
                param.getAsset().getCoefficients().getFromDate(),
                param.getAsset().getOuterId()));
        final LocalDate expDate = param.getOptExpDate().toLocalDate();
        List<Underlying> futures = new ArrayList<>();
        if (availableFuturesDates.contains(expDate)) {
            add(futures, calculationManager.getFuturesBy(param.getAsset().getOuterId(), expDate));
        } else {
            Pair<LocalDate, LocalDate> dates = findClosestDates(availableFuturesDates, expDate);
            Underlying futureLeft = calculationManager.getFuturesBy(param.getAsset().getOuterId(), dates.getLeft());
            add(futures, futureLeft);
            Underlying futureRight = calculationManager.getFuturesBy(param.getAsset().getOuterId(), dates.getRight());
            add(futures, futureRight);
        }
        return MessageBuilder.fromMessage(message).setHeader(FUTURES, futures).build();
    }

    private void add(List<Underlying> series, Underlying s) {
        if (s != null) series.add(s);
    }

    private Pair<LocalDate, LocalDate> findClosestDates(List<LocalDate> availableSeriesDates, LocalDate expDate) {
        availableSeriesDates.add(expDate);
        Collections.sort(availableSeriesDates);
        final Pair<LocalDate, LocalDate> pair = new Pair<>();
        final int i = Collections.binarySearch(availableSeriesDates, expDate);
        if (i > 0) pair.setLeft(availableSeriesDates.get(i - 1));
        if (i + 1 < availableSeriesDates.size()) pair.setRight(availableSeriesDates.get(i + 1));
        return pair;
    }
}
