package ru.open.spc.calc.kernel.resolvers;

import org.springframework.stereotype.Service;
import ru.open.spc.model.SpreadVolatilityParams;

import java.time.LocalDate;

import static ru.open.spc.dao.util.DateUtil.countDaysBetween;

@Service
public class SpreadResolver {

	public Double getSpread(LocalDate date, SpreadVolatilityParams spread) {
		if (checkParamForNull(spread)) {
			return null;
		}
		if (date.isBefore(spread.getMinVolatilityDate())) {
			return spread.getMinVolatility();
		} else if (date.isAfter(spread.getMaxVolatilityDate())) {
			return spread.getMaxVolatility();
		} else {
			double days1 = countDaysBetween(spread.getMinVolatilityDate(), date);
			double days2 = countDaysBetween(spread.getMinVolatilityDate(), spread.getMaxVolatilityDate());
			return days1 / days2 * (spread.getMaxVolatility() - spread.getMinVolatility()) + spread.getMinVolatility();
		}
	}

	private boolean checkParamForNull(SpreadVolatilityParams spread) {
		return (spread == null ||
			spread.getMaxVolatility() == null ||
			spread.getMaxVolatilityDate() == null ||
			spread.getMinVolatility() == null ||
			spread.getMinVolatilityDate() == null);
	}
}
