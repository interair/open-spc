package ru.open.spc.calc.kernel.activators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import ru.open.spc.calc.kernel.services.ApproximationCalculationService;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.calc.model.simple.exchange.StocksDepositSimpleParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.model.Series;
import ru.open.spc.model.Underlying;
import ru.open.spc.model.result.PricingResult;

import java.util.Collections;
import java.util.List;

import static ru.open.spc.calc.kernel.SPCalculationCommon.toRub;
import static ru.open.spc.calc.kernel.enrichers.AssetPriceEnricher.getInstrumentType;
import static ru.open.spc.calc.model.Spread.BID;
import static ru.open.spc.calc.util.HeaderParams.*;
import static ru.open.spc.calc.util.MathUtil.fromPercent;
import static ru.open.spc.calc.util.MathUtil.toPercent;
import static ru.open.spc.dao.util.DateUtil.countDaysBetween;
import static ru.open.spc.model.enums.InstrumentType.SPOT;
import static ru.open.spc.model.enums.OptionType.CALL;

@MessageEndpoint
public class StocksDepositCalculationService extends AbstractCalculationService {

    private final static double GO_INIT = 0.15;
    private final static double PROB_ITM = 0.3;
    private final static double REWARD_CONST = 2d / 3d;

    @Autowired private ApproximationCalculationService approximationCalculating;

    @ServiceActivator(inputChannel = "stocksDepositChannel")
    public void calculate(final Message<CalculationParameters> message) {
        wrapCalculate(message);
    }

    @Override
    protected List<PricingResult> doCalculating(MessageHeaders headers, DescriptionWriter dsc,
                                                CalculationParameters payload) throws Exception {
        final StocksDepositSimpleParameters param = (StocksDepositSimpleParameters) payload.getCalculationParams();
        final List<Series> series = (List<Series>) headers.get(SERIES);
        final CalculationBase calculationBase = (CalculationBase) headers.get(CALCULATION_BASE);
        final List<Underlying> underlying = (List<Underlying>) headers.get(FUTURES);

        final PriceCalculationFunction priceCalculationFunction = new PriceCalculationFunction(param, calculationBase).desc(dsc).underlyings(underlying);
        dsc.write("strike.price", param.getTotalAssetPriceWithStrike1Coef());
        series.forEach(s -> dsc.write("series", s.getName(), s.getRealPrice()));
        dsc.write("repo", param.getRepoRate());
        dsc.write("dscnt", param.getRepoDiscount());
        dsc.write("go_init", toPercent(GO_INIT));
        dsc.write("prob_itm", toPercent(PROB_ITM));

        final double assetPrice = calculationBase == CalculationBase.EXCHANGE ? param.getAsset().getAssetPrice().getRealPrice() :
            param.getAsset().getAssetPrice().getRealPrice() / param.getUnderlyingAssetMultiplicator();
        final double cash = calculateCashSize(param);
        dsc.write("invest.sum", cash);
        final long optNum = calculateOptionNumber(param);
        dsc.write("options.number", optNum);
        final double toDepo = calculateToDepo(cash, param);
        dsc.write("toDepo", toDepo);
        final double toDepoAvg = toDepoAvg(toDepo, cash);
        dsc.write("toDepoAvg", toDepoAvg);
        final double t2expfut = calculationBase == CalculationBase.FLEX ? 0 : calculateT2expfut(series.get(0));
        dsc.write("t2expfut", t2expfut);
        final double cantango = calculateCantango(param);
        dsc.write("cantango", cantango);
        final double optPrice = priceCalculationFunction.serieses(series).optionType(CALL).interval(BID).strike(param.getTotalAssetPriceWithStrike1Coef())
            .apply(param.getOptionPrice());
        final double optPriceInRub = toRub(optPrice, param);
        dsc.write("calculated.price", CALL, optPrice, optPriceInRub);
        final double pnlMoney = pnlMoney(cash, toDepoAvg, t2expfut, cantango, param);
        dsc.write("pnlMoney", pnlMoney);
        final double pnlOpt = calculatePnl(optPrice, assetPrice, param, param.getDaysCountToEndProduct());
        dsc.write("pnlOpt", pnlOpt);
        final double sumPnl = pnlMoney + pnlOpt;
        dsc.write("sumPnl", sumPnl);
        final double stockDepo = calculateStockDepo(sumPnl);
        final ResultBuilder builder = ResultBuilder.buildFromParams(param).desc(dsc).product(payload.getProduct()).sumToOptions(cash)
            .strike1(param.getTotalAssetPriceWithStrike1Coef()).couponSize(stockDepo).optionPrice(optPrice);
        return Collections.singletonList(getResultFactory().createPricingResult(builder));
    }

    private double calculateStockDepo(double sumPnl) {
        return toPercent(sumPnl * REWARD_CONST);
    }

    private Double calculateCashSize(StocksDepositSimpleParameters param) {
        return param.getStockNum() * param.getTotalAssetPriceWithStrike1Coef();
    }

    private Long calculateOptionNumber(StocksDepositSimpleParameters param) {
        return param.getStockNum(); // param.getAsset().getMultiplier();
    }

    private double calculateToDepo(double cash, StocksDepositSimpleParameters param) {
        //to_depo = (cash - cash * dscnt - cash * go_init) 
        return cash * (1 - (fromPercent(param.getRepoDiscount()) + GO_INIT));
    }

    private double toDepoAvg(double toDepo, double cash) {
        //to_depo_avg = to_depo * (1 - prob_ITM) + (to_depo - cash * go_init) * prob_ITM
        return toDepo * (1 - PROB_ITM) + (toDepo - cash * GO_INIT) * PROB_ITM;
    }

    private double pnlMoney(double cash, double toDepoAvg, double t2expfut, double cantango,
                            StocksDepositSimpleParameters param) {
        //pnl_money = (-repo * cash * (1 - dscnt) * t + to_depo_avg * rate_us * t - cash * contango * prob_ITM * t2expfut) / cash / t 
        double days = param.getDaysCountToEndProduct() / DAYS_PER_YEAR;
        return (- fromPercent(param.getRepoRate()) * cash * (1 - fromPercent(param.getRepoDiscount())) * days
            + toDepoAvg * fromPercent(param.getInterestRate()) * days - cash * fromPercent(cantango) * PROB_ITM * t2expfut) / cash / days;
    }

    private double calculateT2expfut(final Series series) {
        return countDaysBetween(series.getExpireDate(), series.getUnderlyingDate()) / DAYS_PER_YEAR;
    }

    private double calculateCantango(StocksDepositSimpleParameters param) {
        return approximationCalculating.getLastRateOnDate(param.getAsset(), param.getOptExpDate().toLocalDate());
    }


    private double calculatePnl(double optPrice, double assetPrice, StocksDepositSimpleParameters param, double timeToEndProduct) {
        //pnl_opt = (opt_price / spot_price / mult) / t  PNL от опционной позиции
        return (optPrice / (assetPrice * (getInstrumentType(param) == SPOT ? param.getAsset().getMultiplier() : 1))) / (timeToEndProduct / DAYS_PER_YEAR);
    }

}
