package ru.open.spc.calc.model;

import java.time.Duration;

public interface TimeoutHolder {

	default void setTimeoutHours(Long hours) {
		Duration oldTimeout = getTimeout();
		setTimeout(oldTimeout.minusHours(oldTimeout.toHours()).plusHours(hours));
	}

	default Long getTimeoutHours() {
		return getTimeout().toHours();
	}


	default void setTimeoutMinutes(Long minutes) {
		Duration oldTimeout = getTimeout();
		setTimeout(Duration.ofHours(oldTimeout.toHours()).plusMinutes(minutes));
	}

	default Long getTimeoutMinutes() {
		return getTimeout().toMinutes() % 60;
	}

	Duration getTimeout();

	void setTimeout(Duration timeout);
}
