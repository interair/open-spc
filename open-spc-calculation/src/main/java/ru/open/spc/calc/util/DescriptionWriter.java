package ru.open.spc.calc.util;

public interface DescriptionWriter {

	DescriptionWriter write(String string, Object... params);

	DescriptionWriter writeWithDelimiter(String string, String delim, Object... params);

	DescriptionWriter writeRaw(String string);

	String getResult();

	DescriptionWriter clone();

    String getString(String string);

    DescriptionWriter writeEmptyLine();
}