package ru.open.spc.calc.kernel.activators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import ru.open.spc.calc.kernel.resolvers.interval.IntervalResolverStrategyImpl;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.calc.model.Spread;
import ru.open.spc.calc.model.complex.exchange.wrappers.BasicProtectionCalcFlexParamWrapper;
import ru.open.spc.calc.model.complex.flex.PriceType;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.model.simple.exchange.BasicProtectionSimpleParameters;
import ru.open.spc.calc.model.simple.flex.BasicProtectionFlexSimpleParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.calc.util.Util;
import ru.open.spc.model.Series;
import ru.open.spc.model.Underlying;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.model.enums.OptionType;
import ru.open.spc.model.result.PricingResult;

import java.util.ArrayList;
import java.util.List;

import static ru.open.spc.calc.kernel.SPCalculationCommon.toRub;
import static ru.open.spc.calc.model.ResultBuilder.buildFromParamsBP;
import static ru.open.spc.calc.util.HeaderParams.*;
import static ru.open.spc.calc.util.MathUtil.fromPercent;
import static ru.open.spc.calc.util.MathUtil.toPercent;
import static ru.open.spc.calc.util.Util.checkForNotNull;
import static ru.open.spc.dao.util.RoundUtil.roundDn;

@MessageEndpoint
public class BasicProtectionCalculationFlexService extends AbstractCalculationService {

	private final static Logger LOG = LoggerFactory.getLogger(BasicProtectionCalculationService.class);
	private final static double SENSITIVITY = 0.1;

	@Autowired
	private IntervalResolverStrategyImpl intervalOptionsResolver;

	@SuppressWarnings("unchecked")
	@ServiceActivator(inputChannel = "baseCalculationChannel")
	public void calculate(final Message<CalculationParameters> message) {
		wrapCalculate(message);
	}

	@Override
	protected List<PricingResult> doCalculating(MessageHeaders headers, DescriptionWriter dsc,
	                                            CalculationParameters payload) throws Exception {
		final BasicProtectionFlexSimpleParameters param = (BasicProtectionFlexSimpleParameters) payload.getCalculationParams();
		final OptionType optionType = (OptionType) headers.get(OPTION_TYPE);
		final List<Series> series = (List<Series>) headers.get(SERIES);
		final List<Underlying> underlying = (List<Underlying>) headers.get(FUTURES);
		final Double totalAssetPriceWithStrike1Coef = param.getTotalAssetPriceWithStrike1Coef();

		final boolean priceSet = isPriceSet(param);

		final PriceCalculationFunction priceCalculationFunction = new PriceCalculationFunction(param, CalculationBase.FLEX).desc(dsc).underlyings(underlying);

		final long depoSum = calculateMinToDepo(param);
		dsc.write("depoSum", depoSum);

		final long sumToOptions = calculateSumToOptions(param.getInvestingSum(), depoSum);
		dsc.write("sum.to.options", sumToOptions);

		final double price = priceCalculationFunction.serieses(series).interval(Spread.ASK).optionType(optionType).conditions(priceSet)
				.strike(totalAssetPriceWithStrike1Coef).apply(param.getOptionPrice());

		dsc.write("calculated.price.flex", price, toRub(price, param));

		final List<ResultBuilder> prices = new ArrayList<>();
		prices.add(buildFromParamsBP(param).sumToOptions(sumToOptions).optionPrice(price)
				.desc(dsc).strike1(totalAssetPriceWithStrike1Coef).strike2(0d));
		if (param.getInterval()) {
			if (param instanceof BasicProtectionCalcFlexParamWrapper) {
				BasicProtectionCalcFlexParamWrapper complexParam = (BasicProtectionCalcFlexParamWrapper) param;
				final Double strike2From = complexParam.getParameter().getStrike2From();
				final Double strike2To = complexParam.getParameter().getStrike2To();
				final Double strike2Step = complexParam.getParameter().getStrike2Step();
				final List<Double> array = Util.getArray(strike2From, strike2To, strike2Step);

				for (Double strike2Percent : array) {

					DescriptionWriter dscClone = dsc.clone();

					double strike2 = totalAssetPriceWithStrike1Coef * (1 + optionType.code() * fromPercent(strike2Percent));
					if (param.getDirection() == Direction.RISING && (strike2 - totalAssetPriceWithStrike1Coef) > SENSITIVITY
							|| param.getDirection() == Direction.FALLING && (totalAssetPriceWithStrike1Coef - strike2) > SENSITIVITY) {

						dscClone.write("processing.interval.with.strike", strike2);
						double intervalPrice = priceCalculationFunction.interval(Spread.NONE)
								.optionType(optionType).desc(dscClone)
								.strike(strike2).conditions(false).isInterval(true)
								.apply(param.getIntervalOptionPrice());
						dscClone.write("interval.calculated.price", intervalPrice, toRub(intervalPrice, param));

						double iPrice = price - intervalPrice;
						prices.add(buildFromParamsBP(param)
								.sumToOptions(sumToOptions)
								.optionPrice(iPrice).desc(dscClone)
								.strike1(totalAssetPriceWithStrike1Coef)
								.strike2(strike2).intervalOptionPrice(intervalPrice));
					}
				}

			} else {

				DescriptionWriter dscClone = dsc.clone();

				double strike2;
				PriceType priceType = param.getPriceType();
				switch (priceType) {
					case VALUE:
						strike2 = param.getStrike2();
						break;
					default:
						strike2 = totalAssetPriceWithStrike1Coef * (1 + fromPercent(param.getStrike2()));
						break;
				}

				dscClone.write("processing.interval.with.strike", strike2);
				double intervalPrice = priceCalculationFunction.interval(Spread.NONE).optionType(optionType).desc(dscClone).strike(strike2).conditions(false).isInterval(true)
						.apply(param.getIntervalOptionPrice());
				dscClone.write("interval.calculated.price", intervalPrice, toRub(intervalPrice, param));

				double iPrice = price - intervalPrice;
				prices.add(buildFromParamsBP(param)
						.sumToOptions(sumToOptions)
						.optionPrice(iPrice)
						.desc(dscClone)
						.strike1(totalAssetPriceWithStrike1Coef)
						.strike2(strike2)
						.intervalOptionPrice(intervalPrice));

			}
		}

		final List<PricingResult> pricingResults = new ArrayList<>();
		for (ResultBuilder res : prices) {
			long n = calculateQuantity(toRub(res.getOptionPrice(), param), res.getSumToOptions());
			double coef = calculateCoeff(n, param);
			res.coefficient(coef).product(payload.getProduct());
			PricingResult pricingResult = getResultFactory().createPricingResult(res);
			pricingResults.add(pricingResult);
		}
		return pricingResults;
	}

	long calculateMinToDepo(BasicProtectionSimpleParameters param) {
		// min_to_depo = S * k_prot / (1 + R * T)
		final double daysCountBtwnStartProductEndProduct = param.getDaysCountToEndProduct();
		final double tBtwnStartProductStartEnd = daysCountBtwnStartProductEndProduct / DAYS_PER_YEAR;
		final double amount = param.getInvestingSum() * fromPercent(param.getCoeffAssetProtection()) /
				(1 + fromPercent(param.getInterestRate()) * tBtwnStartProductStartEnd);
		final long minToDepo = Math.round(amount);
		LOG.debug("MinToDepo = {}", minToDepo);
		return minToDepo;
	}

	long calculateSumToOptions(double investingSum, long depoSum) {
		//Sопц = (S - min_to_depo)
		long sumToOptions = Math.round(investingSum - depoSum);
		LOG.debug("sumToOptions = {}", sumToOptions);
		return sumToOptions;
	}

	protected boolean isPriceSet(BasicProtectionSimpleParameters param) {
		return !param.getInterval() || checkForNotNull(param.getOptionPrice());
	}

	long calculateQuantity(double price, double sumToOptions) {
		double n = sumToOptions / price;
		return roundDn(n);
	}

	double calculateCoeff(long n, SimpleCalculationParameters param) {
		double coeff = n / (param.getInvestingSum() / toRub(param.getTotalAssetPriceWithStrike1Coef(), param));
		LOG.debug("КУ = {}", coeff);
		return toPercent(coeff);
	}

	public IntervalResolverStrategyImpl getIntervalOptionsResolver() {
		return intervalOptionsResolver;
	}

}