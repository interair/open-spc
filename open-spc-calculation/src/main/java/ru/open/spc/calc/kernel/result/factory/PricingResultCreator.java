package ru.open.spc.calc.kernel.result.factory;

import org.springframework.beans.factory.annotation.Autowired;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.dao.manager.CurrencyManager;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.model.result.PricingResult;

public abstract class PricingResultCreator<T extends PricingResult> implements Creator<T> {

    @Autowired
    CurrencyManager currencyManager;

	protected T fill(ResultBuilder rb, T result) {
        if (rb.getId() != null) {
            result.setId(rb.getId());
        }
		result.setAsset(rb.getAsset());
		result.setProduct(rb.getProduct());
		result.setPriceDate(rb.getPriceDate());
        result.setProductStartDate(rb.getProductStartDate());
        result.setProductStopDate(rb.getProductStopDate());
        result.setStrikePrice(rb.getStrike1());
        result.setInvestingVolume(rb.getAsset().getMinimumPrice());
        result.setInterestRate(rb.getInterestRate());
        result.setDescription(rb.getDescResult());
        if (rb.getAssetUnity() != null) {
            result.setCurrency(currencyManager.getBy(rb.getAssetUnity().name()));
        } else {
            result.setCurrency(currencyManager.getBy(AssetUnity.RUB.name()));
        }
        result.setTimeout(rb.getTimeout());
        result.setAssetPrice(rb.getAssetPrice());
        result.setOptionPrice(rb.getOptionPrice());
        result.setPercentByLoanDeal(0d);
        result.setInterval(0);
        return result;
	}

    public CurrencyManager getCurrencyManager() {
        return currencyManager;
    }
}
