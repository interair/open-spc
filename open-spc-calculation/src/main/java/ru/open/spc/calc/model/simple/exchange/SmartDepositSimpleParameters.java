package ru.open.spc.calc.model.simple.exchange;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.calc.model.Orientation;

@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = false)
@Data public class SmartDepositSimpleParameters extends AssetProtectionSimpleParameters {

    private Orientation orientation;
    private Double strikeMin;
    private Double strikeMax;
    private Double putLeft;
    private Double putRight;
    private Double callRight;
    private Double callLeft;

    public SmartDepositSimpleParameters() {
    }

}
