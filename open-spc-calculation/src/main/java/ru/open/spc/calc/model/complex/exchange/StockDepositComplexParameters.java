package ru.open.spc.calc.model.complex.exchange;

import lombok.Data;
import ru.open.spc.calc.model.complex.ComplexCalculationParameters;

@Data public class StockDepositComplexParameters extends ComplexCalculationParameters {
    private Long stockNum;
    private Double repoDiscount;
    private Double repoRate;

}
