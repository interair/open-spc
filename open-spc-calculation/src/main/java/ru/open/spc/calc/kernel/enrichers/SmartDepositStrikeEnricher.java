package ru.open.spc.calc.kernel.enrichers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.Orientation;
import ru.open.spc.calc.model.simple.exchange.SmartDepositSimpleParameters;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.model.Option;
import ru.open.spc.model.enums.OptionType;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import static ru.open.spc.calc.util.HeaderParams.*;
import static ru.open.spc.model.enums.OptionType.CALL;
import static ru.open.spc.model.enums.OptionType.PUT;

@MessageEndpoint
public class SmartDepositStrikeEnricher {

    private static final Logger LOG = LoggerFactory.getLogger(SmartDepositStrikeEnricher.class);

    @Transformer(inputChannel = "resolveSmartDepoStrikesChannel", outputChannel = "filterSmartDepoStrikesChannel")
    public Message<CalculationParameters> enrichNearestStrikes(Message<CalculationParameters> message) {
        final SmartDepositSimpleParameters param = (SmartDepositSimpleParameters) message.getPayload().getCalculationParams();
        final List<Option> options = (List<Option>) message.getHeaders().get(OPTIONS);
        final Map<OptionType, Pair<Option, Option>> optionsMap = findOptions(param, options);
        return MessageBuilder.fromMessage(message)
            .setHeader(PUT_OPTIONS, optionsMap.get(PUT))
            .setHeader(CALL_OPTIONS, optionsMap.get(CALL))
            .build();
    }

    protected Map<OptionType, Pair<Option, Option>> findOptions(SmartDepositSimpleParameters param, List<Option> options) {
        Option putRight = null;
        Option callLeft = null;
        Option putLeft = null;
        Option callRight = null;
        final Pair<Option, Option> put = new Pair<>();
        final Pair<Option, Option> call = new Pair<>();
        final Map<OptionType, Pair<Option, Option>> optionMap = new EnumMap(OptionType.class) {{
            put(PUT, put);
            put(CALL, call);
        }};
        //SPL<SPR
        //SCR>SCL
        if (param.getOrientation() == Orientation.INNER) {

            final double strikePutRight = param.getStrikeMin();
            final double strikeCallLeft  = param.getStrikeMax();

            for (Option option : options) {
                final Double strikePrice = option.getStrikePrice();
                if (option.getOptionType() == PUT) {
                    if (strikePrice < strikePutRight) {
                        putLeft = option;
                    }
                    if (strikePrice == strikePutRight) {
                        putRight = option;
                    }
                } else {
                    if (strikePrice > strikeCallLeft && callRight == null) {
                        callRight = option;
                    }
                    if (strikePrice == strikeCallLeft) {
                        callLeft = option;
                    }
                }
            }
        } else {
            final double strikePutLeft = param.getStrikeMin();
            final double strikeCallRight  = param.getStrikeMax();

            for (Option option : options) {
                final Double strikePrice = option.getStrikePrice();
                if (option.getOptionType() == PUT) {
                    if (strikePrice > strikePutLeft && putRight == null) {
                        putRight = option;
                    }
                    if (strikePrice == strikePutLeft) {
                        putLeft = option;
                    }
                } else {
                    if (strikePrice < strikeCallRight) {
                        callLeft = option;
                    }
                    if (strikePrice == strikeCallRight) {
                        callRight = option;
                    }
                }
            }
        }

        put.setLeft(putLeft);
        put.setRight(putRight);

        call.setLeft(callLeft);
        call.setRight(callRight);
        return optionMap;
    }
}
