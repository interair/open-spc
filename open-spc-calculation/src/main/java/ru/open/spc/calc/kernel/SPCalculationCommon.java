package ru.open.spc.calc.kernel;

import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.model.ApproximatingRate;
import ru.open.spc.model.Dividend;
import ru.open.spc.model.Option;
import ru.open.spc.model.enums.AssetUnity;

import java.util.Collections;
import java.util.List;

import static ru.open.spc.calc.util.Util.removeNullElements;

public class SPCalculationCommon {

	public static void sortOptions(final List<Option> options) {
		removeNullElements(options);
		Collections.sort(options, (o1, o2) -> o1.getStrikePrice().compareTo(o2.getStrikePrice()));
	}

	public static List<Dividend> sortDividends(final List<Dividend> dividends, final boolean asc) {

		removeNullElements(dividends);
		Collections.sort(dividends, (o1, o2) -> (o1.getPayDate().compareTo(o2.getPayDate()) * (asc ? 1 : -1)));
		return dividends;
	}

	public static List<ApproximatingRate> sortApproximatingRatesDesc(final List<ApproximatingRate> rates) {
		return sortApproximatingRates(rates, false);
	}

	public static List<ApproximatingRate> sortApproximatingRatesAsc(final List<ApproximatingRate> rates) {
		return sortApproximatingRates(rates, true);
	}

	public static List<ApproximatingRate> sortApproximatingRates(final List<ApproximatingRate> rates, final boolean asc) {
		removeNullElements(rates);
		Collections.sort(rates, (o1, o2) -> (o1.getFromDate().compareTo(o2.getFromDate()) * (asc ? 1 : -1)));
		return rates;
	}

	public static Double toRub(Double price, SimpleCalculationParameters param) {
		Double priceInRub = price * param.getItemCost();
		if (param.getAssetUnity() == AssetUnity.USD) {
			priceInRub = priceInRub * param.getDollarExchangeRate();
		}
		return priceInRub;
	}

    public static List<Option> sortByStrike(final List<Option> options, final boolean asc) {
        removeNullElements(options);
        Collections.sort(options, (o1, o2) -> (o1.getStrikePrice().compareTo(o2.getStrikePrice()) * (asc ? 1 : -1)));
        return options;
    }
}
