package ru.open.spc.calc.kernel.result.factory;

import org.springframework.stereotype.Service;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.model.result.BAPricingResult;

@Service
public class BasicProtectionPricingResultCreator extends PricingResultCreator<BAPricingResult> {

	@Override
	public BAPricingResult createResult(ResultBuilder rb) {
		final BAPricingResult pricingResult = fill(rb, new BAPricingResult());
		pricingResult.setCoefficient(rb.getCoefficient());
		pricingResult.setKzk(rb.getCoeffAssetProtection());
		pricingResult.setDirection(rb.getDirection());
		pricingResult.setStrike2Price(rb.getStrike2());
        pricingResult.setIntervalOptionPrice(rb.getIntervalOptionPrice());
        if (rb.getVzkUnity() != null) {
            pricingResult.setVzkCurrency(getCurrencyManager().getBy(rb.getVzkUnity().name()));
        }
		return pricingResult;
	}

}
