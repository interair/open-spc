package ru.open.spc.calc.kernel.activators;

import org.springframework.messaging.Message;
import ru.open.spc.calc.model.CalculationParameters;

public interface CalculationService {

    void calculate(Message<CalculationParameters> message);

}
