package ru.open.spc.calc.kernel.splitters;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Splitter;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.util.UiCallback;

import java.util.List;

import static ru.open.spc.calc.util.HeaderParams.CALCULATION_BASE;
import static ru.open.spc.calc.util.HeaderParams.UI_CALLBACK;

//TODO: remove it bkz it's not used, clean from workflow before
@MessageEndpoint
public class FixedCouponParameterSplitter extends ComplexParametersSplitter  {

    @Splitter(inputChannel = "splitFixedCouponComplexChannel", outputChannel = "prepareSimpleCalculationChannel")
    public List<CalculationParameters> processMessage(final CalculationParameters param,
                                                      @Header(UI_CALLBACK) UiCallback callback,
                                                      @Header(CALCULATION_BASE) CalculationBase calculationBase) {
        return super.processMessage(param, callback, calculationBase);
    }

}
