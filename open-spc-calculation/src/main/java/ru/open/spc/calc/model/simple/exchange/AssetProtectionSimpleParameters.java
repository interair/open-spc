package ru.open.spc.calc.model.simple.exchange;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;

@EqualsAndHashCode(callSuper = false)
@Data public class AssetProtectionSimpleParameters extends SimpleCalculationParameters {

	private Double strike2;
	private Double coeffAssetProtection;

}
