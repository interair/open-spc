package ru.open.spc.calc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.CalculationType;
import ru.open.spc.model.enums.ProductCode;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDate;

@SuppressWarnings("serial")
@EqualsAndHashCode(of = {"productEndDate", "investingSum"})
@Data
public abstract class BaseParameters implements Serializable, TimeoutHolder {

	@JsonIgnore
	private LocalDate depositStartDate = LocalDate.now();
	private LocalDate productEndDate;
	private Duration timeout = Duration.ofSeconds(0);
	private Double investingSum;

	public Boolean getVariability() {
		return false;
	}

	public void clean(final ParametersRepository parametersRepository,
	                  final ProductCode productCode,
	                  final CalculationType calculationType) {
		cleanAssetFields();
		setDepositStartDate(LocalDate.now());
		parametersRepository.setInitConfig(this, productCode, calculationType);
	}

	public void saveConfigParams(final ParametersRepository parametersRepository,
	                             final ProductCode productCode,
	                             final CalculationType calculationType) {
		parametersRepository.updateConfigParams(this, productCode, calculationType);
	}

	public void updateConfigParams(final ParametersRepository parametersRepository,
	                               final ProductCode productCode,
	                               final CalculationType calculationType) {
		parametersRepository.config(this, productCode, calculationType);
	}

	public void cleanAssetFields() {
		setInvestingSum(0d);
		setTimeout(Duration.ofSeconds(0));
	}

}