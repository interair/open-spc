package ru.open.spc.calc.kernel.splitters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.kernel.resolvers.SpreadResolver;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.complex.AbstractComplexCalculationParameters;
import ru.open.spc.calc.model.complex.ComplexParamContext;
import ru.open.spc.calc.model.complex.exchange.ComplexParametersFactory;
import ru.open.spc.calc.model.complex.flex.DateStepHolder;
import ru.open.spc.calc.util.Function;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.dao.util.DateUtil;
import ru.open.spc.model.Asset;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.Month.*;
import static ru.open.spc.calc.util.HeaderParams.CALCULATION_BASE;
import static ru.open.spc.calc.util.HeaderParams.UI_CALLBACK;
import static ru.open.spc.calc.util.Util.getArray;
import static ru.open.spc.dao.util.DateUtil.getNextDate;
import static ru.open.spc.dao.util.DateUtil.getPreviousDate;

public abstract class ComplexParametersSplitter {

	@Autowired private CalculationManager calculationManager;
	@Autowired private SpreadResolver spreadResolver;
	@Autowired private ComplexParametersFactory parametersFactory;

    private final DateSeriesFilter dateFilter = new DateSeriesFilter();

    protected CalculationManager getCalculationManager() {
        return calculationManager;
    }

	public List<CalculationParameters> processMessage(final CalculationParameters param,
                                                      @SuppressWarnings("rawtypes") @Header(UI_CALLBACK) UiCallback callback,
                                                      @Header(CALCULATION_BASE) CalculationBase calculationBase) {
        final AbstractComplexCalculationParameters calculationParams = (AbstractComplexCalculationParameters) param.getCalculationParams();
        final List<CalculationParameters> cps = new ArrayList<>();
        List<LocalDate> dates = resolveDates(calculationBase, calculationParams);
        final List<Double> rates = getArray(calculationParams.getInterestRateFrom(),
            calculationParams.getInterestRateTo(), calculationParams.getInterestRateStep());
		for (LocalDate date : dates) {
            for (Double rate : rates) {
                final Double spread = calculateSpread(date, calculationParams.getAsset());
                final ComplexParamContext context = new ComplexParamContext.Builder()
                        .optExpDate(DateUtil.setHours(date, 18, 45))
                        .productEndDate(getNextDate(date))
                        .interestRate(rate)
                        .volatilitySpread(spread)
                        .calculationBase(calculationBase)
                        .create();
                cps.add(createSimpleCalculation(param, context));
            }
		}
        callback.setTotalStepsAmount(cps.size());
		return cps;
	}

    protected List<LocalDate> resolveDates(CalculationBase calculationBase, AbstractComplexCalculationParameters calculationParams) {
        switch (calculationBase) {
            case FLEX:
                DateStepHolder flexParameters = (DateStepHolder) calculationParams;
                final Duration dateStepDuration = flexParameters.getDateStepDuration();
                return findAllAvailableFlexSeries(calculationParams, dateStepDuration);
            default:
                return findAllAvailableSeries(calculationParams);
        }
    }

    protected Double calculateSpread(final LocalDate date, final Asset asset) {
		return spreadResolver.getSpread(date, asset.getSpreadVolatility());
	}

    protected List<LocalDate> findAllAvailableFlexSeries(AbstractComplexCalculationParameters param, Duration dateStepDuration) {
        LocalDate startDate = param.getDepositStartDate();
        if (param.getProductStartDate() != null) {
            startDate = param.getProductStartDate();
        }
        List<LocalDate> dates = new ArrayList<>();
        dates.add(startDate);
        while ((startDate = DateUtil.getDateWithShift(startDate, dateStepDuration.toDays(), true)).isBefore(param.getProductEndDate())) {
            dates.add(startDate);
        }
        return dates;
    }

	protected List<LocalDate> findAllAvailableSeries(final AbstractComplexCalculationParameters param) {
        LocalDate startDate = param.getDepositStartDate();
		if (param.getProductStartDate() != null) {
			startDate = param.getProductStartDate();
		}
		return calculationManager.getAvailableSeriesDates(startDate,
            getPreviousDate(param.getProductEndDate()), param.getAsset().getOuterId());
	}

	protected List<LocalDate> filterDates(final List<LocalDate> dates) {
        return dates.stream().filter(dateFilter::apply).collect(Collectors.toList());
	}

	protected CalculationParameters createSimpleCalculation(final CalculationParameters param,
                                                         final ComplexParamContext context) {
		return parametersFactory.create(param, context);
	}

    private static class DateSeriesFilter implements Function<LocalDate, Boolean> {

        private final EnumSet<Month> MONTHS = EnumSet.of(JANUARY, MARCH, JUNE, SEPTEMBER, DECEMBER);

        @Override
        public Boolean apply(LocalDate date) {
            return MONTHS.contains(date.getMonth());
        }
    }

}
