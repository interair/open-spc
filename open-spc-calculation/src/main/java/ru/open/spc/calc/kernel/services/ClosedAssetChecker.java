package ru.open.spc.calc.kernel.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.mapper.ClosedPriceMapper;
import ru.open.spc.model.Asset;

import java.time.LocalDate;
import java.util.List;

@Service
public class ClosedAssetChecker {

    private final static Logger LOG = LoggerFactory.getLogger(DateApproximationChecker.class);

    @Autowired
    private AssetManager service;

    @Autowired
    private ClosedPriceMapper closedAssetChecker;

    @Scheduled(cron = "0 0 0 * * MON-FRI")
    public void check() {
        LOG.info("check started");
        final List<Asset> all = service.all();
        LOG.info("need to update {} assets", all.size());
        all.forEach(this::checkAndSet);
    }

    private void checkAndSet(Asset asset){
        closedAssetChecker.insert(asset.getId(),asset.getAssetPrice().getRealPrice(), LocalDate.now());
    }

}
