package ru.open.spc.calc.model.complex;

public interface ParamWrapper {
    AbstractComplexCalculationParameters getParameter();
    ComplexParamContext getContext();

    void setParameter(AbstractComplexCalculationParameters parameter);
    void setContext(ComplexParamContext context);
}
