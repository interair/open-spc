package ru.open.spc.calc.model.simple.flex;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.calc.model.complex.flex.PriceType;
import ru.open.spc.calc.model.simple.exchange.FixedCouponSimpleParameters;

@EqualsAndHashCode(callSuper = false)
@Data public class FixedCouponSimpleFlexParameters extends FixedCouponSimpleParameters implements IVParameters, PriceTypeParameters {

    private Double optionIV;
    private PriceType priceType;

}
