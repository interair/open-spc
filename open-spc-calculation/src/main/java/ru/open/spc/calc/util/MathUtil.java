package ru.open.spc.calc.util;

public class MathUtil {

    public static final Double HUNDRED = 100d;

    public static double toPercent(double value) {
        return value * HUNDRED;
    }

    public static double fromPercent(double value) {
        return value / HUNDRED;
    }

    public static double normcdf(double z) {

        double az, t, d, p, result;

        if (z <= -10) {
            result = 0d;
        } else {
            az = Math.abs(z);
            t = 1d / (1 + 0.2316419 * az);
            d = 0.3989423 * Math.exp(-0.5 * z * z);
            p = d * t * ((((1.330274 * t - 1.821256) * t + 1.781478) * t - 0.3565638) * t + 0.3193815);
            if (z > 0) {
                p = 1 - p;
            }
            result = p;
        }
        return result;
    }

    public static double findAsk(double d, double spread) {
        return d * (1 + fromPercent(spread));
    }

    public static double findBid(double d, double spread) {
        return d * (1 - fromPercent(spread));
    }

    public static boolean isBetween(double test, double a, double b) {
        return b > a ? test > a && test < b : test > b && test < a;
    }

}
