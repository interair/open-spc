package ru.open.spc.calc.model.complex.flex;

import ru.open.spc.model.enums.StringCodeAware;

public enum PriceType implements StringCodeAware {
	PERCENT("PERCENT"), VALUE("VALUE");

	private final String code;

	PriceType(String code) {
		this.code = code;
	}

	@Override
	public String code() {
		return code;
	}
}
