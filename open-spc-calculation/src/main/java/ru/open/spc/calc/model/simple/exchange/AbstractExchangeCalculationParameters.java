package ru.open.spc.calc.model.simple.exchange;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.calc.model.AbstractCalculationParameters;

@EqualsAndHashCode(callSuper = true)
@Data public class AbstractExchangeCalculationParameters extends AbstractCalculationParameters {

    private Double strikeStep;

    public void cleanAssetFields() {
        super.cleanAssetFields();
        setStrikeStep(0d);
    }
}
