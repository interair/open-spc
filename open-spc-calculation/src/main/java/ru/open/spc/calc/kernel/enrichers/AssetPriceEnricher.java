package ru.open.spc.calc.kernel.enrichers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.handler.annotation.Header;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.model.Underlying;
import ru.open.spc.model.enums.InstrumentType;

import static ru.open.spc.calc.util.HeaderParams.CALCULATION_BASE;
import static ru.open.spc.calc.util.HeaderParams.UI_DESCRIPTION;

@MessageEndpoint
public class AssetPriceEnricher {

    @Autowired private CalculationManager calculationManager;

    @Transformer(inputChannel = "processCalculationChannel", outputChannel = "calculationTypeRouterChannel")
    public CalculationParameters enrichAssetPrice(final CalculationParameters param,
                                                  @Header(UI_DESCRIPTION) DescriptionWriter desc,
                                                  @Header(CALCULATION_BASE) CalculationBase calculationBase) {
        final SimpleCalculationParameters calculationParams = (SimpleCalculationParameters) param.getCalculationParams();
        enrich(calculationParams, desc, calculationBase);
        return param;
    }

    private void enrich(SimpleCalculationParameters param, DescriptionWriter desc, CalculationBase calculationBase) {
        desc.write("asset.price.src", param.getUnderlyingAssetPrice());
        switch (calculationBase) {
            case FLEX:
                switch (getInstrumentType(param)) {
                    case SPOT:
                        param.setTotalAssetPrice(param.getUnderlyingAssetPrice());
                        break;
                    case FUTURES:
                        param.setTotalAssetPrice(param.getUnderlyingAssetPrice() / param.getUnderlyingAssetMultiplicator());
                        break;
                }
                break;
            case EXCHANGE:
                switch (getInstrumentType(param)) {
                    case SPOT:
                        calculateSpotPrice(param);
                        break;
                    case FUTURES:
                        calculateFuturesPrice(param);
                        break;
                }
        }
    }

    public static InstrumentType getInstrumentType(SimpleCalculationParameters param) {
        return param.getAsset().getAssetPrice().getInstrumentType();
    }

    private void calculateSpotPrice(SimpleCalculationParameters param) {
        param.setTotalAssetPrice(param.getUnderlyingAssetMultiplicator() * param.getUnderlyingAssetPrice());
    }

    private void calculateFuturesPrice(SimpleCalculationParameters param) {
        if (param.getAsset().getBasePriceVariative()) {
            final Underlying futurePriceBy = calculationManager.getFuturesBy(param.getAsset().getOuterId(), param.getOptExpDate().toLocalDate());
            param.setTotalAssetPrice(futurePriceBy != null ? futurePriceBy.getRealPrice() : 0d);
        } else {
            param.setTotalAssetPrice(param.getUnderlyingAssetPrice());
        }
    }
}
