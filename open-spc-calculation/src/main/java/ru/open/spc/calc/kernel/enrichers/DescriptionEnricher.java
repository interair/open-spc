package ru.open.spc.calc.kernel.enrichers;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.util.DescriptionWriter;

import static ru.open.spc.calc.util.HeaderParams.UI_DESCRIPTION;

@MessageEndpoint
public class DescriptionEnricher {

    @Autowired private ObjectFactory<DescriptionWriter> dscPrototype;

    @Transformer(inputChannel = "prepareSimpleCalculationChannel", outputChannel = "autoCallRouterChannel")
    public Message<CalculationParameters> enrichDescriptionWriter(Message<CalculationParameters> message) {
        DescriptionWriter descriptionWriter = dscPrototype.getObject();
        return MessageBuilder.fromMessage(message)
            .setHeader(UI_DESCRIPTION, descriptionWriter).build();
    }
}
