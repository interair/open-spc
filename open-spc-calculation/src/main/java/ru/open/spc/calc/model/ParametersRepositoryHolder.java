package ru.open.spc.calc.model;

import org.springframework.beans.factory.annotation.Required;

import java.util.Map;

public class ParametersRepositoryHolder {

	private Map<CalculationBase, ParametersRepository> repositoryMap;

	public ParametersRepository getParametersRepository(CalculationBase id) {
		return repositoryMap.get(id);
	}

	@Required
	public void setRepositoryMap(Map<CalculationBase, ParametersRepository> repositoryMap) {
		this.repositoryMap = repositoryMap;
	}
}
