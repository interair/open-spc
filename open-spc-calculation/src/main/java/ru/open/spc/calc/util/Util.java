package ru.open.spc.calc.util;

import ru.open.spc.model.common.Identifiable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Util {

    public static boolean checkForNotNull(Double ... params) {
        boolean result = true;
        for (Double param : params) {
            result = result && (param != null && !param.equals(0d));
        }
        return result;
    }

    public static <T extends Comparable<? super T>> void sort(List<T> list) {
        removeNullElements(list);
        Collections.sort(list);
    }

    public static Pair<List<LocalDate>, List<LocalDate>> splitDates(List<LocalDate> dates, LocalDate fromDate) {
        if (fromDate == null) {
            throw new IllegalArgumentException("split date is null");
        }
        removeNullElements(dates);
        Collections.sort(dates);
        int binarySearch = Collections.binarySearch(dates, fromDate);
        return new Pair<>(new ArrayList<>(dates.subList(0, binarySearch + 1)),
            new ArrayList<>(dates.subList(binarySearch, dates.size())));
    }

    public static <T> List<T> merge(List<T> list, T object) {
        final int indexOf = list.indexOf(object);
        if (indexOf > 0) {
            list.set(indexOf, object);
        } else {
            list.add(object);
        }
        return list;
    }

    public static <T extends Identifiable> T setTempId(T object) {
        object.setId(System.nanoTime() - Long.MAX_VALUE);
        return object;
    }

    public static void removeNullElements(List<?> list) {
        list.removeAll(Collections.singleton(null));
    }

    public static List<Double> getArray(double from, double to, double step) {
        List<Double> list = new ArrayList<>();
        for (double i = from; i <= to; i += step) {
            list.add(i);
        }
        return list;
    }
}
