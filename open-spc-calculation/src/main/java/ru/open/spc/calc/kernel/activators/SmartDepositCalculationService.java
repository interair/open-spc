package ru.open.spc.calc.kernel.activators;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import ru.open.spc.calc.model.*;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.model.simple.exchange.SmartDepositSimpleParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.model.Option;
import ru.open.spc.model.Underlying;
import ru.open.spc.model.result.PricingResult;

import java.util.List;

import static java.util.Collections.singletonList;
import static ru.open.spc.calc.kernel.SPCalculationCommon.toRub;
import static ru.open.spc.calc.model.Orientation.INNER;
import static ru.open.spc.calc.model.Spread.*;
import static ru.open.spc.calc.util.HeaderParams.*;
import static ru.open.spc.calc.util.MathUtil.fromPercent;
import static ru.open.spc.calc.util.MathUtil.toPercent;
import static ru.open.spc.dao.util.RoundUtil.roundDn;

@MessageEndpoint
public class SmartDepositCalculationService extends AbstractCalculationService {

    @SuppressWarnings("unchecked")
    @ServiceActivator(inputChannel = "smartDepositChannel")
    public void calculate(final Message<CalculationParameters> message) {
        wrapCalculate(message);
    }

    @Override
    protected List<PricingResult> doCalculating(MessageHeaders headers, DescriptionWriter dsc,
                                                CalculationParameters payload) throws Exception {

        dsc = dsc.clone();
        final SmartDepositSimpleParameters param = (SmartDepositSimpleParameters) payload.getCalculationParams();
        final Pair<Option, Option> putOptions = (Pair<Option, Option>) headers.get(PUT_OPTIONS);
        final Pair<Option, Option> callOptions = (Pair<Option, Option>) headers.get(CALL_OPTIONS);
        final CalculationBase calculationBase = (CalculationBase) headers.get(CALCULATION_BASE);
        final List<Underlying> underlying = (List<Underlying>) headers.get(FUTURES);


        dsc.write("orientation.calculation", dsc.getString("orientation." + param.getOrientation().code()));
        final Spread spread = param.getOrientation() == INNER ? BID : ASK;
        final Option putLeft = putOptions.getLeft();
        final Option putRight = putOptions.getRight();
        final Option callLeft = callOptions.getLeft();
        final Option callRight = callOptions.getRight();
        dsc.write("left.put.option", putLeft.getName(), correctPrice(param, dsc, putLeft))
            .write("right.put.option", putRight.getName(), correctPrice(param, dsc, putRight))
            .write("left.call.option", callLeft.getName(), correctPrice(param, dsc, callLeft))
            .write("right.call.option", callRight.getName(), correctPrice(param, dsc, callRight));

        dsc.write("processing.left.put");
        final double strike1 = param.getTotalAssetPriceWithStrike1Coef();
        final PriceCalculationFunction priceCalculationFunction = new PriceCalculationFunction(param, calculationBase)
                .desc(dsc).strike(strike1).conditions(true).underlyings(underlying);
        final double leftPutPrice = priceCalculationFunction.serieses(singletonList(putLeft)).interval(NONE).optionType(putLeft.getOptionType()).apply(param.getPutLeft());
        dsc.write("left.put.calculated.price", leftPutPrice, toRub(leftPutPrice, param));

        dsc.write("processing.right.put");
        final double rightPutPrice = priceCalculationFunction.serieses(singletonList(putRight)).optionType(putRight.getOptionType()).interval(spread)
            .apply(param.getPutRight());
        dsc.write("right.put.calculated.price", rightPutPrice, toRub(rightPutPrice, param));

        dsc.write("processing.left.call");
        final double leftCallPrice = priceCalculationFunction.serieses(singletonList(callLeft)).interval(spread).optionType(callLeft.getOptionType()).apply(param.getCallLeft());
        dsc.write("left.call.calculated.price", leftCallPrice, toRub(leftCallPrice, param));

        dsc.write("processing.right.call");
        final double rightCallPrice = priceCalculationFunction.serieses(singletonList(callRight)).interval(NONE).optionType(callRight.getOptionType())
            .apply(param.getCallRight());
        dsc.write("right.call.calculated.price", rightCallPrice, toRub(rightCallPrice, param));

        final double days = param.getDaysCountToEndProduct() / DAYS_PER_YEAR;
        final double rate = (1 + fromPercent(param.getInterestRate()) * days);

        final double condor = calculateCondor(leftPutPrice, rightPutPrice, leftCallPrice, rightCallPrice, param);
        dsc.write("condor", condor);
        final double maxLoss = calculateMaxLoss(putRight.getStrikePrice(), putLeft.getStrikePrice(), param);
        dsc.write("maxLoss", maxLoss);
        final double cashForCon = calculateCashForCon(param, rate);
        dsc.write("cash4con", cashForCon);
        final long quantity = calculateQuantity(condor, maxLoss, cashForCon, param.getOrientation());
        dsc.write("quantity.positions", quantity);
        final double finResInCurrency = calculateFinResInCurrency(maxLoss, condor, quantity, rate, param);
        dsc.write("finResInCurrency", finResInCurrency);
        final double finRes = calculateFinRes(finResInCurrency, param);
        final ResultBuilder builder = ResultBuilder.buildFromParamsSM(param)
                .desc(dsc).product(payload.getProduct())
                .strike1(param.getStrikeMin()).strike2(param.getStrikeMax())
                .maximumEarning(toPercent(finRes));
        return singletonList(getResultFactory().createPricingResult(builder));
    }

    private double calculateFinRes(double finResInCurrency, SmartDepositSimpleParameters param) {
        final double days = param.getDaysCountToEndProduct() / DAYS_PER_YEAR;
        return (finResInCurrency / param.getInvestingSum() - 1) / days;
    }

    private double calculateFinResInCurrency(double maxLoss, double condor, long quantity, double rate, SmartDepositSimpleParameters param) {
        final double investingSum = param.getInvestingSum();
        if (param.getOrientation() == INNER) {
            return (investingSum - quantity * (maxLoss - condor)) * rate + maxLoss * quantity;
        } else {
            return (investingSum - quantity * condor) * rate + maxLoss * quantity;
        }
    }

    private long calculateQuantity(double condor, double maxLoss, double cashForCon, Orientation orientation) {
        if (orientation == INNER) {
            return roundDn(cashForCon / (maxLoss - condor));
        } else {
            return roundDn(cashForCon / condor);
        }
    }

    private double calculateCashForCon(SmartDepositSimpleParameters param, double rate) {
        final double investingSum = param.getInvestingSum();
        return investingSum * (1 - fromPercent(param.getCoeffAssetProtection()) / rate);
    }

    private double calculateCondor(double leftPutPrice, double rightPutPrice, double leftCallPrice, double rightCallPrice,
                                   SmartDepositSimpleParameters param) {
            return toRub(rightPutPrice + leftCallPrice - leftPutPrice - rightCallPrice, param);
    }

    private double calculateMaxLoss(double rightPutPrice, double leftPutPrice, SimpleCalculationParameters param) {
        return toRub(rightPutPrice - leftPutPrice, param);
    }

}
