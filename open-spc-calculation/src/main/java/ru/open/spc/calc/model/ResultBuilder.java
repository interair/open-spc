package ru.open.spc.calc.model;

import lombok.Getter;
import lombok.Setter;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.model.simple.exchange.BasicProtectionSimpleParameters;
import ru.open.spc.calc.model.simple.exchange.SmartDepositSimpleParameters;
import ru.open.spc.calc.model.simple.exchange.StocksDepositSimpleParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Currency;
import ru.open.spc.model.Product;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.model.enums.Direction;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static ru.open.spc.calc.model.Orientation.INNER;
import static ru.open.spc.model.enums.Direction.FALLING;
import static ru.open.spc.model.enums.Direction.RISING;

@Getter @Setter
public class ResultBuilder implements TimeoutHolder, Serializable {

    public final static Currency rubCurrency = new Currency() {{
        setId(3l);
        setCode("RUB");
    }};

    private Long id;
	private Product product;
	private Double sumToOptions;
	private Double n1n2relation;
	private Pair<Double, Double> price;
    private Double optionPrice;
    private Double intervalOptionPrice;

	private DescriptionWriter desc;
	private Double strike1;
	private Double strike2;
	private Pair<Long, Long> n1n2;
	private Double coefficient;
	private Double maximumEarning;
    private Double couponSize;
    private Direction direction;
    private Double coeffAssetProtection;
    private Duration timeout = Duration.ofSeconds(0);
    private Asset asset;
    private LocalDate productStartDate;
    private LocalDate productStopDate;
    private Double interestRate;
    private LocalDateTime priceDate = LocalDateTime.now();
    private String descResult;
    private Long stockNum;
    private Double assetPrice;
    private AssetUnity vzkUnity;
    private AssetUnity assetUnity;

    public ResultBuilder() {
	}

    public static ResultBuilder buildFromParams(SimpleCalculationParameters parameters) {
        return new ResultBuilder()
                .assetUnity(parameters.getAssetUnity())
                .productStartDate(parameters.getDepositStartDate())
                .productStopDate(parameters.getProductEndDate())
                .asset(parameters.getAsset())
                .interestRate(parameters.getInterestRate())
                .timeout(parameters.getTimeout());
    }

    public static ResultBuilder buildFromParamsBP(BasicProtectionSimpleParameters parameters) {
        return buildFromParams(parameters)
                .vkzUnity(parameters.getVzkUnity())
                .coeffAssetProtection(parameters.getCoeffAssetProtection())
                .direction(parameters.getDirection());
    }

    public static ResultBuilder buildFromParamsSM(SmartDepositSimpleParameters parameters) {
        return buildFromParams(parameters)
                .coeffAssetProtection(parameters.getCoeffAssetProtection())
                .direction(parameters.getOrientation() == INNER ? FALLING : RISING);
    }

    public static ResultBuilder buildFromParamsSD(StocksDepositSimpleParameters parameters) {
        return buildFromParams(parameters)
                .stockNum(parameters.getStockNum());
    }

    public ResultBuilder product(Product product) {
		this.product = product;
		return this;
	}

	public ResultBuilder sumToOptions(Long sumToOptions) {
        if (sumToOptions == null) return this;
        this.sumToOptions = sumToOptions.doubleValue();
		return this;
	}

    public ResultBuilder sumToOptions(Double sumToOptions) {
        this.sumToOptions = sumToOptions;
        return this;
    }

	public ResultBuilder n1n2relation(Double n1n2relation) {
		this.n1n2relation = n1n2relation;
		return this;
	}

	public ResultBuilder price(Pair<Double, Double> price) {
		this.price = price;
		return this;
	}

    public ResultBuilder optionPrice(Double optionPrice) {
        this.optionPrice = optionPrice;
        return this;
    }

    public ResultBuilder intervalOptionPrice(Double intervalOptionPrice) {
        this.intervalOptionPrice = intervalOptionPrice;
        return this;
    }

	public ResultBuilder desc(DescriptionWriter desc) {
		this.desc = desc;
		return this;
	}

	public ResultBuilder strike1(Double strike1) {
		this.strike1 = strike1;
		return this;
	}

	public ResultBuilder strike2(Double strike2) {
		this.strike2 = strike2;
		return this;
	}

	public ResultBuilder n1n2(Pair<Long, Long> n1n2) {
		this.n1n2 = n1n2;
		return this;
	}

	public ResultBuilder coefficient(Double coefficient) {
		this.coefficient = coefficient;
		return this;
	}

	public ResultBuilder maximumEarning(Double totalRetailReward) {
		this.maximumEarning = totalRetailReward;
		return this;
	}

    public ResultBuilder vkzUnity(AssetUnity vzkUnity) {
        this.vzkUnity = vzkUnity;
        return this;
    }

    public Double getCouponSize() {
        return couponSize;
    }

    public ResultBuilder couponSize(Double couponSize) {
        this.couponSize = couponSize;
        return this;
    }

    public ResultBuilder direction(final Direction direction) {
        this.direction = direction;
        return this;
    }

    public ResultBuilder coeffAssetProtection(final Double coeffAssetProtection) {
        this.coeffAssetProtection = coeffAssetProtection;
        return this;
    }

    public ResultBuilder timeout(Duration timeout) {
        this.timeout = timeout;
        return this;
    }

    public ResultBuilder asset(Asset asset) {
        this.asset = asset;
        return this;
    }


    public ResultBuilder productStartDate(LocalDate depositStartDate) {
        this.productStartDate = depositStartDate;
        return this;
    }

    public ResultBuilder productStopDate(LocalDate productEndDate) {
        this.productStopDate = productEndDate;
        return this;
    }

    public ResultBuilder interestRate(Double interestRate) {
        this.interestRate = interestRate;
        return this;
    }

    public ResultBuilder priceDate(LocalDateTime priceDate) {
        this.priceDate = priceDate;
        return this;
    }

    public ResultBuilder descResult(String descResult) {
        this.descResult = descResult;
        return this;
    }

    public ResultBuilder stockNum(Long stockNum) {
        this.stockNum = stockNum;
        return this;
    }

    public ResultBuilder assetPrice(Double assetPrice) {
        this.assetPrice = assetPrice;
        return this;
    }

    public ResultBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public ResultBuilder assetUnity(AssetUnity assetUnity) {
        this.assetUnity = assetUnity;
        return this;
    }

    public Double getAssetPrice() {
        if (assetPrice != null)  return assetPrice;
        if (getAsset() == null)  return 0d;
        return getAsset().getAssetPrice().getRealPrice();
    }

    public Long getId() {
        return id;
    }

    public AssetUnity getCurrency() {
        return AssetUnity.RUB;
    }

    public String getDescResult() {
        if (desc != null) {
            return desc.getResult();
        }
        return descResult;
    }

}
