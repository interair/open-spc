package ru.open.spc.calc.kernel.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.calc.model.ApproximatedPrice;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.dao.util.DateUtil;
import ru.open.spc.model.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static ru.open.spc.calc.kernel.SPCalculationCommon.*;
import static ru.open.spc.calc.util.MathUtil.fromPercent;
import static ru.open.spc.dao.util.DateUtil.*;

@Service
public class ApproximationCalculationService {

	private final static Logger LOG = LoggerFactory.getLogger(ApproximationCalculationService.class);

	@Autowired private CalculationManager calculationManager;

	public double applyNotLiquidPriceCorrection(SimpleCalculationParameters param, DescriptionWriter desc, Underlying underlying) {
		return applyNotLiquidPriceCorrection(param, desc, underlying, underlying.getUnderlyingDate());
	}

	public double applyNotLiquidPriceCorrection(SimpleCalculationParameters param, DescriptionWriter desc, Underlying underlying, LocalDate calcDate) {
		if (checkApproximation(param)) {
			final Asset asset = param.getAsset();
			LOG.debug("Approximation needed");
			Underlying price = calculationManager.getFuturesBy(asset.getOuterId(), asset.getCoefficients().getFromDate());
			if (price == null) {
				LOG.warn("price is null for asset {}, date {}", asset, asset.getCoefficients().getFromDate());
				return underlying.getRealPrice();
			}
			ApproximatedPrice appr = applyNotLiquidApproximation(asset, calcDate, price.getRealPrice());
			writeCorrectionLog(appr, desc);
			return appr.getTotal();
		}
		return underlying.getRealPrice();
	}

	public ApproximatedPrice applyNotLiquidApproximation(Asset asset, LocalDate calculatedDate, double price) {

		AssetCoefficient coefficient = asset.getCoefficients();
		if (calculatedDate.isBefore(coefficient.getFromDate())) {
			return new ApproximatedPrice(price, 0, 0);
		} else {
			return calculateAfterLastLiquidFutures(asset, calculatedDate, price, coefficient);
		}
	}

	public ApproximatedPrice applyBAApproximation(Asset asset, LocalDate currentDate, double price, CalculationBase calculationBase) {
		AssetCoefficient coefficient = asset.getCoefficients();
		List<ApproximatingRate> approximatingRates = getAppropriateApproximatingRates(currentDate,
				asset.getAssetPrice().getUnderlyingDate(), coefficient.getApproximatingRates());
		double a = price;
		double rateSum = 0d;
		sortApproximatingRatesDesc(approximatingRates);
		for (int i = 0; i < approximatingRates.size(); i++) {
			ApproximatingRate rate = approximatingRates.get(i);
			LocalDate date = currentDate;
			if (i + 1 < approximatingRates.size()) {
				date = approximatingRates.get(i + 1).getFromDate();
			}
			double calculateRateValue = calculateRateValue(a, rate, date);
			a -= calculateRateValue;
			rateSum -= calculateRateValue;
		}
		final Integer multiplier = calculationBase == CalculationBase.FLEX ? 1 : asset.getMultiplier();
		double b = calculateDividends(coefficient.getDividends(), getNextDate(currentDate), asset.getAssetPrice().getUnderlyingDate(), multiplier, false);
		return new ApproximatedPrice(a, -b, rateSum);
	}

	public double getLastRateOnDate(Asset asset, LocalDate date) {
		final List<ApproximatingRate> sourceRates = asset.getCoefficients().getApproximatingRates();
		List<ApproximatingRate> approximatingRates = getAppropriateApproximatingRates(LocalDate.now(), date, sourceRates);
		sortApproximatingRatesDesc(approximatingRates);
		if (approximatingRates.isEmpty()) {
			sortApproximatingRatesAsc(sourceRates);
			return sourceRates.isEmpty() ? 0d : sourceRates.get(0).getRate();
		} else {
			return approximatingRates.get(0).getRate();
		}
	}

	public double calculateLiquidFeatures(final LocalDate calculatedDate, final LocalDate beforeUnderlyingDate, final LocalDate afterUnderlyingDate,
	                                      final double aBefore, final double aAfter, final Asset asset) {
		final List<Dividend> appropriateDividends = getAppropriateDividendsNotInclude(asset.getCoefficients().getDividends(), beforeUnderlyingDate, afterUnderlyingDate, true);
		double resultTotal = aAfter;
		double result = aBefore;
		for (Dividend dividend : appropriateDividends) {
			final double dividendPayment = dividend.getAmount() * asset.getMultiplier();
			resultTotal += dividendPayment;
			if (dividend.getPayDate().isBefore(getNextDate(calculatedDate))) {
				result -= dividendPayment;
			}
		}
		final double rate = calculateRate(beforeUnderlyingDate, afterUnderlyingDate, aBefore, resultTotal);
		result += rate * DateUtil.countDaysBetween(beforeUnderlyingDate, calculatedDate);
		return result;
	}

	private double calculateRate(LocalDate beforeUnderlyingDate, LocalDate afterUnderlyingDate1, double beforePrice, double afterPrice) {
		final double datesCount = DateUtil.countDaysBetween(beforeUnderlyingDate, afterUnderlyingDate1);
		return (afterPrice - beforePrice) / datesCount;
	}

	private ApproximatedPrice calculateAfterLastLiquidFutures(Asset asset, LocalDate calculatedDate, double price, AssetCoefficient coefficient) {
		List<ApproximatingRate> approximatingRates = getAppropriateApproximatingRates(coefficient.getFromDate(),
				calculatedDate, coefficient.getApproximatingRates());
		double a = price;
		double rateSum = 0d;
		sortApproximatingRatesAsc(approximatingRates);
		for (int i = 0; i < approximatingRates.size(); i++) {
			ApproximatingRate rate = approximatingRates.get(i);
			LocalDate date;
			if (i + 1 < approximatingRates.size()) {
				date = approximatingRates.get(i + 1).getFromDate();
			} else {
				date = calculatedDate;
			}
			double calculateRateValue = calculateRateValue(a, rate, date);
			a += calculateRateValue;
			rateSum += calculateRateValue;
		}
		double b = calculateDividends(coefficient.getDividends(), coefficient.getFromDate(), calculatedDate, asset.getMultiplier(), true);
		return new ApproximatedPrice(a, b, rateSum);
	}

	private boolean checkApproximation(SimpleCalculationParameters param) {
		LocalDate fromDate = param.getAsset().getCoefficients().getFromDate();
		return (fromDate != null && compareDatesWithoutTime(param.getOptExpDate().toLocalDate(), fromDate));
	}

	private List<ApproximatingRate> getAppropriateApproximatingRates(LocalDate firstDate, LocalDate lastDate, List<ApproximatingRate> rates) {
		return rates.stream().filter(approximatingRate ->
				isDateBetweenInclude(approximatingRate.getFromDate(), firstDate, lastDate))
				.map(approximatingRate -> approximatingRate).collect(Collectors.toList());   //WTF??
	}

	private double calculateRateValue(double value, ApproximatingRate approximatingRate, LocalDate date) {
		if (approximatingRate == null) {
			return 0d;
		}
		double rate = approximatingRate.getRate() == null ? 0d : approximatingRate.getRate();
		return calculatePriceApproximatedPerCoeff(rate, value,
				countDaysBetweenAbsIgnoreOneDay(approximatingRate.getFromDate(), date));
	}

	public static void writeCorrectionLog(ApproximatedPrice appr, DescriptionWriter desc) {
		if (desc != null && appr.getValuePerRate() != 0 && appr.getValuePerDividends() != 0) {
			desc.write("option.price.approximation", appr.getValuePerRate(),
					appr.getValuePerDividends(), appr.getTotal());
		}
		LOG.debug("result of approximation {}", appr);
	}

	private List<Dividend> getAppropriateDividends(List<Dividend> dividends, LocalDate startDate, LocalDate endDate, boolean asc) {
		return sortDividends(dividends, asc).stream().filter(dividend ->
				isDateBetweenInclude(dividend.getPayDate(), startDate, endDate)).collect(Collectors.toList());
	}

	private List<Dividend> getAppropriateDividendsNotInclude(List<Dividend> dividends, LocalDate startDate, LocalDate endDate, boolean asc) {
		return sortDividends(dividends, asc).stream().filter(dividend ->
				isDateBetween(dividend.getPayDate(), startDate, endDate))
				.collect(Collectors.toList());
	}

	private double calculateDividends(List<Dividend> dividends, LocalDate startDate, LocalDate endDate, int assetMultiplier, boolean asc) {
		return getAppropriateDividends(dividends, startDate, endDate, asc).stream().map(Dividend::getAmount)
				.reduce(0d, (divSum, b) -> divSum + b * assetMultiplier);
	}

	private double calculatePriceApproximatedPerCoeff(double rate, double realPrice, double countDaysBetween) {
		return ((countDaysBetween / 365) * fromPercent(rate)) * realPrice;
	}
}
