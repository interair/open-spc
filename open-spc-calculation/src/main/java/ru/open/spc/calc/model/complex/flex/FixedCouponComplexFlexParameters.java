package ru.open.spc.calc.model.complex.flex;

import lombok.Data;
import ru.open.spc.calc.model.complex.ComplexCalculationParameters;

import java.time.Duration;

@Data public class FixedCouponComplexFlexParameters extends ComplexCalculationParameters  implements DateStepHolder {

    private Duration dateStepDuration = Duration.ofDays(1);

}
