package ru.open.spc.calc.kernel.activators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import ru.open.spc.calc.kernel.resolvers.interval.IntervalResolverStrategyImpl;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.calc.model.Spread;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.model.simple.exchange.BasicProtectionSimpleParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.model.Option;
import ru.open.spc.model.enums.OptionType;
import ru.open.spc.model.result.PricingResult;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static ru.open.spc.calc.kernel.SPCalculationCommon.toRub;
import static ru.open.spc.calc.model.ResultBuilder.buildFromParamsBP;
import static ru.open.spc.calc.util.HeaderParams.*;
import static ru.open.spc.calc.util.MathUtil.fromPercent;
import static ru.open.spc.calc.util.MathUtil.toPercent;
import static ru.open.spc.calc.util.Util.checkForNotNull;
import static ru.open.spc.dao.util.RoundUtil.roundDn;

@MessageEndpoint
public class BasicProtectionCalculationService extends AbstractCalculationService {

	private final static Logger LOG = LoggerFactory.getLogger(BasicProtectionCalculationService.class);

    @Autowired private IntervalResolverStrategyImpl intervalOptionsResolver;

	@SuppressWarnings("unchecked")
    @ServiceActivator(inputChannel = "baseCalculationChannel")
	public void calculate(final Message<CalculationParameters> message) {
        wrapCalculate(message);
	}

    @Override
    protected List<PricingResult> doCalculating(MessageHeaders headers, DescriptionWriter dsc,
                                                CalculationParameters payload) throws Exception {
        final BasicProtectionSimpleParameters param = (BasicProtectionSimpleParameters) payload.getCalculationParams();
        final OptionType optionType = (OptionType) headers.get(OPTION_TYPE);
        final List<Option> options = (List<Option>) headers.get(OPTIONS);
        final Pair<Option, Option> strikes = (Pair<Option, Option>) headers.get(NEAREST_STRIKE_PAIR);

        dsc.write("vzk.unity.res", param.getVzkUnity())
            .write("found.options", options.size())
            .write("left.option", strikes.getLeft().getName(), correctPrice(param, dsc, strikes.getLeft()))
            .write("right.option", strikes.getRight().getName(), correctPrice(param, dsc, strikes.getRight()));

        final boolean intervalPriceSet = isIntervalPriceSet(param);
        final boolean priceSet = isPriceSet(param);

        final PriceCalculationFunction priceCalculationFunction = new PriceCalculationFunction(param, CalculationBase.EXCHANGE).desc(dsc).interval(Spread.ASK).strike(0);
        final double n1n2relation = calculateQuantityRelation(strikes.getLeft().getStrikePrice(), strikes.getRight().getStrikePrice(), param);
        dsc.write("n1.n2", n1n2relation);

        final long depoSum = calculateMinToDepo(param);
        dsc.write("depoSum", depoSum);

        final long sumToOptions = calculateSumToOptions(param.getInvestingSum(), depoSum);
        dsc.write("sum.to.options", sumToOptions);

        dsc.write("processing.left");
        final double opt1LeftPrice = priceCalculationFunction.serieses(singletonList(strikes.getLeft())).optionType(strikes.getLeft().getOptionType())
                .conditions(priceSet)
                .apply(param.getOptionPrice());
        dsc.write("left.calculated.price", opt1LeftPrice, toRub(opt1LeftPrice, param));

        dsc.write("processing.right");
        final double opt2RightPrice = priceCalculationFunction.serieses(singletonList(strikes.getRight())).optionType(strikes.getRight().getOptionType())
                .conditions(priceSet)
                .apply(param.getOptionPrice());
        dsc.write("right.calculated.price", opt2RightPrice, toRub(opt2RightPrice, param));

        final Pair<Double, Double> optPrice = new Pair<>(opt1LeftPrice, opt2RightPrice);

        final List<ResultBuilder> prices = new ArrayList<>();
        prices.add(ResultBuilder.buildFromParamsBP(param).sumToOptions(sumToOptions).n1n2relation(n1n2relation)
                .price(optPrice).desc(dsc).strike1(param.getTotalAssetPriceWithStrike1Coef()).strike2(0d));
        if (param.getInterval()) {
            List<Option> inOpts = getIntervalOptionsResolver().resolve(param, dsc, options, optionType, strikes);
            for (Option option : inOpts) {
                DescriptionWriter dscClone = dsc.clone();
                dscClone.write("processing.interval.with.strike", option.getStrikePrice());
                double opt3IntervalPrice = priceCalculationFunction.serieses(singletonList(option)).interval(Spread.NONE).desc(dscClone).isInterval(true)
                        .conditions(intervalPriceSet)
                        .apply(param.getIntervalOptionPrice());
                dscClone.write("interval.calculated.price", opt3IntervalPrice, toRub(opt3IntervalPrice, param));

                double opt1LeftIPrice = opt1LeftPrice - opt3IntervalPrice;
                double opt2RightIPrice = opt2RightPrice - opt3IntervalPrice;
                dscClone.write("optiontype.left.price", optionType, opt1LeftIPrice, toRub(opt1LeftIPrice, param))
                    .write("optiontype.right.price", optionType, opt2RightIPrice, toRub(opt2RightIPrice, param));
                prices.add(buildFromParamsBP(param)
                        .sumToOptions(sumToOptions)
                        .n1n2relation(n1n2relation)
                        .price(new Pair<>(opt1LeftIPrice, opt2RightIPrice))
                        .desc(dscClone).asset(param.getAsset())
                        .strike1(param.getTotalAssetPriceWithStrike1Coef())
                        .strike2(option.getStrikePrice()));
            }
        }

        final List<PricingResult> pricingResults = new ArrayList<>();
        for (ResultBuilder res : prices) {
            Pair<Long, Long> n1n2 = calculateQuantity(res.getN1n2relation(), toRub(res.getPrice().getLeft(), param),
                    toRub(res.getPrice().getRight(), param), res.getSumToOptions());
            res.getDesc().write("quontity.left", n1n2.getLeft()).write("quontity.right", n1n2.getRight());
            double coef = calculateCoeff(n1n2, param);
            res.coefficient(coef).product(payload.getProduct()).n1n2(n1n2);
            PricingResult pricingResult = getResultFactory().createPricingResult(res);
            pricingResults.add(pricingResult);
        }
        return pricingResults;
	}

	long calculateMinToDepo(BasicProtectionSimpleParameters param) {
		// min_to_depo = S * k_prot / (1 + R * T)
        final double daysCountBtwnStartProductEndProduct = param.getDaysCountToEndProduct();
		final double tBtwnStartProductStartEnd = daysCountBtwnStartProductEndProduct / DAYS_PER_YEAR;
		final double amount = param.getInvestingSum() * fromPercent(param.getCoeffAssetProtection()) /
			(1 + fromPercent(param.getInterestRate()) * tBtwnStartProductStartEnd);
		final long minToDepo = Math.round(amount);
		LOG.debug("MinToDepo = {}", minToDepo);
		return minToDepo;
	}

	double calculateQuantityRelation(double str1, double str2, SimpleCalculationParameters param) {
		double pMult = param.getTotalAssetPriceWithStrike1Coef();
		double quontityRelation = (str2 - pMult) / (pMult - str1);
		LOG.debug("quontityRelation = {}", quontityRelation);
		return Math.abs(quontityRelation);
	}

	long calculateSumToOptions(double investingSum, long depoSum) {
		//Sопц = (S - min_to_depo)
		long sumToOptions = Math.round(investingSum - depoSum);
		LOG.debug("sumToOptions = {}", sumToOptions);
		return sumToOptions;
	}

    protected boolean isIntervalPriceSet(BasicProtectionSimpleParameters param) {
        return !param.getInterval() || checkForNotNull(param.getIntervalOptionPrice());
    }

    protected boolean isPriceSet(BasicProtectionSimpleParameters param) {
        return !param.getInterval() || checkForNotNull(param.getOptionPrice());
    }

    Pair<Long, Long> calculateQuantity(double n1n2relation, double opt1LeftPrice, double opt2RightPrice, double sumToOptions) {
		double n2 = sumToOptions / (n1n2relation * opt1LeftPrice + opt2RightPrice);
		long n2rounded = roundDn(n2);
		double n1 = (sumToOptions - n2rounded * opt2RightPrice) / opt1LeftPrice;
		long n1rounded = roundDn(n1);
		return new Pair<>(n1rounded, n2rounded);
	}

	double calculateCoeff(Pair<Long, Long> n1n2, SimpleCalculationParameters param) {
		//К_уч = (N1+N2) / (S/(P * mult))
		double coeffCurrency = (n1n2.getLeft() + n1n2.getRight()) / (param.getInvestingSum() / toRub(param.getTotalAssetPriceWithStrike1Coef(), param));
		LOG.debug("coeffCurrency = {}", coeffCurrency);
		return toPercent(coeffCurrency);
	}

    public IntervalResolverStrategyImpl getIntervalOptionsResolver() {
        return intervalOptionsResolver;
    }

}
