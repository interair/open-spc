package ru.open.spc.calc.model.simple.exchange;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.model.enums.Direction;

@EqualsAndHashCode(callSuper = false)
@Data public class BasicProtectionSimpleParameters extends AssetProtectionSimpleParameters {

	private Boolean interval;
	private Double intervalOptionPrice;
	private Direction direction;
	private Boolean showAllOptions;
    private AssetUnity vzkUnity = AssetUnity.RUB;

}
