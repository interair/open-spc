package ru.open.spc.calc.kernel.enrichers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.model.Option;

import java.util.List;

import static java.lang.Double.MAX_VALUE;
import static ru.open.spc.calc.util.HeaderParams.NEAREST_STRIKE_PAIR;
import static ru.open.spc.calc.util.HeaderParams.OPTIONS;

@MessageEndpoint
public class NearestStrikeEnricher {

	private static final Logger LOG = LoggerFactory.getLogger(NearestStrikeEnricher.class);

	@Transformer(inputChannel = "resolveStrikesChannel", outputChannel = "filterStrikesChannel")
	public Message<CalculationParameters> enrichNearestStrikes(Message<CalculationParameters> message) {
        final SimpleCalculationParameters param = (SimpleCalculationParameters) message.getPayload().getCalculationParams();
        final List<Option> options = (List<Option>) message.getHeaders().get(OPTIONS);
        final Pair<Option, Option> strikes = findOptions(param, options);
		return MessageBuilder.fromMessage(message)
			.setHeader(NEAREST_STRIKE_PAIR, strikes).build();
	}

	public Pair<Option, Option> findOptions(SimpleCalculationParameters param, List<Option> options) {
		//Рассчитываем Str1, Str2 – ближайшие страйки кратные шагу базового актива и ближайшие к текущей рыночной цене такие,
		//что выполняется условие Str1<P*mult<=Str2. Либо Str1 и  Str2 определяются из набора страйков OnlineIV.

		double left = 0d;
		double right = MAX_VALUE;
        final Pair<Option, Option> pair = new Pair<>();
        final double totalAssetPrice = param.getTotalAssetPriceWithStrike1Coef();
		LOG.debug("option TotalAssetPrice {}", totalAssetPrice);
		for (Option option : options) {
			LOG.debug("option strike {}", option.getStrikePrice());
			if (option.getStrikePrice() < totalAssetPrice) {
				if (option.getStrikePrice() > left) {
					left = option.getStrikePrice();
					pair.setLeft(option);
				}
			} else {
				if (option.getStrikePrice() < right) {
					right = option.getStrikePrice();
					pair.setRight(option);
				}
			}
		}
		LOG.debug("options {}", pair);
		return pair;
	}
}
