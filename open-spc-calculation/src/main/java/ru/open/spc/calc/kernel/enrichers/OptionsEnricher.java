package ru.open.spc.calc.kernel.enrichers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.model.simple.exchange.BasicProtectionSimpleParameters;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.model.Option;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.model.enums.OptionType;

import java.util.List;

import static ru.open.spc.calc.kernel.SPCalculationCommon.sortOptions;
import static ru.open.spc.calc.util.HeaderParams.OPTIONS;
import static ru.open.spc.calc.util.HeaderParams.OPTION_TYPE;
import static ru.open.spc.model.enums.Direction.FALLING;
import static ru.open.spc.model.enums.OptionType.CALL;
import static ru.open.spc.model.enums.OptionType.PUT;

@MessageEndpoint
public class OptionsEnricher {

	@Autowired private CalculationManager calculationManager;

	@Transformer(inputChannel = "baseProtectChannel", outputChannel = "filterOptionsChannel")
	public Message<CalculationParameters> enrichOptions(Message<CalculationParameters> message) {
        final SimpleCalculationParameters param = (SimpleCalculationParameters) message.getPayload().getCalculationParams();
        OptionType optionType = null;
        if (param instanceof BasicProtectionSimpleParameters) {
            final BasicProtectionSimpleParameters protectionSimpleParameters = (BasicProtectionSimpleParameters) param;
            final Direction direction = protectionSimpleParameters.getDirection();
            optionType = direction == null ? null : getOptionType(protectionSimpleParameters.getDirection());
        }
        final List<Option> options = calculationManager.getStrikesBy(param.getAsset().getOuterId(), param.getOptExpDate().toLocalDate(), optionType);
		sortOptions(options);
		return MessageBuilder.fromMessage(message)
			.setHeader(OPTION_TYPE, optionType)
			.setHeader(OPTIONS, options)
			.build();
	}

	private OptionType getOptionType(Direction direction) {
		if (direction == FALLING) {
			return PUT;
		} else {
			return CALL;
		}
	}
}
