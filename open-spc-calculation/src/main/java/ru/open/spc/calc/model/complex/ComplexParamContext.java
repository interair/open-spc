package ru.open.spc.calc.model.complex;

import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.Orientation;
import ru.open.spc.model.enums.Direction;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ComplexParamContext {

    private final LocalDateTime optExpDate;
    private final LocalDate productEndDate;
    private final double coeffAssetProtection;
    private final double interestRate;
    private final Direction direction;
    private final double strike1;
    private final double strike2;
    private final Double volatilitySpread;
	private final boolean interval;
    private final Orientation orientation;
    private final Long stockNum;
    private final Double repoDiscount;
    private final Double repoRate;
    private final CalculationBase calculationBase;

    private ComplexParamContext(LocalDateTime optExpDate, LocalDate productEndDateNew,
                                Double coeffAssetProtection, Double interestRate,
                                Direction direction, Double strike1, Double strike2,
                                Double volatilitySpread, Boolean interval,
                                Orientation orientation, Long stockNum,
                                Double repoDiscount, Double repoRate,
                                CalculationBase calculationBase) {
        this.optExpDate = optExpDate;
        this.productEndDate = productEndDateNew;
        this.coeffAssetProtection = coeffAssetProtection;
        this.interestRate = interestRate;
        this.direction = direction;
        this.strike1 = strike1;
        this.strike2 = strike2;
        this.volatilitySpread = volatilitySpread;
		this.interval = interval;
        this.orientation = orientation;
        this.stockNum = stockNum;
        this.repoDiscount = repoDiscount;
        this.repoRate = repoRate;
        this.calculationBase = calculationBase;
    }

    public LocalDateTime getOptExpDate() {
        return optExpDate;
    }

    public LocalDate getProductEndDate() {
        return productEndDate;
    }

    public double getCoeffAssetProtection() {
        return coeffAssetProtection;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public Direction getDirection() {
        return direction;
    }

    public double getStrike1() {
        return strike1;
    }

    public double getStrike2() {
        return strike2;
    }

    public Double getVolatilitySpread() {
        return volatilitySpread;
    }

	public boolean isInterval() {
		return interval;
	}

    public Orientation getOrientation() {
        return orientation;
    }

    public Long getStockNum() {
        return stockNum;
    }

    public Double getRepoDiscount() {
        return repoDiscount;
    }

    public Double getRepoRate() {
        return repoRate;
    }

    public CalculationBase getCalculationBase() {
        return calculationBase;
    }

    public static class Builder {

        private LocalDateTime optExpDate;
        private LocalDate productEndDate;
        private double coeffAssetProtection;
        private double interestRate;
        private Direction direction;
        private double strike1;
        private double strike2;
        private Double volatilitySpread;
		private boolean interval = false;
        private Orientation orientation;
        private Long stockNum;
        private Double repoDiscount;
        private Double repoRate;
        private CalculationBase calculationBase;

        public Builder() {
        }

        public Builder optExpDate(LocalDateTime optExpDateNew) {
            this.optExpDate = optExpDateNew;
            return this;
        }

        public Builder productEndDate(LocalDate productEndDate) {
            this.productEndDate = productEndDate;
            return this;
        }

        public Builder coeffAssetProtection(double coeffAssetProtection) {
            this.coeffAssetProtection = coeffAssetProtection;
            return this;
        }

        public Builder interestRate(double interestRate) {
            this.interestRate = interestRate;
            return this;
        }

        public Builder direction(Direction direction) {
            this.direction = direction;
            return this;
        }

        public Builder strike1(double strike1) {
            this.strike1 = strike1;
            return this;
        }

        public Builder strike2(double strike2) {
            this.strike2 = strike2;
            return this;
        }

        public Builder volatilitySpread(Double volatilitySpread) {
            this.volatilitySpread = volatilitySpread;
            return this;
        }

		public Builder interval(boolean interval) {
			this.interval = interval;
			return this;
		}

        public Builder orientation(Orientation orientation) {
            this.orientation = orientation;
            return this;
        }

        public Builder stockNum(Long stockNum) {
            this.stockNum = stockNum;
            return this;
        }

        public Builder repoDiscount(Double repoDiscount) {
            this.repoDiscount = repoDiscount;
            return this;
        }

        public Builder repoRate(Double repoRate) {
            this.repoRate = repoRate;
            return this;
        }

        public Builder calculationBase(CalculationBase calculationBase) {
            this.calculationBase = calculationBase;
            return this;
        }

        public ComplexParamContext create() {
            return new ComplexParamContext(optExpDate,
                    productEndDate,
                    coeffAssetProtection,
                    interestRate,
                    direction,
                    strike1,
                    strike2,
                    volatilitySpread,
					interval,
                    orientation,
                    stockNum,
                    repoDiscount,
                    repoRate,
                    calculationBase);
        }

    }
}
