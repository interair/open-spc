package ru.open.spc.calc.model.complex;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.calc.model.AbstractCalculationParameters;

import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@SuppressWarnings("serial")
@Data public abstract class AbstractComplexCalculationParameters extends AbstractCalculationParameters {

	@JsonIgnore
	private LocalDate productStartDate;
	private Double interestRateFrom;
	private Double interestRateTo;
	private Double interestRateStep;

    public AbstractComplexCalculationParameters() { }

    @Override
    public Boolean getVariability() {
        return true;
    }

}
