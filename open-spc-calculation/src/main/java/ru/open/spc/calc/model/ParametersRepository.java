package ru.open.spc.calc.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import ru.open.spc.dao.manager.SettingsManager;
import ru.open.spc.model.CalculationType;
import ru.open.spc.model.Settings;
import ru.open.spc.model.enums.ProductCode;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class ParametersRepository {

	private final static Logger LOG = LoggerFactory.getLogger(ParametersRepository.class);

	private Map<ProductCode, List<Map<String, Object>>> productTabsComplexParams;
	private Map<ProductCode, List<Map<String, Object>>> productTabsSimpleParams;

	private Map<ProductCode, List<Map<String, Object>>> productTabsComplexParamsInit;
	private Map<ProductCode, List<Map<String, Object>>> productTabsSimpleParamsInit;

	private SettingsManager settingsManager;
	private ObjectMapper objectMapper = objectMapperBuilder();

	public synchronized void config(final BaseParameters calculationParameters,
	                                final ProductCode productCode,
	                                final CalculationType calculationType) {
		if (load(productCode, calculationType, calculationParameters)) {
			return;
		}
		for (Map<String, Object> config : getConfigs(productCode, calculationType)) {
			try {
				BeanUtils.populate(calculationParameters, config);
			} catch (IllegalAccessException | InvocationTargetException e) {
				LOG.error("can't set properties", e);
			}
		}
	}

	private boolean load(ProductCode productCode, CalculationType calculationType, BaseParameters calculationParameters) {
		try {
			Settings settingsBy = settingsManager.getSettingsBy(productCode, calculationType);
			if (settingsBy != null) {
				Object orig = objectMapper.readValue(settingsBy.getData(), Class.forName(settingsBy.getClassName()));
				if (orig != null) {
					BeanUtils.copyProperties(calculationParameters, orig);
					return true;
				}
			}

		} catch (Exception e) {
			LOG.error("can't get settings", e);
		}
		return false;
	}

	private void save(ProductCode productCode, CalculationType calculationType, BaseParameters calculationParameters) {
		try {
			String data = objectMapper.writeValueAsString(calculationParameters);
			Settings settings = new Settings();
			settings.setCalculationType(calculationType);
			settings.setProductCode(productCode);
			settings.setClassName(calculationParameters.getClass().getName());
			settings.setData(data);
			settingsManager.save(settings);
		} catch (Exception e) {
			LOG.error("can't save settings", e);
		}

	}

	public void setInitConfig(final BaseParameters calculationParameters,
	                                final ProductCode productCode,
	                                final CalculationType calculationType) {
		for (Map<String, Object> config : getInitConfigs(productCode, calculationType)) {
			try {
				BeanUtils.populate(calculationParameters, config);
			} catch (IllegalAccessException | InvocationTargetException e) {
				LOG.error("can't set properties", e);
			}
		}
	}

	private List<Map<String, Object>> getInitConfigs(ProductCode productCode, CalculationType calculationType) {
		switch (calculationType) {
			case COMPLEX:
				return productTabsComplexParamsInit.get(productCode);
			default:
				return productTabsSimpleParamsInit.get(productCode);
		}
	}

	private List<Map<String, Object>> getConfigs(final ProductCode productCode, final CalculationType calculationType) {
		switch (calculationType) {
			case COMPLEX:
				return productTabsComplexParams.get(productCode);
			default:
				return productTabsSimpleParams.get(productCode);
		}
	}

	//invert order
	public synchronized void updateConfigParams(final BaseParameters calculationParameters,
	                                            final ProductCode productCode,
	                                            final CalculationType calculationType) {
		final List<Map<String, Object>> configs = getConfigs(productCode, calculationType);
		final Set<String> updatedParams = new HashSet<>();
		for (int i = configs.size() - 1; i >= 0; i--) {
			Map<String, Object> config = configs.get(i);
			for (Map.Entry<String, Object> prop : config.entrySet()) {
				if (updatedParams.contains(prop.getKey()))
					continue;
				updatedParams.add(prop.getKey());
				try {
					setProperty(prop, config, calculationParameters);
				} catch (Exception e) {
					LOG.error("can't set property " + prop);
				}
			}
		}
		save(productCode, calculationType, calculationParameters);
	}

	private void setProperty(final Map.Entry<String, Object> prop,
	                         final Map<String, Object> config,
	                         final BaseParameters calculationParameters) throws Exception {
		final String propName = prop.getKey();
		final Object property = PropertyUtils.getProperty(calculationParameters, propName);
		if (checkForNull(prop.getValue()) && !prop.getValue().equals(property)) {
			LOG.debug("try to update property '{}', from '{}', to '{}'", propName, prop.getValue(), property);
			config.put(propName, property);
		}
	}

	@Required
	public void setSettingsManager(SettingsManager settingsManager) {
		this.settingsManager = settingsManager;
	}

	@Required
	public void setProductTabsComplexParams(Map<ProductCode, List<Map<String, Object>>> productTabsComplexParams) {

		this.productTabsComplexParams = synchronize(productTabsComplexParams);
		this.productTabsComplexParamsInit = deepCopy(productTabsComplexParams);
	}

	@Required
	public void setProductTabsSimpleParams(Map<ProductCode, List<Map<String, Object>>> productTabsSimpleParams) {
		this.productTabsSimpleParams = synchronize(productTabsSimpleParams);
		this.productTabsSimpleParamsInit = deepCopy(productTabsSimpleParams);

	}

	private Map<ProductCode, List<Map<String, Object>>> deepCopy(Map<ProductCode, List<Map<String, Object>>> map) {
		Map<ProductCode, List<Map<String, Object>>> newMap = new HashMap<>();
		for (Map.Entry<ProductCode, List<Map<String, Object>>> productCodeListEntry : map.entrySet()) {
			newMap.put(productCodeListEntry.getKey(), productCodeListEntry.getValue().stream().map(HashMap<String, Object>::new).collect(Collectors.<Map<String,Object>>toList()));
		}
		return Collections.unmodifiableMap(newMap);
	}

	private Map<ProductCode, List<Map<String, Object>>> synchronize(Map<ProductCode, List<Map<String, Object>>> productTabsComplexParams) {
		Map<ProductCode, List<Map<String, Object>>> params = new ConcurrentHashMap<>();
		for (Map.Entry<ProductCode, List<Map<String, Object>>> entryMap : productTabsComplexParams.entrySet()) {
			List<Map<String, Object>> entries = new CopyOnWriteArrayList<>();
			for (Map<String, Object> entry : entryMap.getValue()) {
				entries.add(Collections.synchronizedMap(entry));
			}
			params.put(entryMap.getKey(), entries);
		}
		return params;
	}

	private boolean checkForNull(final Object value) {
		return value != null && !Double.valueOf(0d).equals(value);
	}

	public ObjectMapper objectMapperBuilder() {
		ObjectMapper objectMapper = new ObjectMapper()
				.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
				.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		objectMapper.registerModule(new JavaTimeModule());
		return objectMapper;
	}

}
