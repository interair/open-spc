package ru.open.spc.calc.kernel.resolvers.interval;

import org.springframework.stereotype.Service;
import ru.open.spc.calc.model.simple.exchange.BasicProtectionSimpleParameters;
import ru.open.spc.calc.util.DescriptionWriter;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.model.Option;
import ru.open.spc.model.enums.OptionType;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class IntervalResolverStrategyImpl implements IntervalResolverStrategy {

	@Resource(name = "intervalResolverStrategyMap")
	private Map<Class<?>, IntervalResolverStrategy> resolvers;

	public List<Option> resolve(BasicProtectionSimpleParameters param,
								DescriptionWriter dsc,
								List<Option> options,
								OptionType optionType,
								Pair<Option, Option> strikes) {
		IntervalResolverStrategy resolver = resolvers.get(param.getClass());
		if (resolver == null) {
			throw new IllegalArgumentException("Ooops can't find resolver for class "
				+ param.getClass().getName());
		}
		return resolver.resolve(param, dsc, options, optionType, strikes);
	}
}
