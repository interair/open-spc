package ru.open.spc.calc.kernel.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.open.spc.dao.util.DateUtil;
import ru.open.spc.model.Asset;
import ru.open.spc.model.AssetCoefficient;
import ru.open.spc.model.Dividend;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ApproximationCalculationServiceTest {

    private ApproximationCalculationService calculationService = new ApproximationCalculationService();
    private Asset asset = new Asset();
    private LocalDate today = LocalDate.now();

    @Before
    public void before() {
        AssetCoefficient assetCoefficient = new AssetCoefficient();
        List<Dividend> dividends = new ArrayList<>();
        Dividend dividend = new Dividend();
        dividend.setAmount(1d);
        dividend.setPayDate(addDays(today, 20));
        dividends.add(dividend);
        assetCoefficient.setDividends(dividends);
        asset.setCoefficients(assetCoefficient);
        asset.setMultiplier(1000);
    }

    @Test
    public void testCalculateLiquidFeatures() throws Exception {
        final double res = calculationService.calculateLiquidFeatures(addDays(today, 30),
                                                    today,
                                                    addDays(today, 40),
                                                    1000d, 1000d, asset);

        Assert.assertEquals(res, 750d, 0.01d);
    }

    private LocalDate addDays(LocalDate date, int count) {
        return DateUtil.getDateWithShift(date, count, false);
    }
}