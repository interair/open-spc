package ru.open.spc.calc.kernel.activators;

import org.junit.*;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.model.simple.exchange.BasicProtectionSimpleParameters;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.dao.util.DateUtil;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Option;
import ru.open.spc.model.SmileParams;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.model.enums.OptionType;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static ru.open.spc.dao.util.RoundUtil.roundHUP;

public class BasicProtectionCalculationServiceTest {
	private final BasicProtectionCalculationService calculation = new BasicProtectionCalculationService();
	private static final CalculationParameters params = new CalculationParameters();
	private final CalculationManager calculationManager = mock(CalculationManager.class);
	private final List<Option> options = new ArrayList<>();

	@BeforeClass
	public static void setUpClass() throws Exception {
		BasicProtectionSimpleParameters subParams = new BasicProtectionSimpleParameters();
		Asset asset = new Asset();
		asset.setBaseTicker("SR");
		asset.setPointPrice(102.57);
		asset.setMultiplier(100);
		subParams.setAsset(asset);
		subParams.setDirection(Direction.RISING);
		subParams.setCoeffAssetProtection(95d);
        LocalDate start = LocalDate.of(2013, Month.JANUARY, 22);
		subParams.setDepositStartDate(start);
        LocalDate end = LocalDate.of(2013, Month.MARCH, 15);
		subParams.setProductEndDate(end);
		subParams.setOptExpDate(DateUtil.setHours(DateUtil.getPreviousDate(end), 0, 0));
		subParams.setUnderlyingAssetPrice(102.57);
		subParams.setUnderlyingAssetMultiplicator(100);
		subParams.setItemCost(1d);
		subParams.setAssetUnity(AssetUnity.RUB);
		subParams.setInterestRate(6d);
		subParams.setInvestingSum(1000000d);
        subParams.setVolatilitySpread(5d);
		subParams.setTotalAssetPrice(subParams.getUnderlyingAssetMultiplicator() * subParams.getUnderlyingAssetPrice());
		subParams.setTotalAssetPriceInRub(subParams.getTotalAssetPrice());
		subParams.setTotalAssetPriceWithStrike1Coef(subParams.getTotalAssetPrice());
        double countDaysBetweenEndPr = DateUtil.countDaysBetween(subParams.getDepositStartDate(), subParams.getProductEndDate());
        subParams.setDaysCountToEndProduct(countDaysBetweenEndPr);
        double countDaysToOptExp = DateUtil.countDaysBetweenABS(subParams.getDepositStartDate(), subParams.getOptExpDate());
        subParams.setDaysCountToExpOpt(countDaysToOptExp);
		params.setCalculationParams(subParams);

	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@Before
	public void setUpMethod() throws Exception {
		options.clear();
		Option optionLeft = new Option();
        LocalDate endCal = LocalDate.of(2013, Month.MARCH, 14);
		optionLeft.setExpireDate(endCal);
		optionLeft.setMultiplicator(100d);
		optionLeft.setStrikePrice(10250d);
		optionLeft.setLastPrice(10338d);
		optionLeft.setBestAsk(10338d);
		optionLeft.setBestBid(10338d);
		optionLeft.setName("left");
		optionLeft.setOptionType(OptionType.CALL);
		SmileParams leftSmParams = new SmileParams();
		//0.25417	0.58217	-0.00417	2.11643	0	1.20993
		leftSmParams.setBottom(0.25417);
		leftSmParams.setHeight(0.58217);
		leftSmParams.setLoc(-0.00417);
		leftSmParams.setKurt(2.11643);
		leftSmParams.setSkew(0d);
		leftSmParams.setScal(1.20993);
		optionLeft.setSmileParams(leftSmParams);
		Option optionRight = new Option();
		optionRight.setOptionType(OptionType.CALL);
		optionRight.setExpireDate(endCal);
		optionRight.setMultiplicator(100d);
		optionRight.setLastPrice(10338d);
		optionRight.setBestAsk(10338d);
		optionRight.setBestBid(10338d);
		optionRight.setStrikePrice(10500d);
		optionRight.setName("right");
		SmileParams rightSmParams = new SmileParams();
		//0.25417	0.58217	-0.00417	2.11643	0	1.20993
		rightSmParams.setBottom(0.25417);
		rightSmParams.setHeight(0.58217);
		rightSmParams.setLoc(-0.00417);
		rightSmParams.setKurt(2.11643);
		rightSmParams.setSkew(0d);
		rightSmParams.setScal(1.20993);
		optionRight.setSmileParams(rightSmParams);
		options.add(optionLeft);
		options.add(optionRight);
		when(calculationManager.getStrikesBy(any(Long.class), any(LocalDate.class), any(OptionType.class)))
			.thenReturn(options);
	}

	@After
	public void tearDownMethod() throws Exception {
	}

	@Test
	public void testCountDays() {
		SimpleCalculationParameters calculationParams = (SimpleCalculationParameters) params.getCalculationParams();
		double countDaysBetweenEndOpt = DateUtil.countDaysBetween(calculationParams.getDepositStartDate(),
            DateUtil.getPreviousDate(calculationParams.getProductEndDate()));
		assertEquals(roundHUP(countDaysBetweenEndOpt, 0), 51d);

		double countDaysBetweenEndPrdct = DateUtil.countDaysBetween(calculationParams.getDepositStartDate(),
            calculationParams.getProductEndDate());
		assertEquals(roundHUP(countDaysBetweenEndPrdct, 0), 52d);
	}

	@Test
	public void testMinToDepo() {
		BasicProtectionSimpleParameters calculationParams = (BasicProtectionSimpleParameters) params.getCalculationParams();

		Long calculateMinToDepo = calculation.calculateMinToDepo(calculationParams);
		assertEquals(calculateMinToDepo.longValue(), 941948l);
		Long calculateToOptions = calculation.calculateSumToOptions(calculationParams.getInvestingSum(), calculateMinToDepo);
		assertEquals(calculateToOptions.doubleValue(), (calculationParams.getInvestingSum() - calculateMinToDepo));
	}

	@Test
	public void testReletion() {
		//N1/N2 = (10500-102.57*100)/(102.57*100-10250)= 243/7=34.7
		SimpleCalculationParameters calculationParams = (SimpleCalculationParameters) params.getCalculationParams();
		Double calculateQuontityRelation = calculation.calculateQuantityRelation(10250d, 10500d, calculationParams);
		assertEquals(roundHUP(calculateQuontityRelation, 1), 34.7d);
	}

	@Test
	public void testCoef() {
		Pair<Long, Long> pair = calculation.calculateQuantity(34.7d, 396d, 270d, 41879d);
		double result = calculation.calculateCoeff(pair, (SimpleCalculationParameters) params.getCalculationParams());
		/*N2(1+x)->max
			N2(x*Opt1+Opt2)<=S_opt

			N2 * (34.7*396+270) <=41879
			N2 <= 2.99
			N2=2
		*/
		assertEquals(result, 108.7242, 0.001d);
	}
}
