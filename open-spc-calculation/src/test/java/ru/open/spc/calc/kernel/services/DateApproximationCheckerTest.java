package ru.open.spc.calc.kernel.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.model.Asset;
import ru.open.spc.model.AssetCoefficient;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DateApproximationCheckerTest {

    private AssetManager service = mock(AssetManager.class);
    private CalculationManager calculationManager = mock(CalculationManager.class);
    private DateApproximationChecker checker = new DateApproximationChecker();

    @Before
    public void prepare() {
        final List<Asset> assets = new ArrayList<>();
        final Asset asset = new Asset() {{
            setCoefficients(new AssetCoefficient() {{
                setFromDate(LocalDate.now());
            }});
        }};
        assets.add(asset);
        when(service.all()).thenReturn(assets);

        when(calculationManager.getLastFutureDate(anyLong())).thenReturn(LocalDate.now());
    }

    @Test
    public void testChecker() {
        checker.setAssetService(service);
        checker.setCalculationManager(calculationManager);
        checker.check();
        final List<Asset> all = service.all();
        for (Asset asset : all) {
            Assert.assertFalse(asset.getCoefficients().getFromDate().isAfter(LocalDate.now()));
        }
    }
}
