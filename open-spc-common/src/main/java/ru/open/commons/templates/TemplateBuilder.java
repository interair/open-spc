package ru.open.commons.templates;

import java.util.Map;

public interface TemplateBuilder {

    String buildTemplate(final Map<String, Object> argMap, final String templateName) throws Exception;
}
