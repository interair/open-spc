package ru.open.commons.templates;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

public class DefaultTemplateBuilder implements TemplateBuilder {

    private Configuration freemarkerConfiguration;

    @Override
    public String buildTemplate(final Map<String, Object> argMap, final String templateName) throws Exception {
        final Template template = freemarkerConfiguration.getTemplate(templateName);
        final Writer writer = new StringWriter();
        template.process(argMap, writer);
        return writer.toString();
    }

    public void setFreemarkerConfiguration(Configuration freemarkerConfiguration) {
        this.freemarkerConfiguration = freemarkerConfiguration;
    }
}
