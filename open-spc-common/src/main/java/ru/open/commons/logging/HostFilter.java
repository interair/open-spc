package ru.open.commons.logging;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;

public class HostFilter extends Filter<ILoggingEvent> {
    private final Set<String> hostNames = new HashSet<>();
    private final String hostName;

    public HostFilter() throws UnknownHostException {
        this.hostName = InetAddress.getLocalHost().getHostName();
    }

    @Override
    public FilterReply decide(final ILoggingEvent event) {
        return hostNames.contains(hostName) ? FilterReply.ACCEPT : FilterReply.DENY;
    }

    public void setHostNames(String values){
        hostNames.clear();
        for (String value : values.split(",")){
            hostNames.add(value.trim());
        }
    }

}
