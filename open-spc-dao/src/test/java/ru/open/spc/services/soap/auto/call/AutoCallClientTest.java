package ru.open.spc.services.soap.auto.call;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.open.auto.call.ArrayOfArrayOfdouble;
import ru.open.auto.call.ArrayOfdouble;
import ru.open.auto.call.GetPricingData;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.List;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationDaoContext.xml"})
public class AutoCallClientTest {

	@Autowired
	AutoCallClientImpl autoCallClient;

	@Autowired
	Jaxb2Marshaller marshaller;


	@Test
	public void test() {
		GetPricingData getPricingData = new GetPricingData();
		getPricingData.setAdditionalCoupon(4d);
		getPricingData.setBottomBarrierRate(90d);
		getPricingData.setCoupon(4d);
		getPricingData.setInitInvest(100000d);
		getPricingData.setTimePassed(0d);
		ArrayOfdouble arrayOfdouble = new ArrayOfdouble();
		arrayOfdouble.getDouble().add(100d);
		arrayOfdouble.getDouble().add(100d);
		arrayOfdouble.getDouble().add(100d);
		arrayOfdouble.getDouble().add(100d);
		JAXBElement<ArrayOfdouble> arrayOfdoubleJAXBElement = new JAXBElement<ArrayOfdouble>(new QName("ArrayOfdouble"),
				ArrayOfdouble.class, arrayOfdouble);
		getPricingData.setCurrentAssetVals(arrayOfdoubleJAXBElement);
		getPricingData.setSpotPriceRef(arrayOfdoubleJAXBElement);

		ArrayOfArrayOfdouble arrayOfArrayOfdouble = new ArrayOfArrayOfdouble();
		List<ArrayOfdouble> arrayOfdoubleList = arrayOfArrayOfdouble.getArrayOfdouble();
		for (int i = 0; i < 4; i++) {
			arrayOfdoubleList.add(new ArrayOfdouble());
			for (int j = 0; j < 4; j++) {
				arrayOfdoubleList.get(i).getDouble().add(100d);
			}
		}
		JAXBElement<ArrayOfArrayOfdouble> arrayOfArrayOfdoubleJAXBElement = new JAXBElement<ArrayOfArrayOfdouble>(new QName("ArrayOfArrayOfdouble"),
				ArrayOfArrayOfdouble.class, arrayOfArrayOfdouble);

		getPricingData.setCorr(arrayOfArrayOfdoubleJAXBElement);
		getPricingData.setInterest(15d);

		final StringWriter out = new StringWriter();
		marshaller.marshal(getPricingData, new StreamResult(out));
		String xml = out.toString();
		log.debug("result {}", xml);

		autoCallClient.calculate(getPricingData);
	}

}