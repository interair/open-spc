package ru.open.spc.model.manager.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Currency;
import ru.open.spc.model.Product;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.model.enums.ProductCode;
import ru.open.spc.model.result.BAPricingResult;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testApplicationContext.xml"})
public abstract class AbstractSpringTest {

	protected Product createProduct() {
		Product product = new Product();
		product.setName("_PRODUCT_TEST_");
		product.setDescription("_PRODUCT_DESCRIPTION_");
		product.setCode(ProductCode.BaseProtect);
		product.setAssets(Collections.<Asset>emptyList());
		return product;
	}

	protected Asset createAsset(Product product) {
		Asset asset = new Asset();
		asset.setExchange(Boolean.TRUE);
		asset.setMinimumPrice(-100d);
		asset.setMultiplier(-1);
		asset.setName("_FAKE_ASSET_TODAY");
		asset.setPointPrice(-200d);
		asset.setStrikeStep(10);
		asset.setUnity(AssetUnity.RUB);
        asset.getCoefficients().setFromDate(LocalDate.now());
		List<Product> products = new ArrayList<>();
		products.add(product);
		asset.setProducts(products);
		return asset;
	}

	protected BAPricingResult createPricingResult(Asset asset, Currency currency, Product product) {
		BAPricingResult pricingResult = new BAPricingResult();
		pricingResult.setAsset(asset);
		pricingResult.setProduct(product);
		pricingResult.setPriceDate(LocalDateTime.now());
		pricingResult.setCurrency(currency);
		pricingResult.setCoefficient(-100d);
		pricingResult.setInterval(-102);
		pricingResult.setKzk(-103d);
		pricingResult.setPercentByLoanDeal(-105d);
		pricingResult.setProductStartDate(LocalDate.now());
		pricingResult.setProductStopDate(LocalDate.now());
		pricingResult.setStrikePrice(-106d);
		pricingResult.setDirection(Direction.RISING);
        pricingResult.setStrike2Price(100d);
        pricingResult.setDescription("description");
        pricingResult.setTimeout(Duration.ofHours(3));
		return pricingResult;
	}

}
