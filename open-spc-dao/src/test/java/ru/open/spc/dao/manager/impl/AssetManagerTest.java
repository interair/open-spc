package ru.open.spc.dao.manager.impl;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.dao.mapper.ClosedPriceMapper;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Product;
import ru.open.spc.model.manager.test.AbstractSpringTest;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class AssetManagerTest extends AbstractSpringTest {

    private final static Logger LOG = LoggerFactory.getLogger(AssetManagerTest.class);

	@Autowired
	private ProductManager productManager;

	@Autowired
	private AssetManager assetManager;

	@Autowired
	private ClosedPriceMapper closedPriceMapper;

	@Test
	public void newAssetTest() {

		Product product = createProduct();
		Asset asset = createAsset(product);
		try {
			productManager.save(product);
			assetManager.save(asset);
		} finally {
			if (asset.getId() != null) {
				assetManager.remove(asset);
			}
			if (product.getId() != null) {
				productManager.remove(product);
			}
		}

	}

	@Test
	public void updateAssetTest() {

		Product product = createProduct();
		Asset asset = createAsset(product);
		try {
			productManager.save(product);
			assetManager.save(asset);
			asset.setName(asset.getName() + 1);
			assetManager.save(asset);

			Asset updatedAsset = assetManager.getBy(asset.getId(), product.getId());
			Assert.assertNotNull(updatedAsset);
			Assert.assertFalse(updatedAsset.getProducts().isEmpty());
			Assert.assertEquals(asset, updatedAsset);
		} finally {
			if (asset != null && asset.getId() != null) {
				assetManager.remove(asset);
			}
			if (product != null && product.getId() != null) {
				productManager.remove(product);
			}
		}
	}

	@Test
	public void selectAllForTest() {

		List<Asset> newAssets = new ArrayList<>();
		Product product = createProduct();

		try {
			productManager.save(product);
			try {
				for (int i = 0; i < 3; i++) {
					Asset asset = createAsset(product);
					assetManager.save(asset);
					newAssets.add(asset);
				}
				List<Asset> assets = assetManager.allFor(product.getId());
				Assert.assertEquals(newAssets, assets);
				List<Asset> currentAssets = assetManager.allCalculatedForToday(31l);
				Assert.assertNotNull(currentAssets);

			} finally {
                newAssets.forEach(assetManager::remove);
			}
		} finally {
			if (product != null && product.getId() != null) {
				productManager.remove(product);
			}
		}
	}
    @Test
    public void getByCoefficientFromDate(){

        Product product = createProduct();
        Asset assetNextWeek = createAsset(product);
        Asset assetToday = createAsset(product);
        try {

            LocalDate date = LocalDate.now();
            final LocalDate plus = date.plus(5, ChronoUnit.DAYS);
            LOG.info("check calendar +5 days {} from {} ", plus, date);

            assetNextWeek.setName("FAKE_NEXT_WEEK");
            assetNextWeek.getCoefficients().setFromDate(date);
            productManager.save(product);
            assetManager.save(assetNextWeek);
            assetManager.save(assetToday);

            final List<Asset> all = assetManager.getByCoefficientFromDate(plus);
            LOG.info("GetByCoefficientfromdate returns elemnts:  {}",all.size());

            for (Asset queriedAsset : all) {
                LOG.info("fetched asset with name {} and date {} compared with today asset with name {} and today date {}",
                    queriedAsset.getName(),
                    queriedAsset.getCoefficients().getFromDate(),
                    assetToday.getName(),
                    assetToday.getCoefficients().getFromDate());
                Assert.assertFalse(queriedAsset.getName().equals(assetNextWeek.getName()));
            }


        } finally {
            if (assetNextWeek.getId() != null) {
                assetManager.remove(assetNextWeek);
            }
            if (assetToday.getId() != null) {
                assetManager.remove(assetToday);
            }
            if (product.getId() != null) {
                productManager.remove(product);
            }
        }
    }

}
