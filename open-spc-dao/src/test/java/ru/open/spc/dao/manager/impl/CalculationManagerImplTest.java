package ru.open.spc.dao.manager.impl;

import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.model.StockAsset;
import ru.open.spc.model.manager.test.AbstractSpringTest;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@Slf4j
public class CalculationManagerImplTest extends AbstractSpringTest {

    @Autowired
    private CalculationManager calculationManager;

    @Test
    @Ignore
    public void testGetAvailableTickers() throws Exception {
        final List<StockAsset> availableTickers = calculationManager.getAvailableTickers();
        assertFalse(availableTickers.isEmpty());
        for (StockAsset availableTicker : availableTickers) {
            assertNotNull(availableTicker.getName());
            log.debug("availableTickers = {}", availableTickers);

        }
    }
}