package ru.open.spc.dao.manager.impl;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.CurrencyManager;
import ru.open.spc.dao.manager.PricingResultManager;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.dao.mapper.ClosedPriceMapper;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Currency;
import ru.open.spc.model.Product;
import ru.open.spc.model.manager.test.AbstractSpringTest;
import ru.open.spc.model.result.PricingResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PricingResultManagerTest extends AbstractSpringTest {

	@Autowired
	private ProductManager productManager;

	@Autowired
	private AssetManager assetManager;

	@Autowired
	private ClosedPriceMapper closedPriceMapper;


	@Autowired
	private PricingResultManager pricingResultManager;

	@Autowired
	private CurrencyManager currencyManager;

	@Test
	public void newPricingResult() {

		Currency currency = currencyManager.getBy("USD");
		Product product = createProduct();
		Asset asset = createAsset(product);
		PricingResult pricingResult = createPricingResult(asset, currency, product);
		try {
			productManager.save(product);
			assetManager.save(asset);
			pricingResultManager.save(pricingResult);
			Assert.assertNotNull(pricingResult);
			Assert.assertNotNull(pricingResult.getId());
		} finally {
			if (pricingResult.getId() != null) {
				pricingResultManager.remove(pricingResult);
			}
			if (asset.getId() != null) {
				assetManager.remove(asset);
			}
			if (product.getId() != null) {
				productManager.remove(product);
			}
		}
	}

	@Test
	public void selectByIdTest() {

		Currency currency = currencyManager.getBy("RUB");
		Product product = createProduct();
		Asset asset = createAsset(product);
		PricingResult pricingResult = createPricingResult(asset, currency, product);

		try {
			productManager.save(product);
			assetManager.save(asset);
			pricingResultManager.save(pricingResult);
			PricingResult foundPricingResult = pricingResultManager.getById(pricingResult.getId());
			Assert.assertEquals(foundPricingResult, pricingResult);
		} finally {
			if (pricingResult.getId() != null) {
				pricingResultManager.remove(pricingResult);
			}
			if (asset.getId() != null) {
				assetManager.remove(asset);
			}
			if (product.getId() != null) {
				productManager.remove(product);
			}
		}
	}

	@Test
	public void allForProductTest() {

		Currency currency = currencyManager.getBy("RUB");
		Product product = createProduct();
		Asset asset = createAsset(product);

		List<PricingResult> results = new ArrayList<>();
		try {
			productManager.save(product);
			assetManager.save(asset);
			for (int i = 0; i < 3; i++) {
				PricingResult pricingResult = createPricingResult(asset, currency, product);
				pricingResultManager.save(pricingResult);
				results.add(pricingResult);
			}
			Collections.sort(results, (o1, o2) -> o1.getId().compareTo(o2.getId()));
			List<PricingResult> data = pricingResultManager.getByProduct(product.getId());
			Assert.assertEquals(data, results);
		} finally {
			for (PricingResult pricingResult : results) {
				pricingResultManager.remove(pricingResult);
			}
			if (asset.getId() != null) {
				assetManager.remove(asset);
			}
			if (product.getId() != null) {
				productManager.remove(product);
			}
		}
	}

	@Test
	public void allForProductAndsAssetTest() {

		Currency currency = currencyManager.getBy("RUB");
		Product product = createProduct();
		Asset asset = createAsset(product);

		List<PricingResult> results = new ArrayList<>();
		try {
			productManager.save(product);
			assetManager.save(asset);
			for (int i = 0; i < 3; i++) {
				PricingResult pricingResult = createPricingResult(asset, currency, product);
				pricingResultManager.save(pricingResult);
				results.add(pricingResult);
			}
			Collections.sort(results, (o1, o2) -> o1.getId().compareTo(o2.getId()));
			List<PricingResult> data = pricingResultManager.getByProductAndAsset(product.getId(), asset.getId());
			Assert.assertEquals(data, results);
		} finally {
			for (PricingResult pricingResult : results) {
				pricingResultManager.remove(pricingResult);
			}
			if (asset.getId() != null) {
				assetManager.remove(asset);
			}
			if (product.getId() != null) {
				productManager.remove(product);
			}
		}
	}



	@Test
	public void checkSelectByProductInvestIds() {

        int count = 0;
        for (Product product: productManager.all()) {

            List<Long> investIds = pricingResultManager.getPreparedInvestIds(product.getId());
            count = count < investIds.size() ? investIds.size(): count;
        }
        Assert.assertTrue(count > 0);
	}

}
