package ru.open.spc.dao.manager.impl;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.open.spc.model.auth.UserAuthDetails;

import javax.naming.directory.Attributes;
import java.util.List;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationDaoContext.xml"})
public class LdapTest {

	@Autowired
	private LdapTemplate ldapTemplate;

	@Test
	public void ldapTest() {
		final List<UserAuthDetails> search = ldapTemplate.search(LdapQueryBuilder.query()
						.where("sAMAccountName").is("velcheva"),
				(Attributes attrs) -> {
					UserAuthDetails userDetails = new UserAuthDetails();
                    log.debug("attrs {}", attrs);
					return userDetails;
				});
	}


}
