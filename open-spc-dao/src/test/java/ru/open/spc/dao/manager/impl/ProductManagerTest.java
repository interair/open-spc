package ru.open.spc.dao.manager.impl;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.model.Product;
import ru.open.spc.model.manager.test.AbstractSpringTest;

public class ProductManagerTest extends AbstractSpringTest {

	@Autowired
	private ProductManager productManager;

	@Test
	public void newProductTest() {
		Product product = createProduct();
		try {
			productManager.save(product);
			Assert.assertNotNull(product.getId());
		} finally {
			if (product.getId() != null) {
				productManager.remove(product);
			}
		}
	}

	@Test
	public void updateProductTest() {
		Product product = createProduct();
		try {
			productManager.save(product);
			product.setName(product.getName() + 1);
			productManager.save(product);
			Product updatedProduct = productManager.getById(product.getId());
			Assert.assertEquals(product.getName(), updatedProduct.getName());
		} finally {
			if (product.getId() != null) {
				productManager.remove(product);
			}
		}
	}

}

