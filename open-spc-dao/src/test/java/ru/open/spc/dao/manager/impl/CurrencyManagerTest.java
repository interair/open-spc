package ru.open.spc.dao.manager.impl;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.open.spc.dao.manager.CurrencyManager;
import ru.open.spc.model.Currency;
import ru.open.spc.model.manager.test.AbstractSpringTest;

public class CurrencyManagerTest extends AbstractSpringTest {

	@Autowired
	private CurrencyManager currencyManager;

	@Test
	public void selectByCodeTest() {
		Currency currency = currencyManager.getBy("RUB");
		Assert.assertNotNull(currency);
	}

	@Test
	public void selectByIdentityTest() {
		Currency currency = currencyManager.getBy("RUB");
		Assert.assertNotNull(currency);
		currency = currencyManager.getById(currency.getId());
		Assert.assertNotNull(currency);
	}
}
