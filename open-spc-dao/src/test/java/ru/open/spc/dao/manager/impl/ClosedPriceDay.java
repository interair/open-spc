package ru.open.spc.dao.manager.impl;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.dao.mapper.ClosedPriceMapper;
import ru.open.spc.model.Asset;
import ru.open.spc.model.AssetClosePrice;
import ru.open.spc.model.Product;
import ru.open.spc.model.manager.test.AbstractSpringTest;

import java.time.LocalDate;
import java.util.Map;

public class ClosedPriceDay extends AbstractSpringTest {


    @Autowired
    private ProductManager productManager;

    @Autowired
    private AssetManager assetManager;

    @Autowired
    private ClosedPriceMapper closedPriceMapper;

    @Test
    public void AddClosedPriceTest() {

        Product product = createProduct();
        Asset asset = createAsset(product);
        Asset assetNew = createAsset(product);
        try {
            productManager.save(product);
            try {
                asset.setId(+50L);
                assetNew.setId(+100L);
                closedPriceMapper.insert(asset.getId(), asset.getPointPrice(), LocalDate.now());
                closedPriceMapper.insert(assetNew.getId(), asset.getPointPrice(), LocalDate.now());
                Assert.assertNotNull(closedPriceMapper.getAllAssetsForToday());
                Assert.assertEquals(closedPriceMapper.getAllAssetsForToday().size(), 2);

            } finally {
                final Map<Long, AssetClosePrice> allAssetsForToday = closedPriceMapper.getAllAssetsForToday();
                allAssetsForToday.keySet().forEach(closedPriceMapper::removeByAssertId);
            }
        } finally {
            if (product != null && product.getId() != null) {
                productManager.remove(product);
            }
        }
    }

    @Test(expected = DuplicateKeyException.class)
    public void CheckClosedPriceTest() {

        Product product = createProduct();
        Asset asset = createAsset(product);
        asset.setId(100L);
        Asset assetNew = createAsset(product);
        assetNew.setId(100L);
        try {
            productManager.save(product);
            try {
                closedPriceMapper.insert(asset.getId(), asset.getPointPrice(), LocalDate.now());
                closedPriceMapper.insert(assetNew.getId(), asset.getPointPrice(), LocalDate.now());
                Assert.assertNotNull(closedPriceMapper.getAllAssetsForToday());

            } finally {
                closedPriceMapper.removeByAssertId(assetNew.getId());
                closedPriceMapper.removeByAssertId(asset.getId());

            }
        } finally {
            if (product != null && product.getId() != null) {
                productManager.remove(product);
            }
        }

    }
}
