package ru.open.spc.dao.mapper.type;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import ru.open.spc.model.enums.Direction;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DirectionTypeHandler extends BaseTypeHandler<Direction> {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, Direction parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.code());
	}

	@Override
	public Direction getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return Direction.fromCode(rs.getString(columnName));
	}

	@Override
	public Direction getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return Direction.fromCode(rs.getString(columnIndex));
	}

	@Override
	public Direction getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return Direction.fromCode(cs.getString(columnIndex));
	}
}
