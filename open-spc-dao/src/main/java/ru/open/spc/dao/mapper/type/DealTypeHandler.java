package ru.open.spc.dao.mapper.type;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import ru.open.spc.model.enums.DealType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DealTypeHandler extends BaseTypeHandler<DealType> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, DealType parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i, parameter.code());
    }

    @Override
    public DealType getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return DealType.fromCode(rs.getString(columnName));
    }

    @Override
    public DealType getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return DealType.fromCode(rs.getString(columnIndex));
    }

    @Override
    public DealType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return DealType.fromCode(cs.getString(columnIndex));
    }
}