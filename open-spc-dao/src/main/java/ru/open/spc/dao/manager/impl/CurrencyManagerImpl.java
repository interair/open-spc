package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.CurrencyManager;
import ru.open.spc.dao.mapper.CurrencyMapper;
import ru.open.spc.model.Currency;

@Service
public class CurrencyManagerImpl extends CrudManager<Currency> implements CurrencyManager {

	private CurrencyMapper currencyMapper;

	@Autowired
	public CurrencyManagerImpl(CurrencyMapper currencyMapper) {
		super(currencyMapper);
		this.currencyMapper = currencyMapper;
	}

	@Override
	public Currency getBy(String code) {
		return currencyMapper.getByCode(code);
	}

}
