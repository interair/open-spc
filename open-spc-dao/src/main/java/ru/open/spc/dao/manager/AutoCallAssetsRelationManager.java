package ru.open.spc.dao.manager;

import ru.open.spc.model.AutoCallAssetsRelation;

import java.util.List;

public interface AutoCallAssetsRelationManager extends ExtendedGenericManager<AutoCallAssetsRelation> {
	AutoCallAssetsRelation getRelation(Long left, Long right);

	List<AutoCallAssetsRelation> getAllRelation(Long id);
}
