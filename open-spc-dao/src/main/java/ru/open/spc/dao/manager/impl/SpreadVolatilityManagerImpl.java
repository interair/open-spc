package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.SpreadVolatilityManager;
import ru.open.spc.dao.mapper.SpreadVolatilityMapper;
import ru.open.spc.model.SpreadVolatilityParams;

import java.util.List;

@Service
public class SpreadVolatilityManagerImpl extends CrudManager<SpreadVolatilityParams> implements SpreadVolatilityManager {

    private SpreadVolatilityMapper spreadVolatilityMapper;

    @Autowired
    public SpreadVolatilityManagerImpl(SpreadVolatilityMapper spreadVolatilityMapper) {
        super(spreadVolatilityMapper);
        this.spreadVolatilityMapper = spreadVolatilityMapper;
    }

    @Override
    public List<SpreadVolatilityParams> getSpreadByAsset(Long assetId) {
        return spreadVolatilityMapper.getSpreadByAsset(assetId);
    }

}
