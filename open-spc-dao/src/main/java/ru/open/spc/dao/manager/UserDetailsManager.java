package ru.open.spc.dao.manager;

import ru.open.spc.model.auth.UserAuthDetails;

public interface UserDetailsManager extends ExtendedGenericManager<UserAuthDetails> {

	UserAuthDetails findByUsername(String username);
}
