package ru.open.spc.dao.mapper;

import ru.open.spc.model.auth.Authority;

import java.util.List;

public interface AuthorityMapper {

	List<Authority> getAuthorityListByUser();
}
