package ru.open.spc.dao.util;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class RoundUtil {

	public static BigDecimal round(double value, int prec, RoundingMode roundingMode) {
		return new BigDecimal(value).setScale(prec, roundingMode);
	}

	public static double roundToDouble(double value, int prec, RoundingMode roundingMode) {
		return round(value, prec, roundingMode).doubleValue();
	}

	public static long roundToLong(double value, int prec, RoundingMode roundingMode) {
		return round(value, prec, roundingMode).longValue();
	}

	public static double roundHUP(double d, int i) {
		return roundToDouble(d, i, RoundingMode.HALF_UP);
	}

	public static long roundDn(double d) {
		return roundToLong(d, 0, RoundingMode.DOWN);
	}

	public static long roundUP(Double d) {
		return roundToLong(d, 0, RoundingMode.UP);
	}
}
