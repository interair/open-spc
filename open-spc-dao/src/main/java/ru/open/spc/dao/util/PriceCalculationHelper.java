package ru.open.spc.dao.util;

import ru.open.spc.model.MarketPrice;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class PriceCalculationHelper {
	public static double calculateRealPrice(MarketPrice mp) {
		double result;
		checkForNull(mp);
		if (mp.getBestAsk() > 0) {
			if (mp.getBestBid() > 0) {
				if (mp.getLastPrice() > 0) {
					result = min(max(mp.getLastPrice(), mp.getBestBid()), mp.getBestAsk());
				} else {
					result = (mp.getBestBid() + mp.getBestAsk()) / 2;
				}
			} else {
				if (mp.getLastPrice() > 0) {
					result = min(mp.getLastPrice(), mp.getBestAsk());
				} else {
					result = min(mp.getLastSettlement(), mp.getBestAsk());
				}
			}
		} else {
			if (mp.getLastPrice() > 0) {
				result = max(mp.getLastPrice(), mp.getBestAsk());
			} else {
				result = max(mp.getBestBid(), mp.getLastSettlement());
			}
		}
		return result;
	}

	private static void checkForNull(MarketPrice mp) {
        if (mp == null) {
            mp = new MarketPrice();
        }
		if (mp.getBestAsk() == null) mp.setBestAsk(0d);
		if (mp.getBestBid() == null) mp.setBestBid(0d);
		if (mp.getLastPrice() == null) mp.setLastPrice(0d);
		if (mp.getLastSettlement() == null) mp.setLastSettlement(0d);
	}

}
