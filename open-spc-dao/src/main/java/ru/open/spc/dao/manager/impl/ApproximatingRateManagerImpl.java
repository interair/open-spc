package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.ApproximatingRateManager;
import ru.open.spc.dao.mapper.ApproximatingRateMapper;
import ru.open.spc.model.ApproximatingRate;

import java.util.List;

@Service
public class ApproximatingRateManagerImpl extends CrudManager<ApproximatingRate> implements ApproximatingRateManager {

	private ApproximatingRateMapper rateMapper;

	@Autowired
	public ApproximatingRateManagerImpl(ApproximatingRateMapper rateMapper) {
		super(rateMapper);
		this.rateMapper = rateMapper;
	}

	@Override
	public List<ApproximatingRate> getApproximatingRatesByAsset(Long assetId) {
		return rateMapper.getApproximatingRates(assetId);
	}

}
