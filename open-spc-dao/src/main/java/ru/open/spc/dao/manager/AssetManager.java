package ru.open.spc.dao.manager;

import ru.open.spc.model.Asset;

import java.time.LocalDate;
import java.util.List;

public interface AssetManager extends ExtendedGenericManager<Asset> {

	Asset getBy(Long assetId, Long productId);

	List<Asset> allFor(Long productId);

	List<Asset> allCalculatedForToday(Long productId);

    List<Asset> getByCoefficientFromDate(LocalDate date);

    List<String> getExternalTickerCodes();
}
