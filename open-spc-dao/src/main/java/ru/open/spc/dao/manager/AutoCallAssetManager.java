package ru.open.spc.dao.manager;

import ru.open.spc.model.AutoCallAsset;

public interface AutoCallAssetManager extends ExtendedGenericManager<AutoCallAsset> {}
