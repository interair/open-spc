package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.UserDetailsManager;
import ru.open.spc.dao.mapper.UserDetailsMapper;
import ru.open.spc.model.auth.UserAuthDetails;

@Service
public class UserDetailsManagerImpl extends CrudManager<UserAuthDetails> implements UserDetailsManager {

	private UserDetailsMapper userDetailsMapper;

	@Autowired
	public UserDetailsManagerImpl(UserDetailsMapper userDetailsMapper) {
		super(userDetailsMapper);
		this.userDetailsMapper = userDetailsMapper;
	}

	@Override
	public UserAuthDetails findByUsername(String username) {
		return userDetailsMapper.findByUsername(username);
	}

}
