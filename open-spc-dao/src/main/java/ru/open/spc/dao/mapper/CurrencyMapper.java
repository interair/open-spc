package ru.open.spc.dao.mapper;

import org.apache.ibatis.annotations.*;
import ru.open.spc.model.Currency;

import java.util.List;

public interface CurrencyMapper extends Mapper<Currency> {

	String SELECT_ALL = "SELECT PHACTIVE_ID, CURRENCY_CODE, CURRENCY_NUMBER, CURRENCY_NAME FROM DBO.SPC_CURRENCY ";

	@Select(SELECT_ALL)
	@ResultMap(value = "currencyMap")
	List<Currency> all();

	@Select(SELECT_ALL + "WHERE PHACTIVE_ID = #{id}")
	@ResultMap(value = "currencyMap")
	Currency getById(Long id);

	@Select(SELECT_ALL + "WHERE CURRENCY_CODE = #{code}")
	@ResultMap(value = "currencyMap")
	Currency getByCode(String code);

	@Update("UPDATE DBO.SPC_CURRENCY SET CURRENCY_NAME = #{name}, CURRENCY_CODE = #{code}, CURRENCY_NUMBER=${number} WHERE PHACTIVE_ID = #{id}")
	@Options(flushCache = true)
	void update(Currency currency);

	@Insert("INSERT INTO DBO.SPC_CURRENCY(CURRENCY_NAME, CURRENCY_CODE, CURRENCY_NUMBER) VALUES(#{name}, #{code}, ${number})")
	@Options(useGeneratedKeys = true, keyProperty = "PHACTIVE_ID", flushCache = true)
	@SelectKey(statement = "SELECT @@IDENTITY AS PHACTIVE_ID",
		before = false,
		keyProperty = "id",
		resultType = long.class)
	void insert(Currency currency);

	@Delete("DELETE FROM DBO.SPC_CURRENCY WHERE PHACTIVE_ID = #{id}")
	@Options(flushCache = true)
	void delete(Long id);


}
