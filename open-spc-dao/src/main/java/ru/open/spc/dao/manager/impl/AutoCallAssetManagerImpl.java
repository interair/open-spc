package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import ru.open.spc.dao.manager.AutoCallAssetManager;
import ru.open.spc.dao.manager.AutoCallAssetsRelationManager;
import ru.open.spc.dao.mapper.Mapper;
import ru.open.spc.model.AutoCallAsset;

public class AutoCallAssetManagerImpl extends CrudManager<AutoCallAsset> implements AutoCallAssetManager {

	@Autowired
	private AutoCallAssetsRelationManager autoCallAssetsRelationManager;

	public AutoCallAssetManagerImpl(Mapper<AutoCallAsset> mapper) {
		super(mapper);
	}

	@Override
	public void remove(AutoCallAsset model) {
		autoCallAssetsRelationManager.getAllRelation(model.getId()).forEach(a -> autoCallAssetsRelationManager.remove(a));
		super.remove(model);
	}
}
