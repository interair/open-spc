package ru.open.spc.dao.mapper;

import ru.open.spc.model.SpreadVolatilityParams;

import java.util.List;

public interface SpreadVolatilityMapper extends Mapper<SpreadVolatilityParams> {

	void insert(SpreadVolatilityParams params);

	void update(SpreadVolatilityParams params);

	void delete(Long productId);

	List<SpreadVolatilityParams> getSpreadByAsset(Long assetId);
}
