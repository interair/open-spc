package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.AutoCallAssetsRelationManager;
import ru.open.spc.dao.mapper.AutoCallAssetsRelationMapper;
import ru.open.spc.model.AutoCallAsset;
import ru.open.spc.model.AutoCallAssetsRelation;

import java.util.List;

@Service
public class AutoCallAssetsRelationManagerImpl extends CrudManager<AutoCallAssetsRelation> implements AutoCallAssetsRelationManager {

	private AutoCallAssetsRelationMapper mapper;
	@Autowired
	public AutoCallAssetsRelationManagerImpl(AutoCallAssetsRelationMapper mapper) {
		super(mapper);
		this.mapper = mapper;
	}

	@Override
	public void save(AutoCallAssetsRelation model) {
		AutoCallAsset assetLeft = model.getAssetLeft();
		AutoCallAsset assetRight = model.getAssetRight();

		Long left = assetLeft.getId();
		Long right = assetRight.getId();

		if (right < left) {
			model.setAssetLeft(assetRight);
			model.setAssetRight(assetLeft);
		}
		super.save(model);
	}

	@Override
	public AutoCallAssetsRelation getRelation(Long left, Long right) {
		if (left < right) {
			return mapper.getRelation(left, right);
		}
		return mapper.getRelation(right, left);
	}

	@Override
	public List<AutoCallAssetsRelation> getAllRelation(Long id) {
		return mapper.getAllRelation(id);
	}
}
