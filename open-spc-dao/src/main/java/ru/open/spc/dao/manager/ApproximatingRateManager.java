package ru.open.spc.dao.manager;

import ru.open.spc.model.ApproximatingRate;

import java.util.List;

public interface ApproximatingRateManager extends GenericManager<ApproximatingRate> {
	List<ApproximatingRate> getApproximatingRatesByAsset(Long assetId);
}
