package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.dao.mapper.AssetMapper;
import ru.open.spc.dao.mapper.OptionMapper;
import ru.open.spc.model.Asset;
import ru.open.spc.model.enums.Exchanges;

import java.time.LocalDate;
import java.util.List;

import static ru.open.spc.dao.util.ManagerUtil.checkForExisting;

@Service
public class AssetManagerImpl extends CrudManager<Asset> implements AssetManager {

	private AssetMapper assetMapper;
	@Autowired private ProductManager productManager;
	@Autowired private OptionMapper optionMapper;

	@Autowired
	public AssetManagerImpl(AssetMapper assetMapper) {
		super(assetMapper);
		this.assetMapper = assetMapper;
	}

	@Override
	public Asset getBy(Long assetId, Long productId) {
		return assetMapper.getByAssetAndProduct(assetId, productId);
	}

	@Override
	public void save(Asset asset) {
		super.save(asset);
		productManager.updateProductsAssetLink(asset);
	}

    @Override
	public void remove(Asset asset) {
		if (checkForExisting(asset)) {
			asset.getProducts().clear();
			productManager.updateProductsAssetLink(asset);
			assetMapper.deleteAsset(asset.getId());
		}
	}

	@Override
	public List<Asset> allFor(Long productId) {
		return setPrice(assetMapper.getByProduct(productId));
	}

	@Override
	public List<Asset> allCalculatedForToday(Long productId) {
		return setPrice(assetMapper.getCalculatedFor(productId));
	}

	@Override
	public Asset getById(Long id) {
		return setPrice(assetMapper.getById(id));
	}

   @Override
	public List<Asset> all() {
		return setPrice(assetMapper.all());
	}

	private List<Asset> setPrice(List<Asset> assets) {
        assets.forEach(this::setPrice);
		return assets;
	}

    public List<String> getExternalTickerCodes() {
        return assetMapper.getExternalTickerCodes();
    }

    private Asset setPrice(Asset asset) {    //TODO: remove it from manager and add to mapper
        asset.setAssetPrice(optionMapper.getAssetPriceBy(asset.getBaseTicker(), Exchanges.FORTS.code()));
        return asset;
    }

    public List<Asset> getByCoefficientFromDate(LocalDate date){
        return assetMapper.getByCoefficientFromDate(date);
    }
}
