package ru.open.spc.dao.mapper;

import org.apache.ibatis.annotations.Param;
import ru.open.spc.model.CalculationType;
import ru.open.spc.model.Settings;
import ru.open.spc.model.enums.ProductCode;

public interface SettingsMapper extends Mapper<Settings> {

	Settings getSettings(@Param("productCode") ProductCode productCode, @Param("calculationType") CalculationType calculationType);
}