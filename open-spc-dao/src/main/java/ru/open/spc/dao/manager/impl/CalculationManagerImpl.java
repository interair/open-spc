package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.dao.mapper.OptionMapper;
import ru.open.spc.model.Option;
import ru.open.spc.model.Series;
import ru.open.spc.model.StockAsset;
import ru.open.spc.model.Underlying;
import ru.open.spc.model.enums.Exchanges;
import ru.open.spc.model.enums.OptionType;

import java.time.LocalDate;
import java.util.List;

@Service
public class CalculationManagerImpl implements CalculationManager {

	@Autowired OptionMapper optionMapper;

	@Override
	public List<Option> getStrikesBy(Long id, LocalDate expDate, OptionType optionType) {
		return optionMapper.getStrikesBy(id, expDate, optionType);
	}

	@Override
    public List<Double> getAllStrikesOnDate(Long id, LocalDate date) {
        return optionMapper.getAllStrikesOnDate(id, date);
    }

	@Override
	public Underlying getAssetPriceBy(String ticker) {
		return optionMapper.getAssetPriceBy(ticker, Exchanges.FORTS.code());
	}

	@Override
	public List<LocalDate> getAvailableSeriesDates(LocalDate startDate, LocalDate endDate, Long id) {
		return optionMapper.getAvailableSeriesDates(startDate, endDate, id);
	}

    @Override
    public List<LocalDate> getAvailableSeriesDates(LocalDate startDate, Long assetId) {
        return optionMapper.getAvailableSeriesDatesForPeriod(startDate, assetId);
    }

    @Override
	public List<LocalDate> getAvailableFuturesDates(LocalDate startDate, LocalDate endDate, Long id) {
		return optionMapper.getAvailableFuturesDates(startDate, endDate, id);
	}

	@Override
	public Underlying getFuturesBy(Long id, LocalDate expDate) {
		return optionMapper.getFuturesBy(id, expDate);
	}

	@Override
	public LocalDate getLastFutureDate(Long id) {
		return optionMapper.getLastFutureDate(id);
	}

    @Override
    public LocalDate getNextFutureDate(Long id){
        //ToDo: create sql request for next dateFuture
        return optionMapper.getNextFutureDate(id);
    }

	@Override
	public Series getSeriesBy(Long id, LocalDate expDate) {
		return optionMapper.getSeriesBy(id, expDate);
	}

    @Override
    public List<StockAsset> getAvailableTickers() {
        return optionMapper.getAvailableTickers();
    }

}
