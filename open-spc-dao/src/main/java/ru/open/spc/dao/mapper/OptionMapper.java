package ru.open.spc.dao.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import ru.open.spc.model.Option;
import ru.open.spc.model.Series;
import ru.open.spc.model.StockAsset;
import ru.open.spc.model.Underlying;
import ru.open.spc.model.enums.OptionType;

import java.time.LocalDate;
import java.util.List;

public interface OptionMapper {

	String SELECT_LAST_DATE_FUTURES =
		"SELECT MAX(U.EXPIRE_DATE) AS LAST_DATE " +
			"FROM DBO.BASE_TICKERS_TBL T " +
			"INNER JOIN DBO.UNDERLYINGS_TBL U ON U.BASE_TICKER_ID = T.BASE_TICKER_ID " +
			"WHERE T.BASE_ASSET_ID = #{id} " +
            "AND CAST(U.EXPIRE_DATE AS DATE) <= '2050-01-01' ";//TODO: fix shit in DB

    String SELECT_ALL_TICKERS = "SELECT U.TICKER, U.UNDERLYING_ID, A.FULL_NAME, A.BASE_ASSET_ID FROM DBO.UNDERLYINGS_TBL U " +
            "INNER JOIN DBO.BASE_TICKERS_TBL T ON U.BASE_TICKER_ID = T.BASE_TICKER_ID " +
            "INNER JOIN DBO.BASE_ASSETS_TBL A ON A.BASE_ASSET_ID = T.BASE_ASSET_ID " +
            "ORDER BY U.TICKER";
    //ToDo: create sql request to get NEXT future date
    String SELECT_NEXT_DATE_FUTURES = "";

    List<Option> getStrikesBy(@Param("id") Long id, @Param("expDate") LocalDate expDate, @Param("optionType") OptionType optionType);

	List<Double> getAllStrikesOnDate(@Param("id") Long id, @Param("date") LocalDate date);

	Series getSeriesBy(@Param("id") Long id, @Param("expDate") LocalDate expDate);

	Underlying getAssetPriceBy(@Param("ticker") String ticker, @Param("exchangeId") int exchangeId);

	Underlying getFuturesBy(@Param("id") Long id, @Param("expDate") LocalDate expDate);

	@Select(SELECT_LAST_DATE_FUTURES)
	@Result(column = "last_date")
    LocalDate getLastFutureDate(Long id);

    @Select(SELECT_NEXT_DATE_FUTURES)
    @Result(column = "last_date")
    LocalDate getNextFutureDate(Long id);

	List<LocalDate> getAvailableSeriesDates(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate, @Param("id") Long id);

	List<LocalDate> getAvailableFuturesDates(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate, @Param("id") Long id);

    List<LocalDate> getAvailableSeriesDatesForPeriod(@Param("startDate") LocalDate startDate, @Param("id") Long assetId);

    @Select(SELECT_ALL_TICKERS)
    @Results({
        @Result(column = "UNDERLYING_ID", property = "id", id = true),
        @Result(column = "TICKER", property = "ticker"),
        @Result(column = "BASE_ASSET_ID", property = "assetId"),
        @Result(column = "FULL_NAME", property = "name")
    })
    List<StockAsset> getAvailableTickers();

}
