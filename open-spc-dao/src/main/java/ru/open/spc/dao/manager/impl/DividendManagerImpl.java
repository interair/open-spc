package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.DividendManager;
import ru.open.spc.dao.mapper.DividendMapper;
import ru.open.spc.model.Dividend;

import java.util.List;

@Service
public class DividendManagerImpl extends CrudManager<Dividend> implements DividendManager {

	private DividendMapper dividendMapper;

	@Autowired
	public DividendManagerImpl(DividendMapper dividendMapper) {
		super(dividendMapper);
		this.dividendMapper = dividendMapper;
	}

	@Override
	public List<Dividend> getDividendsByAsset(Long assetId) {
		return dividendMapper.getDividends(assetId);
	}

}
