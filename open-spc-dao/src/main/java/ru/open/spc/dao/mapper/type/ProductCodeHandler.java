package ru.open.spc.dao.mapper.type;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import ru.open.spc.model.enums.ProductCode;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductCodeHandler extends BaseTypeHandler<ProductCode> {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, ProductCode parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.code());
	}

	@Override
	public ProductCode getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return ProductCode.fromCode(rs.getString(columnName));
	}

	@Override
	public ProductCode getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return ProductCode.fromCode(rs.getString(columnIndex));
	}

	@Override
	public ProductCode getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return ProductCode.fromCode(cs.getString(columnIndex));
	}

}
