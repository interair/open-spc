package ru.open.spc.dao.mapper;

import java.util.List;

public interface Mapper<T> {

	void insert(T t);

	void update(T t);

	List<T> all();

	T getById(Long id);

	void delete(Long id);
}
