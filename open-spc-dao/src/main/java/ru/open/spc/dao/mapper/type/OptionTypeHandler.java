package ru.open.spc.dao.mapper.type;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import ru.open.spc.model.enums.OptionType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OptionTypeHandler extends BaseTypeHandler<OptionType> {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, OptionType parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.code());
	}

	@Override
	public OptionType getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return OptionType.fromCode(rs.getInt(columnName));
	}

	@Override
	public OptionType getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return OptionType.fromCode(rs.getInt(columnIndex));
	}

	@Override
	public OptionType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return OptionType.fromCode(cs.getInt(columnIndex));
	}
}
