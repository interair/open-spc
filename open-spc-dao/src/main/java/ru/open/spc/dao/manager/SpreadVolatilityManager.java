package ru.open.spc.dao.manager;

import ru.open.spc.model.SpreadVolatilityParams;

import java.util.List;

public interface SpreadVolatilityManager extends GenericManager<SpreadVolatilityParams> {

    List<SpreadVolatilityParams> getSpreadByAsset(Long assetId);

}
