package ru.open.spc.dao.mapper.type;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BooleanTypeHandler extends BaseTypeHandler<Boolean> {

	private final static String YES_VALUE = "Y";
	private final static String NO_VALUE = "N";

	@Override
	public void setParameter(PreparedStatement ps, int i, Boolean parameter, JdbcType jdbcType) throws SQLException {
		setNonNullParameter(ps, i, parameter, jdbcType);
	}

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, Boolean parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, Boolean.TRUE.equals(parameter) ? YES_VALUE : NO_VALUE);
	}

	@Override
	public Boolean getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return YES_VALUE.equals(rs.getString(columnName)) ? Boolean.TRUE : Boolean.FALSE;
	}

	@Override
	public Boolean getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return YES_VALUE.equals(rs.getString(columnIndex)) ? Boolean.TRUE : Boolean.FALSE;
	}

	@Override
	public Boolean getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return YES_VALUE.equals(cs.getString(columnIndex)) ? Boolean.TRUE : Boolean.FALSE;
	}

}
