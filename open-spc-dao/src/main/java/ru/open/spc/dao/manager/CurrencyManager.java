package ru.open.spc.dao.manager;

import ru.open.spc.model.Currency;

public interface CurrencyManager extends ExtendedGenericManager<Currency> {

	Currency getBy(String code);

}
