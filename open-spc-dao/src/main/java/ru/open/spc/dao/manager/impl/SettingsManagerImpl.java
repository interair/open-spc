package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.SettingsManager;
import ru.open.spc.dao.mapper.SettingsMapper;
import ru.open.spc.model.CalculationType;
import ru.open.spc.model.Settings;
import ru.open.spc.model.enums.ProductCode;

@Service
public class SettingsManagerImpl extends CrudManager<Settings> implements SettingsManager {

	private SettingsMapper settingsMapper;

	@Autowired
	public SettingsManagerImpl(SettingsMapper mapper) {
		super(mapper);
		this.settingsMapper = mapper;
	}

	@Override
	public Settings getSettingsBy(ProductCode productCode, CalculationType calculationType) {
		return settingsMapper.getSettings(productCode, calculationType);
	}

	@Override
	public void save(Settings model) {
		Settings settingsBy = getSettingsBy(model.getProductCode(), model.getCalculationType());
		if (settingsBy == null) {
			insert(model);
		} else {
			update(model);
		}
	}
}
