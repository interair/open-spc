package ru.open.spc.dao.manager;

import ru.open.spc.model.common.Identifiable;

import java.util.List;

public interface ExtendedGenericManager<T extends Identifiable> extends GenericManager<T> {

    List<T> all();

    T getById(Long id);

}
