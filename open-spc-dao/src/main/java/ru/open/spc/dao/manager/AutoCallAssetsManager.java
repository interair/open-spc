package ru.open.spc.dao.manager;

import ru.open.spc.model.AutoCallAsset;

public interface AutoCallAssetsManager extends ExtendedGenericManager<AutoCallAsset> {
}
