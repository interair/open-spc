package ru.open.spc.dao.mapper;

import ru.open.spc.model.Dividend;

import java.util.List;

public interface DividendMapper extends Mapper<Dividend> {

	List<Dividend> getDividends(Long assetId);
}
