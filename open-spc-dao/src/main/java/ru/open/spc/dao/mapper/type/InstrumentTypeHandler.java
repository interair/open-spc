package ru.open.spc.dao.mapper.type;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import ru.open.spc.model.enums.InstrumentType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class InstrumentTypeHandler extends BaseTypeHandler<InstrumentType> {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, InstrumentType instrumentType, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, instrumentType.code());
	}

	@Override
	public InstrumentType getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return InstrumentType.fromCode(rs.getInt(columnName));
	}

	@Override
	public InstrumentType getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return InstrumentType.fromCode(rs.getInt(columnIndex));
	}

	@Override
	public InstrumentType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return InstrumentType.fromCode(cs.getInt(columnIndex));
	}
}