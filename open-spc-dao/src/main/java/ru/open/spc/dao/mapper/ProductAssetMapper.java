package ru.open.spc.dao.mapper;

import org.apache.ibatis.annotations.CacheNamespaceRef;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

@CacheNamespaceRef(value = ProductMapper.class)
public interface ProductAssetMapper {

	@Insert("INSERT INTO DBO.SPC_PRODUCT_ASSET_LINK (ASSET_ID, PRODUCT_ID) VALUES (#{assetId}, #{productId})")
	void insert(@Param("productId") Long productId, @Param("assetId") Long assetId);

	@Insert("DELETE FROM DBO.SPC_PRODUCT_ASSET_LINK WHERE PRODUCT_ID = #{productId} AND ASSET_ID = #{assetId}")
	void delete(@Param("productId") Long productId, @Param("assetId") Long assetId);

	@Insert("DELETE FROM DBO.SPC_PRODUCT_ASSET_LINK WHERE ASSET_ID = #{assetId}")
	void deleteByAsset(Long assetId);

	@Insert("DELETE FROM DBO.SPC_PRODUCT_ASSET_LINK WHERE PRODUCT_ID = #{productId}")
	void deleteByProduct(Long productId);
}
