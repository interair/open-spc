package ru.open.spc.dao.manager;

import ru.open.spc.model.CalculationType;
import ru.open.spc.model.Settings;
import ru.open.spc.model.enums.ProductCode;

public interface SettingsManager extends ExtendedGenericManager<Settings> {

	Settings getSettingsBy(ProductCode productCode, CalculationType calculationType);

}
