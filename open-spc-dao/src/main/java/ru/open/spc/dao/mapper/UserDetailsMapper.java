package ru.open.spc.dao.mapper;

import ru.open.spc.model.auth.UserAuthDetails;

public interface UserDetailsMapper extends Mapper<UserAuthDetails> {

	UserAuthDetails findByUsername(String username);

}
