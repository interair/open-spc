package ru.open.spc.dao.manager;

import ru.open.spc.model.PricingResultQuery;
import ru.open.spc.model.result.PricingResult;

import java.util.List;

public interface PricingResultManager extends ExtendedGenericManager<PricingResult> {

	void remove(PricingResult pricingResult, boolean all);

	void remove(PricingResult pricingResult);

    List<Long> getPreparedInvestIds(Long productId);

	List<PricingResult> getByProduct(Long productId);

	List<PricingResult> getByProductAndAsset(Long productId, Long assetId);

	List<PricingResult> getBySearchModel(PricingResultQuery param);

    List<PricingResult> getActiveInvestIdeas();
}
