package ru.open.spc.dao.manager;

import ru.open.spc.model.Dividend;

import java.util.List;

public interface DividendManager extends GenericManager<Dividend> {
	List<Dividend> getDividendsByAsset(Long assetId);
}
