package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.PricingResultManager;
import ru.open.spc.dao.mapper.PricingResultMapper;
import ru.open.spc.model.PricingResultQuery;
import ru.open.spc.model.result.PricingResult;

import java.util.List;

@Service
public class PricingResultManagerImpl extends CrudManager<PricingResult> implements PricingResultManager {

	private PricingResultMapper pricingResultMapper;

	@Autowired
	public PricingResultManagerImpl(PricingResultMapper pricingResultMapper) {
		super(pricingResultMapper);
		this.pricingResultMapper = pricingResultMapper;
	}

	@Override
	public void save(PricingResult model) {
		checkModel(model);
        super.save(model);
	}

	@Override
	public List<PricingResult> getByProduct(Long productId) {
		return pricingResultMapper.getByProduct(productId);
	}

	@Override
	public List<PricingResult> getByProductAndAsset(Long productId, Long assetId) {
		return pricingResultMapper.getByProductAndAsset(productId, assetId);
	}

	@Override
	public void remove(PricingResult pricingResult, boolean all) {
		checkModel(pricingResult);
		if (all) {
			pricingResultMapper.deleteByProduct(pricingResult.getProduct().getId());
		} else {
			pricingResultMapper.deleteByProductAndAsset(pricingResult.getProduct().getId(), pricingResult.getAsset().getId());
		}
	}

    private void checkModel(PricingResult model) {
//		if (model.getAsset() == null || model.getAsset().getId() == null) {
//			throw new IllegalArgumentException("Asset is not set");
//		}
		if (model.getProduct() == null || model.getProduct().getId() == null) {
			throw new IllegalArgumentException("Product is not set");
		}
	}

    @Override
    public List<Long> getPreparedInvestIds(Long productId) {
        return pricingResultMapper.getPreparedInvestIds(productId);
    }

	@Override
	public List<PricingResult> getBySearchModel(PricingResultQuery param) {
		return pricingResultMapper.getBySearchModel(param);
	}

    @Override
    public List<PricingResult> getActiveInvestIdeas() {
        return pricingResultMapper.getActiveInvestIdeas();
    }
}
