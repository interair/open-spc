package ru.open.spc.dao.manager;

import ru.open.spc.model.Asset;
import ru.open.spc.model.Product;

public interface ProductManager extends ExtendedGenericManager<Product> {

	void updateProductsAssetLink(Asset asset);
}
