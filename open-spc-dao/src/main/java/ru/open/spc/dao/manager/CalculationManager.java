package ru.open.spc.dao.manager;

import ru.open.spc.model.Option;
import ru.open.spc.model.Series;
import ru.open.spc.model.StockAsset;
import ru.open.spc.model.Underlying;
import ru.open.spc.model.enums.OptionType;

import java.time.LocalDate;
import java.util.List;

public interface CalculationManager {

	List<Option> getStrikesBy(Long assetId, LocalDate expDate, OptionType optionType);

	List<Double> getAllStrikesOnDate(Long assetId, LocalDate date);

	Underlying getAssetPriceBy(String ticker);

	List<LocalDate> getAvailableSeriesDates(LocalDate startDate, LocalDate endDate, Long assetId);

    List<LocalDate> getAvailableSeriesDates(LocalDate startDate, Long assetId);

	Underlying getFuturesBy(Long id, LocalDate expDate);

	List<LocalDate> getAvailableFuturesDates(LocalDate startDate, LocalDate endDate, Long assetId);

    LocalDate getLastFutureDate(Long assetId);

	Series getSeriesBy(Long assetId, LocalDate expDate);

    List<StockAsset> getAvailableTickers();

    //TODO: implement it
    LocalDate getNextFutureDate(Long id);

}
