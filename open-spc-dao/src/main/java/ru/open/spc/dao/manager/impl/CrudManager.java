package ru.open.spc.dao.manager.impl;

import ru.open.spc.dao.manager.GenericManager;
import ru.open.spc.dao.mapper.Mapper;
import ru.open.spc.model.common.Identifiable;

import java.util.List;

import static ru.open.spc.dao.util.ManagerUtil.checkForExisting;

public abstract class CrudManager<T extends Identifiable> implements GenericManager<T> {

    private Mapper<T> mapper;

    public CrudManager(Mapper<T> mapper) {
        this.mapper = mapper;
    }

    public void save(T model) {
        if (checkForExisting(model)) {
            update(model);
        } else {
            insert(model);
        }
    }

    public void insert(T model) {
        mapper.insert(model);

    }

    public void update(T model) {
        mapper.update(model);
    }

    public void remove(T model) {
        if (checkForExisting(model)){
            mapper.delete(model.getId());
        }
    }

    public T getById(Long id) {
        return mapper.getById(id);
    }

    public List<T> all() {
        return mapper.all();
    }
}
