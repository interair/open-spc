package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.dao.mapper.ProductAssetMapper;
import ru.open.spc.dao.mapper.ProductMapper;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Product;

import static ru.open.spc.dao.util.ManagerUtil.checkForExisting;

@Service
public class ProductManagerImpl extends CrudManager<Product> implements ProductManager {

	private ProductMapper productMapper;
	@Autowired private ProductAssetMapper productAssetMapper;

	@Autowired
	public ProductManagerImpl(ProductMapper productMapper) {
		super(productMapper);
		this.productMapper = productMapper;
	}

	@Override
	public void save(Product product) {
		super.save(product);
		productAssetMapper.deleteByProduct(product.getId());
		for (Asset asset : product.getAssets()) {
			productAssetMapper.insert(product.getId(), asset.getId());
		}
	}

    @Override
	public void remove(Product product) {
		if (checkForExisting(product)) {
			productAssetMapper.deleteByProduct(product.getId());
			productMapper.delete(product.getId());
		}
	}

	@Override
	public void updateProductsAssetLink(Asset asset) {
		if (checkForExisting(asset)) {
			productAssetMapper.deleteByAsset(asset.getId());
			for (Product product : asset.getProducts()) {
				productAssetMapper.insert(product.getId(), asset.getId());
			}
		}
	}
}
