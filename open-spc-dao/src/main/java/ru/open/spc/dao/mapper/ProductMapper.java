package ru.open.spc.dao.mapper;

import ru.open.spc.model.Product;

import java.util.List;

public interface ProductMapper extends Mapper<Product> {

	List<Product> getByAsset(Long assetId);

}
