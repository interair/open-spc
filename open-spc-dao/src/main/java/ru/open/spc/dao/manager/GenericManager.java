package ru.open.spc.dao.manager;

import ru.open.spc.model.common.Identifiable;

import java.util.List;

public interface GenericManager<T extends Identifiable> {

	void save(T model);

	void remove(T model);

    default void remove(List<T> list) {
        list.forEach(this :: remove);
    }

    default void save(List<T> list) {
        list.forEach(this :: save);
    }

}
