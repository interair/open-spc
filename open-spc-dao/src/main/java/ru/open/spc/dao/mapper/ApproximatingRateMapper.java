package ru.open.spc.dao.mapper;

import ru.open.spc.model.ApproximatingRate;

import java.util.List;

public interface ApproximatingRateMapper extends Mapper<ApproximatingRate>{

	List<ApproximatingRate> getApproximatingRates(Long assetId);
}
