package ru.open.spc.dao.mapper;

import org.apache.ibatis.annotations.Param;
import ru.open.spc.model.AutoCallAssetsRelation;

import java.util.List;

public interface AutoCallAssetsRelationMapper extends Mapper<AutoCallAssetsRelation> {

	AutoCallAssetsRelation getRelation(@Param("left") Long left, @Param("right") Long right);

	List<AutoCallAssetsRelation> getAllRelation(Long id);
}
