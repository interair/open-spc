package ru.open.spc.dao.util;

import java.time.*;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static java.time.LocalDateTime.of;
import static java.time.LocalDateTime.ofInstant;
import static java.time.ZoneId.systemDefault;
import static java.time.temporal.ChronoUnit.*;

public class DateUtil {

	private static final double MILLISECONDS_IN_DAY = 1000 * 60 * 60 * 24d;

	public static <T> List<T> getEmptyRes() {
		return Collections.<T>emptyList();
	}

    public static double countDaysBetweenABS(LocalDate start, LocalDateTime end) {
        final LocalDateTime startDateTime = of(start, LocalTime.MIDNIGHT);
        return countDaysBetweenABS(startDateTime, end);
    }

    public static double countDaysBetweenAbsIgnoreOneDay(LocalDate start, LocalDate end) {
        if (DAYS.between(start, end) == 0) {
            return 0d;
        }
        return Math.abs(countDaysBetween(start, end));
    }

	public static double countDaysBetweenABS(LocalDateTime start, LocalDateTime end) {
		return Math.abs((MILLIS.between(start, end)) / MILLISECONDS_IN_DAY);
	}

	public static double countDaysBetween(LocalDate start, LocalDate end) {
		return DAYS.between(start, end);
	}

	public static LocalDate getPreviousDate(LocalDate date) {
		return getDateWithShift(date, -1, true);
	}

	public static LocalDate getNextDate(LocalDate date) {
		return getDateWithShift(date, 1, true);
	}

	public static LocalDate getCalendarPreviousDate(LocalDate date) {
		return getDateWithShift(date, -1, false);
	}

	public static LocalDate getCalendarNextDate(LocalDate date) {
		return getDateWithShift(date, 1, false);
	}

	public static LocalDate addMonths(LocalDate date, int months) {
        return date.plus(months, MONTHS);
	}

	public static LocalDate getDateWithShift(LocalDate date, long shift, boolean isExcludeWeekends) {
        LocalDate resultDate = date.plusDays(shift);
		if (isExcludeWeekends) {
			if (resultDate.get(ChronoField.DAY_OF_WEEK) > 5) {
                if (shift < 0) {
                    TemporalAdjuster adj = TemporalAdjusters.previous(DayOfWeek.FRIDAY);
                    resultDate = resultDate.with(adj);
                } else {
                    TemporalAdjuster adj = TemporalAdjusters.next(DayOfWeek.MONDAY);
                    resultDate = resultDate.with(adj);
                }
            }
		}
		return resultDate;
	}

	public static LocalDateTime setHours(LocalDateTime date, int hours, int minutes) {
		if (date == null) {
			return null;
		}
		return date.with(LocalTime.MIDNIGHT).plus(hours, HOURS).plus(minutes, MINUTES);
	}

    public static LocalDateTime setHours(LocalDate date, int hours, int minutes) {
        if (date == null) {
            return null;
        }
        return date.atStartOfDay().plus(hours, HOURS).plus(minutes, MINUTES);
    }

    public static Date toDate(LocalDateTime dateTime) {
        ZonedDateTime zdt = dateTime.atZone(ZoneId.systemDefault());
        return Date.from(zdt.toInstant());
    }

    public static Date toDate(LocalDate date) {
        return toDate(date.atTime(LocalTime.MIDNIGHT));
    }

	public static boolean compareDatesWithoutTime(LocalDate afterDate, LocalDate beforeDate) {
		return beforeDate == null || afterDate == null || !afterDate.isBefore(beforeDate);
	}

	public static boolean isDateBetweenInclude(LocalDate checkedDate, LocalDate dateFrom, LocalDate dateTo) {
		return compareDatesWithoutTime(checkedDate, dateFrom) &&
			compareDatesWithoutTime(dateTo, checkedDate);
	}

    public static boolean isDateBetween(LocalDate checkedDate, LocalDate dateFrom, LocalDate dateTo) {
        return checkedDate.isAfter(dateFrom) &&
            checkedDate.isBefore(dateTo);
    }

    public static LocalDateTime toLocalDateTime(Date date) {
        if (date == null) {
            return null;
        }
        return ofInstant(date.toInstant(), systemDefault());
    }

    public static LocalDateTime toLocalDateTime(LocalDate date) {
        if (date == null) {
            return null;
        }
        return of(date, LocalTime.MIDNIGHT);
    }

    public static LocalDate toLocalDate(Date date) {
        if (date == null) {
            return null;
        }
        return toLocalDateTime(date).toLocalDate();
    }

}
