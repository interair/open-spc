package ru.open.spc.dao.mapper.type;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import ru.open.spc.model.enums.OptionStyle;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OptionStyleTypeHandler extends BaseTypeHandler<OptionStyle> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, OptionStyle parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i, parameter.code());
    }

    @Override
    public OptionStyle getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return OptionStyle.fromCode(rs.getString(columnName));
    }

    @Override
    public OptionStyle getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return OptionStyle.fromCode(rs.getString(columnIndex));
    }

    @Override
    public OptionStyle getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return OptionStyle.fromCode(cs.getString(columnIndex));
    }
}