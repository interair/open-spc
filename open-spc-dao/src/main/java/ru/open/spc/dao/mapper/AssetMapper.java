package ru.open.spc.dao.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;
import ru.open.spc.model.Asset;

import java.time.LocalDate;
import java.util.List;

public interface AssetMapper extends Mapper<Asset> {

	void deleteAsset(Long id);

	Asset getByAssetAndProduct(@Param("assetId") Long assetId, @Param("productId") Long productId);

	List<Asset> getByProduct(Long productId);

	List<Asset> getCalculatedFor(Long productId);

    List<Asset> getByCoefficientFromDate(LocalDate date);

    @Select("select Dop from [directum].[dbo].AllBaseAssets order by Dop")
    @Result(column = "Dop")
    List<String> getExternalTickerCodes();

	Long getOuterAssetID(String baseTicker);
}
