package ru.open.spc.dao.util;

import ru.open.spc.model.common.Identifiable;

public class ManagerUtil {
	public static boolean checkForExisting(Identifiable identifiable) {
		return (identifiable.getId() != null && identifiable.getId() > 0);
	}
}
