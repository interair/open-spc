package ru.open.spc.dao.mapper.type;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import ru.open.spc.model.CalculationType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CalculationTypeHandler extends BaseTypeHandler<CalculationType> {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, CalculationType parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.name());
	}

	@Override
	public CalculationType getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return CalculationType.valueOf(rs.getString(columnName));
	}

	@Override
	public CalculationType getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return CalculationType.valueOf(rs.getString(columnIndex));
	}

	@Override
	public CalculationType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return CalculationType.valueOf(cs.getString(columnIndex));
	}

}