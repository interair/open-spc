package ru.open.spc.dao.manager;

public interface Authenticator {

    /**
     * if exception does not thrown then ok
     * @param userName
     * @param password
     */
    void authenticate(String userName, String password);
}
