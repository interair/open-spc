package ru.open.spc.dao.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;
import ru.open.spc.model.PricingResultQuery;
import ru.open.spc.model.result.PricingResult;

import java.util.List;

public interface PricingResultMapper extends Mapper<PricingResult> {

	void deleteByProduct(Long productId);

	void deleteByProductAndAsset(@Param("productId") Long productId, @Param("assetId") Long assetId);

    @Select("SELECT DISTINCT START_PRICE FROM DBO.SPC_PRICING_RESULT WHERE PRODUCT_ID = #{productId}")
    @Result(column = "PR.PRICING_RESULT_ID")
    List<Long> getPreparedInvestIds(Long productId);

	List<PricingResult> getBySearchModel(PricingResultQuery param);

	List<PricingResult> getByProduct(Long productId);

	List<PricingResult> getByProductAndAsset(@Param("productId") Long productId, @Param("assetId") Long assetId);

    List<PricingResult> getActiveInvestIdeas();
}
