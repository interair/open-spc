package ru.open.spc.dao.mapper.type;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import ru.open.spc.model.enums.AssetUnity;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AssetUnityTypeHandler extends BaseTypeHandler<AssetUnity> {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, AssetUnity parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.code());
	}

	@Override
	public AssetUnity getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return AssetUnity.fromCode(rs.getString(columnName));
	}

	@Override
	public AssetUnity getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return AssetUnity.fromCode(rs.getString(columnIndex));
	}

	@Override
	public AssetUnity getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return AssetUnity.fromCode(cs.getString(columnIndex));
	}
}
