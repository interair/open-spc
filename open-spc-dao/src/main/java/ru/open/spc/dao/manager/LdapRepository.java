package ru.open.spc.dao.manager;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.ContainerCriteria;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.ldap.query.SearchScope;
import ru.open.spc.model.auth.UserAuthDetails;

import javax.naming.directory.Attributes;
import java.util.List;

@Slf4j
public class LdapRepository implements Authenticator {

	@Autowired
	private LdapTemplate ldapTemplate;

	/**
	 * TODO: maybe add ldap side filtering but it very slow up to 1 minutes
	 * Uses for looking new users
	 *
	 * @return list of all users
	 */
	@Cacheable("ldap")
	public List<UserAuthDetails> getAllPersonNames() {
		LdapQuery query = query();
		return ldapTemplate.search(query,
				(Attributes attrs) -> {
					UserAuthDetails userDetails = new UserAuthDetails();
					userDetails.setUsername((String) attrs.get("sAMAccountName").get());
					userDetails.setFullName((String) attrs.get("cn").get());
					userDetails.setTitle((String) attrs.get("title").get());
					userDetails.setDescription((String) attrs.get("streetaddress").get());
					userDetails.setDepartment((String) attrs.get("department").get());
					userDetails.setCompany((String) attrs.get("company").get());
					return userDetails;
				});
	}

	public void authenticate(String userName, String password) {
        LdapQuery query = LdapQueryBuilder.query().base("OU=БД Открытие,DC=open,DC=ru")
            .where("sAMAccountName").is(userName);
		ldapTemplate.authenticate(query, password);
	}

	private ContainerCriteria query() {
		return LdapQueryBuilder.query()
				.base("OU=БД Открытие,DC=open,DC=ru")
				.searchScope(SearchScope.SUBTREE)
				.countLimit(10_000)
				.timeLimit(40_000)
				.where("objectClass").is("person")
				.and("CN").not().is("Computer")
				.and("title").isPresent()
				.and("department").isPresent()
				.and("company").isPresent()
				.and("streetaddress").isPresent()
				.and("sAMAccountName").not().is("sAMAccountName")
				.and("sAMAccountName").isPresent();
	}
}
