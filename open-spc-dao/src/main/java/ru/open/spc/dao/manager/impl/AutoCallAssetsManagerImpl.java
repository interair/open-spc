package ru.open.spc.dao.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.dao.manager.AutoCallAssetsManager;
import ru.open.spc.dao.mapper.AutoCallAssetsMapper;
import ru.open.spc.model.AutoCallAsset;

@Service
public class AutoCallAssetsManagerImpl extends CrudManager<AutoCallAsset> implements AutoCallAssetsManager {

	@Autowired
	public AutoCallAssetsManagerImpl(AutoCallAssetsMapper mapper) {
		super(mapper);
	}

}
