package ru.open.spc.dao.mapper;

import org.apache.ibatis.annotations.*;
import ru.open.spc.model.AssetClosePrice;

import java.time.LocalDate;
import java.util.Map;

@CacheNamespace(implementation = org.mybatis.caches.ehcache.EhcacheCache.class)
public interface ClosedPriceMapper {

	@Insert("INSERT INTO SPC_CLOSED_PRICE_OF_DAY (ASSET_ID, CLOSED_PRICE, DATE_OF_PRICE) " +
        "VALUES (#{assetId}, #{price}, #{date})")
	void insert(@Param("assetId") Long assetId, @Param("price") Double price, @Param("date") LocalDate date);

    @Select("SELECT ASSET_ID AS ID, CLOSED_PRICE AS PRICE FROM SPC_CLOSED_PRICE_OF_DAY WHERE DATE_OF_PRICE = CAST(GETDATE() AS DATE)")
    @MapKey("id")
    Map<Long, AssetClosePrice> getAllAssetsForToday();

    @Delete("DELETE FROM SPC_CLOSED_PRICE_OF_DAY WHERE ASSET_ID = #{assertId}")
    void removeByAssertId(Long assertId);
}
