package ru.open.spc.model.auth;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;


/**
 * By default there are two roles: user and admin - for accessing user management page
 */
@Data
public class Authority implements GrantedAuthority {

    private Long id;
    private String role;

    public Authority(String role) {
        this.role = role;
    }

    public Authority() {
    }

    @Override
    public String getAuthority() {
        return role;
    }

    @Override
    public String toString() {
        return role;
    }
}
