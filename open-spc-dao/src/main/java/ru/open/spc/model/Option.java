package ru.open.spc.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.open.spc.model.enums.OptionType;

@Getter @Setter @ToString(callSuper=true, includeFieldNames=true)
public class Option extends Series {

	private Double strikePrice; //Strike1
	private OptionType optionType; //PUT/CALL

    public boolean equals(Object o) {
        return super.equals(o);
    }

    public int hashCode() {
        return super.hashCode();
    }

}
