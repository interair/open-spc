package ru.open.spc.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Getter @Setter @ToString(callSuper=true, includeFieldNames=true)
public class Series extends Underlying {

    private SmileParams smileParams;
	private Double multiplicator; //рамер лота
    private LocalDate expireDate;

    public boolean equals(Object o) {
        return super.equals(o);
    }

    public int hashCode() {
        return super.hashCode();
    }

}
