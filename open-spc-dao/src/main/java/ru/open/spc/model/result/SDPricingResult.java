package ru.open.spc.model.result;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.ResultVisitor;

@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = true)
@Data public class SDPricingResult extends PricingResult implements StocksDepositPricingResult {

    private Double couponSize; // Размер купона (% годовых)
    private Long investingAmount;

    @Override
    public void applyVisitor(ResultVisitor visitor) {
        visitor.visit(this);
    }
}
