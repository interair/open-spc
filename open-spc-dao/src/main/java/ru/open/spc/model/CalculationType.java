package ru.open.spc.model;

public enum CalculationType {
    SIMPLE, COMPLEX
}
