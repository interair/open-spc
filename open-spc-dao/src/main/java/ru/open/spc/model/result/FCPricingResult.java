package ru.open.spc.model.result;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.open.spc.model.ResultVisitor;

@SuppressWarnings("serial")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Data public class FCPricingResult extends PricingResult implements FixCouponPricingResult {

	private Double maximumEarning; // Максимальная доходность (в % годовых


	@Override
	public void applyVisitor(ResultVisitor visitor) {
		visitor.visit(this);
	}

}
