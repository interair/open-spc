package ru.open.spc.model;

import lombok.Data;

import java.io.Serializable;

@Data public class SmileParams implements Serializable {
	//Smile_params (aka greeks)
	//P.bottom, P.height, P.loc, P.kurt, P.skew

    private Double bottom;
    private Double height;
    private Double loc;
    private Double kurt;
    private Double skew;
    private Double scal;

}
