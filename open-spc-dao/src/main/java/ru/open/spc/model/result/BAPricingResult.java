package ru.open.spc.model.result;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.Currency;
import ru.open.spc.model.ResultVisitor;

@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = true)
@Data public class BAPricingResult extends PricingResult implements BaseAssetPricingResult {

	private Double coefficient; // КУ
	private Double kzk; // КЗК
	private Double strike2Price; // Strike2
    private Double intervalOptionPrice;
    private Currency vzkCurrency;

	@Override
	public void applyVisitor(ResultVisitor visitor) {
		visitor.visit(this);
	}

}
