package ru.open.spc.model.auth;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.open.spc.model.common.Identifiable;
import ru.open.spc.model.common.Named;

import java.util.Collection;
import java.util.Collections;

/**
 * User details almost all fields are filled by LDAP
 * does not contains password!
 */
@Data
@NoArgsConstructor
public class UserAuthDetails implements UserDetails, Identifiable, Named {

	private Long id;
	private String username;
	private String fullName;
	private String description;
	private String title;
	private String department;
	private String company;
	private String role;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.singleton(new Authority(role));
	}

	@Override
	public String getPassword() {
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getName() {
		return username;
	}

	@Override
	public void setName(String name) {
	}
}
