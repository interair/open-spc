package ru.open.spc.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.open.spc.model.common.Named;
import ru.open.spc.model.enums.InstrumentType;

import java.time.LocalDate;

@ToString(callSuper=true, includeFieldNames=true)
@EqualsAndHashCode(callSuper = false, of = {"id"})
@Data public class Underlying extends MarketPrice implements Named {

    private Long id;
    private String name; //ticker
    private LocalDate underlyingDate;
    private InstrumentType instrumentType;

}
