package ru.open.spc.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data public class AssetCoefficient implements Serializable {

	private LocalDate fromDate;
	private List<ApproximatingRate> approximatingRates = new ArrayList<>();
	private List<Dividend> dividends = new ArrayList<>();

}
