package ru.open.spc.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.common.Identifiable;

import java.time.LocalDate;

@EqualsAndHashCode(exclude = {"minVolatilityDate", "minVolatility", "maxVolatilityDate", "maxVolatility"})
@Data public class SpreadVolatilityParams implements Identifiable {

    private Long id;
    private Long assetId;
    private LocalDate minVolatilityDate;
    private Double minVolatility;
    private LocalDate maxVolatilityDate;
    private Double maxVolatility;

}
