package ru.open.spc.model.enums;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum Direction implements StringCodeAware {
	RISING("R"), FALLING("F");

	private final String code;

	private Direction(String code) {
		this.code = code;
	}

	public String code() {
		return code;
	}

	public static Direction fromCode(String code) {
		for (Direction direction : Direction.values()) {
			if (direction.code.equals(code)) {
				return direction;
			}
		}
		return null;
	}
}
