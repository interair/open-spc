package ru.open.spc.model;

import lombok.Data;
import ru.open.spc.model.common.Identifiable;

@Data public class AssetClosePrice implements Identifiable {

   private Long id;
   private Double price;

}
