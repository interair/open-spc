package ru.open.spc.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.common.Identifiable;

import java.time.LocalDate;

@EqualsAndHashCode(exclude = {"assetId", "amount","payDate"})
@Data public class Dividend implements Identifiable {

	private Long id;
	private Long assetId;
	private Double amount;
	private LocalDate payDate;

}
