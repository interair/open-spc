package ru.open.spc.model.enums;

public interface IntCodeAware {
	int code();
}
