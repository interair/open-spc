package ru.open.spc.model.enums;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum AssetType implements IntCodeAware {
	COMMON_STOCK(0),
	MICEX_INDEX(1),
	DOLLAR_RUBLE(2);

	private final int code;

	private AssetType(int code) {
		this.code = code;
	}

	@Override
	public int code() {
		return code;
	}
}
