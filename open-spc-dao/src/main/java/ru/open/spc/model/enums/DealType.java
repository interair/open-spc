package ru.open.spc.model.enums;

public enum  DealType implements StringCodeAware {
    BUY("B"), SELL("S"), LOAN("L");

    private final String code;

    private DealType(String code) {
        this.code = code;
    }

    @Override
    public String code() {
        return code;
    }

    public static DealType fromCode(String code) {
        for (DealType productCode : DealType.values()) {
            if (productCode.code.equals(code)) {
                return productCode;
            }
        }
        return null;
    }
}
