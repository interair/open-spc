package ru.open.spc.model;

import lombok.Data;
import ru.open.spc.model.common.Identifiable;
import ru.open.spc.model.enums.ProductCode;

import java.time.LocalDateTime;

@Data
public class Settings implements Identifiable {

	private Long id;
	private ProductCode productCode;
	private CalculationType calculationType;
	private String className;
	private String data;
	private LocalDateTime date = LocalDateTime.now();

}
