package ru.open.spc.model.result;

import ru.open.spc.model.Currency;

public interface BaseAssetPricingResult {

	Double getCoefficient();
	Double getKzk();
	Double getStrike2Price();
    Currency getVzkCurrency();
}
