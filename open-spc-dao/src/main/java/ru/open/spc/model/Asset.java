package ru.open.spc.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.common.Named;
import ru.open.spc.model.enums.AssetType;
import ru.open.spc.model.enums.AssetUnity;

import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

@SuppressWarnings("serial")
@EqualsAndHashCode(exclude = {"products", "coefficients", "assetPrice", "spreadVolatility"})
@Data public class Asset implements Named {

	private Long id;
    private final StockAsset stockAsset = new StockAsset();
	private AssetUnity unity;
	private Integer multiplier;  //рамер лота
	private Double pointPrice;   //цена единицы актива в валюте
	private Boolean exchange;
	private Integer strikeStep;
	private Double minimumPrice; //Минимальная сумма инвестирования  TODO: rename to minimumVolume
	private Double startPriceOfDay;
	private List<Product> products;
	private Boolean commodity;
	private Boolean basePriceVariative = false; //варировать ли цену БА
    private String externalContractTicker; //тикер для обработки поручения во фронт офисе
	@XmlTransient
	private AssetCoefficient coefficients = new AssetCoefficient();
	@XmlTransient
	private Underlying assetPrice;
	@XmlTransient
	private SpreadVolatilityParams spreadVolatility = new SpreadVolatilityParams();
	private String stateRegistration;
	private String emitter;
	private AssetType assetType;
	private String assetCode;

    @Override
    public void setName(String name) {
        stockAsset.setName(name);
    }

    @Override
    public String getName() {
        return stockAsset.getName();
    }

    public Long getOuterId() {
        return stockAsset.getAssetId();
    }

    public String getBaseTicker() {
        return stockAsset.getTicker();
    }

    public void setBaseTicker(String baseTicker) {
        stockAsset.setTicker(baseTicker);
    }

    @Override
    public String toString() {
        return "Asset{ name=" + stockAsset.getName() +
                ", baseTicker=" + stockAsset.getTicker() +
                ", emitter=" + emitter + ", assetType=" + assetType +
                ", assetCode=" + assetCode + '}';
    }

}
