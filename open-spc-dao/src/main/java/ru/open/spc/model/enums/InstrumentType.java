package ru.open.spc.model.enums;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum InstrumentType implements IntCodeAware {
	SPOT(1), FUTURES(0);

	private final int code;

	private InstrumentType(int code) {
		this.code = code;
	}

	public int code() {
		return code;
	}

	public static InstrumentType fromCode(int code) {
		for (InstrumentType instrumentType : InstrumentType.values()) {
			if (instrumentType.code == code) {
				return instrumentType;
			}
		}
		return null;
	}
}
