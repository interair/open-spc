package ru.open.spc.model.common;

import java.io.Serializable;

public interface Identifiable extends Serializable {

	Long getId();

	void setId(Long id);
}
