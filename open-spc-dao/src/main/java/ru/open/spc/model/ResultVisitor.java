package ru.open.spc.model;

import ru.open.spc.model.result.*;

public interface ResultVisitor {
	void visit(BAPricingResult baPricingResult);

	void visit(FCPricingResult fcPricingResult);

    void visit(SDPricingResult sdPricingResult);

    void visit(SMPricingResult smPricingResult);

	void visit(ACPricingResult acPricingResult);
}
