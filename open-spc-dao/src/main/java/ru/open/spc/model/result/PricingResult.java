package ru.open.spc.model.result;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Currency;
import ru.open.spc.model.Product;
import ru.open.spc.model.ResultVisitor;
import ru.open.spc.model.common.Identifiable;
import ru.open.spc.model.enums.Direction;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

@SuppressWarnings("serial")
@EqualsAndHashCode(of = {"id"})
@Data public abstract class PricingResult implements Identifiable {

	private Long id; // Идентификатор записи
	private Product product; // Продукт
	private Asset asset; // Базовый актив
	private LocalDateTime priceDate; // Дата и время расчета для продукта
	private Currency currency; // Валюта СП
	private LocalDate productStartDate; // Дата начала СП
	private LocalDate productStopDate; // Дата окончания СП (исполнения)
	private Double strikePrice; // Strike1
	private Integer interval; // Интервал
	private Double percentByLoanDeal; // % по сделке займа
	private Direction direction; //buy/sell
	private Double interestRate; //процентная ставка
	private Long productId;
	private Long assetId;
	private String description;
    private Duration timeout; //время жизни расчета
    private Double assetPrice; //цена БА при расчете
    private Double investingVolume;
    private Double optionPrice; //цена опциона по Б/Ш

	public abstract void applyVisitor(ResultVisitor visitor);
}
