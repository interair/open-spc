package ru.open.spc.model.enums;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum OptionStyle implements StringCodeAware{
    EUR("E"), USA("U");
    
    private final String code;

    private OptionStyle(String code) {
        this.code = code;
    }

    @Override
    public String code() {
        return code;
    }

    public static OptionStyle fromCode(String code) {
        for (OptionStyle productCode : OptionStyle.values()) {
            if (productCode.code.equals(code)) {
                return productCode;
            }
        }
        return null;
    }
}
