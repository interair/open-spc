package ru.open.spc.model;

import lombok.Data;
import ru.open.spc.model.common.Named;

@Data public class StockAsset implements Named {

    private Long id = -1L; //underlying_id
    private Long assetId;
    private String ticker;
    private String name;

}
