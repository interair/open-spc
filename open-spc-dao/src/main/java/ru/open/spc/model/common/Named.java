package ru.open.spc.model.common;

public interface Named extends Identifiable {

	String getName();

	void setName(String name);
}
