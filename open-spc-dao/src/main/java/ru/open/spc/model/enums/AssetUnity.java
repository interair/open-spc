package ru.open.spc.model.enums;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum AssetUnity implements StringCodeAware {
	RUB("R"), USD("U"), POINT("P");

	private final String code;

	private AssetUnity(String code) {
		this.code = code;
	}

	public String code() {
		return code;
	}

	public static AssetUnity fromCode(String code) {

		for (AssetUnity assetUnity : AssetUnity.values()) {
			if (assetUnity.code.equals(code)) {
				return assetUnity;
			}
		}
		return null;
	}
}
