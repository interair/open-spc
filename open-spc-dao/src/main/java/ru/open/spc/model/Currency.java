package ru.open.spc.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.common.Identifiable;

@SuppressWarnings("serial")
@EqualsAndHashCode(exclude = {"number", "name"})
@Data public class Currency implements Identifiable {

	private Long id;
	private String code;
	private Long number;
	private String name;

}
