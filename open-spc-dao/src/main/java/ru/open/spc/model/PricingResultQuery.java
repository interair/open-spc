package ru.open.spc.model;


import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data public class PricingResultQuery implements Serializable {

	private LocalDate startDate = LocalDate.now();
	private LocalDate endDate = LocalDate.now();
	private Product product;

	public PricingResultQuery setStartDate(LocalDate startDate) {
		this.startDate = startDate;
		return this;
	}

	public PricingResultQuery setEndDate(LocalDate endDate) {
		this.endDate = endDate;
		return this;
	}

	public PricingResultQuery setProduct(Product product) {
		this.product = product;
		return this;
	}
}
