package ru.open.spc.model.result;

public interface StocksDepositPricingResult {
    Double getCouponSize();
    Long getInvestingAmount();
}
