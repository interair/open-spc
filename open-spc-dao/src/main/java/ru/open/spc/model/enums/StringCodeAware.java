package ru.open.spc.model.enums;

public interface StringCodeAware {
	String code();
}
