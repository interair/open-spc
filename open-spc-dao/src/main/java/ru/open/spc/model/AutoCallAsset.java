package ru.open.spc.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.open.spc.model.common.Named;

import java.util.List;

@SuppressWarnings("serial")
@Data
@ToString(exclude = "autoCallAssetsRelations")
@EqualsAndHashCode(exclude = "autoCallAssetsRelations")
public class AutoCallAsset implements Named {

	private Long id;
	private Asset asset;
	private List<AutoCallAssetsRelation> autoCallAssetsRelations;
	private Double sigma = 0.25;
	private Double initRelativePrice = 100d;
	private Double relativePrice= 100d;

	@Override
	public String getName() {
		if (asset == null) {
			return null;
		}
		return asset.getName();
	}

	@Override
	public void setName(String name) {
	}

	public Double getInitRelativePrice() {
		return initRelativePrice;
	}

	public Double getRelativePrice() {
		return relativePrice;
	}
}
