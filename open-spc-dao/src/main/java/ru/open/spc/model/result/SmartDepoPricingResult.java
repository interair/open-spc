package ru.open.spc.model.result;

public interface SmartDepoPricingResult {

    Double getMaximumEarning();
    Double getStrike2Price();
    Double getKzk();
}
