package ru.open.spc.model.enums;

public enum ProductCode implements StringCodeAware {
	BaseProtect("BP"), FixedCoupon("FC"), StocksDeposit("SD"), SmartDeposit("SM"), AutoCall("AC");

	private final String code;

	private ProductCode(String code) {
		this.code = code;
	}

	@Override
	public String code() {
		return code;
	}

	public static ProductCode fromCode(String code) {
		for (ProductCode productCode : ProductCode.values()) {
			if (productCode.code.equals(code)) {
				return productCode;
			}
		}
		return null;
	}
}
