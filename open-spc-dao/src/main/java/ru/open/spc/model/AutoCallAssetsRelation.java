package ru.open.spc.model;

import lombok.Data;
import ru.open.spc.model.common.Identifiable;

@Data
public class AutoCallAssetsRelation implements Identifiable {
	private Long id;
	private Double coefficient;
	private AutoCallAsset assetLeft; // if og left less then right
	private AutoCallAsset assetRight;

}
