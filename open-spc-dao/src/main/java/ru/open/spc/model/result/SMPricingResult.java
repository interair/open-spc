package ru.open.spc.model.result;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.ResultVisitor;

@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = true)
@Data public class SMPricingResult extends PricingResult implements SmartDepoPricingResult {

    private Double maximumEarning;
    private Double strike2Price; // Strike2
    private Double kzk; // КЗК

    @Override
    public void applyVisitor(ResultVisitor visitor) {
        visitor.visit(this);
    }
}
