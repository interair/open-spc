package ru.open.spc.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.common.Named;
import ru.open.spc.model.enums.ProductCode;

import java.util.List;

@SuppressWarnings("serial")
@EqualsAndHashCode(exclude = {"name", "description", "assets"})
@Data public class Product implements Named {

	private Long id;
	private String name;
	private ProductCode code;
	private String description;
	private List<Asset> assets;

}
