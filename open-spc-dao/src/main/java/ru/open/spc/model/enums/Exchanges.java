package ru.open.spc.model.enums;

public enum Exchanges implements IntCodeAware {
	FORTS(1);

	private final int id;

	private Exchanges(int id) {
		this.id = id;
	}

	public int code() {
		return id;
	}
}
