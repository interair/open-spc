package ru.open.spc.model;

import lombok.Data;
import ru.open.spc.dao.util.PriceCalculationHelper;

@Data public class MarketPrice {

	private Double lastPrice;
	private Double bestAsk;
	private Double bestBid;
	private Double lastSettlement;

	public double getRealPrice() {
		return PriceCalculationHelper.calculateRealPrice(this);
	}

}
