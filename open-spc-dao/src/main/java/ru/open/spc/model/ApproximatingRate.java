package ru.open.spc.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.common.Identifiable;

import java.time.LocalDate;

@EqualsAndHashCode(exclude = {"rate", "fromDate"})
@Data public class ApproximatingRate implements Identifiable {

    private Long id;
    private Long assetId;
    private Double rate;
    private LocalDate fromDate;
}
