package ru.open.spc.model.result;

public interface FixCouponPricingResult {

	Double getMaximumEarning();
}
