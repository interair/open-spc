package ru.open.spc.model.enums;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum OptionType implements IntCodeAware {

	PUT(-1), CALL(1);

	private final int code;

	private OptionType(int code) {
		this.code = code;
	}

	public int code() {
		return code;
	}

	public static OptionType fromCode(int code) {
		for (OptionType optionType : OptionType.values()) {
			if (optionType.code == code) {
				return optionType;
			}
		}
		return null;
	}
}
