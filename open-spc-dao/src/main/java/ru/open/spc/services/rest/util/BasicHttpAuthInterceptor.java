package ru.open.spc.services.rest.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.crypto.codec.Base64;

import java.io.IOException;

public class BasicHttpAuthInterceptor implements ClientHttpRequestInterceptor {
	private final String username;
	private final String password;

	public BasicHttpAuthInterceptor(final String username, final String password) {
		this.username = username;
		this.password = password;
	}

	@Override
	public ClientHttpResponse intercept(final HttpRequest request, final byte[] body,
										final ClientHttpRequestExecution execution) throws IOException {
		final HttpHeaders headers = request.getHeaders();
		headers.add("Authorization", "Basic " + new String(Base64.encode((username + ":" + password).getBytes())));
		return execution.execute(request, body);
	}
}