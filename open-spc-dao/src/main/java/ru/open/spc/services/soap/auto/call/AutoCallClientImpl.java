package ru.open.spc.services.soap.auto.call;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import ru.open.auto.call.GetPricingData;
import ru.open.auto.call.GetPricingDataResponse;

public class AutoCallClientImpl extends WebServiceGatewaySupport implements AutoCallClient {

	public GetPricingDataResponse calculate(GetPricingData getPricingData) {

		return (GetPricingDataResponse) getWebServiceTemplate().marshalSendAndReceive(getPricingData,
				new SoapActionCallback("http://tempuri.org/IAutocallService/GetPricingData"));
	}

}