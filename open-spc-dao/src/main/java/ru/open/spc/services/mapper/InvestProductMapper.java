package ru.open.spc.services.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import ru.open.spc.services.dto.InvestProduct;

import java.util.List;

public interface InvestProductMapper {
    InvestProduct getById(@Param("resultId") Long resultId);

    @Result(column = "pricing_result_id")
    List<Long> getPreparedInvestIds(@Param("productId") Long productId);
}
