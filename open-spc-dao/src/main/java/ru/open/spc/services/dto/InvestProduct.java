package ru.open.spc.services.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.open.spc.model.enums.DealType;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.model.enums.OptionStyle;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@EqualsAndHashCode(of = {"investProductId"})
@Data
public class InvestProduct implements Serializable {
	private Long investProductId;
	private Long productId;
    private String marketPlace; // Торговая площадка
	private Direction direction; //Направление на рост/Падение рынка (аналог Put/Call в терминологии калькулятора)
	private String assetRegNum;  //Номер выпуска ЦБ
	private String assetTicker;  //Тикер БА
	private String assetName;    //Наименование БА
	private Integer paymentCurrency; //Валюта платежа
	private Integer assetCurrency;   //Валюта БА
	private LocalDate productStopDate; //Дата исполнения СП
	private LocalDateTime productCalcDate; //Дата/Время расчетов
	private Double kzk; //Коэф. защиты капитала
	private Double strikePrice; //Пороговая цена 1
	private Double strike2Price; //Пороговая цена 2
	private Integer strikeCurrency; //Валюта пороговой цены
	private Double participationRate; //КУ
	private Double couponSize;  //Размер купона/% ставка (% годовых)
	private DealType dealType;  //Вид сделки СПЗК, УД - покупка; ФК - продажа; ДА - займ
	private OptionStyle optionStyle; //всегда EUR
	private Double assetPrice; //Оценочная стоимость БА
	private Double investingValue; //Минимальная сумма инвестирования
	private Long investingAmount; //Минимальное количество ЦБ
	private Long timeOutInSec;//Время жизни продукта в секундах
	private Boolean isActive; //Признак актуальности инвест идеи
    private String publicId;
    private String classCode;
    private String description;
    private Integer transactionCurrency = 810;  //810 is RUB code
    private Integer vzkCurrency;
}

