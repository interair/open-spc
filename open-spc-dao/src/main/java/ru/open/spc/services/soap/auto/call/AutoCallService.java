package ru.open.spc.services.soap.auto.call;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class AutoCallService {

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("ru.open.auto.call");
		return marshaller;
	}

	@Bean
	@Autowired
	public AutoCallClientImpl autoCallClient(Jaxb2Marshaller marshaller,
	                                     @Value("${auto.call.url:http://bd-vm-autocall.open.ru/AutocallService/AutocallService.svc/Autocall}") String soapUrl) {
		AutoCallClientImpl client = new AutoCallClientImpl();
		client.setDefaultUri(soapUrl);
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

}
