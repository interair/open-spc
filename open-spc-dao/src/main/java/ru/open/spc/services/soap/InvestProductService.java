package ru.open.spc.services.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.open.spc.services.dto.InvestProduct;
import ru.open.spc.services.mapper.InvestProductMapper;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Service("investProductService")
public class InvestProductService {

	@Autowired
	private InvestProductMapper investProductMapper;

	@WebMethod
	public InvestProduct getById(@WebParam(name = "investProductId") Long resultId) {
        return investProductMapper.getById(resultId);
	}
    @WebMethod
    public List<Long> getPreparedInvestIds(@WebParam(name = "productId") Long productId){
        return investProductMapper.getPreparedInvestIds(productId);
    }
}
