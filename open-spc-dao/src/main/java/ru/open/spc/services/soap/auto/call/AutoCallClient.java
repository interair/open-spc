package ru.open.spc.services.soap.auto.call;

import ru.open.auto.call.GetPricingData;
import ru.open.auto.call.GetPricingDataResponse;

public interface AutoCallClient {

	GetPricingDataResponse calculate(GetPricingData getPricingData);
}
