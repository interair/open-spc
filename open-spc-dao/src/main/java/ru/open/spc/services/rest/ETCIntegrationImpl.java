package ru.open.spc.services.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.open.spc.services.dto.FinancialInstrumentQuote;
import ru.open.spc.services.rest.util.BasicHttpAuthInterceptor;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Service
public class ETCIntegrationImpl implements ETCIntegration {

	private static final Logger LOG = LoggerFactory.getLogger(ETCIntegrationImpl.class);

	private final RestTemplate restTemplate = new RestTemplate();

	@Value("${user-name}")
	private String username;

	@Value("${password}")
	private String password;

	@Value("${usdQuotePath}")
	private String usdQuotePath;

    private volatile double currentPrice;

	@PostConstruct
	public void init() {
		restTemplate.setInterceptors(
			Collections.<ClientHttpRequestInterceptor>singletonList(
				new BasicHttpAuthInterceptor(username, password)));
	}

	@Override
	public double getUSDPrice() {
        return currentPrice;
	}

    @Scheduled(fixedDelayString = "${fetchDelay}")
    public void updateUSDPrice() {
        fetchValue();
    }


    private void fetchValue() {
        try {
            FinancialInstrumentQuote quote = restTemplate.getForObject(usdQuotePath, FinancialInstrumentQuote.class);
            LOG.info("GET USD quote {}", quote);
            currentPrice = quote.getValue();
        } catch (Exception e) {
            LOG.error("Ooops... can't GET USD quote", e);
        }
    }

}
