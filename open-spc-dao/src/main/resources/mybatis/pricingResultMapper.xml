<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="ru.open.spc.dao.mapper.PricingResultMapper">
    <cache type="org.mybatis.caches.ehcache.EhcacheCache">
        <property name="timeToLiveSeconds" value="5"/>
    </cache>
    <resultMap id="pricingResultMap" type="PricingResult">
        <id column="PRICING_RESULT_ID" property="id" />
        <result column="PRICE_DATE" property="priceDate" />
        <result column="PRODUCT_START_DATE" property="productStartDate" />
        <result column="PRODUCT_STOP_DATE" property="productStopDate" />
        <result column="START_PRICE" property="strikePrice" />
        <result column="INTERVAL" property="interval" />
        <result column="PERCENT_BY_LOAN_DEAL" property="percentByLoanDeal" />
        <result column="DESCRIPTION" property="description" />
        <result column="INTEREST_RATE" property="interestRate" />
        <result column="DIRECTION" property="direction" />
        <result column="TIMEOUT" property="timeout" />
        <result column="ASSET_PRICE" property="assetPrice" />
        <result column="INVESTING_VOLUME" property="investingVolume" />
        <association column="PRODUCT_ID" property="product" javaType="Product"
                     resultMap="ru.open.spc.dao.mapper.ProductMapper.productMap" columnPrefix="product_" />
        <association column="ASSET_ID" property="asset" javaType="Asset"
                     resultMap="ru.open.spc.dao.mapper.AssetMapper.assetMap" columnPrefix="ASSET_" />
        <association property="currency" javaType="Currency" column="currency_id">
            <id column="CURRENCY_ID" property="id" />
            <result column="CURRENCY_NAME" property="code" />
        </association>
        <discriminator javaType="ProductCode" column="discriminator"
                       typeHandler="ru.open.spc.dao.mapper.type.ProductCodeHandler">
            <case value="BaseProtect" resultType="BAPricingResult">
                <result column="COEFF" property="coefficient" />
                <result column="KZK" property="kzk" />
                <result column="STRIKE2_PRICE" property="strike2Price" />
                <association property="vzkCurrency" javaType="Currency" column="VZK_CURR_ID">
                    <id column="VZK_CURR_ID" property="id" />
                    <result column="VZK_CURRENCY_CODE" property="code" />
                </association>
            </case>
            <case value="FixedCoupon" resultType="FCPricingResult">
                <result column="maximum_earning" property="maximumEarning" />
            </case>
            <case value="StocksDeposit" resultType="SDPricingResult">
                <result column="COUPON_SIZE" property="couponSize" />
                <result column="INVESTING_AMOUNT" property="investingAmount" />
            </case>
            <case value="SmartDeposit" resultType="SMPricingResult">
                <result column="MAXIMUM_EARNING" property="maximumEarning" />
                <result column="STRIKE2_PRICE" property="strike2Price" />
                <result column="KZK" property="kzk" />
            </case>
            <case value="AutoCall" resultType="ACPricingResult">
                <result column="maximum_earning" property="maximumEarning" />
            </case>
        </discriminator>
    </resultMap>

    <sql id="selectPricingResult">
		PR.*,
		P.PRODUCT_ID AS PRODUCT_PRODUCT_ID,
		P.NAME AS PRODUCT_NAME,
		P.CODE AS PRODUCT_CODE,
		P.DESCRIPTION AS PRODUCT_DESCRIPTION,
		A.ASSET_ID AS ASSET_ASSET_ID,
		A.NAME AS ASSET_NAME,
		A.EXTERNAL_CONTRACT_TICKER AS ASSET_EXTERNAL_CONTRACT_TICKER,
		A.MINIMUM_PRICE AS ASSET_MINIMUM_PRICE,
		A.BASE_TICKER AS ASSET_BASE_TICKER,
		C.CURRENCY_CODE AS CURRENCY_NAME,
		VZK.CURRENCY_CODE AS VZK_CURRENCY_CODE
		FROM
		DBO.SPC_PRICING_RESULT PR
		INNER JOIN DBO.SPC_PRODUCT P ON P.PRODUCT_ID = PR.PRODUCT_ID
		LEFT JOIN DBO.SPC_ASSET A ON A.ASSET_ID = PR.ASSET_ID
		LEFT JOIN DBO.SPC_CURRENCY VZK ON VZK.PHACTIVE_ID = PR.VZK_CURR_ID
		INNER JOIN DBO.SPC_CURRENCY C ON C.PHACTIVE_ID = PR.CURRENCY_ID
	</sql>

    <select id="getBySearchModel" resultMap="pricingResultMap" parameterType="PricingResultQuery">
        SELECT
        <include refid="selectPricingResult" />
        <where>
            <if test="startDate != null">
                CAST (PRICE_DATE AS DATE) &gt;= cast (#{startDate} AS DATE)
            </if>
            <if test="endDate != null">
                AND CAST (PRICE_DATE AS DATE) &lt;= cast (#{endDate} AS DATE)
            </if>
            <if test="product != null">
                AND #{product.id} = PR.PRODUCT_ID
            </if>
        </where>
        ORDER BY PRICING_RESULT_ID
    </select>

    <select id="getActiveInvestIdeas" resultMap="pricingResultMap">
        SELECT
        <include refid="selectPricingResult" />
        WHERE DATEDIFF(SECOND ,PR.PRICE_DATE, GETDATE()) &lt;= PR.TIMEOUT
        ORDER BY PR.PRICING_RESULT_ID DESC
    </select>

    <select id="getById" resultMap="pricingResultMap">
        SELECT
        <include refid="selectPricingResult" />
        WHERE PR.PRICING_RESULT_ID = #{id}
        ORDER BY PR.PRICING_RESULT_ID
    </select>

    <select id="getByProduct" resultMap="pricingResultMap">
        SELECT
        <include refid="selectPricingResult" />
        WHERE PR.PRODUCT_ID = #{productId} AND
        CAST(PR.PRICE_DATE AS DATE) = CAST(GETDATE() AS DATE)
        ORDER BY PR.PRICING_RESULT_ID
    </select>

    <select id="all" resultMap="pricingResultMap">
        SELECT
        <include refid="selectPricingResult" />
    </select>

    <select id="getByProductAndAsset" resultMap="pricingResultMap">
        SELECT
        <include refid="selectPricingResult" />
        WHERE
        PR.PRODUCT_ID = #{productId} AND
        PR.ASSET_ID = #{assetId} AND
        CAST(PR.PRICE_DATE AS DATE) = CAST(GETDATE() AS DATE)
        ORDER BY PRICING_RESULT_ID
    </select>

    <insert id="insert" useGeneratedKeys="true" keyColumn="PRICING_RESULT_ID" keyProperty="id">
        <selectKey order="AFTER" keyProperty="id" resultType="long">
            select @@identity as pricing_result_id;
        </selectKey>
        INSERT INTO
        DBO.SPC_PRICING_RESULT(
        PRODUCT_ID,
        ASSET_ID,
        PRICE_DATE,
        CURRENCY_ID,
        PRODUCT_START_DATE,
        PRODUCT_STOP_DATE,
        START_PRICE,
        INTERVAL,
        PERCENT_BY_LOAN_DEAL,
        DIRECTION,
        DESCRIPTION,
        INTEREST_RATE,
        DISCRIMINATOR,
        TIMEOUT,
        ASSET_PRICE,
        INVESTING_VOLUME
        <if test='product.code.code().equals("BP")'>
            ,KZK
            ,COEFF
            ,STRIKE2_PRICE
            ,VZK_CURR_ID
        </if>
        <if test='product.code.code().equals("FC") || product.code.code().equals("AC")'>
            ,MAXIMUM_EARNING
        </if>
        <if test='product.code.code().equals("SD")'>
            ,COUPON_SIZE
            ,INVESTING_AMOUNT
        </if>
        <if test='product.code.code().equals("SM")'>
            ,MAXIMUM_EARNING
            ,STRIKE2_PRICE
            ,KZK
        </if>
        )
        VALUES(
        #{product.id},
        #{asset.id},
        #{priceDate},
        #{currency.id},
        #{productStartDate},
        #{productStopDate},
        #{strikePrice},
        #{interval},
        #{percentByLoanDeal},
        #{direction},
        #{description},
        #{interestRate},
        #{product.code},
        #{timeout},
        #{assetPrice},
        #{investingVolume}
        <if test='product.code.code().equals("BP")'>
            ,#{kzk}
            ,#{coefficient}
            ,#{strike2Price}
            ,#{vzkCurrency.id}
        </if>
        <if test='product.code.code().equals("FC") || product.code.code().equals("AC")'>
            ,#{maximumEarning}
        </if>
        <if test='product.code.code().equals("SD")'>
            ,#{couponSize}
            ,#{investingAmount}
        </if>
        <if test='product.code.code().equals("SM")'>
            ,#{maximumEarning}
            ,#{strike2Price}
            ,#{kzk}
        </if>
        );
    </insert>

    <update id="update">
        UPDATE DBO.SPC_PRICING_RESULT
        SET
        PRODUCT_ID = #{product.id},
        ASSET_ID = #{asset.id},
        PRICE_DATE = #{priceDate},
        CURRENCY_ID = #{currency.id},
        PRODUCT_START_DATE = #{productStartDate},
        PRODUCT_STOP_DATE = #{productStopDate},
        START_PRICE = #{strikePrice},
        INTERVAL = #{interval},
        PERCENT_BY_LOAN_DEAL = #{percentByLoanDeal},
        DIRECTION = #{direction},
        DESCRIPTION = #{description},
        INTEREST_RATE = #{interestRate},
        DISCRIMINATOR = #{product.code},
        TIMEOUT=#{timeout},
        ASSET_PRICE=#{assetPrice},
        INVESTING_VOLUME=#{investingVolume}
        <if test='product.code.code().equals("BP")'>
            ,KZK = #{kzk}
            ,COEFF = #{coefficient}
            ,STRIKE2_PRICE = #{strike2Price}
            ,VZK_CURR_ID = #{vzkCurrency.id}
        </if>
        <if test='product.code.code().equals("FC") || product.code.code().equals("AC")'>
            ,MAXIMUM_EARNING = #{maximumEarning}
        </if>
        <if test='product.code.code().equals("SD")'>
            ,COUPON_SIZE = #{couponSize}
            ,INVESTING_AMOUNT = #{investingAmount}
        </if>
        <if test='product.code.code().equals("SM")'>
            ,MAXIMUM_EARNING = #{maximumEarning}
            ,STRIKE2_PRICE = #{strike2Price}
            ,KZK = #{kzk}
        </if>
        where PRICING_RESULT_ID = #{id};
    </update>

    <delete id="deleteByProduct">
        DELETE FROM DBO.SPC_PRICING_RESULT WHERE PRODUCT_ID = #{productId}
    </delete>

    <delete id="delete" flushCache="true">
        DELETE FROM DBO.SPC_PRICING_RESULT WHERE PRICING_RESULT_ID = #{id}
    </delete>

    <delete id="deleteByProductAndAsset">
        DELETE FROM DBO.SPC_PRICING_RESULT WHERE PRODUCT_ID = #{productId} AND ASSET_ID = ${assetId}
    </delete>

</mapper>
