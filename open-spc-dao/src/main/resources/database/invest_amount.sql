alter table dbo.spc_pricing_result add INVESTING_AMOUNT bigint;
alter table dbo.spc_pricing_result add INVESTING_VOLUME numeric(15, 2);