CREATE TABLE "dbo"."spc_auto_call_assets" (
  asset_autocall_id bigint NOT NULL PRIMARY KEY,
  asset_id bigint NOT NULL UNIQUE,
  sigma numeric(15, 4),
  relative_price numeric(15, 4),
  init_relative_price  numeric(15, 4)
) ;


CREATE TABLE "dbo"."spc_auto_call_assets_relation" (
  assets_relation_id bigint NOT NULL PRIMARY KEY,
  coefficient numeric(15, 4),
  asset_left_id bigint NOT NULL,
  asset_right_id bigint NOT NULL
) ;
