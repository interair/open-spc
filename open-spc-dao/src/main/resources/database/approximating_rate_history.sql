alter table onlineiv.dbo.spc_apx_rates add change_date datetime;
begin transaction apx_rates_history;
    update onlineiv.dbo.spc_apx_rates set change_date = getdate();
commit transaction apx_rates_history;
alter table onlineiv.dbo.spc_apx_rates add constraint df_apx_rates default getdate() for change_date;
alter table onlineiv.dbo.spc_apx_rates alter column change_date datetime not null;
alter table onlineiv.dbo.spc_apx_rates drop constraint pk_rates;
alter table onlineiv.dbo.spc_apx_rates add constraint pk_rates primary key (apx_rate_id, change_date);
alter table onlineiv.dbo.spc_apx_rates add deleted_date datetime;