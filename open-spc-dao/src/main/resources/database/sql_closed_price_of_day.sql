sp_rename spc_starting_price_of_day, spc_closed_price_of_day; sp_rename  'spc_closed_price_of_day.STARTING_PRICE', 'CLOSED_PRICE', 'COLUMN';
truncate table dbo.spc_closed_price_of_day
alter table spc_closed_price_of_day add constraint spc_closed_price_of_day_id  UNIQUE (asset_id, date_of_price)