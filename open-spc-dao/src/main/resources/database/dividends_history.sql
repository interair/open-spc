alter table onlineiv.dbo.spc_dividend add change_date datetime;
begin transaction dividend_history;
    update onlineiv.dbo.spc_dividend set change_date = getdate();
commit transaction dividend_history;
alter table onlineiv.dbo.spc_dividend add constraint df_dividend default getdate() for change_date;
alter table onlineiv.dbo.spc_dividend alter column change_date datetime not null;
alter table onlineiv.dbo.spc_dividend drop constraint pk_dividend;
alter table onlineiv.dbo.spc_dividend add constraint pk_dividend primary key (dividend_id, change_date);
alter table onlineiv.dbo.spc_dividend add deleted_date datetime;