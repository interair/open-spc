CREATE TABLE "dbo"."spc_authority" (
  id bigint PRIMARY KEY NOT NULL ,
  role varchar(255) UNIQUE NOT NULL,

) ;

CREATE TABLE "dbo"."spc_user_auth_details" (
  id bigint NOT NULL PRIMARY KEY,
  username varchar(100) UNIQUE NOT NULL,
  fullName varchar(255) NOT NULL,
  description  varchar(255) NOT NULL,
  title varchar(255) NOT NULL,
  department varchar(255) NOT NULL,
  company varchar(255) NOT NULL,

) ;


CREATE TABLE "dbo"."spc_user_auth_details_authorities" (
  user_auth_details_id bigint NOT NULL ,
  authorities_id bigint NOT NULL,
  PRIMARY KEY (user_auth_details_id,authorities_id),
  CONSTRAINT FK_user_auth_details_id FOREIGN KEY (user_auth_details_id) REFERENCES spc_user_auth_details (id),
  CONSTRAINT FK_authorities_id FOREIGN KEY (authorities_id) REFERENCES spc_authority (id)
) ;
