package ru.open.spc.web;

import com.googlecode.wicket.jquery.ui.widget.tabs.AjaxTab;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.open.spc.model.Product;
import ru.open.spc.model.enums.ProductCode;
import ru.open.spc.web.price.parameters.panel.TabFactory;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testWebApplicationContext.xml", "classpath:defFormParamConfig.xml"})
public class TestContext {

	private final static Logger LOG = LoggerFactory.getLogger(TestContext.class);


	@Resource(name = "productTabs")
	private Map<ProductCode, List<Class<?>>> productTabs;

	@Autowired
	private TabFactory factory;

	@Test
	public void testProductsTab() {
		for (Map.Entry<ProductCode, List<Class<?>>> entry : productTabs.entrySet()) {
			ProductCode productCode = entry.getKey();
			List<Class<?>> list = entry.getValue();
			LOG.info("productCode {} : list {}", productCode, list);
			Assert.assertFalse(list.isEmpty());
		}
	}

	@Test
	public void testTabFactory() {
		Product product = new Product() {{
			setCode(ProductCode.FixedCoupon);
		}};
		List<ITab> createTabsList = factory.createTabsList(product);
		Assert.assertFalse(createTabsList.isEmpty());
		Assert.assertTrue(createTabsList.size() == 2);
		for (ITab iTab : createTabsList) {
			Assert.assertNotNull(iTab);
			Assert.assertTrue(iTab instanceof AjaxTab);
		}
		Assert.assertNotEquals(createTabsList.get(0), createTabsList.get(1));
	}
}
