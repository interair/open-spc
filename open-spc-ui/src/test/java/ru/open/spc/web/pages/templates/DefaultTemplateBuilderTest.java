package ru.open.spc.web.pages.templates;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import ru.open.commons.templates.TemplateBuilder;
import ru.open.spc.services.dto.InvestProduct;
import ru.open.spc.services.mapper.InvestProductMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//@ContextConfiguration(locations = {"classpath:testTemplateBuilderContext.xml"})
public class DefaultTemplateBuilderTest {

    private final static Logger LOGGER = LoggerFactory.getLogger(DefaultTemplateBuilderTest.class);

    //@Autowired
    //@Qualifier("defaultTemplateBuilder")
    private TemplateBuilder builder;

    //@Autowired
    private InvestProductMapper productMapper;

    //@Test
    public void testBuildTemplate() {
        try {
            final List<InvestProduct> allActive = productMapper.getPreparedInvestIds(1L).stream().map(productMapper::getById).collect(Collectors.toList());
            Map<String, Object> objectMap = new HashMap<>();
            objectMap.put("investProducts", allActive);
            final String result = builder.buildTemplate(objectMap, "crm-integration-page.ftl");
            LOGGER.debug("result = {}", result);
            Assert.assertTrue(StringUtils.hasText(result));
        } catch (Exception e) {
            LOGGER.error("Ooops...", e);
        }
    }
}