package ru.open.spc.web.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;

import static junit.framework.TestCase.fail;

public class DurationConverterTest {

	private DurationConverter durationConverter;

	@Before
	public void setUp() {
		durationConverter = new DurationConverter();
	}

	@Test
	public void testConvertToObject() throws Exception {
		Duration expected = Duration.ofMinutes(60 * 2 + 25);
		Assert.assertEquals(durationConverter.convertToObject("2ч:25м", null), expected);
	}

	@Test
	public void testConvertToString() throws Exception {
		String expected = "3ч:14м";
		String actual = durationConverter.convertToString(Duration.ofMinutes(3 * 60 + 14), null);
		Assert.assertEquals(actual, expected);
	}

	public void testIllegalString() {
		try {

			durationConverter.convertToObject("12ч", null);
			fail();
		} catch (IllegalArgumentException e) {
		}
	}
}