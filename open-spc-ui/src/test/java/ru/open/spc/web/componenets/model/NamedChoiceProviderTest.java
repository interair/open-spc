package ru.open.spc.web.componenets.model;

import com.vaynberg.wicket.select2.Response;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.open.spc.calc.kernel.services.DateApproximationChecker;
import ru.open.spc.model.Product;
import ru.open.spc.model.common.Named;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;


public class NamedChoiceProviderTest {

    private NamedChoiceProvider provider = new NamedChoiceProvider();
    private final static String[] names = {"zero", "_first", "_second", "third"};
    private final static String[] ids = {"0", "1", "2", "3"};
    private final static Logger LOGGER = LoggerFactory.getLogger(DateApproximationChecker.class);

    @Before
    public void prepare() {
        provider.setList(new ArrayList<Named>() {{
            for (int i = 0; i < names.length; i++) {
                final String name = names[i];
                final long id = i;
                add(new Product() {{
                    setId(id);
                    setName(name);
                }});
            }
        }});
    }

    @Test
    public void testQuery() throws Exception {
        Response<Named> responseFirst = new Response<>();
        final String first = names[1];
        LOGGER.info("List size {}", provider.getSize());
        LOGGER.info("first objects is {}", first);
        provider.query(first, 0, responseFirst);
        assertNotNull(responseFirst.getResults());
        LOGGER.info("List size of provider {}", responseFirst.getResults().size());
        assertSame(responseFirst.getResults().size(), 1);
        LOGGER.info("response{} and size is {}", responseFirst.getResults().toArray()[0], responseFirst.getResults().size());
        assertSame(responseFirst.getResults().get(0).getName(), first);

        Response<Named> _responseByPattern = new Response<>();
        String pattern = "_";
        provider.query(pattern, 0, _responseByPattern);
        assertNotNull(_responseByPattern.getResults());
        LOGGER.info("_response size has {}", _responseByPattern.getResults().size());
        assertSame(_responseByPattern.getResults().size(), 2);
        for (Named named : _responseByPattern) {
            assertTrue(named.getName().startsWith(pattern));
        }


        Response<Named> responseAll = new Response<>();
        provider.query("", 0, responseAll);
        assertNotNull(responseAll.getResults());
        assertSame(responseAll.getResults().size(), 4);
    }

    @Test
    public void testToChoices() throws Exception {
        final List<String> strings = Arrays.asList(ids);
        final Collection<Named> namedList = provider.toChoices(strings);
        assertSame(strings.size(), namedList.size());
        for (Named named : namedList) {
            assertTrue(named.getId().toString().equals(strings.get(named.getId().intValue())));
        }
    }
}
