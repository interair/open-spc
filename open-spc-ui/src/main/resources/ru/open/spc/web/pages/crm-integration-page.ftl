<table border="1">
    <thead>
        <tr>
            <th>ID инвест идей</th>
            <th>ID продукта</th>
            <th>Направление</th>
            <th>Номер выпуска ЦБ</th>
            <th>Тикер БА</th>
            <th>Наименование БА</th>
            <th>Валюта платежа</th>
            <th>Валюта БА</th>
            <th>Дата исполнения СП</th>
            <th>Дата/Время расчетов</th>
            <th>Коэф. защиты капитала</th>
            <th>Пороговая цена 1</th>
            <th>Пороговая цена 2</th>
            <th>Валюта пороговой цены</th>
            <th>КУ</th>
            <th>Размер купона/% ставка (% годовых)</th>
            <th>Вид сделки</th>
            <th>Стиль опционов</th>
            <th>Оценочная стоимость БА</th>
            <th>Минимальная сумма инвестирования</th>
            <th>Минимальное количество ЦБ</th>
            <th>Time out</th>

            <th>Directum: publicId</th>
	        <th>Directum: classCode</th>
	        <th>Directum: description</th>
            <th>Directum: marketPlace</th>
            <th>Const: transactionCurrency</th>
            <th>ВЗК</th>

        </tr>
    </thead>
    <tbody>
        <#list investProducts as product>
            <tr>
                <td>${product.investProductId!''}</td>
                <td>${product.productId!''}</td>
                <td>${product.direction!''}</td>
                <td>${product.assetRegNum!''}</td>
                <td>${product.assetTicker!''}</td>
                <td>${product.assetName!''}</td>
                <td>${product.paymentCurrency!''}</td>
                <td>${product.assetCurrency!''}</td>
                <td>${product.productStopDate!''}</td>
                <td>${product.productCalcDate!''}</td>
                <td>${product.kzk!''}</td>
                <td>${product.strikePrice!''}</td>
                <td>${product.strike2Price!''}</td>
                <td>${product.strikeCurrency!''}</td>
                <td>${product.participationRate!''}</td>
                <td>${product.couponSize!''}</td>
                <td>${product.dealType!''}</td>
                <td>${product.optionStyle!''}</td>
                <td>${product.assetPrice!''}</td>
                <td>${product.investingValue!''}</td>
                <td>${product.investingAmount!''}</td>
                <td>${product.timeOutInSec!''}</td>

	            <td>${product.publicId!''}</td>
	            <td>${product.classCode!''}</td>
	            <td>${product.description!''}</td>
                <td>${product.marketPlace!''}</td>
                <td>${product.transactionCurrency!''}</td>
                <td>${product.vzkCurrency!''}</td>

            </tr>
        </#list>
    </tbody>
</table>