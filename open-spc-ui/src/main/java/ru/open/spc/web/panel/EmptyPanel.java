package ru.open.spc.web.panel;

import org.apache.wicket.markup.html.panel.Panel;

@SuppressWarnings("serial")
public class EmptyPanel extends Panel {

	public EmptyPanel(String id) {
		super(id);
		setOutputMarkupId(true);
	}

}
