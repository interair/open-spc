package ru.open.spc.web.excel;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.open.spc.web.util.ResourceBundleAware;
import ru.open.spc.web.util.UIUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import static ru.open.spc.web.excel.PricingResultExcelExport.getReportHeaderColumns;
import static ru.open.spc.web.util.UIUtil.EXCEL_DATE_TIME_FORMAT;

public abstract class AbstractExcelExport<T extends Serializable> extends ResourceBundleAware {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractExcelExport.class);

	private static final String WHITESPACE = " ";
	private static final String XLSX_EXTENSION = ".xlsx";
	private static final int inMemoryFrame = 1000;

	public File generateReport(List<T> data) {
		try {
			SXSSFWorkbook workbook = new SXSSFWorkbook(inMemoryFrame);
			Map<ExcelStyles, CellStyle> styles = createStyles(workbook);
			Sheet sheet = workbook.createSheet(get("navigation.saved.data.link.label"));
			String[] columns = getReportHeaderColumns();
			addHeaderTo(sheet, styles.get(ExcelStyles.HEADER), columns);
			fillWorkbook(sheet, data, styles);
			File file = new File(getReportName());
			try (FileOutputStream out = new FileOutputStream(file)) {
				workbook.write(out);
			}
			return file;
		} catch (Exception ex) {
			LOG.error("Oooops... excel export error", ex);
		}
		return null;
	}

	private static void addHeaderTo(Sheet sheet, CellStyle headerCellStyle, String[] columns) {
		Row headRow = sheet.createRow(0);
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headRow.createCell(i, Cell.CELL_TYPE_STRING);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue(columns[i]);
		}
		sheet.createFreezePane(0, 1);
	}

	protected void fillWorkbook(Sheet sheet, List<T> data,
								Map<ExcelStyles, CellStyle> styles) {
		for (int r = 0; r < data.size(); r++) {
			createRow(r + 1, sheet, styles, data);
		}
	}

	abstract void createRow(int r, Sheet sheet, Map<ExcelStyles, CellStyle> styles, List<T> data);

	protected void addCellTo(Row row, int cellNumber, Object value, Map<ExcelStyles, CellStyle> styles) {
		if (value != null) {
			if (value instanceof Number) {
				row.createCell(cellNumber, Cell.CELL_TYPE_NUMERIC)
					.setCellValue(((Number) value).doubleValue());
			} else if (value instanceof LocalDate) {
				Cell dateCell = row.createCell(cellNumber);
				dateCell.setCellValue(ru.open.spc.dao.util.DateUtil.toDate((LocalDate)value));
				dateCell.setCellStyle(styles.get(ExcelStyles.DATE));
			} else if (value instanceof Boolean) {
				row.createCell(cellNumber, Cell.CELL_TYPE_BOOLEAN)
					.setCellValue((Boolean) value);
			} else {
				row.createCell(cellNumber, Cell.CELL_TYPE_STRING)
					.setCellValue(value.toString());
			}
		} else {
			row.createCell(cellNumber, Cell.CELL_TYPE_BLANK);
		}
	}

	protected static String toString(Object value) {
		return value != null ? value.toString() : "";
	}

	protected String getReportName() {
		return getName() + WHITESPACE + DateTimeFormatter.ofPattern(EXCEL_DATE_TIME_FORMAT).format(LocalDateTime.now()) + XLSX_EXTENSION;
	}

	protected abstract String getName();

	private Map<ExcelStyles, CellStyle> createStyles(SXSSFWorkbook workbook) {
		Map<ExcelStyles, CellStyle> styles = new EnumMap<>(ExcelStyles.class);
		styles.put(ExcelStyles.DATE, createDateCellStyle(workbook));
		styles.put(ExcelStyles.HEADER, createHeaderCellStyle(workbook));
		return styles;
	}

	protected CellStyle createDateCellStyle(Workbook workbook) {
		CellStyle dateStyle = workbook.createCellStyle();
		DataFormat df = workbook.createDataFormat();
		dateStyle.setDataFormat(df.getFormat(UIUtil.DATE_TIME_FORMAT));
		return dateStyle;
	}

	protected static CellStyle createHeaderCellStyle(Workbook workbook) {
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerCellStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerCellStyle.setBorderTop(CellStyle.BORDER_THIN);
		Font font = workbook.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerCellStyle.setFont(font);
		return headerCellStyle;
	}
}

