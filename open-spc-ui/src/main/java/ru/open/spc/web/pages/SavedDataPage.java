package ru.open.spc.web.pages;

import com.googlecode.wicket.jquery.core.Options;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import com.googlecode.wicket.kendo.ui.form.datetime.DatePicker;
import com.googlecode.wicket.kendo.ui.form.dropdown.DropDownList;
import com.googlecode.wicket.kendo.ui.widget.splitter.SplitterAdapter;
import com.googlecode.wicket.kendo.ui.widget.splitter.SplitterBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.*;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.dao.manager.PricingResultManager;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.model.PricingResultQuery;
import ru.open.spc.model.Product;
import ru.open.spc.model.result.PricingResult;
import ru.open.spc.web.AbstractWebPage;
import ru.open.spc.web.ajaxhelps.AJAXDownload;
import ru.open.spc.web.componenets.*;
import ru.open.spc.web.componenets.model.ProductsModel;
import ru.open.spc.web.componenets.model.SortableDataProviderImpl;
import ru.open.spc.web.componenets.renders.ChoiceRendererImpl;
import ru.open.spc.web.dialog.DeleteDialog;
import ru.open.spc.web.dialog.MultiLinesMessageDialog;
import ru.open.spc.web.dialog.PricingResultDialog;
import ru.open.spc.web.dialog.PricingResultWizard;
import ru.open.spc.web.excel.PricingResultExcelExport;
import ru.open.spc.web.ui.model.PricingResultUIWrapper;
import ru.open.spc.web.util.UIUtil;
import ru.open.spc.web.util.UpdateEvent;
import ru.open.spc.web.util.WicketUtil;

import java.io.File;
import java.time.LocalDate;
import java.util.List;

import static ru.open.spc.calc.util.Util.merge;
import static ru.open.spc.web.util.WicketUtil.createSortableColumns;

@SuppressWarnings("serial")
public class SavedDataPage extends AbstractWebPage {

	@SpringBean private PricingResultManager pricingResultManager;
	@SpringBean private PricingResultExcelExport pricingResultExcelExport;
	@SpringBean private ProductManager productManager;

	private static final String RES_LIST = "resultList";
	private static final String RES_LIST_PANEL = "resultListPanel";

	private Form<String> panel;
	private final DataTable<PricingResultUIWrapper, Class> resultListView;
	private final SortableDataProviderImpl<PricingResultUIWrapper> dataProvider = new SortableDataProviderImpl<>();

	public SavedDataPage() {
		setOutputMarkupId(true);
		Options options = new Options();
		options.set("panes", "[ { size: '50px' } ]");
		options.set("orientation", "'vertical'");
		SplitterBehavior splitterBehavior = new SplitterBehavior("#splitter", new SplitterAdapter());
		splitterBehavior.setOptions(options);
		add(splitterBehavior);
		final DeleteDialog<PricingResult> deleteMessageDialog = new DeleteDialog<>("deleteConfirmation", getString("delete.dialog.title"),
			getString("result.delete.message"), DialogButtons.YES_NO);
		deleteMessageDialog.setGenericManager(pricingResultManager);

        PricingResultWizard resultWizardEdit = new PricingResultWizard("resultWizardEdit", new ResourceModel("result.wizard.edit"), true);
        this.add(resultWizardEdit);

        PricingResultWizard resultWizardNew = new PricingResultWizard("resultWizardNew", new ResourceModel("result.wizard.new"), false);
        this.add(resultWizardNew);

		List<PricingResult> searchModel = pricingResultManager.getBySearchModel(
            new PricingResultQuery().setStartDate(LocalDate.now()).setEndDate(LocalDate.now()));
		dataProvider.setList(UIUtil.transformPricingResult(searchModel));
		Form<PricingResultQuery> form = new Form<>("exportToExcelForm", new CompoundPropertyModel<>(new PricingResultQuery()));
		final AJAXDownload download = new AJAXDownload();
		form.add(download);
		final DatePicker startDate = WicketUtil.createDefaultDatePicker("startDate");
		startDate.setRequired(true).setOutputMarkupId(true);
		form.add(startDate);

		final DatePicker endDate = WicketUtil.createDefaultDatePicker("endDate");
		endDate.setRequired(true).setOutputMarkupId(true);
		form.add(endDate);
		final DropDownList<Product> productsDropDown = new DropDownList<>(
			"product",
			new ProductsModel(productManager),
			new ChoiceRendererImpl<>());
		productsDropDown.setOutputMarkupId(true);
		form.add(productsDropDown);

		form.add(new AjaxButton("calculateButton") {

            @SuppressWarnings("unchecked")
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                Form<PricingResultQuery> savedForm = (Form<PricingResultQuery>) form;
                PricingResultQuery modelObject = savedForm.getModelObject();
                List<PricingResult> resultList = pricingResultManager.getBySearchModel(modelObject);
                dataProvider.setList(UIUtil.transformPricingResult(resultList));
                target.add(panel);
                super.onSubmit(target, form);
            }
        });
		form.add(new AjaxButton("exportToExcel") {

			@Override
			protected void onSubmit(AjaxRequestTarget target, final Form<?> form) {
				Form<PricingResultQuery> savedForm = (Form<PricingResultQuery>) form;
				List<PricingResult> resultList = pricingResultManager.getBySearchModel(savedForm.getModelObject());
				final File generateReport = pricingResultExcelExport.generateReport(UIUtil.transformPricingResult(resultList));
				download.initiate(target, generateReport);
				super.onSubmit(target, form);
			}
		});

		form.add(new AjaxButton("newPricingResultLink") {

			@Override
			public void onSubmit(AjaxRequestTarget target, final Form<?> form) {

                resultWizardNew.setModelObject(new ResultBuilder());
                resultWizardNew.open(target);
			}
		});
		add(form);
		final MultiLinesMessageDialog descriptionDialog = new MultiLinesMessageDialog("descriptionDialog", getString("desc.dialog.title"));
		List<IColumn<PricingResultUIWrapper, String>> columns = createSortableColumns("id", "asset.name", "priceDate","offerEnd", "currency.code",
			"productStartDate", "productStopDate", "kzk", "strikePrice", "strike2Price", "coefficient", "interestRate", "maximumEarning", "assetPrice");

		columns.add(0, new HeaderlessColumn<PricingResultUIWrapper, String>() {
			public void populateItem(Item<ICellPopulator<PricingResultUIWrapper>> cellItem, String componentId,
									 final IModel<PricingResultUIWrapper> model) {
                cellItem.add(new EditLink(componentId).add(new AjaxLink<String>("editLink") {

					@Override
					public void onClick(AjaxRequestTarget target) {
						resultWizardEdit.setModelObject(model.getObject().toResultBuilder());
                        resultWizardEdit.open(target);
					}
				}));
			}
		});
		columns.add(1, new HeaderlessColumn<PricingResultUIWrapper, String>() {
			public void populateItem(Item<ICellPopulator<PricingResultUIWrapper>> cellItem, String componentId,
									 final IModel<PricingResultUIWrapper> model) {
                cellItem.add(new DeleteLink(componentId).add(new AjaxLink<String>("deleteLink") {

					@Override
					public void onClick(AjaxRequestTarget target) {
						deleteMessageDialog.setObject(model.getObject());
						deleteMessageDialog.open(target);
					}
				}));
			}
		});
        columns.add(3, new PropertyColumn<PricingResultUIWrapper, String>(new ResourceModel("product.name"), "product.name", "product.name") {
            public void populateItem(Item<ICellPopulator<PricingResultUIWrapper>> cellItem, String componentId,
                                     final IModel<PricingResultUIWrapper> model) {
                cellItem.add(new BadgeInfoLabel(componentId, model.getObject().getProduct().getName()).setEscapeModelStrings(false));
            }
        });

		columns.add(new AbstractColumn<PricingResultUIWrapper, String>(new ResourceModel("direction"), "direction") {
			public void populateItem(Item<ICellPopulator<PricingResultUIWrapper>> cellItem, String componentId,
									 final IModel<PricingResultUIWrapper> model) {
				cellItem.add(new ArrowPanel(componentId, model.getObject().getDirection()));
			}
		});

		columns.add(new AbstractColumn<PricingResultUIWrapper, String>(new ResourceModel("description")) {
            public void populateItem(Item<ICellPopulator<PricingResultUIWrapper>> cellItem, String componentId,
                                     final IModel<PricingResultUIWrapper> model) {
                cellItem.add(new DescriptionPanel(componentId).add(new AjaxLink<String>("description-link") {

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        descriptionDialog.setModelObject(model.getObject().getDescription());
                        descriptionDialog.open(target);
                    }
                }));
            }
        });
		resultListView = new AjaxFallbackDefaultDataTable(RES_LIST, columns, dataProvider, 100);

		panel = new Form<String>(RES_LIST_PANEL) {{
			add(resultListView);
		}};
		panel.setOutputMarkupId(true);
		panel.add(resultListView.setOutputMarkupId(true));
		add(panel);

        add(resultWizardEdit);
		add(deleteMessageDialog);
		add(descriptionDialog);
	}

	@Override
	public void onEvent(IEvent<?> event) {
		Object payload = event.getPayload();
		if (payload instanceof UpdateEvent) {
			UpdateEvent uevent = (UpdateEvent) payload;
			if (uevent.isAccepted(DeleteDialog.DELETE_EVENT)) {
				if (uevent.getPayload() instanceof PricingResult) {
					dataProvider.remove(uevent.getPayload());
				}
			} else if (uevent.isAccepted(PricingResultDialog.PRICING_RESULT_EVENT)) {
                dataProvider.setList(merge(dataProvider.getList(), uevent.getPayload()));
			}
			if (uevent.getTarget() != null) {
				uevent.getTarget().add(panel);
			}
		}
		super.onEvent(event);
	}
}
