package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.event.Broadcast;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.validation.validator.RangeValidator;
import ru.open.spc.model.Dividend;
import ru.open.spc.web.util.UpdateEvent;
import ru.open.spc.web.util.WicketUtil;

import java.util.List;

import static ru.open.spc.web.util.DefaultDialogButtons.getDialogButtons;
import static ru.open.spc.web.util.DefaultDialogButtons.getSaveButton;

public class DividendDialog extends ResizableDialog<Dividend> {

	private final Form<Dividend> dividendForm;
	public final static String DIVIDEND_ADD_EVENT = "DIVIDEND_ADD_EVENT";

	public DividendDialog(String id) {
		super(id, new ResourceModel("dividend.title").getObject(), true);
		dividendForm = new Form<>("dividendForm");
		dividendForm.setOutputMarkupId(true);
		dividendForm.add(new JQueryFeedbackPanel("divFeedbackPanel"));
		dividendForm.add(WicketUtil.createDefaultDatePicker("payDate").setRequired(true));
		dividendForm.add(new Spinner<>("amount").setRequired(true).setType(Double.class).add(new RangeValidator<>(0d, Double.MAX_VALUE)));
		add(dividendForm);
	}

	@Override
	protected DialogButton getSubmitButton() {
		return getSaveButton();
	}

	@Override
	public Form<?> getForm() {
		return dividendForm;
	}

	@Override
	protected void onError(AjaxRequestTarget target) {
		target.add(dividendForm.get("divFeedbackPanel"));
	}

	@Override
	protected void onSubmit(AjaxRequestTarget target) {
		send(getParent(), Broadcast.BREADTH,
			new UpdateEvent(target, dividendForm.getModelObject(), DIVIDEND_ADD_EVENT));
	}

	@Override
	protected List<DialogButton> getButtons() {
		return getDialogButtons();
	}

	public void setDefaultModelObject(Dividend dividend) {
		dividendForm.setDefaultModel(new CompoundPropertyModel<>(dividend));
		dividendForm.clearInput();
	}

	@Override
	protected void onOpen(AjaxRequestTarget target) {
		target.add(dividendForm);
	}


}
