package ru.open.spc.web.price.parameters.panel.products.exchange;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import com.googlecode.wicket.kendo.ui.form.dropdown.DropDownList;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.validation.AbstractFormValidator;
import org.apache.wicket.util.convert.converter.DoubleConverter;
import org.apache.wicket.validation.validator.RangeValidator;
import ru.open.spc.calc.model.Orientation;
import ru.open.spc.calc.model.simple.exchange.SmartDepositSimpleParameters;
import ru.open.spc.web.componenets.renders.OrientationRenderer;
import ru.open.spc.web.price.parameters.panel.SimplePriceParameterTabPanel;

import java.util.Arrays;
import java.util.Locale;

@SuppressWarnings("serial")
public class SmartDepositSimpleParamTabPanel extends SimplePriceParameterTabPanel<SmartDepositSimpleParameters> {

    private final static double STRIKE_MAX_VALUE = 1_000_000_000;

    private class StrikeMinMaxValidator extends AbstractFormValidator {

        private final DoubleConverter spcDoubleConverter = new DoubleConverter();
        private final Locale locale = new Locale("ru", "RU");

        @Override
        @SuppressWarnings("unchecked")
        public FormComponent<Double>[] getDependentFormComponents() {
            return new FormComponent[] { strikeMinField, strikeMaxField };
        }

        @Override
        public void validate(Form<?> form) {
            Double strikeMinValue = spcDoubleConverter.convertToObject(strikeMinField.getInput(), locale);
            Double strikeMaxValue = spcDoubleConverter.convertToObject(strikeMaxField.getInput(), locale);
            if (!(strikeMinValue < strikeMaxValue)) {
                getCalculationParamForm().error(getString("product.sd.error.strike_min_max_validation"));
            }
        }

    }

    private final FormComponent<Double> strikeMinField, strikeMaxField;

    public SmartDepositSimpleParamTabPanel(String id) {
        super(id, new SmartDepositSimpleParameters());

        addToForm(new Spinner<Double>("coeffAssetProtection").setRequired(true));
        addToForm(new Spinner<>("strike2").setEnabled(false)); //TODO: add behavior for work with percents
        disableOnForm("strike1");                              //TODO: add behavior for work with percents
        disableOnForm("optionPrice");                         //TODO: remove it at all
        this.strikeMinField = new RequiredTextField<Double>("strikeMin").add(new RangeValidator<>(1d, STRIKE_MAX_VALUE));
        this.strikeMaxField = new RequiredTextField<Double>("strikeMax").add(new RangeValidator<>(1d, STRIKE_MAX_VALUE));
        addToForm(strikeMinField);
        addToForm(strikeMaxField);
        getCalculationParamForm().add(new StrikeMinMaxValidator());
        addToForm(new DropDownList<>("orientation", Arrays.asList(Orientation.values()), new OrientationRenderer()).setRequired(true));
        addToForm(new TextField<Double>("putLeft"));
        addToForm(new TextField<Double>("putRight"));
        addToForm(new TextField<Double>("callRight"));
        addToForm(new TextField<Double>("callLeft"));
    }

}
