package ru.open.spc.web.dialog.tabs;

import com.googlecode.wicket.jquery.core.JQueryBehavior;
import com.googlecode.wicket.jquery.core.Options;
import com.googlecode.wicket.jquery.ui.widget.dialog.AbstractDialog;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.tabs.AjaxTab;
import com.googlecode.wicket.jquery.ui.widget.tabs.TabbedPanel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.ResourceModel;
import ru.open.spc.model.Asset;
import ru.open.spc.web.dialog.AssetDialog;
import ru.open.spc.web.util.UpdateEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AssetTabbedPanelDialog extends AbstractDialog<Asset> {

    private final List<ModelObjectAwarePanel> panels = new ArrayList<>();
    private Asset model;

    public AssetTabbedPanelDialog(String id) {
        super(id, new ResourceModel("asset.title").getObject(), true);
        final List<ITab> tabs = new ArrayList<>();
        tabs.add(new AjaxTab(new ResourceModel("asset.name")) {

            @Override
            public WebMarkupContainer getLazyPanel(String panelId) {
                return initPanel(new AssetTabPanel(panelId));
            }
        });
        tabs.add(new AjaxTab(new ResourceModel("asset.name.volatility")) {

            @Override
            public WebMarkupContainer getLazyPanel(String panelId) {
                return initPanel(new AssetVolatilityTabPanel(panelId));
            }
        });
        tabs.add(new AjaxTab(new ResourceModel("rate.coeff.title")) {

            @Override
            public WebMarkupContainer getLazyPanel(String panelId) {
                return initPanel(new RateCoeffTabPanel(panelId));
            }
        });
        tabs.add(new AjaxTab(new ResourceModel("dividend.name")) {

            @Override
            public WebMarkupContainer getLazyPanel(String panelId) {
                return initPanel(new DividendTabPanel(panelId));
            }

        });

        final Options options = new Options().set("collapsible", true);
        final TabbedPanel tabPanel = new TabbedPanel("tabs", tabs, options);
        tabPanel.setOutputMarkupId(true).setOutputMarkupPlaceholderTag(true);
        add(tabPanel);
    }

    private ModelObjectAwarePanel initPanel(final ModelObjectAwarePanel components) {
        components.setModelObject(model);
        panels.add(components);
        return components;
    }

    @Override
    protected List<DialogButton> getButtons() {
        return Arrays.asList();
    }

    @Override
    public void onClose(AjaxRequestTarget target, DialogButton button) {
    }

    public void setModel(Asset model) {
        this.model = model;
        setModels(model);
    }

    private void setModels(Asset model) {
        panels.stream().filter(component -> component != null).forEach(component -> component.setModelObject(model));
    }

    @Override
    protected void onOpen(AjaxRequestTarget target) {
        super.onOpen(target);
        collectNotNull(target);
    }

    private void collectNotNull(AjaxRequestTarget target) {
        panels.stream().filter(component -> component != null).forEach(target::add);
    }

    @Override
    public void onEvent(IEvent<?> event) {
        Object payload = event.getPayload();
        if (payload instanceof UpdateEvent) {
            final UpdateEvent uevent = (UpdateEvent) payload;
            if (uevent.isAccepted(AssetDialog.DIALOG_EVENT)) {
               close(uevent.getTarget(), null);
            }
        }
        super.onEvent(event);
    }

    @Override
    public void onConfigure(JQueryBehavior behavior) {
        super.onConfigure(behavior);
        behavior.setOption("width", "550");
    }
}
