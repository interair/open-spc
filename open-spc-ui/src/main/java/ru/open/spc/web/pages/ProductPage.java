package ru.open.spc.web.pages;

import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.HeaderlessColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Product;
import ru.open.spc.web.AbstractWebPage;
import ru.open.spc.web.componenets.DeleteLink;
import ru.open.spc.web.componenets.EditLink;
import ru.open.spc.web.componenets.model.Model;
import ru.open.spc.web.componenets.model.SortableDataProviderImpl;
import ru.open.spc.web.dialog.DeleteDialog;
import ru.open.spc.web.dialog.ProductDialog;
import ru.open.spc.web.util.UIUtil;

import java.util.List;

import static ru.open.spc.web.util.WicketUtil.createSortableColumnsWithPrefix;

@SuppressWarnings("serial")
public class ProductPage extends AbstractWebPage {

	@SpringBean private ProductManager productManager;
    private final SortableDataProviderImpl<Product> dataProvider = new SortableDataProviderImpl<>();

	public ProductPage() {
		final ProductDialog productDialog = new ProductDialog("productDialog");
		final DeleteDialog<Product> deleteMessageDialog = new DeleteDialog("deleteConfirmation", getString("delete.dialog.title"),
			getString("product.delete.message"), DialogButtons.YES_NO);
		deleteMessageDialog.setGenericManager(productManager);

		add(new AjaxLink<String>("newProductLink") {

			@Override
			public void onClick(AjaxRequestTarget target) {
				productDialog.setDefaultModelObject(new Product());
				productDialog.open(target);
			}

		});
        List<IColumn<Product, String>> columns = createSortableColumnsWithPrefix("product", "id", "name", "description");
        columns.add(0, new HeaderlessColumn<Product, String>() {
            public void populateItem(Item<ICellPopulator<Product>> cellItem, String componentId,
                                     final IModel<Product> model) {
                cellItem.add(new EditLink(componentId).add(new AjaxLink<String>("editLink") {

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        productDialog.setDefaultModelObject(model.getObject());
                        productDialog.open(target);
                    }
                }));
            }
        });

        columns.add(1, new HeaderlessColumn<Product, String>() {
            public void populateItem(Item<ICellPopulator<Product>> cellItem, String componentId,
                                     final IModel<Product> model) {
                cellItem.add(new DeleteLink(componentId).add(new AjaxLink<String>("deleteLink") {

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        deleteMessageDialog.setObject(model.getObject());
                        deleteMessageDialog.open(target);
                    }
                }));
            }
        });
        columns.add(5, new AbstractColumn<Product, String>(new ResourceModel("product.assets.long")) {
            public void populateItem(Item<ICellPopulator<Product>> cellItem, String componentId,
                                     final IModel<Product> model) {
                cellItem.add(new MultiLineLabel(componentId, new Model<String>() {
                    @Override
                    public String getObject() {
                        StringBuilder productsNames = new StringBuilder();
                        for (Asset asset : model.getObject().getAssets()) {
                            productsNames.append(asset.getName()).append(UIUtil.COMMA);
                        }
                        return productsNames.toString();
                    }
                }));
            }
        });
        final DataTable<Product, Class> resultListView = new AjaxFallbackDefaultDataTable("productList", columns, dataProvider, 100);
		add(resultListView);
		add(productDialog);
		add(deleteMessageDialog);
		setOutputMarkupId(true);
	}

	@Override
	protected void onBeforeRender() {
        dataProvider.setList(productManager.all());
		super.onBeforeRender();
	}
}