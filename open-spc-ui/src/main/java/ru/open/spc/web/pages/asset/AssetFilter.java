package ru.open.spc.web.pages.asset;

import org.springframework.stereotype.Service;
import ru.open.spc.calc.util.Function;
import ru.open.spc.web.util.ResourceBundleAware;

import javax.annotation.PostConstruct;

@Service
public class AssetFilter extends ResourceBundleAware implements Function<String, Boolean> {

    private String[] filteredAssetsArray;

    @PostConstruct
    public void init() {
        filteredAssetsArray = get("filteredAssets").split(",");
    }

    @Override
    public Boolean apply(String key) {
        for (String filteredAsset : filteredAssetsArray) {
            if (key.contains(filteredAsset)) {
                return false;
            }
        }
        return true;
    }
}