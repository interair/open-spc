package ru.open.spc.web.price.parameters;

import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogIcon;
import com.googlecode.wicket.jquery.ui.widget.dialog.MessageDialog;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.CheckGroup;
import org.apache.wicket.markup.html.form.CheckGroupSelector;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.ComponentDetachableModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.PricingResultManager;
import ru.open.spc.model.result.PricingResult;
import ru.open.spc.web.ajaxhelps.AJAXDownload;
import ru.open.spc.web.componenets.*;
import ru.open.spc.web.componenets.model.Model;
import ru.open.spc.web.componenets.model.SortableDataProviderImpl;
import ru.open.spc.web.dialog.MultiLinesMessageDialog;
import ru.open.spc.web.excel.PricingResultExcelExport;
import ru.open.spc.web.ui.model.PricingResultUIWrapper;
import ru.open.spc.web.util.UIUtil;
import ru.open.spc.web.util.UpdateEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static ru.open.spc.web.util.WicketUtil.createSortableColumns;

@SuppressWarnings("serial")
public class PriceResultPanel extends Panel {

	@SpringBean private PricingResultManager pricingResultManager;
	@SpringBean private PricingResultExcelExport pricingResultExcelExport;

	public static final String RESULT_LIST = "resultList";
	private static final int ROW_SIZE = 100;
	private DataTable<PricingResult, Class<?>> resultList;
	private final SortableDataProviderImpl<PricingResultUIWrapper> dataProvider = new SortableDataProviderImpl<>();

	@SuppressWarnings("serial")
	public PriceResultPanel(String id) {
		super(id);
		Form<String> form = new Form<>("exportToExcelForm");
		final AJAXDownload download = new AJAXDownload();
		form.add(download);
		final MessageDialog informDialog = new MessageDialog("infoSaveDialog", new ResourceModel("saving.done").getObject(),
			getString("saving.result"), DialogButtons.OK, DialogIcon.INFO) {
			@Override
			public void onClose(AjaxRequestTarget target, DialogButton button) {
			}
		};
		add(informDialog);
		form.add(new AjaxButton("exportToExcel") {

			@Override
			protected void onSubmit(AjaxRequestTarget target, final Form<?> form) {
				List<PricingResultUIWrapper> resultList = getSelectedElements(form);
				final File generateReport = pricingResultExcelExport.generateReport(resultList);
				download.initiate(target, generateReport);
				super.onSubmit(target, form);
			}
		});
		form.add(new AjaxButton("savePriceResults") {

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				List<PricingResultUIWrapper> resultList = getSelectedElements(form);
                resultList.forEach(pricingResultManager::save);
				informDialog.setModelObject(getString("saving.result") + resultList.size());
				informDialog.open(target);
				super.onSubmit(target, form);
			}

		});
		form.add(new AjaxButton("clearData") {

			@Override
			protected void onSubmit(AjaxRequestTarget target, final Form<?> form) {
				PriceResultPanel.this.clear();
				target.add(resultList, form);
				super.onSubmit(target, form);
			}
		});
		final CheckGroup<PricingResultUIWrapper> group = new CheckGroup<>("group", new ArrayList<>());

		final CheckGroupSelector groupSelector = new CheckGroupSelector("groupselector") {
			@Override
			protected boolean wantAutomaticUpdate() {
				return true;
			}
		};

		group.add(groupSelector);

		final MultiLinesMessageDialog descriptionDialog = new MultiLinesMessageDialog("descriptionDialog", new ResourceModel("desc.dialog.title").getObject());

		List<IColumn<PricingResultUIWrapper, String>> columns = createSortableColumns("asset.name", "priceDate",
			"currency.code", "productStartDate", "productStopDate", "kzk", "strikePrice", "strike2Price", "coefficient", "interestRate", "maximumEarning");
		columns.add(0, new AbstractColumn<PricingResultUIWrapper, String>(new ComponentDetachableModel<>()) {
			public void populateItem(Item<ICellPopulator<PricingResultUIWrapper>> cellItem, String componentId,
									 final IModel<PricingResultUIWrapper> model) {
				final PricingResultUIWrapper pricingResult = model.getObject();
				CheckBoxPanel panel = new CheckBoxPanel(componentId);
				Check<PricingResultUIWrapper> check = new Check<>("selected", new Model<PricingResultUIWrapper>() {

					@Override
					public PricingResultUIWrapper getObject() {
						return pricingResult;
					}
				});
				panel.add(check);
				cellItem.add(panel);
			}

			@Override
			public Component getHeader(String componentId) {
				CheckGroupPanel panel = new CheckGroupPanel(componentId);
				panel.add(groupSelector);
				return panel;
			}
		});
        columns.add(1, new PropertyColumn<PricingResultUIWrapper, String>(new ResourceModel("product.name"), "product.name", "product.name") {
            public void populateItem(Item<ICellPopulator<PricingResultUIWrapper>> cellItem, String componentId,
                                     final IModel<PricingResultUIWrapper> model) {
                cellItem.add(new BadgeInfoLabel(componentId, model.getObject().getProduct().getName()).setEscapeModelStrings(false));
            }
        });
		columns.add(new AbstractColumn<PricingResultUIWrapper, String>(new ResourceModel("direction"), "direction") {
			public void populateItem(Item<ICellPopulator<PricingResultUIWrapper>> cellItem, String componentId,
									 final IModel<PricingResultUIWrapper> model) {
				cellItem.add(new ArrowPanel(componentId, model.getObject().getDirection()));
			}
		});
		columns.add(new AbstractColumn<PricingResultUIWrapper, String>(new ResourceModel("description")) {
			public void populateItem(Item<ICellPopulator<PricingResultUIWrapper>> cellItem, String componentId,//last
									 final IModel<PricingResultUIWrapper> model) {
				DescriptionPanel panel = new DescriptionPanel(componentId);
				AjaxLink link = new AjaxLink<String>("description-link") {

					@Override
					public void onClick(AjaxRequestTarget target) {
						descriptionDialog.setModelObject(model.getObject().getDescription());
						descriptionDialog.open(target);

					}
				};
				panel.add(link);
				cellItem.add(panel);
			}
		});
		resultList = new AjaxFallbackDefaultDataTable(RESULT_LIST, columns, dataProvider, ROW_SIZE);
		group.add(resultList.setOutputMarkupId(true));
		form.add(group);
		add(descriptionDialog);

		add(form);
	}

	@Override
	public void onEvent(IEvent<?> event) {
		Object payload = event.getPayload();
		if (payload instanceof UpdateEvent) {
			UpdateEvent updateEvent = (UpdateEvent) payload;
			List<PricingResult> resultList = updateEvent.getPayload();
			dataProvider.setList(UIUtil.transformPricingResult(resultList));
		}
		super.onEvent(event);
	}
	public void clear() {
		dataProvider.clear();
	}

	private List<PricingResultUIWrapper> getSelectedElements(Form form) {
		@SuppressWarnings("unchecked")
		CheckGroup<PricingResultUIWrapper> group = (CheckGroup<PricingResultUIWrapper>) form.get("group");
		if (group.getModelObject().size() >= ROW_SIZE - 1) {
			return dataProvider.getList();
		} else {
			return new ArrayList<>(group.getModelObject());
		}
	}
}
