package ru.open.spc.web.componenets.model;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.core.util.lang.PropertyResolver;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.ISortState;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.ISortStateLocator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.IFilterStateLocator;
import org.apache.wicket.extensions.markup.html.repeater.util.SingleSortState;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.springframework.cglib.core.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.*;

//TODO merge with SortableDataProviderImpl
public class KendoDataProvider<N extends Serializable> extends ListDataProvider<N> implements ISortStateLocator<String>, IFilterStateLocator<N> {

	private final SingleSortState<String> state = new SingleSortState<>();

	private volatile N filterableBean;
	private List<N> originalList;

	public KendoDataProvider() {
		super(new ArrayList<N>());
	}

	@Override
	protected List<N> getData() {
		List<N> list = new ArrayList<>(this.originalList);
		if (filterableBean != null) {
			try {
				final Map<String, String> original = BeanUtils.describe(filterableBean);
				final Collection filter = CollectionUtils.filter(list, o -> {
					try {
						final Map<String, String> check = BeanUtils.describe(o);
						for (Map.Entry<String, String> entry : original.entrySet()) {
							if (!StringUtils.isEmpty(entry.getValue())) {
								final String value = check.get(entry.getKey());
								if (StringUtils.isEmpty(value) || !value.toLowerCase().contains(entry.getValue().toLowerCase())) {
										return false;
								}
							}
						}
						return true;
					} catch (Exception e) {
						return false;
					}
				});

				list = new ArrayList<>(filter);
			} catch (Exception e) { }
		}

		final SortParam<String> param = this.state.getSort();
		if (param != null) {
			Collections.sort(list, (c1, c2) -> {
				final Object o1 = PropertyResolver.getValue(param.getProperty(), c1);
				final Object o2 = PropertyResolver.getValue(param.getProperty(), c2);
				if (o1 != null && o2 != null) {
					return toComparable(o1).compareTo(toComparable(o2)) * (param.isAscending() ? 1 : -1);
				} else if (o1 == null && o2 == null) {
					return 0;
				} else if (o1 == null) {
					return 1;
				} else {
					return -1;
				}
			});
		}
		return list;
	}

	@Override
	public ISortState<String> getSortState() {
		return this.state;
	}

	private static Comparable<Object> toComparable(Object o) {
		if (o instanceof Comparable<?>) {
			if (o instanceof String) {
				final String lc = ((String) o).toLowerCase();
				final Object ob = lc;
				return (Comparable<Object>) ob;
			}
			return (Comparable<Object>) o;
		}
		throw new WicketRuntimeException("Object should be a Comparable");
	}


	public void setList(List<N> originalList) {
		super.getData().clear();
		super.getData().addAll(originalList);
		this.originalList = originalList;
	}

	public List<N> getList() {
		return new ArrayList<>(originalList);
	}


	@Override
	public N getFilterState() {
		try {
			filterableBean =  (N) filterableBean.getClass().newInstance();
			return filterableBean;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void setFilterState(N state) {
		this.filterableBean = state;

	}




}
