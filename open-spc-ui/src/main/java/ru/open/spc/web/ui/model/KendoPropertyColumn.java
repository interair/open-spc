package ru.open.spc.web.ui.model;

import com.googlecode.wicket.kendo.ui.datatable.column.PropertyColumn;
import org.apache.wicket.model.IModel;

public class KendoPropertyColumn extends PropertyColumn {
	public KendoPropertyColumn(String title) {
		super(title);
	}

	public KendoPropertyColumn(String title, int width) {
		super(title, width);
	}

	public KendoPropertyColumn(String title, String property) {
		super(title, property);
	}

	public KendoPropertyColumn(String title, String property, int width) {
		super(title, property, width);
	}

	public KendoPropertyColumn(IModel<String> title, String property) {
		super(title, property);
	}

	public KendoPropertyColumn(IModel<String> title, String property, int width) {
		super(title, property, width);
	}

	@Override
	public String getFilterable() {
		return "{ cell: { showOperators: false } }";
	}
}
