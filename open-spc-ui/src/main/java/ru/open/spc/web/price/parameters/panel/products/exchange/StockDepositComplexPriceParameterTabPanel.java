package ru.open.spc.web.price.parameters.panel.products.exchange;

import ru.open.spc.calc.model.complex.exchange.StockDepositComplexParameters;
import ru.open.spc.web.price.parameters.panel.ComplexPriceParameterTabPanel;

public class StockDepositComplexPriceParameterTabPanel extends ComplexPriceParameterTabPanel {

    public StockDepositComplexPriceParameterTabPanel(String id) {
        super(id, new StockDepositComplexParameters());
    }
}