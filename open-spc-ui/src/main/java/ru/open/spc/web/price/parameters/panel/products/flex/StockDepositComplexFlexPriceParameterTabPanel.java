package ru.open.spc.web.price.parameters.panel.products.flex;

import ru.open.spc.calc.model.complex.flex.StockDepositComplexFlexParameters;

public class StockDepositComplexFlexPriceParameterTabPanel extends ComplexFlexPriceParameterTabPanel {

    public StockDepositComplexFlexPriceParameterTabPanel(String id) {
        super(id, new StockDepositComplexFlexParameters());
    }
}