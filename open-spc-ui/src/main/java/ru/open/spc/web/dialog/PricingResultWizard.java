package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import com.googlecode.wicket.jquery.ui.widget.wizard.AbstractWizard;
import com.googlecode.wicket.kendo.ui.form.dropdown.DropDownList;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.event.Broadcast;
import org.apache.wicket.extensions.wizard.WizardModel;
import org.apache.wicket.extensions.wizard.WizardModel.ICondition;
import org.apache.wicket.extensions.wizard.WizardStep;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.calc.kernel.result.factory.ResultFactory;
import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.PricingResultManager;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.model.Product;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.model.enums.ProductCode;
import ru.open.spc.model.result.PricingResult;
import ru.open.spc.web.componenets.model.ProductsModel;
import ru.open.spc.web.componenets.renders.AssetUnityRender;
import ru.open.spc.web.componenets.renders.ChoiceRendererImpl;
import ru.open.spc.web.componenets.renders.DirectionRender;
import ru.open.spc.web.ui.model.PricingResultUIWrapper;
import ru.open.spc.web.util.UpdateEvent;

import java.util.Arrays;

import static ru.open.spc.web.util.WicketUtil.createDefaultDatePicker;

public class PricingResultWizard extends AbstractWizard<ResultBuilder> {
    private PricingResultUIWrapper pricingResultWrapper;
    @SpringBean private ProductManager productManager;
    @SpringBean private AssetManager assetManager;
    @SpringBean private ResultFactory resultFactory;
    @SpringBean private PricingResultManager pricingResultManager;
    private final WizardModel wizardModel = new WizardModel();

    public PricingResultWizard(String id, IModel<String> title, boolean editForm) {
        super(id, title);

        wizardModel.add(new ProductSelectStep(editForm));
        wizardModel.add(new ProductFieldsFillStep());
        wizardModel.add(new BaseAssetProductFieldsFillStep());
        wizardModel.add(new FixCouponProductFieldsFillStep());
        wizardModel.add(new SmartDepoProductFieldsFillStep());
        wizardModel.add(new StockDepoProductFieldsFillStep());
        wizardModel.add(new TimeOutFieldsFillStep());
        wizardModel.setLastVisible(editForm);
        this.init(wizardModel);
    }

    @Override
    protected void onFinish(final AjaxRequestTarget target) {
        final ResultBuilder resultBuilder = getModel().getObject();
        if (resultBuilder != null) {
            final PricingResult pricingResult = resultFactory.createPricingResult(resultBuilder);
            pricingResultManager.save(pricingResult);
            send(getParent(), Broadcast.BREADTH,
                    new UpdateEvent(target, new PricingResultUIWrapper(pricingResult), PricingResultDialog.PRICING_RESULT_EVENT));
        }
    }

    @Override
    protected IModel<?> initModel() {
        return new CompoundPropertyModel<ResultBuilder>(new Model<ResultBuilder>());
    }

    class ProductSelectStep extends WizardStep {

        public ProductSelectStep(boolean lastVisible) {
            super(new ResourceModel("select.product.type"), lastVisible ? new ResourceModel("result.wizard.timeout") : null);
            final DropDownList<Product> productsDropDown = new DropDownList<>(
                    "product", new ProductsModel(productManager), new ChoiceRendererImpl<>());
            productsDropDown.setRequired(true).setOutputMarkupId(true);
            this.add(productsDropDown);
        }
    }

    class ProductFieldsFillStep extends WizardStep {

        public ProductFieldsFillStep() {
            super(new ResourceModel("input.common.fields"), null);

            this.add(createDefaultDatePicker("productStartDate").setRequired(true));
            this.add(createDefaultDatePicker("productStopDate").setRequired(true));
            this.add(new RequiredTextField<>("strike1").setType(Double.class).setRequired(true));
//            this.add(new TextField<>("coefficient").setType(Double.class));
            this.add(new RequiredTextField<>("interestRate").setType(Double.class).setRequired(true));
            this.add(new DropDownChoice<>("asset",
                    assetManager.all(),
                    new ChoiceRendererImpl<>()).setRequired(true));

        }
    }

    static class TimeOutFieldsFillStep extends WizardStep {

        public TimeOutFieldsFillStep() {
            super(new ResourceModel("input.timeout.fields"), null);

            this.add(new Spinner<Long>("timeoutHours").setRequired(true));
            this.add(new Spinner<Long>("timeoutMinutes").setRequired(true));

        }
    }

    class BaseAssetProductFieldsFillStep extends WizardStep implements ICondition {

        public BaseAssetProductFieldsFillStep() {
            super(new ResourceModel("input.spec.fields"), null);

            this.add(new RequiredTextField<>("coeffAssetProtection").setType(Double.class).setRequired(true));
            this.add(new RequiredTextField<>("coefficient").setType(Double.class).setRequired(true));
            this.add(new RequiredTextField<>("strike2").setType(Double.class).setRequired(true));
            DropDownChoice<AssetUnity> vzkUnity = new DropDownChoice<>("vzkUnity",
                Arrays.asList(AssetUnity.RUB, AssetUnity.USD),
                new AssetUnityRender());
            this.add(vzkUnity);
//            this.add(new RequiredTextField<>("intervalOptionPrice").setType(Double.class).setRequired(true));
            this.add(new DropDownChoice<>("direction",
                    Arrays.asList(Direction.values()),
                    new DirectionRender()).setRequired(true));
        }

        @Override
        public boolean evaluate() {
            return ProductCode.BaseProtect == evaluateCurrentProductCode();
        }
    }

    class FixCouponProductFieldsFillStep extends WizardStep implements ICondition {

        public FixCouponProductFieldsFillStep() {
            super(new ResourceModel("input.spec.fields"), null);
            this.add(new RequiredTextField<>("maximumEarning").setType(Double.class).setRequired(true));
        }

        @Override
        public boolean evaluate() {
            return ProductCode.FixedCoupon == evaluateCurrentProductCode();
        }
    }

    class SmartDepoProductFieldsFillStep extends WizardStep implements ICondition {

        public SmartDepoProductFieldsFillStep() {
            super(new ResourceModel("input.spec.fields"), null);

            this.add(new RequiredTextField<>("maximumEarning").setType(Double.class).setRequired(true));
            this.add(new RequiredTextField<>("coeffAssetProtection").setType(Double.class).setRequired(true));
            this.add(new RequiredTextField<>("strike2").setType(Double.class).setRequired(true));
            this.add(new DropDownChoice<>("direction",
                    Arrays.asList(Direction.values()),
                    new DirectionRender()).setRequired(true));
        }

        @Override
        public boolean evaluate() {
            return ProductCode.SmartDeposit == evaluateCurrentProductCode();
        }
    }

    class StockDepoProductFieldsFillStep extends WizardStep implements ICondition {

        public StockDepoProductFieldsFillStep() {
            super(new ResourceModel("input.spec.fields"), null);

            this.add(new RequiredTextField<>("couponSize").setType(Double.class).setRequired(true));
            this.add(new RequiredTextField<>("stockNum").setType(Long.class).setRequired(true));
        }

        @Override
        public boolean evaluate() {
            return ProductCode.StocksDeposit == evaluateCurrentProductCode();
        }
    }

    ProductCode evaluateCurrentProductCode() {
        final Product product = getModel().getObject().getProduct();
        return product != null ? product.getCode() : null;
    }

}
