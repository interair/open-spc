package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.ui.widget.dialog.AbstractFormDialog;

import java.io.Serializable;

public abstract class ResizableDialog<T extends Serializable> extends AbstractFormDialog<T> {

    protected ResizableDialog(String id, String title, boolean modal) {
        super(id, title, modal);
    }

    public boolean isResizable() {
        return true;
    }
}
