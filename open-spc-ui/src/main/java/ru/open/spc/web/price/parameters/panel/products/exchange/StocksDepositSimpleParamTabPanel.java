package ru.open.spc.web.price.parameters.panel.products.exchange;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import org.apache.wicket.markup.html.form.RequiredTextField;
import ru.open.spc.calc.model.simple.exchange.StocksDepositSimpleParameters;
import ru.open.spc.web.price.parameters.panel.SimplePriceParameterTabPanel;

public class StocksDepositSimpleParamTabPanel extends SimplePriceParameterTabPanel<StocksDepositSimpleParameters> {

	public StocksDepositSimpleParamTabPanel(String id) {
		super(id, new StocksDepositSimpleParameters());

		addToForm(new RequiredTextField<Double>("fixStrike1"));
		addToForm(new Spinner<Double>("repoDiscount"));
		addToForm(new Spinner<Double>("repoRate"));
        addToForm(new Spinner<Integer>("stockNum"));
	}
}
