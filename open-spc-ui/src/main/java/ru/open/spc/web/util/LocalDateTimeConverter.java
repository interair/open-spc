package ru.open.spc.web.util;

import org.apache.wicket.util.convert.ConversionException;
import org.apache.wicket.util.convert.IConverter;
import org.apache.wicket.util.lang.Args;
import ru.open.spc.dao.util.DateUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class LocalDateTimeConverter implements IConverter<LocalDateTime> {

    private final String pattern;

    public LocalDateTimeConverter() {
        this(UIUtil.DATE_TIME_FORMAT);
    }

    public LocalDateTimeConverter(String pattern) {
        this.pattern = Args.notNull(pattern, "Pattern");
    }

    private DateTimeFormatter getFormatter() {
        return DateTimeFormatter.ofPattern(pattern);
    }

    @Override
    public LocalDateTime convertToObject(String value, Locale locale) {
        try {
            return DateUtil.toLocalDateTime(DateFormat.getDateInstance(DateFormat.SHORT, locale).parse(value));
        } catch (final ParseException e) {
            throw new ConversionException(e.getMessage(), e);
        }
    }

	@Override
    public String convertToString(LocalDateTime value, Locale locale) {
        return value == null ? "" : getFormatter().format(value);
    }

}