package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.CurrencyManager;
import ru.open.spc.model.Currency;

import static ru.open.spc.web.util.DefaultDialogButtons.getSaveButton;

@SuppressWarnings("serial")
public class CurrencyDialog extends ResizableDialog<Currency> {

	@SpringBean private CurrencyManager currencyManager;
	private final Form<Currency> currencyForm;

	public CurrencyDialog(String id) {
		super(id, new ResourceModel("currency.title").getObject(), true);
		currencyForm = new Form<>("currencyForm");
		currencyForm.setOutputMarkupId(true);
		currencyForm.add(new JQueryFeedbackPanel("feedbackPanel"));
		currencyForm.add(new RequiredTextField<String>("name"));
		currencyForm.add(new RequiredTextField<String>("code"));
		currencyForm.add(new RequiredTextField<Long>("number"));
		add(currencyForm);
	}

	@Override
	protected DialogButton getSubmitButton() {
		return getSaveButton();
	}

	public void setDefaultModelObject(Currency currency) {
		currencyForm.setDefaultModel(new CompoundPropertyModel<>(currency));
	}

	@Override
	public Form<?> getForm() {
		return currencyForm;
	}

	@Override
	protected void onOpen(AjaxRequestTarget target) {
		target.add(currencyForm);
	}

	@Override
	protected void onError(AjaxRequestTarget target) {
		target.add(get("currencyForm").get("feedbackPanel"));
	}

	@Override
	protected void onSubmit(AjaxRequestTarget target) {
		Currency currency = this.currencyForm.getModel().getObject();
		if (currency != null) {
			currencyManager.save(currency);
		}
		target.add(getParent());
	}

}
