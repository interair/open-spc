package ru.open.spc.web.pages;

import com.googlecode.wicket.jquery.core.Options;
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import com.googlecode.wicket.kendo.ui.datatable.DataTable;
import com.googlecode.wicket.kendo.ui.datatable.button.CommandButton;
import com.googlecode.wicket.kendo.ui.datatable.button.ToolbarButton;
import com.googlecode.wicket.kendo.ui.datatable.column.CommandColumn;
import com.googlecode.wicket.kendo.ui.datatable.column.IColumn;
import com.googlecode.wicket.kendo.ui.form.button.AjaxButton;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.LdapRepository;
import ru.open.spc.dao.manager.UserDetailsManager;
import ru.open.spc.model.auth.UserAuthDetails;
import ru.open.spc.web.AbstractWebPage;
import ru.open.spc.web.componenets.model.UserDataProvider;
import ru.open.spc.web.ui.model.KendoPropertyColumn;
import ru.open.spc.web.ui.model.Role;

import java.util.*;

import static ru.open.spc.web.ui.model.Role.*;
import static ru.open.spc.web.util.WicketUtil.createColumns;

public class AdminPage extends AbstractWebPage {

	@SpringBean
	private UserDetailsManager userDetailsManager;
	@SpringBean
	private LdapRepository ldapRepository;

	private Map<String, UserAuthDetails> ldapMap;

	private final UserDataProvider provider = new UserDataProvider();

	public AdminPage() {
		final Form<Void> form = new Form<Void>("form");
		final JQueryFeedbackPanel feedback = new JQueryFeedbackPanel("feedbackPanel");
		form.add(feedback);
		List<IColumn> columns = createColumns("username", "fullName", "title", "department", "company", "description", "role");
		columns.add(0, new KendoPropertyColumn(new ResourceModel("id"), "id", 50) {

			@Override
			public String getFilterable() {
				return "false";
			}
		});

		columns.add(new CommandColumn("") {

			@Override
			public List<CommandButton> newButtons() {
				return Arrays.asList(new CommandButton(NONE.name(), new ResourceModel(NONE.name()), "username"),
						new CommandButton(ROLE_USER.name(), new ResourceModel(ROLE_USER.name()), "username"),
						new CommandButton(ROLE_ADMIN.name(), new ResourceModel(ROLE_ADMIN.name()), "username"));
			}
		});
		final Options tableOptions = new Options();
		tableOptions.set("pageable", "{ pageSizes: [ 10, 25, 50 ] }");
		tableOptions.set("columnMenu", true);
		tableOptions.set("groupable", true);
		tableOptions.set("filterable", "{mode: 'row', showOperators: 'false'}");
		tableOptions.set("reorderable", true);
		tableOptions.set("sortable", true);
		tableOptions.set("resizable", true);
//		tableOptions.set("selectable", Options.asString("multiple"));
//		tableOptions.set("toolbar", "[ { name: 'allUsers', text: 'Все пользователи' }, { name: 'systemUsers', text: 'Зарегистрированные пользователи' } ]");

		final DataTable<UserAuthDetails> table = new DataTable<UserAuthDetails>("usersTables", columns, provider, 50, tableOptions) {

			/**
			 * Triggered when a toolbar button is clicked.
			 */
			@Override
			public void onClick(AjaxRequestTarget target, ToolbarButton button, List<String> values) {
				this.info(button.getText().getObject() + " " + values);
				target.add(feedback);
			}

			/**
			 * Triggered when a column button is clicked.
			 */
			@Override
			public void onClick(AjaxRequestTarget target, CommandButton button, String value) {

				final UserAuthDetails authDetails = ldapMap.get(value);
				final Role role = Role.valueOf(button.getName());
				this.info(button.getName() + " #" + authDetails.getFullName());
				authDetails.setRole(role.role());
				userDetailsManager.save(authDetails);
				target.add(feedback);
				refresh(target);
			}

		};

		form.add(new AjaxButton("showAll", new ResourceModel("showAll")) {

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				provider.setShowAll(true);
				table.refresh(target);
			}
		});

		form.add(new AjaxButton("existingUser", new ResourceModel("existingUser")) {

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				provider.setShowAll(false);
				table.refresh(target);
			}
		});

		add(table);
		add(form);


	}

	@Override
	protected void onBeforeRender() {
		provider.setFilterState(new UserAuthDetails());
		final List<UserAuthDetails> allPersonNames = ldapRepository.getAllPersonNames();
		ldapMap = createMap(allPersonNames);
		final List<UserAuthDetails> all = userDetailsManager.all();
		for (UserAuthDetails authDetails : all) {
			final UserAuthDetails enrichedLdapDetails = ldapMap.get(authDetails.getUsername());
			if (enrichedLdapDetails != null) {
				map(authDetails, enrichedLdapDetails);
			}
		}
		final List<UserAuthDetails> values = new ArrayList<>(ldapMap.values());
		Collections.sort(values, (o1, o2) -> Long.compare(o1.getId() == null? Integer.MAX_VALUE : o1.getId(), o2.getId() == null? Integer.MAX_VALUE: o2.getId()));
		provider.setList(values);
		super.onBeforeRender();
	}

	private UserAuthDetails map(UserAuthDetails item, UserAuthDetails details) {
		details.setId(item.getId());
		details.setRole(item.getRole());
		return details;

	}

	private Map<String, UserAuthDetails> createMap(List<UserAuthDetails> allPersonNames) {
		Map<String, UserAuthDetails> map = new HashMap<>();
		allPersonNames.forEach(userAuthDetails -> map.put(userAuthDetails.getUsername(), userAuthDetails));
		return map;
	}
}

