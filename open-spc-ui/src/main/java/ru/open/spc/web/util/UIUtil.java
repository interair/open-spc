package ru.open.spc.web.util;

import ru.open.spc.model.result.PricingResult;
import ru.open.spc.web.ui.model.PricingResultUIWrapper;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UIUtil {
	public static final String LINE_WRAP = "\n";
	public static final String COMMA = ", ";
	public static final String EXCEL_DATE_TIME_FORMAT = "dd.MM.yy HH.mm.ss";
    public static final String DATE_TIME_FORMAT = "dd.MM.yy HH:mm:ss";
	public static final String TIME_FORMAT = "HH:mm:ss";
	public static final String DATE_FORMAT = "dd.MM.yy";
	private static final String JS_DATE = "Date.UTC( {0}, {1}, {2} )";
	public static final String ID_FORMAT = "##########";
	public static final String DURATION_FORMAT="%1$dч:%2$dм";

	public static String getDateInUTC(LocalDate date) {
		return MessageFormat.format(JS_DATE, formatId(date.getYear()), formatId(date.getMonth().ordinal()),
            formatId(date.getDayOfMonth()));
	}

	public static NumberFormat getIdFormatter() {
		return getNumberFormatter(ID_FORMAT);
	}

	public static String formatId(int identifier) {
		return getIdFormatter().format(identifier);
	}

	public static NumberFormat getNumberFormatter(String format) {
		return new DecimalFormat(format);
	}

	public static List<PricingResultUIWrapper> transformPricingResult(List<PricingResult> resultList) {
		List<PricingResultUIWrapper> pr = new ArrayList<>(resultList.size());
        pr.addAll(resultList.stream().map(PricingResultUIWrapper::new).collect(Collectors.toList()));
		return pr;
	}
}
