package ru.open.spc.web.pages;

import ru.open.spc.web.price.parameters.panel.PriceFlexParametersPanel;

public class FlexPricePage extends DefaultPricePage {

    public FlexPricePage() {
        super("priceParamsPanel", PriceFlexParametersPanel.class);
    }
}