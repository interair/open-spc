package ru.open.spc.web.pages;

import ru.open.spc.web.price.parameters.panel.PriceExchangeParametersPanel;

public class ExchangePricePage extends DefaultPricePage {

    public ExchangePricePage() {
        super("priceParamsPanel", PriceExchangeParametersPanel.class);
    }
}