package ru.open.spc.web.util;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleAware {

	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("ru.open.spc.web.spc",
		Locale.ROOT, DescriptionWriterImpl.class.getClassLoader());

	protected static String get(String key) {
		return resourceBundle.getString(key);
	}

	protected static String[] get(String... keys) {
		String[] result = new String[keys.length];
		for (int i = 0; i < keys.length; i++) {
			result[i] = get(keys[i]);
		}
		return result;
	}
}
