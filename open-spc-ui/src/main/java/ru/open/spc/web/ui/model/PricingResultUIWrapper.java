package ru.open.spc.web.ui.model;

import ru.open.spc.calc.model.ResultBuilder;
import ru.open.spc.calc.model.TimeoutHolder;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Currency;
import ru.open.spc.model.Product;
import ru.open.spc.model.ResultVisitor;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.model.result.*;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

@SuppressWarnings("serial")
public class PricingResultUIWrapper extends PricingResult implements BaseAssetPricingResult,
		FixCouponPricingResult, StocksDepositPricingResult, SmartDepoPricingResult, TimeoutHolder {
	private final PricingResult pricingResult;
	private final Context context;

	public PricingResultUIWrapper(PricingResult pricingResult) {
		this.pricingResult = pricingResult;
		ResultVisitorDispatcher dispatcher = new ResultVisitorDispatcher();
		pricingResult.applyVisitor(dispatcher);
		this.context = dispatcher.context;
	}

	private static class Context implements Serializable {
		Double coefficient;
		Double kzk;
		Double maximumEarning;
		Double strike2Price;
		Double couponSize;
        Long investingAmount;
        Currency vzkCurrency;
	}

	private static class ResultVisitorDispatcher implements ResultVisitor {

		final Context context = new Context();

		@Override
		public void visit(BAPricingResult baPricingResult) {
			context.coefficient = baPricingResult.getCoefficient();
			context.kzk = baPricingResult.getKzk();
			context.strike2Price = baPricingResult.getStrike2Price();
            context.vzkCurrency = baPricingResult.getVzkCurrency();
		}

		@Override
		public void visit(FCPricingResult fcPricingResult) {
			context.maximumEarning = fcPricingResult.getMaximumEarning();
		}

		@Override
		public void visit(SDPricingResult sdPricingResult) {
			context.maximumEarning = sdPricingResult.getCouponSize();
			context.couponSize = sdPricingResult.getCouponSize();
            context.investingAmount = sdPricingResult.getInvestingAmount();
		}

		@Override
		public void visit(SMPricingResult smPricingResult) {
			context.maximumEarning = smPricingResult.getMaximumEarning();
			context.kzk = smPricingResult.getKzk();
			context.strike2Price = smPricingResult.getStrike2Price();
		}

		@Override
		public void visit(ACPricingResult acPricingResult) {
			context.maximumEarning = acPricingResult.getMaximumEarning();
		}
	}

	@Override
	public Double getCoefficient() {
		return context.coefficient;
	}

	@Override
	public Double getKzk() {
		return context.kzk;
	}

	@Override
	public Double getMaximumEarning() {
		return context.maximumEarning;
	}

	@Override
	public void applyVisitor(ResultVisitor visitor) {
		pricingResult.applyVisitor(visitor);
	}

	public PricingResult getPricingResult() {
		return pricingResult;
	}

	@Override
	public Long getProductId() {
		return pricingResult.getProductId();
	}

	@Override
	public Long getAssetId() {
		return pricingResult.getAssetId();
	}

	@Override
	public Direction getDirection() {
		return pricingResult.getDirection();
	}

	@Override
	public Long getId() {
		return pricingResult.getId();
	}

	@Override
	public Product getProduct() {
		return pricingResult.getProduct();
	}

	@Override
	public Asset getAsset() {
		return pricingResult.getAsset();
	}

	@Override
	public LocalDateTime getPriceDate() {
		return pricingResult.getPriceDate();
	}

	@Override
	public Currency getCurrency() {
		return pricingResult.getCurrency();
	}

	@Override
	public LocalDate getProductStartDate() {
		return pricingResult.getProductStartDate();
	}

	@Override
	public LocalDate getProductStopDate() {
		return pricingResult.getProductStopDate();
	}

	@Override
	public Double getStrikePrice() {
		return pricingResult.getStrikePrice();
	}

	@Override
	public Integer getInterval() {
		return pricingResult.getInterval();
	}

	@Override
	public Double getCouponSize() {
		return context.couponSize;
	}

    @Override
    public Long getInvestingAmount() {
        return context.investingAmount;
    }

    @Override
	public Double getPercentByLoanDeal() {
		return pricingResult.getPercentByLoanDeal();
	}

	@Override
	public Double getStrike2Price() {
		return context.strike2Price;
	}

    @Override
    public Currency getVzkCurrency() {
        return context.vzkCurrency;
    }

    @Override
	public String getDescription() {
		return pricingResult.getDescription();
	}

	@Override
	public Double getInterestRate() {
		return pricingResult.getInterestRate();
	}

	@Override
	public Duration getTimeout() {
		return pricingResult.getTimeout();
	}

    @Override
    public Double getAssetPrice() {return pricingResult.getAssetPrice();}

    @Override
    public Double getInvestingVolume() {return pricingResult.getInvestingVolume();}

	public LocalDateTime getOfferEnd() {
		return getPriceDate().plus(getTimeout());
	}

    public ResultBuilder toResultBuilder() {

        ResultBuilder resultBuilder = new ResultBuilder()
                .id(getId())
                .asset(getAsset())
                .assetPrice(getAssetPrice())
                .product(getProduct())
                .priceDate(getPriceDate())
                .productStopDate(getProductStopDate())
                .productStartDate(getProductStartDate())
                .strike1(getStrikePrice())
                .descResult(getDescription())
                .direction(getDirection())
                .timeout(getTimeout())
                .optionPrice(pricingResult.getOptionPrice())
                .interestRate(getInterestRate())
                .coefficient(getCoefficient())
                .coeffAssetProtection(getKzk())
                .maximumEarning(getMaximumEarning())
                .strike2(getStrike2Price())
                .couponSize(getCouponSize())
                .stockNum(getInvestingAmount());
        if (getVzkCurrency() != null) {
            resultBuilder.vkzUnity(AssetUnity.fromCode(getVzkCurrency().getCode().substring(0, 1)));
        }
        return resultBuilder;
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof PricingResultUIWrapper)) {
			return false;
		}

		PricingResultUIWrapper that = (PricingResultUIWrapper) o;

        return !(getId() != null ? !getId().equals(that.getId()) : that.getId() != null);

    }

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (getId() != null ? getId().hashCode() : 0);
		return result;
	}

}
