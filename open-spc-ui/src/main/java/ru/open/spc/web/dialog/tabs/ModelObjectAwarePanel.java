package ru.open.spc.web.dialog.tabs;

import org.apache.wicket.markup.html.panel.Panel;
import ru.open.spc.model.Asset;

public abstract class ModelObjectAwarePanel extends Panel {

    public ModelObjectAwarePanel(String id) {
        super(id);
    }

    public abstract void setModelObject(Asset identifiable);
}
