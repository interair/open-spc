package ru.open.spc.web.componenets.renders;

import org.apache.wicket.markup.html.form.IChoiceRenderer;
import ru.open.spc.model.enums.StringCodeAware;
import ru.open.spc.web.util.ResourceBundleAware;

public abstract class StringChoiceRenderer<T extends StringCodeAware>
	extends ResourceBundleAware implements IChoiceRenderer<T> {

	@Override
	public String getIdValue(T object, int index) {
		return object.code();
	}

	@Override
	public Object getDisplayValue(T object) {
		return get(getKey(object));
	}

	public abstract String getKey(T object);
}
