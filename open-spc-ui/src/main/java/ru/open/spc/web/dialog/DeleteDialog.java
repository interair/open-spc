package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogIcon;
import com.googlecode.wicket.jquery.ui.widget.dialog.MessageDialog;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.event.Broadcast;
import ru.open.spc.dao.manager.GenericManager;
import ru.open.spc.model.common.Identifiable;
import ru.open.spc.web.util.UpdateEvent;

@SuppressWarnings("serial")
public class DeleteDialog<T extends Identifiable> extends MessageDialog {

	public static final String DELETE_EVENT = "DELETE_EVENT";
	private GenericManager<T> manager;
	private T modelObject;

	public DeleteDialog(String id, String title, String message, DialogButtons buttons) {
		super(id, title, message, buttons, DialogIcon.WARN);
	}

	public void setObject(T model) {
		modelObject = model;
	}

	public void setGenericManager(GenericManager<T> manager) {
		this.manager = manager;
	}

	@Override
	public void onClose(AjaxRequestTarget target, DialogButton button) {

		if (button != null && button.match(LBL_YES.getObject().toUpperCase()) && modelObject != null) {
			if (manager != null) {
				manager.remove(modelObject);
				target.add(getParent());
			}
			send(getParent(), Broadcast.BREADTH,
				new UpdateEvent(target, modelObject, DELETE_EVENT));
		}
	}
}
