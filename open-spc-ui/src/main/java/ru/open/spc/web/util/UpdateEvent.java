package ru.open.spc.web.util;

import lombok.Data;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;

import java.io.Serializable;

@Data public class UpdateEvent implements Serializable {

	private final AjaxRequestTarget target;
	private final Object payload;
	private final String topic;

	public UpdateEvent(AjaxRequestTarget target, Object payload, String topic) {
		this.target = target;
		this.payload = payload;
		this.topic = topic;
	}

	public <T> T getPayload() {
		return (T) payload;
	}

	public boolean isAccepted(Component comp, Class<?> clazz) {
		return isAccepted(clazz) && isAccepted(comp.getId());
	}

	public boolean isAccepted(Class<?> clazz) {
		return clazz.isInstance(payload);
	}

	public boolean isAccepted(String id) {
		return id.startsWith(topic);
	}

}
