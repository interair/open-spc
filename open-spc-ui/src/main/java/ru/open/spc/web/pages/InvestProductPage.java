package ru.open.spc.web.pages;

import com.googlecode.wicket.jquery.core.Options;
import com.googlecode.wicket.kendo.ui.datatable.DataTable;
import com.googlecode.wicket.kendo.ui.datatable.column.IColumn;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.PricingResultManager;
import ru.open.spc.web.componenets.model.KendoDataProvider;
import ru.open.spc.web.ui.model.PricingResultUIWrapper;

import java.util.List;
import java.util.stream.Collectors;

import static ru.open.spc.web.util.WicketUtil.createColumns;

public class InvestProductPage extends WebPage {

	@SpringBean
	private PricingResultManager pricingResultManager;

	private final KendoDataProvider<PricingResultUIWrapper> provider = new KendoDataProvider<>();

	public InvestProductPage() {
		List<IColumn> columns = createColumns("id:50", "product.name:90", "asset.name", "asset.externalContractTicker", "investingVolume", "investingAmount", "priceDate", "currency.code:60",
				"productStartDate", "productStopDate", "kzk:50", "strikePrice", "strike2Price", "coefficient:60", "maximumEarning", "assetPrice");

		final Options tableOptions = new Options();
		tableOptions.set("pageable", "{ pageSizes: [ 10, 25, 50 ] }");
		tableOptions.set("groupable", true);
		tableOptions.set("columnMenu", true);
		tableOptions.set("reorderable", true);
		tableOptions.set("resizable", true);
//		tableOptions.set("filterable", "{mode: 'row', showOperators: 'false'}");
		add(new DataTable("investProductList", columns, provider, 50, tableOptions));
	}

	@Override
	protected void onBeforeRender() {
		provider.setList(pricingResultManager.getActiveInvestIdeas().stream()
				.map(item -> new PricingResultUIWrapper(item)).collect(Collectors.toList()));
		super.onBeforeRender();
	}
}
