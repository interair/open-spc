package ru.open.spc.web.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.open.spc.calc.util.UiCallback;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

//TODO: Agggr... remove it! try aggregator
public class UiCallbackImpl<T> implements UiCallback<T> {

	private final static Logger LOG = LoggerFactory.getLogger(UiCallbackImpl.class);

	private final AtomicInteger totalSteps = new AtomicInteger();
	private final AtomicInteger phase = new AtomicInteger();
	private final AtomicInteger currentPhase = new AtomicInteger();
	private final AtomicInteger currentSteps = new AtomicInteger();
	private final AtomicBoolean isDone = new AtomicBoolean(false);
	private final List<T> result = new CopyOnWriteArrayList<>();

	public void setTotalStepsAmount(final int totalStep) {
		LOG.info("Total amount of steps {}", totalStep);
		if (totalStep == 0) {
			finish();
		}
		this.totalSteps.set(totalStep);
		currentSteps.set(1);
		incrementPhase();
	}

	public int getIndicatorValue() {
		if (totalSteps.get() == 0) {
			return -1;
		} else {
			return currentSteps.get() * 100 / totalSteps.get();
		}
	}

	public int getTotalSteps() {
		return totalSteps.get();
	}

	public boolean isDone() {
		return isDone.get();
	}

	public List<T> getResult() {
		return result;
	}

	public void finish() {
		isDone.set(true);
	}

	public int getTotalPhases() {
		return phase.get();
	}

	public int getCurrentPhase() {
		return currentPhase.get();
	}

	public void increment() {
		int curr = currentSteps.getAndIncrement();
		if (getCurrentPhase() >= getTotalPhases() && curr >= getTotalSteps()) {
			finish();
		}
		LOG.info("curr step {}", curr);
	}

	public void setPhaseCount(final int count) {
		phase.set(count);
	}

	public void incrementPhase() {
		currentPhase.getAndIncrement();
		LOG.info("curr phase {}", currentPhase);
	}

	public void addResults(final List<T> results) {
		result.addAll(results);
		increment();
	}
}
