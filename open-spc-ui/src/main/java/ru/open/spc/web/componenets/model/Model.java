package ru.open.spc.web.componenets.model;

import org.apache.wicket.model.IModel;
import ru.open.spc.web.util.ResourceBundleAware;

public abstract class Model<T> extends ResourceBundleAware implements IModel<T> {

	@Override
	public abstract T getObject();

	@Override
	public void setObject(T object) { }

	@Override
	public void detach() { }
}
