package ru.open.spc.web.price.parameters.panel;

import com.googlecode.wicket.jquery.ui.widget.tabs.AjaxTab;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.ParametersRepository;
import ru.open.spc.model.Product;
import ru.open.spc.model.enums.ProductCode;
import ru.open.spc.web.price.parameters.panel.annotation.PricingTabComponent;

import java.io.Serializable;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.open.spc.model.CalculationType.COMPLEX;
import static ru.open.spc.model.CalculationType.SIMPLE;

public class TabFactoryImpl implements TabFactory {

	private static final Logger LOG = LoggerFactory.getLogger(TabFactory.class);

	private Map<ProductCode, List<Class<?>>> productTabs;
	private CalculationBase calculationBase;
	private ParametersRepository parametersRepository;

	private final Map<ProductCode, List<ITab>> cacheTabs = new EnumMap<>(ProductCode.class);

	private ITab createTab(final TabHolder tabHolder) {
		return new Tab(new ResourceModel(tabHolder.tabNameResource), tabHolder, calculationBase);
	}

	public List<ITab> createTabsList(Product product) {
		if (cacheTabs.get(product.getCode()) == null) {
			List<Class<?>> components = productTabs.get(product.getCode());
			List<ITab> collect = components.stream().map(compClass -> createTab(createTabHolder(compClass, product.getCode()))).collect(Collectors.toList());
			cacheTabs.put(product.getCode(), collect);
		}
		return cacheTabs.get(product.getCode());
	}

	private TabHolder createTabHolder(Class<?> compClass, ProductCode code) {
		if (!compClass.isAnnotationPresent(PricingTabComponent.class)) {
			throw new IllegalArgumentException("Component must have PricingTabComponent annot.!");
		}
		PricingTabComponent pc = compClass.getAnnotation(PricingTabComponent.class);
		return new TabHolder(compClass, pc.isSimple(), pc.nameResourceKey(), code);
	}

	private final static class TabHolder implements Serializable {
		public TabHolder(Class<?> componentClass, boolean simple, String tabNameResource, ProductCode code) {
			this.componentClass = componentClass;
			this.simple = simple;
			this.tabNameResource = tabNameResource;
			this.code = code;
		}

		private final Class<?> componentClass;
		private final boolean simple;
		private final String tabNameResource;
		private final ProductCode code;
	}

	private final static class Tab extends AjaxTab {

		private final TabHolder tabHolder;
		private final CalculationBase calculationBase;

		public Tab(IModel<String> title, TabHolder tabHolder, CalculationBase calculationBase) {
			super(title);
			this.tabHolder = tabHolder;
			this.calculationBase = calculationBase;
		}

		@Override
		protected WebMarkupContainer getLazyPanel(String panelId) {
			try {
				BaseParametersPanel container = (BaseParametersPanel) tabHolder
						.componentClass.getConstructor(String.class).newInstance(panelId
						);
				container.setCalculationType(tabHolder.simple ? SIMPLE : COMPLEX);
				container.setProductCode(tabHolder.code);
				container.setCalculationBase(calculationBase);
				container.setOutputMarkupId(true);
				return container;
			} catch (Exception ex) {
				LOG.error("Oooops.. can't create tab!", ex);
				throw new IllegalStateException(ex);// we can't restore here
			}
		}
	}

	@Required
	public void setParametersRepository(ParametersRepository parametersRepository) {
		this.parametersRepository = parametersRepository;
	}

	@Required
	public void setProductTabs(Map<ProductCode, List<Class<?>>> productTabs) {
		this.productTabs = productTabs;
	}

	@Required
	public void setCalculationBase(CalculationBase calculationBase) {
		this.calculationBase = calculationBase;
	}
}
