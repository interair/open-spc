package ru.open.spc.web.price.parameters.panel.annotation;

import java.lang.annotation.*;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Documented
@Target(ElementType.TYPE)
@Inherited
public @interface PricingTabComponent {
	String nameResourceKey();

	boolean isSimple() default true;
}
