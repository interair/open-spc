package ru.open.spc.web.util;

import org.apache.wicket.util.convert.ConversionException;
import org.apache.wicket.util.convert.IConverter;

import java.time.Duration;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DurationConverter implements IConverter<Duration> {

	private static final Pattern pattern = Pattern.compile("(\\d+)ч:(\\d+)м");

	@Override
	public Duration convertToObject(String value, Locale locale) throws ConversionException {
		Matcher matcher = pattern.matcher(value);

		if (!matcher.find() || matcher.groupCount() != 2) {
			throw new IllegalArgumentException("Illegal string for parse: " + value);
		}
		long h = Long.parseLong(matcher.group(1));
		long m = Long.parseLong(matcher.group(2));

		return Duration.ofMinutes(h * 60 + m);
	}

	@Override
	public String convertToString(Duration value, Locale locale) {
		long m = value.toMinutes();

		return String.format(UIUtil.DURATION_FORMAT, m / 60, m % 60);
	}
}
