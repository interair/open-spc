package ru.open.spc.web.price.parameters.panel.products.flex;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import org.apache.wicket.validation.validator.RangeValidator;
import ru.open.spc.calc.model.complex.AbstractComplexCalculationParameters;
import ru.open.spc.web.price.parameters.panel.ComplexPriceParameterTabPanel;

public class ComplexFlexPriceParameterTabPanel extends ComplexPriceParameterTabPanel {
    public ComplexFlexPriceParameterTabPanel(String id, AbstractComplexCalculationParameters model) {
        super(id, model);
        addToForm(new Spinner<Long>("dateStep").setMin(1).add(new RangeValidator<Long>(1L, 365L)).setRequired(true));
    }
}
