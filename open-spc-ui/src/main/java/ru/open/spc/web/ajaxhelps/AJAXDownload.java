package ru.open.spc.web.ajaxhelps;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.AbstractAjaxBehavior;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.request.resource.ContentDisposition;
import org.apache.wicket.util.resource.AbstractResourceStreamWriter;
import org.apache.wicket.util.resource.IResourceStream;
import org.apache.xmlbeans.impl.common.IOUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

public class AJAXDownload extends AbstractAjaxBehavior {

	private final boolean addAntiCache;
	private File file;

	public AJAXDownload() {
		this(true);
	}

	public AJAXDownload(boolean addAntiCache) {
		this.addAntiCache = addAntiCache;
	}

	public void initiate(AjaxRequestTarget target, File file) {
		this.file = file;
		String url = getCallbackUrl().toString();

		if (addAntiCache) {
			url = url + (url.contains("?") ? "&" : "?");
			url = url + "antiCache=" + System.currentTimeMillis();
		}
		// the timeout is needed to let Wicket release the channel
		target.appendJavaScript("setTimeout(\"window.location.href='" + url + "'\", 100);");
	}

	public void onRequest() {
		ResourceStreamRequestHandler handler = new ResourceStreamRequestHandler(getResourceStream(), getFileName());
		handler.setContentDisposition(ContentDisposition.ATTACHMENT);
		getComponent().getRequestCycle().scheduleRequestHandlerAfterCurrent(handler);
	}

	protected String getFileName() {
		return file.getName();
	}

	protected IResourceStream getResourceStream() {
		return new AbstractResourceStreamWriter() {
			@Override
			public void write(OutputStream out) throws IOException {
				IOUtil.copyCompletely(new FileInputStream(file), out);
			}
		};
	}
}