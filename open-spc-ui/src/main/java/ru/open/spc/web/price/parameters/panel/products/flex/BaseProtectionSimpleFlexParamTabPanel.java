package ru.open.spc.web.price.parameters.panel.products.flex;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import com.googlecode.wicket.kendo.ui.form.dropdown.DropDownList;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormSubmitBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.open.spc.calc.model.simple.flex.BasicProtectionFlexSimpleParameters;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.model.result.BAPricingResult;
import ru.open.spc.model.result.PricingResult;
import ru.open.spc.web.componenets.renders.DirectionRender;
import ru.open.spc.web.util.UiCallbackImpl;

import java.util.Arrays;
import java.util.List;

public class BaseProtectionSimpleFlexParamTabPanel extends SimplePriceParameterFlexTabPanel<BasicProtectionFlexSimpleParameters> {

	private static final Logger LOG = LoggerFactory.getLogger(BaseProtectionSimpleFlexParamTabPanel.class);

	private final TextField<Double> strike2 = new TextField<>("strike2");
	private final TextField<Double> intervalOptionPrice = new TextField<>("intervalOptionPrice");
	private final CheckBox interval;
	private final TextField<Double> optionIntervalIV = new TextField<>("optionIntervalIV");
	private final CheckBox showAllOptions = new CheckBox("showAllOptions");

	public BaseProtectionSimpleFlexParamTabPanel(String id) {
		super(id, new BasicProtectionFlexSimpleParameters());
		addToForm(new Spinner<Double>("coeffAssetProtection").setRequired(true));
		addToForm(new DropDownList<>("direction", Arrays.asList(Direction.values()), new DirectionRender()).setRequired(true));
		strike2.setOutputMarkupPlaceholderTag(true);
		intervalOptionPrice.setOutputMarkupPlaceholderTag(true).setEnabled(false);
		addToForm(optionIntervalIV);
		interval = new AjaxCheckBox("interval") {
			@Override
			protected void onUpdate(AjaxRequestTarget ajaxRequestTarget) {
				Boolean model = this.getModel().getObject();
				strike2.setEnabled(model);
				optionIntervalIV.setEnabled(model);
				showAllOptions.setEnabled(model);
				ajaxRequestTarget.add(strike2, intervalOptionPrice, optionIntervalIV, showAllOptions);
			}
		};

		AjaxFormSubmitBehavior submitBehavior = new AjaxFormSubmitBehavior(
				getCalculationParamForm(), "onkeyup") {

			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				final UiCallback<PricingResult> callback = new UiCallbackImpl<>();
				try {
					getCalculation().calculateSync(getCurrentParams(), callback, getCalculationBase(), getCalculationType());
				} catch (Exception e) {
					LOG.error("Ooopss...", e);
					getCalculationParamForm().error(e);
				}
				final List<PricingResult> result = callback.getResult();
				Double price = null;
				if (!result.isEmpty()) {
					for (PricingResult pricingResult : result) {
						price = ((BAPricingResult) pricingResult).getIntervalOptionPrice();
						if (price != null && price > 0) break;
					}
				}
				intervalOptionPrice.setModelObject(price);
				target.add(intervalOptionPrice);
			}

			@Override
			protected void onError(AjaxRequestTarget target) {
			}
		};
		optionIntervalIV.add(submitBehavior);
		addToForm(interval);
		addToForm(strike2);
		addToForm(intervalOptionPrice);
		addToForm(showAllOptions);
	}

	@Override
	protected void onBeforeRender() {
		super.onBeforeRender();
		Boolean model = interval.getModel().getObject();
		strike2.setEnabled(model);
		showAllOptions.setEnabled(model);
		optionIntervalIV.setEnabled(model);
	}
}
