package ru.open.spc.web.pages;

import com.googlecode.wicket.jquery.core.Options;
import com.googlecode.wicket.kendo.ui.widget.splitter.SplitterAdapter;
import com.googlecode.wicket.kendo.ui.widget.splitter.SplitterBehavior;
import ru.open.spc.web.AbstractWebPage;
import ru.open.spc.web.pages.asset.AssetDetailsPanel;
import ru.open.spc.web.pages.asset.AssetMainTablePanel;

import static ru.open.spc.web.util.WicketUtil.createAjaxLazyLoadPanel;

@SuppressWarnings("serial")
public class AssetsPage extends AbstractWebPage {

	public AssetsPage() {

		Options options = new Options();
		options.set("panes", "[ { size: '50%' }, { }, { size: '50%' } ]");
		options.set("orientation", "'vertical'");
		SplitterBehavior splitterBehavior = new SplitterBehavior("#splitter", new SplitterAdapter());
		splitterBehavior.setOptions(options);
		add(splitterBehavior);
		add(createAjaxLazyLoadPanel("assetMainTablePanel", AssetMainTablePanel.class));
		add(createAjaxLazyLoadPanel("assetDetailsPanel", AssetDetailsPanel.class));
	}

}
