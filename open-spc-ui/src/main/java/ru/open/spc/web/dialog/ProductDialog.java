package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.vaynberg.wicket.select2.DragAndDropBehavior;
import com.vaynberg.wicket.select2.Select2MultiChoice;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.util.CollectionModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Product;
import ru.open.spc.web.componenets.model.NamedChoiceProvider;

import java.util.List;

import static ru.open.spc.web.util.DefaultDialogButtons.getDialogButtons;
import static ru.open.spc.web.util.DefaultDialogButtons.getSaveButton;

@SuppressWarnings("serial")
public class ProductDialog extends ResizableDialog<Product> {

	@SpringBean private ProductManager productManager;
	@SpringBean private AssetManager assetManager;

	private final Form<Product> productForm;
    private final Select2MultiChoice<Asset> multiSelector;

	public ProductDialog(String id) {
		super(id, new ResourceModel("product.title").getObject(), true);
		productForm = new Form<>("productForm");
		productForm.setOutputMarkupId(true);
        productForm.add(new JQueryFeedbackPanel("feedbackPanel"));
        productForm.add(new RequiredTextField<String>("name"));
        productForm.add(new RequiredTextField<String>("description"));
        NamedChoiceProvider provider = new NamedChoiceProvider();
        provider.setList(assetManager.all());
        multiSelector = new Select2MultiChoice<>("assets", new CollectionModel<Asset>(), provider);
        multiSelector.setOutputMarkupId(true);
        multiSelector.add(new DragAndDropBehavior());
        productForm.add(multiSelector);
        add(productForm);

	}

	@Override
	protected DialogButton getSubmitButton() {
		return getSaveButton();
	}

	@Override
	protected List<DialogButton> getButtons() {
		return getDialogButtons();
	}

	public void setDefaultModelObject(Product product) {
		productForm.setDefaultModel(new CompoundPropertyModel<>(product));
        multiSelector.setModelObject(product.getAssets());
	}

	@Override
	public Form<?> getForm() {
		return productForm;
	}

	@Override
	protected void onOpen(AjaxRequestTarget target) {
		target.add(productForm);
	}

	@Override
	protected void onError(AjaxRequestTarget target) {
		target.add(get("productForm").get("feedbackPanel"));
	}

	@Override
	protected void onSubmit(AjaxRequestTarget target) {
		Product product = productForm.getModel().getObject();
		if (product != null) {
			productManager.save(product);
		}
		target.add(getParent());
	}

}
