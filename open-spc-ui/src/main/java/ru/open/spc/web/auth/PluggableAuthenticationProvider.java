package ru.open.spc.web.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.open.spc.dao.manager.Authenticator;
import ru.open.spc.model.auth.UserAuthDetails;
import ru.open.spc.web.ui.model.Role;

/**
 * Implementation of AuthenticationProvider which tries to authenticate user via ldap then looking for details in database
 */
@Slf4j
public class PluggableAuthenticationProvider implements AuthenticationProvider {

	private final Authenticator authenticator;
	private final UserDetailsService userDetailsService;

	private final String adminUsername;
	private final String adminPassword;

	/**
	 * @param authenticator
	 * @param userDetailsService
	 * @param adminUsername
	 * @param adminPassword
	 */
	public PluggableAuthenticationProvider(Authenticator authenticator, UserDetailsService userDetailsService, String adminUsername, String adminPassword) {
		this.authenticator = authenticator;
		this.userDetailsService = userDetailsService;
		this.adminPassword = adminPassword;
		this.adminUsername = adminUsername;
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		try {
			log.debug("try to authenticate {}", authentication);
			UserDetails userDetails = retrieveUser(authentication);
			if (valid(userDetails)) {
				log.debug("user was authenticated {}", userDetails);
				return createSuccessAuthentication(userDetails, authentication.getDetails());
			}
		} catch (Exception e) {
			log.error("can't auth user " + authentication.toString(), e);
		}

		return null;
	}

	private UserDetails retrieveUser(Authentication authentication) throws AuthenticationException {

		UserDetails userDetails = checkForSuperUser(authentication);
		if (userDetails != null) {
			return userDetails;
		}

		authenticator.authenticate(authentication.getName(), authentication.getCredentials().toString());
		String userName = authentication.getName();
		userDetails = userDetailsService.loadUserByUsername(userName);
		if (userDetails == null) {
			throw new UsernameNotFoundException("Can not authenticate: " + userName);
		}
		return userDetails;
	}

	private UserDetails checkForSuperUser(Authentication authentication) {
		final String name = authentication.getName();
		final String pass = authentication.getCredentials().toString();
		if (adminUsername.equals(name) && adminPassword.equals(pass)) {
			final UserAuthDetails authDetails = new UserAuthDetails();
			authDetails.setUsername(adminUsername);
			authDetails.setRole(Role.ROLE_ADMIN.role());
			log.debug("Hey admin");
			return authDetails;
		}
		return null;
	}


	private boolean valid(UserDetails userDetails) {
		if (userDetails == null) {
			return false;
		}
		return userDetails.isAccountNonExpired() && userDetails.isAccountNonLocked() && userDetails.isEnabled();
	}


	protected Authentication createSuccessAuthentication(UserDetails userDetails, Object details) {
		final UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(
				userDetails, userDetails.getPassword(), userDetails.getAuthorities());
		result.setDetails(details);
		return result;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}
}