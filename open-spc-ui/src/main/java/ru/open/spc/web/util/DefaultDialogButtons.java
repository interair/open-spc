package ru.open.spc.web.util;

import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import org.apache.wicket.model.ResourceModel;

import java.util.Arrays;
import java.util.List;

public class DefaultDialogButtons extends ResourceBundleAware {

	private static final DialogButton saveButton = new DialogButton(get("button.save.label"), new ResourceModel("button.save.label"));
	private static final DialogButton cancelButton = new DialogButton(get("button.cancel.label"), new ResourceModel("button.cancel.label"));

	public static DialogButton getSaveButton() {
		return saveButton;
	}

	public static DialogButton getCancelButton() {
		return cancelButton;
	}

	public static List<DialogButton> getDialogButtons() {
		return Arrays.asList(saveButton, cancelButton);
	}
}
