package ru.open.spc.web.util;

import com.googlecode.wicket.jquery.core.Options;
import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import com.googlecode.wicket.kendo.ui.form.datetime.DatePicker;
import org.apache.wicket.Component;
import org.apache.wicket.extensions.ajax.markup.html.AjaxLazyLoadPanel;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.ResourceModel;
import org.slf4j.LoggerFactory;
import ru.open.spc.web.ui.model.KendoPropertyColumn;

import java.util.ArrayList;
import java.util.List;

import static ru.open.spc.web.util.ResourceBundleAware.get;

public class WicketUtil {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(WicketUtil.class);

	public static final Options createDefaultOptions = createDefaultOptions();

	private final static String[] suffixSteps = {"From", "To", "Step"};

	public static AjaxLazyLoadPanel createAjaxLazyLoadPanel(String id, final Class<? extends Panel> panelClass) {
		return new AjaxLazyLoadPanel(id) {

			@Override
			public Component getLazyLoadComponent(String markupId) {
				try {
					final Panel panel = panelClass.getConstructor(String.class).newInstance(markupId);
					panel.setOutputMarkupId(true);
					return panel;
				} catch (Exception ex) {
					LOG.error("Ooops... cannot create panel", ex);
                    throw new IllegalStateException(ex);// we can't restore here
				}
			}

			@Override
			public Component getLoadingComponent(String markupId) {
				return new Label(markupId, getIndicatorMessage(
						"padding: 10px; width:100%; vertical-align:top;",
						"load.message"))
						.setEscapeModelStrings(false);
			}
		};
	}

	public static <T> List<IColumn<T, String>> createSortableColumns(String... names) {
		List<IColumn<T, String>> columns = new ArrayList<>();
		for (String name : names) {
			columns.add(createSortableColumn(name));
		}
		return columns;
	}

	public static <T> List<IColumn<T, String>> createSortableColumnsWithPrefix(String prefix, String... names) {
		List<IColumn<T, String>> columns = new ArrayList<>();
		for (String name : names) {
			columns.add(createSortableColumnsWithPrefix(prefix, name));
		}
		return columns;
	}

	public static <T> IColumn<T, String> createSortableColumnsWithPrefix(String prefix, String name) {
		return new PropertyColumn<>(new ResourceModel(prefix + "." + name), name, name);
	}

	public static <T> IColumn<T, String> createSortableColumn(String name) {
		return new PropertyColumn<>(new ResourceModel(name), name, name);
	}

	public static Options createDefaultOptions() {
		return new Options("format", Options.asString(UIUtil.DATE_FORMAT));
	}

	public static Options createDefaultDateTimeOptions() {
		return new Options("format", Options.asString(UIUtil.DATE_TIME_FORMAT));
	}

	public static Component[] createDoubleComp(boolean isReq, String... names) {
		List<Component> c = new ArrayList<>();
		for (String name : names) {
			c.add(new TextField<Double>(name).setRequired(isReq));
		}
		return c.toArray(new Component[c.size()]);
	}

	public static List<com.googlecode.wicket.kendo.ui.datatable.column.IColumn> createColumns(String... names) {
		List<com.googlecode.wicket.kendo.ui.datatable.column.IColumn> columns = new ArrayList<>(names.length);
		for (String name : names) {
			columns.add(createColumn(name));
		}
		return columns;
	}

	public static com.googlecode.wicket.kendo.ui.datatable.column.IColumn createColumn(String name) {
		final String[] split = name.split(":");
		if (split.length > 1) {
			return new KendoPropertyColumn(new ResourceModel(split[0]), split[0], Integer.valueOf(split[1]));
		} else {
			return new KendoPropertyColumn(new ResourceModel(name), name);
		}
	}

	public static Component[] createSteppedDoubleComp(String name, boolean isReq) {
		List<Component> c = new ArrayList<>();
		for (String suffix : suffixSteps) {
			c.add(new Spinner<Double>(name + suffix).setRequired(isReq));
		}
		return c.toArray(new Component[c.size()]);
	}

	public static Component[] createSteppedDoubleComp(String name) {
		return createSteppedDoubleComp(name, true);
	}

	public static Component[] createSteppedLongComp(String name) {
		List<Component> c = new ArrayList<>();
		for (String suffix : suffixSteps) {
			c.add(new Spinner<Long>(name + suffix).setRequired(true));
		}
		return c.toArray(new Component[c.size()]);
	}

	public static DatePicker createDefaultDatePicker(String id) {
		return new DatePicker(id, UIUtil.DATE_FORMAT, createDefaultOptions);
	}

	public static DatePicker createDefaultDateTimePicker(String id) {
		return new DatePicker(id, UIUtil.DATE_FORMAT, createDefaultOptions);
	}

	public static String getIndicatorMessage(String messageKey) {
		return "<a class=\"lazyLoadMessage\">" + get(messageKey) + "</a>";
	}

	public static String getIndicatorMessage(String style, String messageKey) {
		return "<div align=\"center\" class=\"lazyMessageOuter\"" + " style=\"" + style + "\">" +
				"<div class=\"lazyMessageInner\" >" + "<div class=\"lazyMessageMiddle\" align=\"center\">" +
				getIndicatorMessage(messageKey) + "</div>" + "</div>" + "</div>";
	}

	public static Component findComponentBy(String markupId, Component component) {
		if (component == null) {
			return null;
		}
		Component foundComponent = component;
		do {
			if (markupId.equals(foundComponent.getId())) {
				break;
			}
			foundComponent = foundComponent.getParent();
		} while (foundComponent != null);
		return foundComponent;
	}
}
