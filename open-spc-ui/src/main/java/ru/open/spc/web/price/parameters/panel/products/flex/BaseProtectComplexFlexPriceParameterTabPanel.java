package ru.open.spc.web.price.parameters.panel.products.flex;

import ru.open.spc.calc.model.complex.flex.BasicProtectionComplexFlexParameters;

import static ru.open.spc.web.util.WicketUtil.createSteppedDoubleComp;

public class BaseProtectComplexFlexPriceParameterTabPanel extends ComplexFlexPriceParameterTabPanel {

	public BaseProtectComplexFlexPriceParameterTabPanel(String id) {
		super(id, new BasicProtectionComplexFlexParameters());
		addToForm(createSteppedDoubleComp("coeffAssetProtection"));
		addToForm(createSteppedDoubleComp("strike2"));
	}
}
