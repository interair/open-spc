package ru.open.spc.web.price.parameters.panel.products.exchange;

import com.googlecode.wicket.kendo.ui.form.dropdown.DropDownList;
import ru.open.spc.calc.model.complex.exchange.BasicProtectionComplexParameters;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.web.componenets.renders.AssetUnityRender;
import ru.open.spc.web.price.parameters.panel.ComplexPriceParameterTabPanel;

import java.util.Arrays;

import static ru.open.spc.web.util.WicketUtil.createSteppedDoubleComp;

public class BaseProtectComplexPriceParameterTabPanel extends ComplexPriceParameterTabPanel {

	public BaseProtectComplexPriceParameterTabPanel(String id) {
		super(id, new BasicProtectionComplexParameters());
		addToForm(createSteppedDoubleComp("coeffAssetProtection"));
		addToForm(createSteppedDoubleComp("strike2"));
        DropDownList<AssetUnity> vzkUnity = new DropDownList<>("vzkUnity", Arrays.asList(AssetUnity.RUB, AssetUnity.USD),
            new AssetUnityRender());
        addToForm(vzkUnity);
	}
}
