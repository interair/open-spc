package ru.open.spc.web.price.parameters;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.validation.AbstractFormValidator;
import org.apache.wicket.model.IModel;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.complex.AbstractComplexCalculationParameters;

@SuppressWarnings("serial")
public class PriceParametersValidator extends AbstractFormValidator {

	@Override
	public FormComponent<?>[] getDependentFormComponents() {
		return null;
	}

	@Override
	public void validate(Form<?> form) {
		IModel<?> model = form.getModel();
		CalculationParameters calculationParameters = (CalculationParameters) model.getObject();
		if (calculationParameters.getCalculationParams() instanceof AbstractComplexCalculationParameters) {
		} else {
		}
	}
}
