package ru.open.spc.web.auth;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.request.Request;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Locale;
import java.util.stream.Collectors;

public class UserAuthenticatedWebSession extends AuthenticatedWebSession {

	public UserAuthenticatedWebSession(Request request) {
		super(request);
		setLocale(new Locale("ru", "RU"));
	}

	@Override
	public boolean authenticate(String username, String password) {
		throw new UnsupportedOperationException("You are supposed to use Spring-Security!!");
	}

	@Override
	public Roles getRoles() {
		final Roles roles = new Roles();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		roles.addAll(authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()));
		return roles;
	}

}