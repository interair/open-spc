package ru.open.spc.web.price.parameters.panel.products.flex;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import ru.open.spc.calc.model.simple.flex.StocksDepositFLexSimpleParameters;

public class StocksDepositSimpleFlexParamTabPanel extends SimplePriceParameterFlexTabPanel<StocksDepositFLexSimpleParameters> {

	public StocksDepositSimpleFlexParamTabPanel(String id) {
		super(id, new StocksDepositFLexSimpleParameters());

		addToForm(new Spinner<Double>("repoDiscount"));
		addToForm(new Spinner<Double>("repoRate"));
        addToForm(new Spinner<Integer>("stockNum"));
	}
}
