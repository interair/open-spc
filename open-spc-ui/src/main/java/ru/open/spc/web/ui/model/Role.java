package ru.open.spc.web.ui.model;

public enum  Role {

	ROLE_ADMIN("ROLE_ADMIN"), ROLE_USER("ROLE_USER"), NONE(null);

	private final String role;

	private Role(String role) {
		this.role = role;
	}

	public String role() {
		return role;
	}


}
