package ru.open.spc.web.componenets.renders;

import ru.open.spc.model.enums.Direction;

public class DirectionRender extends StringChoiceRenderer<Direction> {

	@Override
	public String getKey(Direction object) {
		switch (object) {
			case RISING:
				return "direction.rising";
			case FALLING:
				return "direction.falling";
			default:
				return null;
		}
	}
}
