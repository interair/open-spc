package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import com.googlecode.wicket.jquery.ui.widget.dialog.AbstractDialog;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.progressbar.ProgressBar;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AbstractAjaxTimerBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.event.Broadcast;
import org.apache.wicket.event.IEventSink;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.time.Duration;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.model.result.PricingResult;
import ru.open.spc.web.util.UpdateEvent;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("serial")
public class ProgressBarDialog extends AbstractDialog<String> {

	private final ProgressBar progressBar;
	private UiCallback<PricingResult> callback = null;
	private AbstractAjaxTimerBehavior timer;
	private Component[] componentsToUpdate;
	private IEventSink sink;

	public ProgressBarDialog(String id, String title) {
		this(id, title, new Model<>(""));
	}

	public ProgressBarDialog(String id, String title, IModel<String> message) {
		super(id, title, message, true);

		final Form<Void> form = new Form<>("form");
		this.add(form);

		final FeedbackPanel feedback = new JQueryFeedbackPanel("feedback");
		form.add(feedback.setOutputMarkupId(true));

		timer = new AbstractAjaxTimerBehavior(Duration.milliseconds(500L/*0.5 sec*/)) {

			@Override
			protected void onTimer(AjaxRequestTarget target) {
				if (callback != null) {
					progressBar.setModelObject(callback.getIndicatorValue());
					progressBar.refresh(target);
					if (callback.isDone()) {
						timer.stop(target); //wicket6
						target.add(componentsToUpdate);
						info(format("calculating.result", callback.getResult().size()));
						target.add(feedback);
						close(target, null);
					}
				} else {
					timer.stop(target);
				}
			}
		};

		form.add(timer);

		// ProgressBar //
		this.progressBar = new ProgressBar("progress", new Model<>(100)) {

			@Override
			public void onValueChanged(AjaxRequestTarget target) {
				String phases = "";

				if (callback.getTotalPhases() > 0) {
					phases = format("asset.completed", callback.getCurrentPhase(), callback.getTotalPhases());
				}

				info(format("calculating.completed", this.getDefaultModelObjectAsString(), callback.getTotalSteps(), phases));
				target.add(feedback);
			}

			@Override
			protected void onComplete(AjaxRequestTarget target) {
			}
		};

		form.add(this.progressBar);

	}


	@Override
	public void onClose(AjaxRequestTarget target, DialogButton button) {

		List<PricingResult> res = callback.getResult();
		send(sink, Broadcast.DEPTH, new UpdateEvent(target, res, ""));
		target.add(componentsToUpdate);
		timer.stop(target);

	}

	@Override
	protected void onOpen(AjaxRequestTarget target) {
		progressBar.setModelObject(0);
		info(getString("calculating"));
		progressBar.refresh(target);
		timer.restart(target);
		super.onOpen(target);
	}

	public void setCallback(UiCallback<PricingResult> callback) {
		this.callback = callback;
	}

	public void setComponentsToUpdate(Component... componentsToUpdate) {
		this.componentsToUpdate = componentsToUpdate;
	}

	@Override
	protected List<DialogButton> getButtons() {
		return Collections.<DialogButton>emptyList();
	}

	public void setIEventSink(IEventSink sink) {
		this.sink = sink;
	}

	private String format(String key, Object... params) {
		return MessageFormat.format(getString(key), params);
	}

}
