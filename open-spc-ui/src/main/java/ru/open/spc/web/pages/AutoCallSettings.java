package ru.open.spc.web.pages;

import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.HeaderlessColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.AutoCallAssetsManager;
import ru.open.spc.dao.manager.AutoCallAssetsRelationManager;
import ru.open.spc.model.AutoCallAsset;
import ru.open.spc.model.AutoCallAssetsRelation;
import ru.open.spc.web.AbstractWebPage;
import ru.open.spc.web.componenets.DeleteLink;
import ru.open.spc.web.componenets.EditLink;
import ru.open.spc.web.componenets.model.SortableDataProviderImpl;
import ru.open.spc.web.dialog.AutoCallSettingsDialog;
import ru.open.spc.web.dialog.DeleteDialog;

import java.util.*;

public class AutoCallSettings extends AbstractWebPage {

	@SpringBean private AutoCallAssetsRelationManager relationManager;
	@SpringBean private AutoCallAssetsManager autoCallAssetsManager;
	private AutoCallSettingsDialog autoCallSettingsDialog;
	private DataTable<HashMap<String, Object>, Class> resultListView;
	private SortableDataProviderImpl<HashMap<String, Object>> dataProvider = new SortableDataProviderImpl<>();
	final DeleteDialog<AutoCallAsset> deleteMessageDialog;

	private final static String ID = "id";

	public AutoCallSettings() {
		resultListView = new AjaxFallbackDefaultDataTable("autoCallSettingsList", getColumns(), dataProvider, 100);
		deleteMessageDialog = new DeleteDialog("deleteConfirmation", getString("delete.dialog.title"),
				getString("asset.delete.message"), DialogButtons.YES_NO);
		deleteMessageDialog.setGenericManager(autoCallAssetsManager);
		add(resultListView);
		add(deleteMessageDialog);
		autoCallSettingsDialog = new AutoCallSettingsDialog("autoCallDialog");
		add(new AjaxLink<String>("newAutoCallAssetLink") {
			@Override
			public void onClick(AjaxRequestTarget target) {
				autoCallSettingsDialog.setDefaultModelObject(new AutoCallAsset());
				autoCallSettingsDialog.open(target);
			}
		});
		setOutputMarkupId(true);
		add(autoCallSettingsDialog);

	}

	@Override
	protected void onBeforeRender() {
		if (resultListView != null) remove(resultListView);
		resultListView = new AjaxFallbackDefaultDataTable("autoCallSettingsList", getColumns(), dataProvider, 100);
		add(resultListView);
		super.onBeforeRender();
	}

	private List<? extends IColumn> getColumns() {

		List<AutoCallAsset> all = autoCallAssetsManager.all();

		List<HashMap<String, Object>> data = new ArrayList<>();

		String assetTitle = getString("asset.title");
		String sigmas = getString("sigmas");
		String initRelativePrice = getString("initRelativePrice");
		String relativePrice = getString("relativePrice");
		String baseTicker = getString("baseTicker");
		List<String> columnsKeys = new ArrayList<>(Arrays.asList(assetTitle,
				sigmas, baseTicker, relativePrice, initRelativePrice));
		all.forEach(a -> {
			String ticker = a.getAsset().getBaseTicker();
			columnsKeys.add(ticker);
			HashMap<String, Object> row = new HashMap<>();
			row.put(assetTitle, a.getName());
			row.put(sigmas, a.getSigma());
			row.put(baseTicker, ticker);
			row.put(initRelativePrice, a.getInitRelativePrice());
			row.put(relativePrice, a.getRelativePrice());
			row.put(ID, a.getId());

			all.forEach(b -> {
				AutoCallAssetsRelation relation = relationManager.getRelation(a.getId(), b.getId());
				if (relation != null) {
					row.put(b.getAsset().getBaseTicker(), relation.getCoefficient());
				}
			});
			data.add(row);
			dataProvider.setList(data);

		});

		List<IColumn<Map<String, Object>, String>> columns = new ArrayList<>();

		columnsKeys.forEach(a -> {
			final IColumn<Map<String, Object>, String> c = new AbstractColumn<Map<String, Object>, String>(new Model(a)) {
				@Override
				public void populateItem(Item<ICellPopulator<Map<String, Object>>> cellItem, String componentId, IModel<Map<String, Object>> rowModel) {
					cellItem.add(new Label(componentId, new ru.open.spc.web.componenets.model.Model<Object>() {
						@Override
						public Object getObject() {
							return rowModel.getObject().get(a);
						}
					}));
				}
			};
			columns.add(c);
		});

		columns.add(0, new HeaderlessColumn<Map<String, Object>, String>() {
			public void populateItem(Item<ICellPopulator<Map<String, Object>>> cellItem, String componentId,
			                         final IModel<Map<String, Object>> model) {
				cellItem.add(new EditLink(componentId).add(new AjaxLink<String>("editLink") {

					@Override
					public void onClick(AjaxRequestTarget target) {
						AutoCallAsset byId = autoCallAssetsManager.getById((Long) model.getObject().get(ID));
						autoCallSettingsDialog.setDefaultModelObject(byId);
						autoCallSettingsDialog.open(target);
					}
				}));
			}
		});

		columns.add(1, new HeaderlessColumn<Map<String, Object>, String>() {
			public void populateItem(Item<ICellPopulator<Map<String, Object>>> cellItem, String componentId,
			                         final IModel<Map<String, Object>> model) {
				cellItem.add(new DeleteLink(componentId).add(new AjaxLink<String>("deleteLink") {

					@Override
					public void onClick(AjaxRequestTarget target) {
						AutoCallAsset byId = autoCallAssetsManager.getById((Long) model.getObject().get(ID));
						deleteMessageDialog.setObject(byId);
						deleteMessageDialog.open(target);
					}
				}));
			}
		});


		return columns;
	}


}
