package ru.open.spc.web.pages;

import com.googlecode.wicket.jquery.core.Options;
import com.googlecode.wicket.kendo.ui.widget.splitter.SplitterAdapter;
import com.googlecode.wicket.kendo.ui.widget.splitter.SplitterBehavior;
import org.apache.wicket.markup.html.panel.Panel;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.web.AbstractWebPage;

import static ru.open.spc.web.util.WicketUtil.createAjaxLazyLoadPanel;

@SuppressWarnings("serial")
public class VerticalSplitPage extends AbstractWebPage {

	public VerticalSplitPage(Pair<Pair<String, Class<? extends Panel>>,
                    Pair<String, Class<? extends Panel>>> panels, int leftPart) {
        final SplitterBehavior splitterBehavior = new SplitterBehavior("#splitter", new SplitterAdapter());
        final Options options = new Options();
        options.set("panes", "[{ size: \"" + leftPart + "%\"}, { collapsed: false }, {collapsible: true}]");
        splitterBehavior.setOptions(options);
		add(splitterBehavior);
        add(createAjaxLazyLoadPanel(panels.getLeft().getLeft(), panels.getLeft().getRight()));
		add(createAjaxLazyLoadPanel(panels.getRight().getLeft(), panels.getRight().getRight()));
	}

}
