package ru.open.spc.web.dialog.tabs;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.RangeValidator;
import ru.open.spc.dao.manager.SpreadVolatilityManager;
import ru.open.spc.model.Asset;
import ru.open.spc.model.SpreadVolatilityParams;
import ru.open.spc.web.dialog.AssetDialog;
import ru.open.spc.web.util.UpdateEvent;

import java.util.Collections;

import static org.apache.wicket.event.Broadcast.DEPTH;
import static ru.open.spc.calc.util.MathUtil.HUNDRED;
import static ru.open.spc.web.pages.asset.AssetMainTablePanel.TABBED_PANEL_PAGE;
import static ru.open.spc.web.util.WicketUtil.createDefaultDatePicker;
import static ru.open.spc.web.util.WicketUtil.findComponentBy;

public class AssetVolatilityTabPanel extends ModelObjectAwarePanel {

    private final Form<SpreadVolatilityParams> volatilityForm = new Form<>("volatilityForm");
    private final FeedbackPanel feedbackPanel = new JQueryFeedbackPanel("assetFeedbackPanel");
    public static final String DIALOG_VOLATILITY_EVENT = "dialogVolatilityEvent";
    private Asset asset;

    @SpringBean private SpreadVolatilityManager manager;

    public AssetVolatilityTabPanel(String id){
        super(id);
        Injector.get().inject(this);

        volatilityForm.add(feedbackPanel);

        volatilityForm.add(createDefaultDatePicker("minVolatilityDate").setRequired(true));
        volatilityForm.add(new Spinner<>("minVolatility").setRequired(true).setType(Double.class).add(new RangeValidator<>(-HUNDRED, HUNDRED)));
        volatilityForm.add(createDefaultDatePicker("maxVolatilityDate").setRequired(true));
        volatilityForm.add(new Spinner<>("maxVolatility").setRequired(true).setType(Double.class).add(new RangeValidator<>(-HUNDRED, HUNDRED)));

        add(volatilityForm);

        volatilityForm.add(new AjaxButton("saveButton") {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                //тут кидаем сообщение, выше его ловим и вставляем остальные табы
                SpreadVolatilityParams params = volatilityForm.getModel().getObject();
                if (params != null) {
                    params.setAssetId(asset.getId());  // asset have to be saved before!
                    manager.save(params);
                }
                send(findComponentBy(TABBED_PANEL_PAGE, getParent()).getParent(), DEPTH,
                        new UpdateEvent(target, Collections.singletonList(params), AssetDialog.DIALOG_EVENT));
            }

            @Override
            public void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }
        });
    }

    public void setModelObject(Asset asset) {
        volatilityForm.setDefaultModel(new CompoundPropertyModel<>(asset.getSpreadVolatility()));
        this.asset = asset;
    }

}
