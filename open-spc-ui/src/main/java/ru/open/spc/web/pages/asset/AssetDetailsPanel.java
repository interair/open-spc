package ru.open.spc.web.pages.asset;

import com.googlecode.wicket.jquery.core.Options;
import com.googlecode.wicket.jquery.ui.widget.tabs.AjaxTab;
import com.googlecode.wicket.jquery.ui.widget.tabs.TabbedPanel;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.ResourceModel;

import java.util.ArrayList;

public class AssetDetailsPanel extends Panel {

	public AssetDetailsPanel(String id) {
		super(id);
		ArrayList<ITab> tabs = new ArrayList<>();
		tabs.add(new AjaxTab(new ResourceModel("asset.details.table.label")) {
			@Override
			public WebMarkupContainer getLazyPanel(String panelId) {
				AssetDetailsTablePanel detailsTablePanel = new AssetDetailsTablePanel(panelId);
				detailsTablePanel.setOutputMarkupId(true);
				return detailsTablePanel;
			}
		});

		tabs.add(new AjaxTab(new ResourceModel("asset.details.graph.label")) {
			@Override
			public WebMarkupContainer getLazyPanel(String panelId) {
				AssetDetailsGraphPanel graphPanel = new AssetDetailsGraphPanel(panelId);
				graphPanel.setOutputMarkupId(true);
				return graphPanel;
			}
		});
		Options options = new Options();
		options.set("collapsible", true);
		options.set("selected", Options.asString(0));
		add(new TabbedPanel("details", tabs, options));
	}
}
