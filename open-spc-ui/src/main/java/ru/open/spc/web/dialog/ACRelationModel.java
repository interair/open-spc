package ru.open.spc.web.dialog;

import lombok.Builder;
import lombok.Data;
import ru.open.spc.model.AutoCallAsset;

import java.io.Serializable;

@Builder
@Data
public class ACRelationModel implements Serializable {
	private Long id;
	private Double coefficient;
	private AutoCallAsset assetLeft;
}
