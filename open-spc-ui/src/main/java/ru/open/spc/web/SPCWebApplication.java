package ru.open.spc.web;

import org.apache.wicket.ConverterLocator;
import org.apache.wicket.IConverterLocator;
import org.apache.wicket.Page;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AnnotationsRoleAuthorizationStrategy;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.resource.loader.BundleStringResourceLoader;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import ru.open.spc.web.auth.UserAuthenticatedWebSession;
import ru.open.spc.web.pages.*;
import ru.open.spc.web.util.DurationConverter;
import ru.open.spc.web.util.LocalDateConverter;
import ru.open.spc.web.util.LocalDateTimeConverter;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class SPCWebApplication extends AuthenticatedWebApplication {

	@Override
	public Class<? extends Page> getHomePage() {
		return ExchangePricePage.class;
	}

	@Override
	protected void init() {
		super.init();
		getComponentInstantiationListeners().add(new SpringComponentInjector(this));
		getSecuritySettings().setAuthorizationStrategy(new AnnotationsRoleAuthorizationStrategy(this));
		getResourceSettings().getStringResourceLoaders().add(new BundleStringResourceLoader("ru.open.spc.web.spc"));
		getMarkupSettings().setStripWicketTags(true);

		mountPage("/price/exchange", ExchangePricePage.class);
        mountPage("/price/flex.html", FlexPricePage.class);

		mountPage("/settings/assets", AssetsPage.class);
		mountPage("/settings/products", ProductPage.class);
		mountPage("/settings/currency", CurrencyPage.class);
		mountPage("/settings/auto-call", AutoCallSettings.class);

		mountPage("/result/saved-data", SavedDataPage.class);
		mountPage("/login", LoginPage.class);
		mountPage("/logout", LogoutPage.class);
		mountPage("/admin/users", AdminPage.class);
		mountPage("/result/crm-integration", InvestProductPage.class);
        mountPage("/result/crm-integration.html", InvestProductPage.class);
		mountPage("/crm/data", CrmIntegrationInvestPage.class);
	}

	@Override
	protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
		return UserAuthenticatedWebSession.class;
	}

	@Override
	protected Class<? extends WebPage> getSignInPageClass() {
		return LoginPage.class;
	}

	@Override
	protected IConverterLocator newConverterLocator() {
		ConverterLocator converterLocator = new ConverterLocator();
		converterLocator.set(LocalDate.class, new LocalDateConverter());
		converterLocator.set(LocalDateTime.class, new LocalDateTimeConverter());
		converterLocator.set(Duration.class, new DurationConverter());
		return converterLocator;
	}
}
