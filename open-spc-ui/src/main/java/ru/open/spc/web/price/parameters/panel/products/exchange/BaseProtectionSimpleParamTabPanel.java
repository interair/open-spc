package ru.open.spc.web.price.parameters.panel.products.exchange;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import com.googlecode.wicket.kendo.ui.form.dropdown.DropDownList;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.TextField;
import ru.open.spc.calc.model.simple.exchange.BasicProtectionSimpleParameters;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.web.componenets.renders.AssetUnityRender;
import ru.open.spc.web.componenets.renders.DirectionRender;
import ru.open.spc.web.price.parameters.panel.SimplePriceParameterTabPanel;

import java.util.Arrays;

public class BaseProtectionSimpleParamTabPanel extends SimplePriceParameterTabPanel<BasicProtectionSimpleParameters> {

	private final TextField<Double> strike2 = new TextField<>("strike2");
	private final TextField<Double> intervalOptionPrice = new TextField<>("intervalOptionPrice");
	private final CheckBox interval;
	private final CheckBox showAllOptions = new CheckBox("showAllOptions");

	public BaseProtectionSimpleParamTabPanel(String id) {
		super(id, new BasicProtectionSimpleParameters());
		addToForm(new Spinner<Double>("coeffAssetProtection").setRequired(true));
		addToForm(new DropDownList<>("direction", Arrays.asList(Direction.values()), new DirectionRender()).setRequired(true));
		strike2.setOutputMarkupPlaceholderTag(true);
        DropDownList<AssetUnity> vzkUnity = new DropDownList<>("vzkUnity", Arrays.asList(AssetUnity.RUB, AssetUnity.USD),
            new AssetUnityRender());
        addToForm(vzkUnity);
		intervalOptionPrice.setOutputMarkupPlaceholderTag(true);
		interval = new AjaxCheckBox("interval") {
			@Override
			protected void onUpdate(AjaxRequestTarget ajaxRequestTarget) {
				Boolean model = this.getModel().getObject();
				strike2.setEnabled(model);
				intervalOptionPrice.setEnabled(model);
				showAllOptions.setEnabled(model);
				ajaxRequestTarget.add(strike2, intervalOptionPrice, showAllOptions);
			}
		};
		addToForm(interval);
		addToForm(strike2);
		addToForm(intervalOptionPrice);
		addToForm(showAllOptions);
	}

	@Override
	protected void onBeforeRender() {
		super.onBeforeRender();
		Boolean model = interval.getModel().getObject();
		showAllOptions.setEnabled(model);
		strike2.setEnabled(model);
		intervalOptionPrice.setEnabled(model);
	}
}
