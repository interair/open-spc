package ru.open.spc.web.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import ru.open.spc.calc.util.DescriptionWriter;

import java.text.MessageFormat;

@Service("descriptionWriter") //have to be created via factory
@Scope("prototype")
public class DescriptionWriterImpl extends ResourceBundleAware implements DescriptionWriter {

	private final static Logger LOG = LoggerFactory.getLogger(DescriptionWriterImpl.class);

	private final StringBuilder description;
	private final static String NEW_LINE = System.getProperty("line.separator");

	public DescriptionWriterImpl() {
		description = new StringBuilder();
	}

	public DescriptionWriterImpl(String initString) {
		description = new StringBuilder(initString);
	}

	public DescriptionWriterImpl write(String string, Object... params) {
		writeWithDelimiter(string, NEW_LINE, params);
		return this;
	}

	public DescriptionWriterImpl writeRaw(String string) {
		description.append(string);
		LOG.debug(string);
		return this;
	}

    public DescriptionWriterImpl writeEmptyLine() {
        description.append(NEW_LINE);
        return this;
    }

	public DescriptionWriterImpl writeWithDelimiter(String string, String delimiter, Object... params) {
		String format = MessageFormat.format(get(string), params);
		description.append(format).append(delimiter);
		LOG.debug(format);
		return this;
	}

	public String getResult() {
		return description.toString();
	}

	public DescriptionWriterImpl clone() {
		return new DescriptionWriterImpl(this.getResult());
	}

    public String getString(String key) {
        return get(key);
    }
}