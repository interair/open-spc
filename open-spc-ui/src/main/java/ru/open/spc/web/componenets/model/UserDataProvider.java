package ru.open.spc.web.componenets.model;

import org.springframework.cglib.core.CollectionUtils;
import ru.open.spc.model.auth.UserAuthDetails;

import java.util.List;

public class UserDataProvider extends KendoDataProvider<UserAuthDetails> {

	private boolean showAll;

	protected List<UserAuthDetails> getData() {
		final List<UserAuthDetails> data = super.getData();
		if (showAll || data.size() < getList().size()) {
			return super.getData();
		} else {
			return (List<UserAuthDetails>) CollectionUtils.filter(super.getData(), o -> ((UserAuthDetails)o).getRole() != null);
		}
	}

	public boolean isShowAll() {
		return showAll;
	}

	public void setShowAll(boolean showAll) {
		this.showAll = showAll;
	}
}
