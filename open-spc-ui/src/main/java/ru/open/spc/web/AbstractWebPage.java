package ru.open.spc.web;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.http.WebResponse;
import ru.open.spc.web.menu.MenuBorder;

@SuppressWarnings("serial")
public abstract class AbstractWebPage extends WebPage {

	public AbstractWebPage() {
		super();
		add(new MenuBorder("navigationBorder"));
	}

	@Override
	protected void setHeaders(WebResponse response) {
		super.setHeaders(response);
		response.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
		response.setHeader("Expires", "Thu, 01 Jan 1970 00:00:00 GMT");
		response.setHeader("Pragma", "no-cache");
	}
}
