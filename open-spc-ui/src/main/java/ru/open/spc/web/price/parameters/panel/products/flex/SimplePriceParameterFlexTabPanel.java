package ru.open.spc.web.price.parameters.panel.products.flex;

import com.googlecode.wicket.jquery.ui.form.RadioChoice;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormSubmitBehavior;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.validation.validator.RangeValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.open.spc.calc.model.complex.flex.PriceType;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.model.result.PricingResult;
import ru.open.spc.web.componenets.renders.PriceTypeRender;
import ru.open.spc.web.price.parameters.panel.SimplePriceParameterTabPanel;
import ru.open.spc.web.util.UiCallbackImpl;

import java.util.List;

import static java.util.Arrays.asList;

public abstract class SimplePriceParameterFlexTabPanel<T extends SimpleCalculationParameters> extends SimplePriceParameterTabPanel<T> {

    private static final Logger LOG = LoggerFactory.getLogger(SimplePriceParameterFlexTabPanel.class);
    public static final String OPTION_PRICE = "optionPrice";
    private static final String[] JS_EVENTS = {"spinstop", "onkeyup", "onchange"};

    protected SimplePriceParameterFlexTabPanel(String id, T model) {
		super(id, model);
        addToForm(new TextField<Double>("optionIV").setType(Double.class).add(RangeValidator.minimum(0d)));
		RadioChoice<PriceType> priceTypeChoice = new RadioChoice<>("priceType", asList(PriceType.values()),new PriceTypeRender());
		priceTypeChoice.setRequired(true);
		addToForm(priceTypeChoice);
		disableOnForm(OPTION_PRICE);
	}

    @Override
    protected void onInitialize() {
        getInputFields().forEach(s ->
                asList(JS_EVENTS).forEach(event -> s.add(new AjaxFormSubmitBehavior(
                        getCalculationParamForm(), event) {
                        @Override
                        protected void onSubmit(AjaxRequestTarget target) {
                            final FormComponent optionPrice = (FormComponent) getCalculationParamForm().get(OPTION_PRICE);
                            Double price = null;
                            try {
                                final UiCallback<PricingResult> callback = new UiCallbackImpl<>();
                                getCalculation().calculateSync(getCurrentParams(), callback, getCalculationBase(), getCalculationType());
                                final List<PricingResult> result = callback.getResult();
                                if (!result.isEmpty()) {
                                    final PricingResult pricingResult = result.get(0);
                                    price = pricingResult.getOptionPrice();
                                }
                                getFeedBack().info(getString("option.price.label") + ": " + price);
                            } catch (Exception e) {
                                LOG.error("Ooopss...", e);
                                getFeedBack().error("price.calc.error");
                            }
                            optionPrice.setModelObject(price);
                            target.add(optionPrice, getFeedBack());
                        }

                        @Override
                        protected void onError(AjaxRequestTarget target) { target.add(getFeedBack());}
                    }))
            );
        super.onInitialize();
    }

    //TODO: clean this shit
    @Override
    protected void onBeforeRender() {
        super.onBeforeRender();
	    getCalculationParamForm().get("priceType").setDefaultModelObject(PriceType.PERCENT);
    }
}
