package ru.open.spc.web.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.open.spc.dao.manager.Authenticator;
import ru.open.spc.dao.manager.LdapRepository;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Value("${admin.username:admin}")
	private String adminUsername;

	@Value("${admin.password:passw}")
	private String adminPassword;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/favicon.ico").permitAll()
				.antMatchers("/**/*.png").permitAll()
				.antMatchers("/**/*.css").permitAll()
				.antMatchers("/**/*.js").permitAll()
				.antMatchers("/services**").permitAll()
				.antMatchers("/result/crm-*").permitAll()
				.antMatchers("/crm/data").permitAll()
				.antMatchers("/price/**").hasAnyRole("USER", "ADMIN")
				.antMatchers("/settings/**").hasAnyRole("USER", "ADMIN")
				.antMatchers("/result/saved-data").hasAnyRole("USER", "ADMIN")
				.antMatchers("/admin/**").hasAnyRole("ADMIN")
				.and().csrf().disable()
				.formLogin().loginPage("/login").permitAll()
				.and().logout().logoutUrl("/logout");
	}

	@Bean
	GlobalAuthenticationConfigurerAdapter coreAuthenticationConfigurerAdapter() {
		return new GlobalAuthenticationConfigurerAdapter() {
			@Override
			public void configure(AuthenticationManagerBuilder auth) throws Exception {
				auth.authenticationProvider(authenticationProvider());
			}
		};
	}

	@Autowired
	UserDetailsService userDetailsService;

	@Bean
	public Authenticator ldapRepository() {
		return new LdapRepository();
	}

	@Bean
	public AuthenticationProvider authenticationProvider() {
		return new PluggableAuthenticationProvider(ldapRepository()
				, userDetailsService, adminUsername, adminPassword);
	}

}