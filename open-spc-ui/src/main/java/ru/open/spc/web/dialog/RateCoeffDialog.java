package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.event.Broadcast;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.validation.validator.RangeValidator;
import ru.open.spc.model.ApproximatingRate;
import ru.open.spc.model.Dividend;
import ru.open.spc.web.util.UpdateEvent;
import ru.open.spc.web.util.WicketUtil;

import java.util.List;

import static ru.open.spc.web.util.DefaultDialogButtons.getDialogButtons;
import static ru.open.spc.web.util.DefaultDialogButtons.getSaveButton;

public class RateCoeffDialog extends ResizableDialog<ApproximatingRate> {

	private final Form<Dividend> rateCoeffForm;
	public static final String RATE_COEF_ADD_EVENT = "RATE_COEF_ADD_EVENT";

	public RateCoeffDialog(String id) {
		super(id, new ResourceModel("rate.coeff.title").getObject(), true);
		rateCoeffForm = new Form<>("rateCoeffForm");
		rateCoeffForm.setOutputMarkupId(true);
		rateCoeffForm.add(WicketUtil.createDefaultDatePicker("fromDate").setRequired(true));
		rateCoeffForm.add(new Spinner<>("rate").setRequired(true).setType(Double.class).add(new RangeValidator<>(-1000d, 1000d)));
		add(rateCoeffForm);
	}

	@Override
	protected DialogButton getSubmitButton() {
		return getSaveButton();
	}

	@Override
	public Form<?> getForm() {
		return rateCoeffForm;
	}

	@Override
	protected void onError(AjaxRequestTarget target) {
	}

	@Override
	protected void onSubmit(AjaxRequestTarget target) {
		send(getParent(), Broadcast.BREADTH,
			new UpdateEvent(target, rateCoeffForm.getModelObject(), RATE_COEF_ADD_EVENT));
	}

	@Override
	protected List<DialogButton> getButtons() {
		return getDialogButtons();
	}

	public void setDefaultModelObject(ApproximatingRate rate) {
		rateCoeffForm.setDefaultModel(new CompoundPropertyModel<>(rate));
		rateCoeffForm.clearInput();
	}

	@Override
	protected void onOpen(AjaxRequestTarget target) {
		target.add(rateCoeffForm);
	}
}
