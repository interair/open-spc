package ru.open.spc.web.componenets.model;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.core.util.lang.PropertyResolver;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

//TODO merge with KendoDataProvider
public class SortableDataProviderImpl<N extends Serializable> extends SortableDataProvider<N, String> {

	private static final Logger LOG = LoggerFactory.getLogger(SortableDataProviderImpl.class);

	private final List<N> list = new ArrayList<>();

	@Override
	public Iterator<N> iterator(long first, long count) {
		Iterator<N> iterator = null;
		try {
			if (getSort() != null) {
				Collections.sort(list, (c1, c2) -> {
                    final Object o1 = PropertyResolver.getValue(getSort().getProperty(), c1);
                    final Object o2 = PropertyResolver.getValue(getSort().getProperty(), c2);
                    if (o1 != null && o2 != null) {
                        return toComparable(o1).compareTo(toComparable(o2)) * (getSort().isAscending() ? 1 : -1);
                    } else if (o1 == null && o2 == null) {
                        return 0;
                    }  else if (o1 == null) {
                        return 1;
                    } else {
                        return -1;
                    }
                });
			}
			if (first > 0) {
				count--;
			}
			if (list.size() > (first + count)) {
				iterator = list.subList(new Long(first).intValue(), new Long(first + count - 1).intValue()).iterator();
			} else {
				iterator = list.iterator();
			}
		} catch (Exception e) {
			LOG.error("Ooops...", e);
		}

		return iterator;
	}

    private static Comparable<Object> toComparable(Object o) {
        if (o instanceof Comparable<?>) {
            return (Comparable<Object>) o;
        }
        throw new WicketRuntimeException("Object should be a Comparable");
    }

	@Override
	public long size() {
		return list.size();
	}


	public List<N> getList() {
		return new ArrayList<>(list);
	}

	public boolean remove(N n) {
		return list.remove(n);
	}

	public void setList(List<N> dataList) {
		this.list.clear();
		this.list.addAll(dataList);
	}

	public void clear() {
		this.list.clear();
	}

	@Override
	public IModel<N> model(N object) {
		return new Model<>(object);
	}
}
