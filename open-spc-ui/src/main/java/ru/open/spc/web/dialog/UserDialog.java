package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.core.renderer.TextRenderer;
import com.googlecode.wicket.jquery.ui.form.autocomplete.AutoCompleteTextField;
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.LdapRepository;
import ru.open.spc.dao.manager.UserDetailsManager;
import ru.open.spc.model.Currency;
import ru.open.spc.model.auth.UserAuthDetails;
import ru.open.spc.web.componenets.model.NamedChoiceProvider;

import java.util.List;

import static ru.open.spc.web.util.DefaultDialogButtons.getDialogButtons;
import static ru.open.spc.web.util.DefaultDialogButtons.getSaveButton;

public class UserDialog  extends ResizableDialog<Currency> {

	@SpringBean
	private LdapRepository ldapRepository;
	@SpringBean
	private UserDetailsManager userDetailsManager;

	private final Form<UserAuthDetails> userForm;

	public UserDialog(String id) {
		super(id, new ResourceModel("user.title").getObject(), true);

		userForm = new Form<>("userForm");
		userForm.setOutputMarkupId(true);
		userForm.add(new JQueryFeedbackPanel("feedbackPanel"));
		final RequiredTextField<String> username = new RequiredTextField<>("username");
		final RequiredTextField<String> fullName = new RequiredTextField<>("fullName");
		final RequiredTextField<String> title = new RequiredTextField<>("title");
		final RequiredTextField<String> department = new RequiredTextField<>("department");
		final RequiredTextField<String> company = new RequiredTextField<>("company");
		userForm.add(username);
		userForm.add(fullName);
		userForm.add(title);
		userForm.add(department);
		userForm.add(company);


		final NamedChoiceProvider<UserAuthDetails> tickerProvider = new NamedChoiceProvider<UserAuthDetails>() {
			@Override
			protected String getName(UserAuthDetails named) {
				return getString(named);
			}
		};
		tickerProvider.setList(ldapRepository.getAllPersonNames());
		final AutoCompleteTextField<UserAuthDetails> tickerSelector = new AutoCompleteTextField<UserAuthDetails>("stockAsset", new TextRenderer<UserAuthDetails>() {
			@Override
			public String getText(UserAuthDetails named) {
				if (named == null || named.getUsername() == null) return null;
				return getString(named);
			}

		}, UserAuthDetails.class) {
			@Override
			protected List<UserAuthDetails> getChoices(String input) {
				return tickerProvider.matchingName(input, 0);
			}

			@Override
			protected void onSelected(AjaxRequestTarget target) {
				UserAuthDetails authDetails = this.getModelObject();
				username.setDefaultModelObject(authDetails.getUsername());
				fullName.setDefaultModelObject(authDetails.getFullName());
				title.setDefaultModelObject(authDetails.getTitle());
				department.setDefaultModelObject(authDetails.getDepartment());
				company.setDefaultModelObject(authDetails.getCompany());
				target.add(username, fullName, title, department, company);
			}

		};


		add(userForm);
	}

	private static String getString(UserAuthDetails named) {
		return  named.getFullName() + " (" + named.getUsername() + "): " + named.getDepartment();
	}

	@Override
	protected DialogButton getSubmitButton() {
		return getSaveButton();
	}

	@Override
	protected List<DialogButton> getButtons() {
		return getDialogButtons();
	}

	public void setDefaultModelObject(UserAuthDetails authDetails) {
		userForm.setDefaultModel(new CompoundPropertyModel<>(authDetails));
	}

	@Override
	public Form<?> getForm() {
		return userForm;
	}

	@Override
	protected void onOpen(AjaxRequestTarget target) {
		target.add(userForm);
	}

	@Override
	protected void onError(AjaxRequestTarget target) {
		target.add(get("userForm").get("feedbackPanel"));
	}

	@Override
	protected void onSubmit(AjaxRequestTarget target) {
		UserAuthDetails authDetails = this.userForm.getModel().getObject();
		if (authDetails != null) {
			userDetailsManager.save(authDetails);
		}
		target.add(getParent());
	}

}