package ru.open.spc.web.componenets.renders;

import ru.open.spc.calc.model.complex.flex.PriceType;

/**
 * Created by kuleshov-n on 30.07.2014.
 */
public class PriceTypeRender extends StringChoiceRenderer<PriceType> {

	@Override
	public String getKey(PriceType object) {
		switch (object) {
			case PERCENT:
				return "priceType.percent.label";
			case VALUE:
				return "priceType.abs.label";
			default:
				throw new IllegalArgumentException();
		}
	}
}
