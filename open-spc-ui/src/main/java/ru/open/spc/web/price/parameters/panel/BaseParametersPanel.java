package ru.open.spc.web.price.parameters.panel;

import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogIcon;
import com.googlecode.wicket.jquery.ui.widget.dialog.MessageDialog;
import com.googlecode.wicket.jquery.ui.widget.tooltip.TooltipBehavior;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.open.spc.calc.kernel.gateway.Calculation;
import ru.open.spc.calc.model.BaseParameters;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.calc.model.ParametersRepositoryHolder;
import ru.open.spc.calc.util.UiCallback;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.model.CalculationType;
import ru.open.spc.model.enums.ProductCode;
import ru.open.spc.model.result.PricingResult;
import ru.open.spc.web.dialog.ProgressBarDialog;
import ru.open.spc.web.pages.DefaultPricePage;
import ru.open.spc.web.price.parameters.PriceResultPanel;
import ru.open.spc.web.util.UiCallbackImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ru.open.spc.web.price.parameters.PriceParametersPanel.CALC_FORM_ID;
import static ru.open.spc.web.price.parameters.PriceParametersPanel.MAIN_FORM_ID;
import static ru.open.spc.web.util.WicketUtil.createDefaultDatePicker;
import static ru.open.spc.web.util.WicketUtil.findComponentBy;

public class BaseParametersPanel<T extends BaseParameters> extends Panel {

	private static final Logger LOG = LoggerFactory.getLogger(BaseParametersPanel.class);

	@SpringBean private AssetManager assetManager;
	@SpringBean private Calculation calculation;

	@SpringBean private ParametersRepositoryHolder parametersRepositoryHolder;
	private final TextField<Double> investingSum = new TextField<>("investingSum");
	private final Form<T> calculationParamForm;
	private CalculationType calculationType;
	private CalculationBase calculationBase;
	private final List<Component> inputFields = new ArrayList<>();
	private ProductCode productCode;

	public BaseParametersPanel(String id, final T model) {
		super(id, new CompoundPropertyModel<>(model));
		setOutputMarkupId(true);
		this.add(new TooltipBehavior());
		calculationParamForm = new Form<>(CALC_FORM_ID, new CompoundPropertyModel<>(model));
		add(calculationParamForm);
		addToForm(createDefaultDatePicker("depositStartDate").setRequired(true));
		addToForm(investingSum.setRequired(true));

		final MessageDialog informDialog = new MessageDialog("infoDialog", getString("calculating.done"),
				getString("calculating.result"), DialogButtons.OK, DialogIcon.INFO) {

			@Override
			public void onClose(AjaxRequestTarget target, DialogButton button) { }
		};

		final MessageDialog errorDialog = new MessageDialog("errorDialog", getString("error"),
				getString("error.title"), DialogButtons.OK, DialogIcon.ERROR) {

			@Override
			public void onClose(AjaxRequestTarget target, DialogButton button) { }
		};
		final ProgressBarDialog progress = new ProgressBarDialog("progressDialog", getString("calculating"));

		calculationParamForm.add(progress);
		calculationParamForm.add(informDialog);
		calculationParamForm.add(errorDialog);
		calculationParamForm.add(new AjaxButton("calculateButton") {

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				@SuppressWarnings("unchecked")
				Form<CalculationParameters> formComponent = (Form<CalculationParameters>) findComponentBy(MAIN_FORM_ID, getParent());
				try {
					DefaultPricePage pricePage = (DefaultPricePage) formComponent.getParent().getParent().getParent();
					PriceResultPanel resultList = pricePage.getPriceResultPanel();
					if (getCurrentParams().getProduct() == null) {
						getFeedBack().error(getString("product.Required"));
					} else {
						getCurrentParams();
						final UiCallback<PricingResult> callback = new UiCallbackImpl<>();
						getCurrentParams().getCalculationParams().saveConfigParams(parametersRepositoryHolder.getParametersRepository(calculationBase), productCode, calculationType);
						calculation.calculate(getCurrentParams(), callback, calculationBase, calculationType);
						progress.setCallback(callback);
						progress.setIEventSink(resultList);
						progress.setComponentsToUpdate(getFeedBack(), resultList);
						progress.open(target);
						target.add(resultList);
						super.onSubmit(target, form);
					}
				} catch (Exception e) {
					LOG.error("Ooops...", e);
					errorDialog.setModelObject(e.toString());
					errorDialog.open(target);
				}
				target.add(getFeedBack());
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				target.add(getFeedBack());
			}
		});
		calculationParamForm.add(new AjaxButton("resetButton") {

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				model.clean(parametersRepositoryHolder.getParametersRepository(calculationBase), productCode, calculationType);
				form.setModel(new CompoundPropertyModel(model));
				for (int i = 0; i < form.size(); i++) {
					if (form.get(i).getOutputMarkupId()) {
						target.add(form.get(i));
					}
				}
				target.add(getFeedBack());
				target.add(form);
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				target.add(getFeedBack());
			}
		}.setDefaultFormProcessing(false));
	}

	protected Form<T> getCalculationParamForm() {
		return calculationParamForm;
	}

	@Override
	protected void onBeforeRender() {
		getCurrentParams().getCalculationParams().updateConfigParams(parametersRepositoryHolder.getParametersRepository(calculationBase), productCode, calculationType);
		super.onBeforeRender();
	}

	protected CalculationParameters getCurrentParams() {
		@SuppressWarnings("unchecked")
		Form<CalculationParameters> formComponent = (Form<CalculationParameters>) findComponentBy(MAIN_FORM_ID, getParent());
		if (formComponent != null) {
			final CalculationParameters params = formComponent.getModel().getObject();
			params.setCalculationParams(calculationParamForm.getModelObject());
			return params;
		}
		return null;
	}

	public void addToForm(Component... components) {
		for (Component component : components) {
			component.setOutputMarkupId(true);
			calculationParamForm.add(component);
			inputFields.add(component);
		}
	}

	public void disableOnForm(String component) {
		calculationParamForm.get(component).setEnabled(false);
	}

	protected FeedbackPanel getFeedBack() {
		return (FeedbackPanel) findComponentBy(MAIN_FORM_ID, getParent()).get("feedbackPanel");
	}

	public List<Component> getInputFields() {
		return Collections.unmodifiableList(inputFields);
	}

	public Calculation getCalculation() {
		return calculation;
	}

	public void setCalculationType(CalculationType calculationType) {
		this.calculationType = calculationType;
	}

	public CalculationType getCalculationType() {
		return calculationType;
	}

    public TextField<Double> getInvestingSum() {
        return investingSum;
    }

    public void setCalculationBase(CalculationBase calculationBase) {
		this.calculationBase = calculationBase;
	}

	public CalculationBase getCalculationBase() {
		return calculationBase;
	}

	public void setProductCode(ProductCode productCode) {
		this.productCode = productCode;
	}

	public ProductCode getProductCode() {
		return productCode;
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}

}

