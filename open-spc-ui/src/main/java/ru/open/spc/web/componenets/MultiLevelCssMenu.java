package ru.open.spc.web.componenets;

import org.apache.wicket.core.util.string.JavaScriptUtils;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.IHeaderContributor;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.resource.DynamicJQueryResourceReference;

import java.util.List;

public class MultiLevelCssMenu extends Panel implements IHeaderContributor {

	private final static String DOWN_GIF_URL = "../img/icon-chevron-down.png";
	private final static String RIGHT_GIF_URL = "../img/icon-chevron-right.png";
	private final static String src = "var downGifRelativeLocation='" + DOWN_GIF_URL +
        "';" + "var rightGifRelativeLocation='" + RIGHT_GIF_URL + "';";


    @Override
	public void renderHead(IHeaderResponse response) {
		response.getResponse().write(JavaScriptUtils.SCRIPT_OPEN_TAG);
		response.getResponse().write(src);
		response.getResponse().write(JavaScriptUtils.SCRIPT_CLOSE_TAG);

		response.render(JavaScriptHeaderItem.forReference(DynamicJQueryResourceReference.get()));
	}

	public MultiLevelCssMenu(String id, List<MenuItem> menuItemList) {
		super(id);
		setRenderBodyOnly(true);
		MultiLevelMenu multiLevelMenu = new MultiLevelMenu("multiLevelMenu", menuItemList);
		multiLevelMenu.setRenderBodyOnly(true);
		add(multiLevelMenu);
	}

	private void processResponse(MenuItem menuItem) {
		switch (menuItem.getDestinationType()) {
			case EXTERNAL_LINK:
				// forward to external link
				break;
			case WEB_PAGE_CLASS:
				setResponsePage(menuItem.getResponsePageClass());
				break;
			case WEB_PAGE_INSTANCE:
				setResponsePage(menuItem.getResponsePage());
				break;
			case AJAX_TARGET:
				break;
			case NONE:
				break;
		}
	}

	class MultiLevelMenu extends Panel {

		public MultiLevelMenu(String id, List<MenuItem> menuItemList) {
			super(id);
			if (menuItemList == null || menuItemList.isEmpty()) {
				return;
			}
			ListView<MenuItem> menu = buildMultiLevelMenu("menuList", menuItemList);
			menu.setReuseItems(true);
			add(menu);
		}

		private ListView<MenuItem> buildMultiLevelMenu(String id, List<MenuItem> menuItemList) {
			return new ListView<MenuItem>(id, menuItemList) {
				public void populateItem(final ListItem<MenuItem> item) {
					final MenuItem menuItem = item.getModelObject();
					Link<MenuItem> link = new Link<MenuItem>("menuLink") {
						@Override
						public void onClick() {
							if (menuItem != null) {
								processResponse(menuItem);
							}
						}
					};
					Label linkText = new Label("menuLinkText", menuItem.getMenuText());
					linkText.setRenderBodyOnly(true);
					link.add(linkText);
					item.add(link);

					List<MenuItem> submenuItemList = menuItem.getSubMenuItemList();
					// INFO If submenu exists, output it to html. If not, add
					// empty markup container and hide it.
					if (submenuItemList != null && !submenuItemList.isEmpty()) {
						MultiLevelMenu subLevelMenu = new MultiLevelMenu("submenuListContainer", submenuItemList);
						subLevelMenu.setRenderBodyOnly(true);
						item.add(subLevelMenu);
					} else {
						WebMarkupContainer submenuMarkupContainer = new WebMarkupContainer("submenuListContainer");
						submenuMarkupContainer.setRenderBodyOnly(true);
						item.add(submenuMarkupContainer);
					}
				}
			};
		}
	}
}
