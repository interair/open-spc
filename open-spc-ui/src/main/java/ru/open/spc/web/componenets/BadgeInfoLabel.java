package ru.open.spc.web.componenets;

import org.apache.wicket.markup.html.basic.Label;

public class BadgeInfoLabel extends Label {
    public BadgeInfoLabel(String id, String label) {
        super(id, "<span class='badge badge-info'>" + label + "</span>");
    }
}
