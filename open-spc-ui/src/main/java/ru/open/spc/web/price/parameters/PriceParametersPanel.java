package ru.open.spc.web.price.parameters;

import com.googlecode.wicket.jquery.core.Options;
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import com.googlecode.wicket.jquery.ui.widget.tabs.TabbedPanel;
import com.googlecode.wicket.kendo.ui.form.dropdown.DropDownList;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.open.spc.calc.model.CalculationParameters;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.model.Product;
import ru.open.spc.web.componenets.model.ProductsModel;
import ru.open.spc.web.componenets.renders.ChoiceRendererImpl;
import ru.open.spc.web.price.parameters.panel.TabFactory;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public abstract class PriceParametersPanel extends Panel {

	public static final String MAIN_FORM_ID = "priceParamsForm";
	public static final String CALC_FORM_ID = "calcForm";

	private static final Logger LOG = LoggerFactory.getLogger(PriceParametersPanel.class);

	@SpringBean private ProductManager productManager;
    private final DropDownList<Product> productsDropDown;

	private final TabbedPanel tabPanel;

	public PriceParametersPanel(String id) {
		super(id);
		final Form<CalculationParameters> calculationParamForm = new Form<>(MAIN_FORM_ID, new CompoundPropertyModel<>(new CalculationParameters()));
		add(calculationParamForm);
		calculationParamForm.add(new PriceParametersValidator());
		productsDropDown = new DropDownList<>(
			"product", new ProductsModel(productManager), new ChoiceRendererImpl<>());
		productsDropDown.setRequired(true).setOutputMarkupId(true);

		final List<ITab> tabs = new ArrayList<>();
		Options options = new Options().set("collapsible", true).set("selected", Options.asString(0));

		calculationParamForm.add(new JQueryFeedbackPanel("feedbackPanel"));
		calculationParamForm.add(productsDropDown);
		tabPanel = new TabbedPanel("calculationType", tabs, options);
		tabPanel.setOutputMarkupId(true);
		tabPanel.setOutputMarkupPlaceholderTag(true);
		calculationParamForm.add(tabPanel);

		productsDropDown.add(new AjaxFormComponentUpdatingBehavior("onchange") {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				tabPanel.setVisible(true);
				final Product p = productsDropDown.getModelObject();
				tabPanel.getModelObject().clear();
                getTabFactory().createTabsList(p).forEach(tabPanel::add);
				target.add(tabPanel);
				LOG.debug("panel with product {} loaded", p);
			}
		});
	}

	@Override
	protected void onBeforeRender() {
		super.onBeforeRender();
		tabPanel.setVisible(false);
        productsDropDown.clearInput();
        productsDropDown.setModelObject(null);
	}

    protected abstract TabFactory getTabFactory();

}
