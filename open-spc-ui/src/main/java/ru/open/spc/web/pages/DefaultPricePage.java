package ru.open.spc.web.pages;

import org.apache.wicket.markup.html.panel.Panel;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.web.price.parameters.PriceResultPanel;

public class DefaultPricePage extends VerticalSplitPage {

    public final static String PRICE_RESULTS_PANEL = "priceResultsPanel";

    public DefaultPricePage(String id, Class<? extends Panel> panel) {
        super(new Pair<>(new Pair<>(id, panel),
                new Pair<>(PRICE_RESULTS_PANEL, PriceResultPanel.class)), 30);
    }

    public PriceResultPanel getPriceResultPanel() {
        return (PriceResultPanel) get(PRICE_RESULTS_PANEL).get("content");
    }

}
