package ru.open.spc.web.price.parameters.panel;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.markup.html.form.TextField;
import ru.open.spc.calc.model.simple.SimpleCalculationParameters;
import ru.open.spc.web.price.parameters.panel.annotation.PricingTabComponent;

import java.time.LocalDate;

import static ru.open.spc.dao.util.DateUtil.getPreviousDate;
import static ru.open.spc.dao.util.DateUtil.toLocalDateTime;
import static ru.open.spc.web.util.WicketUtil.createDefaultDatePicker;

@PricingTabComponent(isSimple = true, nameResourceKey = "simple.calculation.tab.label")
public abstract class SimplePriceParameterTabPanel<T extends SimpleCalculationParameters> extends
		AbstractPriceParametersPanel<T> {

	protected SimplePriceParameterTabPanel(String id, T model) {
		super(id, model);
		final TextField productEndDate = createDefaultDatePicker("productEndDate");
		productEndDate.setRequired(true).setOutputMarkupId(true);
		addToForm(productEndDate);
		final TextField optExpDate = createDefaultDatePicker("optExpDate");
		addToForm(optExpDate.setOutputMarkupId(true));
		productEndDate.add(new AjaxFormComponentUpdatingBehavior("onchange") {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				optExpDate.setModelObject(toLocalDateTime(getPreviousDate((LocalDate) productEndDate.getModelObject())));
				target.add(optExpDate);
			}
		});
		addToForm(optExpDate);
		addToForm(new Spinner<Double>("interestRate"));
		addToForm(new Spinner<Double>("volatilitySpread").setRequired(true));
		addToForm(new Spinner<Double>("strike1"));
		addToForm(new TextField<Double>("optionPrice"));
		addToForm(new Spinner<Long>("timeoutHours"));
		addToForm(new Spinner<Long>("timeoutMinutes"));
	}

}
