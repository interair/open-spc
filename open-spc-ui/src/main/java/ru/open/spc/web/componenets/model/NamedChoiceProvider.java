package ru.open.spc.web.componenets.model;

import com.vaynberg.wicket.select2.Response;
import com.vaynberg.wicket.select2.TextChoiceProvider;
import ru.open.spc.model.common.Named;

import java.util.*;
import java.util.stream.Collectors;

public class NamedChoiceProvider<T extends Named> extends TextChoiceProvider<T> {

    private final static int PAGE_SIZE = 10;

    private final Map<String, T> choicesByName = new HashMap<>();
    private final Map<String, T> choicesByIds = new HashMap<>();

    public List<T> matchingName(String str, int page) {
        List<T> result = new ArrayList<>();
        int offset = page * PAGE_SIZE;
        str = str.toUpperCase();

        for (String name : choicesByName.keySet()) {
            if (name.contains(str)){
                if (result.size() >= offset) {
                    result.add(choicesByName.get(name));
                    if (result.size() == PAGE_SIZE + 1) break;
                }
            }
        }
        return result;
    }

    @Override
    protected String getDisplayText(T choice) {
        return choice.getName();
    }

    @Override
    protected Object getId(T choice) {
        return choice.getId().toString();
    }

    @Override
    public void query(String term, int page, Response<T> response) {
        response.addAll(matchingName(term, page));
        response.setHasMore(response.size() > PAGE_SIZE + 1);    //  response.size() from 0 to PAGE_SIZE + 1
    }

    @Override
    public Collection<T> toChoices(Collection<String> ids) {
        return ids.stream().map(choicesByIds::get).collect(Collectors.toList());
    }

    public void setList(List<T> list) {
        clearMaps();
        list.forEach(this::fillMaps);
    }

    private void clearMaps() {
        choicesByName.clear();
        choicesByIds.clear();
    }

    private void fillMaps(T t) {
        choicesByName.put(getName(t).toUpperCase(), t);
        choicesByIds.put(t.getId().toString(), t);
    }

    protected String getName(T t) {
        return t.getName();
    }

    public int getSize(){return choicesByIds.size();}
}
