package ru.open.spc.web.pages.asset;

import com.googlecode.wickedcharts.highcharts.options.*;
import com.googlecode.wickedcharts.highcharts.options.series.Coordinate;
import com.googlecode.wickedcharts.highcharts.options.series.CustomCoordinatesSeries;
import com.googlecode.wickedcharts.wicket6.highcharts.Chart;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.calc.kernel.services.ApproximationCalculationService;
import ru.open.spc.calc.model.ApproximatedPrice;
import ru.open.spc.calc.model.CalculationBase;
import ru.open.spc.calc.util.Pair;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.dao.util.DateUtil;
import ru.open.spc.model.ApproximatingRate;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Dividend;
import ru.open.spc.model.Underlying;
import ru.open.spc.model.enums.InstrumentType;
import ru.open.spc.web.util.UpdateEvent;

import java.time.LocalDate;
import java.util.*;

import static java.lang.StrictMath.abs;
import static ru.open.spc.calc.util.Util.*;
import static ru.open.spc.dao.util.DateUtil.*;
import static ru.open.spc.web.util.UIUtil.getDateInUTC;

//TODO: Arghh... govnocode!!!111
@SuppressWarnings("serial")
public class AssetDetailsGraphPanel extends Panel {

    @SpringBean private AssetFilter assetFilter;
	@SpringBean private AssetManager assetManager;
	@SpringBean private CalculationManager calculationManager;
	@SpringBean private ApproximationCalculationService approximationCalculating;

	private final Set<LocalDate> xAxisDates = new TreeSet<>();
	private final Map<String, Map<LocalDate, Double>> data = new HashMap<>();
	private final Options options = new Options();
	private final Chart chart;

	public AssetDetailsGraphPanel(String id) {
		super(id);
		createChart();
		chart = new Chart("chart", options);
		chart.setOutputMarkupId(true);
		add(chart);
		setOutputMarkupId(true);
	}

	@Override
	protected void onBeforeRender() {
		super.onBeforeRender();
		updateData(Collections.emptyList());
	}

	private void prepareData(List<Asset> assets) {
		xAxisDates.clear();
		data.clear();
        if (assets == null || assets.isEmpty()) {
            assetManager.all().stream().filter(this::filterAsset).forEach(this::processAsset);
        } else {
            assets.stream().filter(this::filterAsset).forEach(this::processAsset);
        }
	}

    private boolean filterAsset(final Asset asset) {
         return asset.getAssetPrice() != null;
    }

    private void processAsset(Asset asset) {
		List<LocalDate> dates = collectDates(asset);

		Pair<String, Double> firstValue = null;
		Pair<String, Double> lastValue = null;
		if (asset.getCoefficients().getFromDate() == null) {
			firstValue = processBAApproximation(asset, dates);
		} else {
			dates.add(asset.getCoefficients().getFromDate());
			Pair<List<LocalDate>, List<LocalDate>> splittedDates = splitDates(dates, asset.getCoefficients().getFromDate()); //TODO add 3 part (middle)
			if (!splittedDates.getLeft().isEmpty()) {
				firstValue = processBAApproximation(asset, splittedDates.getLeft());
			}
			if (!splittedDates.getRight().isEmpty()) {
				lastValue = processNotLiquidApproximation(asset, splittedDates.getRight());
			}
		}
		findIntermediateFeatures(asset, firstValue, lastValue);
		xAxisDates.addAll(dates);
	}

    private void createSeries() {
		for (Map.Entry<String, Map<LocalDate, Double>> entry : data.entrySet()) {
			String name = entry.getKey();
			Map<LocalDate, Double> map = entry.getValue();
			createSeries(name, map);
		}
	}

	private void createSeries(String name, Map<LocalDate, Double> values) {
		List<Coordinate<String, Double>> seriesData = createSeriesData(values);
		if (!seriesData.isEmpty()) {
			CustomCoordinatesSeries<String, Double> series = new CustomCoordinatesSeries<>();
			series.setName(name);
			series.setData(seriesData);
			options.addSeries(series);
		}
	}

	private List<LocalDate> getRatesDates(List<ApproximatingRate> rates, LocalDate baExpireDate) {
		List<LocalDate> dates = new ArrayList<>();
		for (ApproximatingRate approximatingRate : rates) {
			boolean isBA = compareDatesWithoutTime(baExpireDate, approximatingRate.getFromDate());
			addRelativePointsDates(approximatingRate.getFromDate(), dates, isBA);
		}
		return dates;
	}

	private List<LocalDate> getDividendsDates(List<Dividend> dividends, LocalDate baExpireDate) {
		if (dividends.isEmpty()) {
			return Collections.<LocalDate>emptyList();
		} else {
			List<LocalDate> dates = new ArrayList<>();
			for (Dividend dividend : dividends) {
				dates.add(getCalendarPreviousDate(dividend.getPayDate()));
				dates.add(dividend.getPayDate());
			}
			return dates;
		}
	}

	private void addRelativePointsDates(LocalDate date, List<LocalDate> list, boolean isBA) {
		list.add(date);
//		if (isBA) {
			list.add(getCalendarNextDate(date));
//		} else {
			list.add(getCalendarPreviousDate(date));
//		}
	}

	private void createChart() {
		ChartOptions chartOptions = new ChartOptions();
		chartOptions.setType(SeriesType.LINE);
		chartOptions.setZoomType(ZoomType.X);
		chartOptions.setSpacingRight(20);
		options.setChartOptions(chartOptions);
		Title title = new Title(new ResourceModel("chart.name").getObject());
		title.setX(-20);
		options.setTitle(title);
		Title subTitle = new Title(new ResourceModel("chart.desc").getObject());
		subTitle.setX(-20);
		options.setSubtitle(subTitle);
		Axis xAxis = new Axis();
		xAxis.setType(AxisType.DATETIME);
		xAxis.setMaxZoom(14 * 24 * 3_600_000);
		Axis yAxis = new Axis();
		yAxis.setTitle(new Title(new ResourceModel("chart.price").getObject()));
		options.setyAxis(yAxis);
		options.setxAxis(xAxis);
		Tooltip tooltip = new Tooltip();
		tooltip.setValueDecimals(2);
		options.setTooltip(tooltip);
		PlotOptions lineOptions = new PlotOptions();
		DataLabels dataLabels = new DataLabels(Boolean.FALSE);
		lineOptions.setDataLabels(dataLabels);
		lineOptions.setEnableMouseTracking(Boolean.TRUE);
		lineOptions.setPointStart(new Date().getTime());
		options.setPlotOptions(new PlotOptionsChoice().setLine(lineOptions));
		options.setTooltip(new Tooltip());
	}

	private List<Coordinate<String, Double>> createSeriesData(Map<LocalDate, Double> values) {
		List<Coordinate<String, Double>> seriesData = new LinkedList<>();
		List<LocalDate> sortedList = new ArrayList<>(values.keySet());
		sort(sortedList);
		for (LocalDate date : sortedList) {
			Double curr = values.get(date);
			seriesData.add(new Coordinate<>(getDateInUTC(date), curr));
		}
		return seriesData;
	}

	private void updateData(List<Asset> assets) {
		options.clearSeries();
		prepareData(assets);
		createSeries();
	}

	@Override
	public void onEvent(IEvent<?> event) {
		Object payload = event.getPayload();
		if (payload instanceof UpdateEvent) {
			final UpdateEvent uevent = (UpdateEvent) payload;
			List<Asset> assets = uevent.getPayload();
			updateData(assets);
			if (uevent.getTarget() != null) {
				uevent.getTarget().add(chart);
			}
		}
		super.onEvent(event);
	}

	private List<LocalDate> collectDates(Asset asset) {
		List<ApproximatingRate> rates = asset.getCoefficients().getApproximatingRates();
		List<Dividend> dividends = asset.getCoefficients().getDividends();
		List<LocalDate> dates = getRatesDates(rates, asset.getAssetPrice().getUnderlyingDate());
		dates.add(asset.getAssetPrice().getUnderlyingDate());
		dates.add(LocalDate.now());
		dates.addAll(getDividendsDates(dividends, asset.getAssetPrice().getUnderlyingDate()));
		dates.addAll(findAllFeaturesDates(asset.getAssetPrice().getUnderlyingDate(), asset.getOuterId()));
		if (!dates.isEmpty()) {
            dates = removeDuplicates(dates);
			Collections.sort(dates);
            LocalDate addMonths = addMonths(dates.get(dates.size() - 1), 1);
			dates.add(addMonths);
		}
		return dates;
	}

	private Pair<String, Double> processBAApproximation(Asset asset, List<LocalDate> dates) {
		sort(dates);
		Collections.reverse(dates);
		double firstValue = 0d;
		Map<LocalDate, Double> seriesData = new HashMap<>();
		for (LocalDate date : dates) {
			if (compareDatesWithoutTime(asset.getAssetPrice().getUnderlyingDate(), date)) {
				double price = getPrice(asset.getAssetPrice(), asset.getMultiplier());
				ApproximatedPrice applayBAApproximation = approximationCalculating.applyBAApproximation(
					asset, date, price, CalculationBase.EXCHANGE);
				if (firstValue == 0d) {
					firstValue = applayBAApproximation.getTotal();
				}
				seriesData.put(date, applayBAApproximation.getTotal());
			}
		}
		String ticker = "{" + asset.getBaseTicker() + "}";
		addToDate(ticker, seriesData);
		return new Pair<>(ticker, firstValue);
	}

	private Pair<String, Double> processNotLiquidApproximation(Asset asset, List<LocalDate> dates) {
		Underlying price = calculationManager.getFuturesBy(asset.getOuterId(),
                asset.getCoefficients().getFromDate());
		if (price == null) {
			throw new IllegalArgumentException("не найдено цены для БА с тикером " + asset.getBaseTicker() +
				" датой экспирации " + asset.getCoefficients().getFromDate());     //не найдена цена начала апроксимации
		}
		Double firstValue = 0d;
		Map<LocalDate, Double> seriesData = new HashMap<>();
		sort(dates);
		for (LocalDate date : dates) {
			ApproximatedPrice applayNLApproximation = approximationCalculating.applyNotLiquidApproximation(asset, date, price.getRealPrice());
			if (firstValue.equals(0d)) {
				firstValue = applayNLApproximation.getTotal();
			}
			seriesData.put(date, applayNLApproximation.getTotal());
		}
		addToDate(price.getName(), seriesData);
		return new Pair<>(price.getName(), firstValue);
	}

    private void addToDate(String baseTicker, Map<LocalDate, Double> seriesData) {
        if (!seriesData.isEmpty()) {
            final Map<LocalDate, Double> localDateDoubleMap = data.get(baseTicker);
            if (localDateDoubleMap != null) {
                Double prev = Double.MAX_VALUE;
                for (Map.Entry<LocalDate, Double> entry : localDateDoubleMap.entrySet()) {
                    final Double value = entry.getValue();
                    if (abs(prev - value) > 0.1) {
                        seriesData.put(entry.getKey(), value);
                        prev = value;
                    }
                }
            }
            data.put(baseTicker, seriesData);
        }
    }

	private List<LocalDate> findAllFeaturesDates(LocalDate startDate, Long assetOuterId) {
        LocalDate endDate = calculationManager.getLastFutureDate(assetOuterId);
		return calculationManager.getAvailableFuturesDates(getCalendarNextDate(startDate), endDate, assetOuterId);
	}

	private void findIntermediateFeatures(Asset asset, Pair<String, Double> firstValue, Pair<String, Double> lastValue) {
        LocalDate prevDate = asset.getAssetPrice().getUnderlyingDate();
		double prevValue = firstValue.getRight();
		List<LocalDate> dates = new ArrayList<>(findAllFeaturesDates(prevDate, asset.getOuterId()));
        findIntermediateDividend(asset, new ArrayList<>(dates));
		Pair<List<LocalDate>, List<LocalDate>> splitDates;
		if (asset.getCoefficients().getFromDate() != null) {
            LocalDate splitDate = getCalendarPreviousDate(asset.getCoefficients().getFromDate());
			dates.add(splitDate);
			dates.remove(asset.getCoefficients().getFromDate());
			splitDates = splitDates(dates, splitDate);
		} else {
			splitDates = new Pair<>(dates, Collections.<LocalDate>emptyList());
		}
		for (LocalDate date : splitDates.getLeft()) {
			Underlying futurePiceBy = calculationManager.getFuturesBy(asset.getOuterId(), date);
			if (futurePiceBy != null && assetFilter.apply(futurePiceBy.getName())) {
				createSeriesDates(prevDate, prevValue, futurePiceBy.getUnderlyingDate(),
					getPrice(futurePiceBy, asset.getMultiplier()), "[" + futurePiceBy.getName() + "]");
				prevDate = date;
				prevValue = getPrice(futurePiceBy, asset.getMultiplier());
			}
		}
		if (lastValue != null && !lastValue.getRight().equals(0d) && asset.getCoefficients().getFromDate() != null) {
			createSeriesDates(prevDate, prevValue,
				asset.getCoefficients().getFromDate(), lastValue.getRight(), "[" + lastValue.getLeft() + "]");
		}
		for (LocalDate date : splitDates.getRight()) {
			Underlying futurePiceBy = calculationManager.getFuturesBy(asset.getOuterId(), date);
			if (futurePiceBy != null) {
				addToDate("(" + futurePiceBy.getName() + ")",
					Collections.singletonMap(date, futurePiceBy.getRealPrice()));
			}
		}
	}

    private void findIntermediateDividend(Asset asset, List<LocalDate> underlyingDates) {

        final LocalDate underlyingDate = asset.getAssetPrice().getUnderlyingDate();
        underlyingDates = new ArrayList<>(underlyingDates);
        underlyingDates.add(asset.getAssetPrice().getUnderlyingDate());
        LocalDate fromDate = asset.getCoefficients().getFromDate();   //asset.getAssetPrice().getUnderlyingDate()
        if (fromDate == null) fromDate = underlyingDate.plusYears(1000);
        final List<Dividend> dividends = new ArrayList<>(asset.getCoefficients().getDividends());
        for (Dividend dividend : dividends) {
            if (dividend.getPayDate().isAfter(underlyingDate) && dividend.getPayDate().isBefore(fromDate)) {
                if (underlyingDates.contains(dividend.getPayDate())) continue;
                final ArrayList<LocalDate> cloneUnderlyingDates = new ArrayList<>(underlyingDates);
                cloneUnderlyingDates.add(dividend.getPayDate());
                sort(cloneUnderlyingDates);
                final int i = Collections.binarySearch(cloneUnderlyingDates, dividend.getPayDate());
                if (i < 1) continue;
                final LocalDate beforeUnderlyingDate = cloneUnderlyingDates.get(i - 1);
                Underlying beforeUnderlying = calculationManager.getFuturesBy(asset.getOuterId(), beforeUnderlyingDate);
                final LocalDate afterUnderlyingDate = cloneUnderlyingDates.get(i + 1);
                Underlying afterUnderlying = calculationManager.getFuturesBy(asset.getOuterId(), afterUnderlyingDate);
                final LocalDate minusDate = DateUtil.getPreviousDate(dividend.getPayDate());
	            final double price = approximationCalculating.calculateLiquidFeatures(dividend.getPayDate(), beforeUnderlyingDate, afterUnderlyingDate,
                    beforeUnderlying.getRealPrice(), afterUnderlying.getRealPrice(), asset);
                final double priceAfter = approximationCalculating.calculateLiquidFeatures(minusDate, beforeUnderlyingDate, afterUnderlyingDate,
                    beforeUnderlying.getRealPrice(), afterUnderlying.getRealPrice(), asset);

                createSeriesDates(dividend.getPayDate(), price, minusDate,  priceAfter, "[" + afterUnderlying.getName() + "]");

            }

        }
    }

	private void createSeriesDates(LocalDate prevDate, Double prevPrice, LocalDate nextDate, Double nextPrice, String name) {

        Map<LocalDate, Double> seriesData = new HashMap<>();
        seriesData.put(prevDate, prevPrice);
        seriesData.put(nextDate, nextPrice);
        addToDate(name, seriesData);
	}

	private List<LocalDate> removeDuplicates(List<LocalDate> dates) {
        removeNullElements(dates);
        Set<LocalDate> uniqDates = new HashSet<>(dates);
		return new ArrayList<>(uniqDates);
	}

	private double getPrice(Underlying underlying, int multiplier) {
		double price = underlying.getRealPrice();
		if (underlying.getInstrumentType() == InstrumentType.SPOT) {
			price *= multiplier;
		}
		return price;
	}

}
