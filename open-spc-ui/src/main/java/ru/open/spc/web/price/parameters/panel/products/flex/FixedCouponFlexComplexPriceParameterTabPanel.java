package ru.open.spc.web.price.parameters.panel.products.flex;

import ru.open.spc.calc.model.complex.flex.FixedCouponComplexFlexParameters;

public class FixedCouponFlexComplexPriceParameterTabPanel extends ComplexFlexPriceParameterTabPanel {

	public FixedCouponFlexComplexPriceParameterTabPanel(String id) {
		super(id, new FixedCouponComplexFlexParameters());
	}
}
