package ru.open.spc.web.componenets;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class MenuItem implements Serializable {

	public enum DestinationType {
		WEB_PAGE_CLASS, WEB_PAGE_INSTANCE, EXTERNAL_LINK, AJAX_TARGET, NONE
    }

	private String menuText;
	// possible menu inputs
	private Class<? extends WebPage> responsePageClass;
	private WebPage responsePage;
	private String externalLink;
	private Link<Void> ajaxLink;

	private final DestinationType destinationType;
	// submenu list
	private final List<MenuItem> subMenuItemList = new ArrayList<>();

	public MenuItem() {
		this.destinationType = DestinationType.NONE;
	}

	public MenuItem(String submenuText) {
		this.menuText = submenuText;
		this.destinationType = DestinationType.NONE;
	}

	public MenuItem(String menuText, Link<Void> ajaxLink) {
		this.menuText = menuText;
		this.ajaxLink = ajaxLink;
		this.destinationType = DestinationType.AJAX_TARGET;
	}

	public MenuItem(final Component... componentsToUpdate) {
		// TODO id needs to be on html
		this.ajaxLink = new AjaxFallbackLink<Void>("menuLink") {

			@Override
			public void onClick(AjaxRequestTarget target) {
				target.add(componentsToUpdate);
			}
		};
		this.destinationType = DestinationType.AJAX_TARGET;
	}

	public MenuItem(String menuText, Class<? extends WebPage> destinationPageClass) {
		this.menuText = menuText;
		this.responsePageClass = destinationPageClass;
		this.destinationType = DestinationType.WEB_PAGE_CLASS;
	}

	public <T extends WebPage> MenuItem(String menuText, T destinationPage) {
		this.menuText = menuText;
		this.responsePage = destinationPage;
		this.destinationType = DestinationType.WEB_PAGE_INSTANCE;
	}

	public static MenuItem getMenuSeperator() {
		return new MenuItem();
	}

	public String getMenuText() {
		return menuText;
	}

	public Class<? extends WebPage> getResponsePageClass() {
		return responsePageClass;
	}

	public WebPage getResponsePage() {
		return responsePage;
	}

	public String getExternalLink() {
		return externalLink;
	}

	public Link<Void> getAjaxLink() {
		return ajaxLink;
	}

	public DestinationType getDestinationType() {
		return destinationType;
	}

	public List<MenuItem> getSubMenuItemList() {
		return subMenuItemList;
	}

	public MenuItem add(MenuItem subMenu) {
		subMenuItemList.add(subMenu);
		return this;
	}
}
