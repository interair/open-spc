package ru.open.spc.web.pages.asset;

import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.event.Broadcast;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.model.Asset;
import ru.open.spc.web.componenets.DeleteLink;
import ru.open.spc.web.componenets.EditLink;
import ru.open.spc.web.componenets.SelectLink;
import ru.open.spc.web.componenets.model.BooleanModel;
import ru.open.spc.web.componenets.model.SortableDataProviderImpl;
import ru.open.spc.web.componenets.model.UnityModel;
import ru.open.spc.web.dialog.AssetDialog;
import ru.open.spc.web.dialog.DeleteDialog;
import ru.open.spc.web.dialog.tabs.AssetTabbedPanelDialog;
import ru.open.spc.web.util.UpdateEvent;
import ru.open.spc.web.util.WicketUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ru.open.spc.calc.util.Util.setTempId;

@SuppressWarnings("serial")
public class AssetMainTablePanel extends Panel {

	private final static String ID = "assetDetailsTablePanel";
	private final SortableDataProviderImpl<Asset> dataProvider = new SortableDataProviderImpl<>();
	private final DataTable<Asset, Class<?>> resultListView;
	private final Form<String> panel;
    public final static String TABBED_PANEL_PAGE = "tabbedPanelPage";
    @SpringBean
    private AssetManager assetManager;

	public AssetMainTablePanel(String id) {
		super(id);
        Injector.get().inject(this);
        final DeleteDialog<Asset> deleteMessageDialog = new DeleteDialog<>("deleteConfirmation", getString("delete.dialog.title"),
			new ResourceModel("asset.delete.message").getObject(), DialogButtons.YES_NO);
        deleteMessageDialog.setGenericManager(assetManager);
        final AssetTabbedPanelDialog tabbedPanelDialog = new AssetTabbedPanelDialog(TABBED_PANEL_PAGE);
		add(new AjaxLink<String>("selectAllLink") {

            @Override
            public void onClick(AjaxRequestTarget target) {
                send(getPageComponent(), Broadcast.BREADTH,
                    new UpdateEvent(target, dataProvider.getList(), AssetMainTablePanel.ID));
            }
        });

		add(new AjaxLink<String>("newAssetLink") {

			@Override
			public void onClick(AjaxRequestTarget target) {
                tabbedPanelDialog.setModel(setTempId(new Asset()));
                tabbedPanelDialog.open(target);
			}

		});
		dataProvider.setList(Collections.<Asset>emptyList());
		List<IColumn<Asset, String>> columns = new ArrayList<>();
        columns.add(new AbstractColumn<Asset, String>(new ResourceModel("void")) {
            public void populateItem(Item<ICellPopulator<Asset>> cellItem, String componentId,
                                     final IModel<Asset> model) {
                SelectLink panel = new SelectLink(componentId);
                AjaxLink link = new AjaxLink<String>("selectLink") {

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        send(getPageComponent(), Broadcast.BREADTH,
                            new UpdateEvent(target, Collections.singletonList(model.getObject()), AssetMainTablePanel.ID));

                    }
                };
                panel.add(link);
                cellItem.add(panel);
            }
        });
		columns.add(new AbstractColumn<Asset, String>(new ResourceModel("void")) {
            public void populateItem(Item<ICellPopulator<Asset>> cellItem, String componentId,
                                     final IModel<Asset> model) {
                EditLink panel = new EditLink(componentId);
                AjaxLink link = new AjaxLink<String>("editLink") {

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        tabbedPanelDialog.setModel(model.getObject());
                        tabbedPanelDialog.open(target);
                    }
                };
                panel.add(link);
                cellItem.add(panel);
            }
        });
		columns.add(new AbstractColumn<Asset, String>(new ResourceModel("void")) {
			public void populateItem(Item<ICellPopulator<Asset>> cellItem, String componentId,
									 final IModel<Asset> model) {
				DeleteLink panel = new DeleteLink(componentId);
				AjaxLink link = new AjaxLink<String>("deleteLink") {

					@Override
					public void onClick(AjaxRequestTarget target) {
						deleteMessageDialog.setObject(model.getObject());
						deleteMessageDialog.open(target);

					}
				};
				panel.add(link);
				cellItem.add(panel);
			}
		});
		List<IColumn<Asset, String>> createSortableColumns = WicketUtil.createSortableColumns("id", "name", "baseTicker");
		columns.addAll(createSortableColumns);
		columns.add(new AbstractColumn<Asset, String>(new ResourceModel("asset.unity"), "unity") {
			public void populateItem(Item<ICellPopulator<Asset>> cellItem, String componentId,
									 final IModel<Asset> model) {
				cellItem.add(new Label(componentId, new UnityModel(model.getObject().getUnity())));
			}
		});
		columns.add(new AbstractColumn<Asset, String>(new ResourceModel("asset.product")) {
            public void populateItem(Item<ICellPopulator<Asset>> cellItem, String componentId,
                                     final IModel<Asset> model) {
                StringBuilder productsNames = new StringBuilder();
                model.getObject().getProducts().forEach(s -> productsNames.append("<span class=\"badge badge-info\">").append(s.getName()).append("</span>"));
                cellItem.add(new Label(componentId, productsNames.toString()).setEscapeModelStrings(false));
            }
        });
		columns.add(new PropertyColumn<>(new ResourceModel("asset.multiplier"), "multiplier", "multiplier"));
		columns.add(new PropertyColumn<>(new ResourceModel("asset.point.price"), "pointPrice", "pointPrice"));
		columns.add(new PropertyColumn<>(new ResourceModel("asset.price.iv"), "assetPrice.realPrice", "assetPrice.realPrice"));
		columns.add(new AbstractColumn<Asset, String>(new ResourceModel("asset.exchange"), "exchange") {
			public void populateItem(Item<ICellPopulator<Asset>> cellItem, String componentId,
									 final IModel<Asset> model) {
				cellItem.add(new Label(componentId,
					new BooleanModel(model.getObject().getExchange())));
			}
		});
		columns.add(new AbstractColumn<Asset, String>(new ResourceModel("basePriceVariative"), "basePriceVariative") {
			public void populateItem(Item<ICellPopulator<Asset>> cellItem, String componentId,
									 final IModel<Asset> model) {
				cellItem.add(new Label(componentId,
					new BooleanModel(model.getObject().getBasePriceVariative())));
			}
		});
		columns.add(new PropertyColumn<>(new ResourceModel("asset.strike.step"), "strikeStep", "strikeStep"));
		columns.add(new PropertyColumn<>(new ResourceModel("asset.minimum.price"), "minimumPrice", "minimumPrice"));
		resultListView = new AjaxFallbackDefaultDataTable("assetList", columns, dataProvider, 100);
		panel = new Form<String>("assetPanel") {{
			add(resultListView.setOutputMarkupId(true));
		}};
		add(panel.setOutputMarkupId(true));
		add(tabbedPanelDialog);
		add(deleteMessageDialog);
		setOutputMarkupId(true);
	}

	@Override
	protected void onBeforeRender() {
        final List<Asset> all = assetManager.all();
        dataProvider.setList(all);
        super.onBeforeRender();
	}

	private Component getPageComponent() {
		return AssetMainTablePanel.this.getParent().getParent();
	}

	@Override
	public void onEvent(IEvent<?> event) {
		Object payload = event.getPayload();
		if (payload instanceof UpdateEvent) {
			UpdateEvent uevent = (UpdateEvent) payload;
			if (uevent.isAccepted(AssetDialog.DIALOG_EVENT) && uevent.getTarget() != null) {
				uevent.getTarget().add(panel);
				send(getPageComponent(), Broadcast.BREADTH,
					new UpdateEvent(uevent.getTarget(), dataProvider.getList(), AssetMainTablePanel.ID));
			}
		}
		super.onEvent(event);
	}
}
