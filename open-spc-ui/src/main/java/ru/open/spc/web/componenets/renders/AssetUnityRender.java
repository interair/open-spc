package ru.open.spc.web.componenets.renders;

import ru.open.spc.model.enums.AssetUnity;

public class AssetUnityRender extends StringChoiceRenderer<AssetUnity> {

	@Override
	public String getKey(AssetUnity object) {
		switch (object) {
			case POINT:
				return "asset.unity.point.value";
			case RUB:
				return "asset.unity.rub.value";
			case USD:
				return "asset.unity.usd.value";
			default:
				return null;
		}
	}
}
