package ru.open.spc.web.componenets.model;

import ru.open.spc.model.enums.AssetUnity;

public class UnityModel extends Model<String> {

    private final AssetUnity unity;

	public UnityModel(AssetUnity unity) {
		this.unity = unity;
	}

	@Override
	public String getObject() {
		switch (unity) {
			case POINT:
				return get("asset.unity.point.value");
			case RUB:
				return get("asset.unity.rub.value");
			case USD:
				return get("asset.unity.usd.value");
			default:
				return null;
		}
	}
}
