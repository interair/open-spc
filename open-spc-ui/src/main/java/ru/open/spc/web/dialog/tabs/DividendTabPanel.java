package ru.open.spc.web.dialog.tabs;

import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.HeaderlessColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.DividendManager;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Dividend;
import ru.open.spc.web.componenets.DeleteLink;
import ru.open.spc.web.componenets.EditLink;
import ru.open.spc.web.componenets.model.SortableDataProviderImpl;
import ru.open.spc.web.dialog.AssetDialog;
import ru.open.spc.web.dialog.DeleteDialog;
import ru.open.spc.web.dialog.DividendDialog;
import ru.open.spc.web.util.UpdateEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.wicket.event.Broadcast.DEPTH;
import static ru.open.spc.calc.kernel.SPCalculationCommon.sortDividends;
import static ru.open.spc.calc.util.Util.merge;
import static ru.open.spc.calc.util.Util.setTempId;
import static ru.open.spc.web.pages.asset.AssetMainTablePanel.TABBED_PANEL_PAGE;
import static ru.open.spc.web.util.WicketUtil.createSortableColumnsWithPrefix;
import static ru.open.spc.web.util.WicketUtil.findComponentBy;

public class DividendTabPanel extends ModelObjectAwarePanel {
    private final Form<Asset> dividendForm = new Form<>("dividendForm", new Model<>());
    private final SortableDataProviderImpl<Dividend> dataProvider = new SortableDataProviderImpl<>();
    private final DividendDialog dividendDialog = new DividendDialog("dividendDialog");
    private final DeleteDialog<Dividend> deleteDividendDialog;
    public static final String DIALOG_DIVIDENDS_EVENT = "dialogDividendsEvent";
    private final DataTable<Dividend, Class<?>> resultListView;
    private final List<Dividend> deletedList = new ArrayList<>();

    @SpringBean private DividendManager dividendManager;

    public DividendTabPanel(String id){
        super(id);
        Injector.get().inject(this);

        deleteDividendDialog = new DeleteDialog<>("deleteDividendDialog", new ResourceModel("delete.dialog.title").getObject(),
                new ResourceModel("delete.dividend.dialog.title").getObject(), DialogButtons.YES_NO);
        dividendForm.setOutputMarkupId(true);
        add(dividendForm);
        add(deleteDividendDialog);
        add(dividendDialog);
        dividendForm.add(new AjaxLink<String>("newDivLink") {
            @Override
            public void onClick(AjaxRequestTarget target) {
                dividendDialog.setDefaultModelObject(setTempId(new Dividend()));
                dividendDialog.open(target);
            }
        });

        dataProvider.setList(Collections.<Dividend>emptyList());
        List<IColumn<Dividend, String>> columns = new ArrayList<>();
        columns.add(new HeaderlessColumn<Dividend, String>() {
            public void populateItem(Item<ICellPopulator<Dividend>> cellItem, String componentId,
                                     final IModel<Dividend> model) {
                cellItem.add(new EditLink(componentId).add(new AjaxLink<String>("editLink") {

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        dividendDialog.setDefaultModelObject(model.getObject());
                        dividendDialog.open(target);

                    }
                }));
            }
        });

        columns.add(new HeaderlessColumn<Dividend, String>() {
            public void populateItem(Item<ICellPopulator<Dividend>> cellItem, String componentId,
                                     final IModel<Dividend> model) {
                cellItem.add(new DeleteLink(componentId).add(new AjaxLink<String>("deleteLink") {

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        deleteDividendDialog.setObject(model.getObject());
                        deleteDividendDialog.open(target);

                    }
                }));
            }
        });

        columns.addAll(createSortableColumnsWithPrefix("asset.coefficients.dividend", "payDate", "amount"));

        resultListView = new AjaxFallbackDefaultDataTable("divList", columns, dataProvider, 100);
        dividendForm.add(resultListView.setOutputMarkupId(true));

        dividendForm.add(new AjaxButton("saveButton") {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                final List<Dividend> updatedList = dataProvider.getList();
                dividendManager.save(updatedList);
                dividendManager.remove(deletedList);
                final Asset asset = dividendForm.getModelObject();
                asset.getCoefficients().setDividends(updatedList);
                send(findComponentBy(TABBED_PANEL_PAGE, getParent()).getParent(), DEPTH,
                        new UpdateEvent(target, Collections.singletonList(asset), AssetDialog.DIALOG_EVENT));
                deletedList.clear();
            }

        });
    }

    public void setModelObject(Asset asset) {
        dataProvider.setList(sortDividends(asset.getCoefficients().getDividends(), true));
        dividendForm.setDefaultModelObject(asset);
    }

    @Override
    public void onEvent(IEvent<?> event) {
        Object payload = event.getPayload();
        final List<Dividend> list = new ArrayList<>(dataProvider.getList());
        final Asset modelObject = dividendForm.getModelObject();
        if (payload instanceof UpdateEvent) {
            UpdateEvent uevent = (UpdateEvent) payload;
            if (uevent.isAccepted(DividendDialog.DIVIDEND_ADD_EVENT)) {
                final Dividend div = uevent.getPayload();
                div.setAssetId(modelObject.getId());
                dataProvider.setList(merge(list, div));
            } else if (uevent.isAccepted(DeleteDialog.DELETE_EVENT)) {
                if (uevent.getPayload() instanceof Dividend) {
                    list.remove(uevent.getPayload());
                    dataProvider.setList(list);
                    deletedList.add(uevent.getPayload());
                }
            }
            uevent.getTarget().add(dividendForm, resultListView);
        }
        super.onEvent(event);
    }
}
