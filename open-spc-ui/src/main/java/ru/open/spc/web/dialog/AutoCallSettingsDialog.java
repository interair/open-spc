package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.core.JQueryBehavior;
import com.googlecode.wicket.jquery.core.Options;
import com.googlecode.wicket.jquery.core.renderer.TextRenderer;
import com.googlecode.wicket.jquery.ui.form.autocomplete.AutoCompleteTextField;
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.kendo.ui.datatable.DataTable;
import com.googlecode.wicket.kendo.ui.datatable.button.CommandButton;
import com.googlecode.wicket.kendo.ui.datatable.column.CommandColumn;
import com.googlecode.wicket.kendo.ui.datatable.column.IColumn;
import com.googlecode.wicket.kendo.ui.datatable.column.IdPropertyColumn;
import com.googlecode.wicket.kendo.ui.datatable.column.PropertyColumn;
import lombok.extern.slf4j.Slf4j;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.json.JSONException;
import org.apache.wicket.ajax.json.JSONObject;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.AutoCallAssetsManager;
import ru.open.spc.dao.manager.AutoCallAssetsRelationManager;
import ru.open.spc.model.Asset;
import ru.open.spc.model.AutoCallAsset;
import ru.open.spc.model.AutoCallAssetsRelation;
import ru.open.spc.web.componenets.model.NamedChoiceProvider;
import ru.open.spc.web.componenets.model.SortableDataProviderImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static ru.open.spc.web.util.DefaultDialogButtons.getDialogButtons;
import static ru.open.spc.web.util.DefaultDialogButtons.getSaveButton;

@SuppressWarnings("serial")
@Slf4j
public class AutoCallSettingsDialog extends ResizableDialog<AutoCallAsset> {

	@SpringBean
	private AutoCallAssetsRelationManager relationManager;
	@SpringBean
	private AssetManager assetManager;
	@SpringBean
	private AutoCallAssetsManager autoCallAssetsManager;
	@SpringBean
	private AutoCallAssetsRelationManager autoCallAssetsRelationManager;

	private final Form<AutoCallAsset> assetForm;
	private SortableDataProviderImpl<ACRelationModel> dataProvider = new SortableDataProviderImpl<>();
	final DataTable<ACRelationModel> table;

	public AutoCallSettingsDialog(String id) {
		super(id, new ResourceModel("auto.call.settings.page.link").getObject(), true);
		assetForm = new Form<>("autoCallAssets");
		assetForm.setOutputMarkupId(true);
		JQueryFeedbackPanel feedback = new JQueryFeedbackPanel("feedbackPanel");
		assetForm.add(feedback);

		final NamedChoiceProvider<Asset> tickerProvider = new NamedChoiceProvider<Asset>() {
			@Override
			protected String getName(Asset named) {
				return named.getName();
			}
		};
		tickerProvider.setList(assetManager.all());
		final AutoCompleteTextField<Asset> assetSelector = new AutoCompleteTextField<Asset>("asset", new TextRenderer<Asset>() {
			@Override
			public String getText(Asset object) {
				if (object == null || object.getName() == null) return null;
				return object.getName();
			}

		}, Asset.class) {
			@Override
			protected List<Asset> getChoices(String input) {
				return tickerProvider.matchingName(input, 0);
			}
		};

		assetSelector.add(new AjaxFormComponentUpdatingBehavior("onchange") {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				Asset modelObject = assetSelector.getModelObject();
				AutoCallAsset autoCallAsset = assetForm.getModelObject();
				autoCallAsset.setAsset(modelObject);
				autoCallAssetsManager.save(autoCallAsset);
				updateTableData(autoCallAsset);
				target.add(table);
			}
		});

		assetForm.add(assetSelector);
		assetForm.add(new RequiredTextField<Double>("sigma"));
		assetForm.add(new RequiredTextField<Double>("relativePrice"));
		assetForm.add(new RequiredTextField<Double>("initRelativePrice"));
		add(assetForm);

		Options options = new Options();
		options.set("height", 430);
		options.set("editable", Options.asString("inline"));
		options.set("pageable", true);

		table = new DataTable<ACRelationModel>("datatable", newColumnList(), dataProvider, 25, options) {

			@Override
			public void onCancel(AjaxRequestTarget target) {
				this.info("Cancelled...");
				target.add(feedback);
			}

			@Override
			public void onUpdate(AjaxRequestTarget target, JSONObject object) {
				try {
					long id = object.getLong("id");
					double coefficient = object.getDouble("coefficient");
					this.warn("Updated #" + object.getLong("id") + " to " + coefficient);
					AutoCallAssetsRelation byId = autoCallAssetsRelationManager.getById(id);
					byId.setCoefficient(coefficient);
					autoCallAssetsRelationManager.save(byId);
					setDataProvider(assetForm.getModelObject());
					target.add(feedback, table);
				} catch (JSONException e) {
					log.error("Can't parse json", e);
				}
			}

			@Override
			public void onDelete(AjaxRequestTarget target, JSONObject object) {
				target.add(feedback);
			}
		};
		this.add(table);
	}

	@Override
	protected DialogButton getSubmitButton() {
		return getSaveButton();
	}

	@Override
	protected List<DialogButton> getButtons() {
		return getDialogButtons();
	}

	public void setDefaultModelObject(AutoCallAsset asset) {
		assetForm.clearInput();
		assetForm.setDefaultModel(new CompoundPropertyModel<>(asset));
		updateTableData(asset);
	}

	private void updateTableData(AutoCallAsset asset) {
		List<AutoCallAsset> all = autoCallAssetsManager.all();
		for (AutoCallAsset left : all) {
			for (AutoCallAsset right : all) {
				AutoCallAssetsRelation relation = autoCallAssetsRelationManager.getRelation(left.getId(), right.getId());
				if (relation == null) {
					relation = new AutoCallAssetsRelation();
					relation.setAssetRight(right);
					relation.setAssetLeft(left);
					relation.setCoefficient(left.getId() != right.getId() ? 0.25 : 1);
					autoCallAssetsRelationManager.save(relation);
				}
			}
		}
		setDataProvider(asset);
	}

	public void setDataProvider(AutoCallAsset asset) {
		List<AutoCallAssetsRelation> allRelation = autoCallAssetsRelationManager.getAllRelation(asset.getId());
		List<ACRelationModel> collect = allRelation.stream().map(a ->
				ACRelationModel.builder()
						.id(a.getId())
						.coefficient(a.getCoefficient())
						.assetLeft(a.getAssetLeft().equals(asset) ? a.getAssetRight() : a.getAssetLeft() ).build())
				.sorted((o1, o2) -> o1.getId().compareTo(o2.getId())).collect(Collectors.toList());
		dataProvider.setList(collect);
	}

	@Override
	public Form<?> getForm() {
		return assetForm;
	}

	@Override
	protected void onOpen(AjaxRequestTarget target) {
		target.add(assetForm, table);
	}

	@Override
	protected void onError(AjaxRequestTarget target) {
		target.add(get("assetForm").get("feedbackPanel"));
	}

	@Override
	protected void onSubmit(AjaxRequestTarget target) {
		AutoCallAsset autoCallAsset = assetForm.getModel().getObject();
		if (autoCallAsset != null) {
			autoCallAssetsManager.save(autoCallAsset);
		}
		target.add(getParent());
	}

	private List<IColumn> newColumnList() {
		List<IColumn> columns = new ArrayList<>();

		columns.add(new IdPropertyColumn("id", "id"));
		columns.add(new PropertyColumn("name", "assetLeft.name") {
			public Boolean isEditable() {
				return false;
			}
		});
		columns.add(new PropertyColumn("coefficient", "coefficient"));


		columns.add(new CommandColumn("", 250) {
			@Override
			public List<CommandButton> newButtons() {
				return Arrays.asList(new CommandButton("edit", Model.of("Edit")));
			}
		});

		return columns;
	}

	@Override
	public void onConfigure(JQueryBehavior behavior) {
		super.onConfigure(behavior);
		behavior.setOption("width", "750");
	}

}
