package ru.open.spc.web.dialog.tabs;

import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.HeaderlessColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.ApproximatingRateManager;
import ru.open.spc.model.ApproximatingRate;
import ru.open.spc.model.Asset;
import ru.open.spc.web.componenets.DeleteLink;
import ru.open.spc.web.componenets.EditLink;
import ru.open.spc.web.componenets.model.SortableDataProviderImpl;
import ru.open.spc.web.dialog.AssetDialog;
import ru.open.spc.web.dialog.DeleteDialog;
import ru.open.spc.web.dialog.RateCoeffDialog;
import ru.open.spc.web.util.UpdateEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.wicket.event.Broadcast.DEPTH;
import static ru.open.spc.calc.kernel.SPCalculationCommon.sortApproximatingRates;
import static ru.open.spc.calc.util.Util.merge;
import static ru.open.spc.calc.util.Util.setTempId;
import static ru.open.spc.web.pages.asset.AssetMainTablePanel.TABBED_PANEL_PAGE;
import static ru.open.spc.web.util.WicketUtil.createSortableColumnsWithPrefix;
import static ru.open.spc.web.util.WicketUtil.findComponentBy;

public class RateCoeffTabPanel extends ModelObjectAwarePanel {
    private final Form<Asset> rateCoeffForm = new Form<>("rateCoeffForm", new Model<>());
    private final SortableDataProviderImpl<ApproximatingRate> dataProvider = new SortableDataProviderImpl<>();
    private final RateCoeffDialog rateCoeffDialog = new RateCoeffDialog ("rateCoeffDialog");
    private final DeleteDialog<ApproximatingRate> deleteRateDialog;
    private final List<ApproximatingRate> deletedList = new ArrayList<>();
    public static final String DIALOG_RATE_EVENT = "dialogRateEvent";

    private @SpringBean ApproximatingRateManager rateManager;

    public RateCoeffTabPanel(String id) {
        super(id);
        Injector.get().inject(this);
        deleteRateDialog = new DeleteDialog<>("deleteRateDialog", new ResourceModel("delete.dialog.title").getObject(),
                new ResourceModel("delete.rate.dialog.title").getObject(), DialogButtons.YES_NO);
        add(rateCoeffDialog);
        add(rateCoeffForm);
        add(deleteRateDialog);
        rateCoeffForm.add(new AjaxLink<String>("newRateLink") {
            @Override
            public void onClick(AjaxRequestTarget target) {
                rateCoeffDialog.setDefaultModelObject(setTempId(new ApproximatingRate()));
                rateCoeffDialog.open(target);
            }
        });

        dataProvider.setList(Collections.<ApproximatingRate>emptyList());
        List<IColumn<ApproximatingRate, String>> columns = new ArrayList<>();
        columns.add(new HeaderlessColumn<ApproximatingRate, String>() {
            public void populateItem(Item<ICellPopulator<ApproximatingRate>> cellItem, String componentId,
                                     final IModel<ApproximatingRate> model) {
                cellItem.add(new EditLink(componentId).add(new AjaxLink<String>("editLink") {

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        rateCoeffDialog.setDefaultModelObject(model.getObject());
                        rateCoeffDialog.open(target);

                    }
                }));
            }
        });

        columns.add(new HeaderlessColumn<ApproximatingRate, String>() {
            public void populateItem(Item<ICellPopulator<ApproximatingRate>> cellItem, String componentId,
                                     final IModel<ApproximatingRate> model) {
                cellItem.add(new DeleteLink(componentId).add(new AjaxLink<String>("deleteLink") {

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        deleteRateDialog.setObject(model.getObject());
                        deleteRateDialog.open(target);

                    }
                }));
            }
        });

        columns.addAll(createSortableColumnsWithPrefix("asset.coefficients", "fromDate", "rate"));

        DataTable<ApproximatingRate, Class<?>> resultListView = new AjaxFallbackDefaultDataTable("coeffList", columns, dataProvider, 100);
        rateCoeffForm.add(resultListView.setOutputMarkupId(true));
        rateCoeffForm.add(new AjaxButton("saveButton") {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                final List<ApproximatingRate> updatedList = dataProvider.getList();
                updatedList.forEach(rateManager::save);
                rateManager.remove(deletedList);
                final Asset asset = rateCoeffForm.getModelObject();
                asset.getCoefficients().setApproximatingRates(updatedList);
                send(findComponentBy(TABBED_PANEL_PAGE, getParent()).getParent(), DEPTH,
                        new UpdateEvent(target, Collections.singletonList(asset), AssetDialog.DIALOG_EVENT));
                deletedList.clear();
            }
        });
    }


    public void setModelObject(Asset asset) {
        dataProvider.setList(sortApproximatingRates(asset.getCoefficients().getApproximatingRates(), true));
        rateCoeffForm.setDefaultModelObject(asset);
    }

    @Override
    public void onEvent(IEvent<?> event) {
        Object payload = event.getPayload();
        final List<ApproximatingRate> list = dataProvider.getList();
        final Asset modelObject = rateCoeffForm.getModelObject();
        if (payload instanceof UpdateEvent) {
            UpdateEvent uevent = (UpdateEvent) payload;
            if (uevent.isAccepted(RateCoeffDialog.RATE_COEF_ADD_EVENT)) {
                final ApproximatingRate rate = uevent.getPayload();
                rate.setAssetId(modelObject.getId());
                dataProvider.setList(merge(list, rate));
            } else if (uevent.isAccepted(DeleteDialog.DELETE_EVENT)) {
                if (uevent.getPayload() instanceof ApproximatingRate) {
                    deletedList.add(uevent.getPayload());
                    list.remove(uevent.getPayload());
                    dataProvider.setList(list);
                }
            }
            uevent.getTarget().add(rateCoeffForm);
        }
        super.onEvent(event);
    }
}
