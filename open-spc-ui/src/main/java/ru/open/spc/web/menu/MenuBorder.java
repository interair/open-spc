package ru.open.spc.web.menu;

import org.apache.wicket.markup.html.border.Border;
import ru.open.spc.web.componenets.MenuItem;
import ru.open.spc.web.componenets.MultiLevelCssMenu;
import ru.open.spc.web.pages.*;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class MenuBorder extends Border {

	public MenuBorder(String id) {
		super(id);
		final List<MenuItem> mainMenu = new ArrayList<>();
		mainMenu.add(new MenuItem(getString("navigation.price.link.label"), ExchangePricePage.class));
        mainMenu.add(new MenuItem(getString("navigation.price.flex.link.label"), FlexPricePage.class));
		mainMenu.add(new MenuItem(getString("navigation.saved.data.link.label"), SavedDataPage.class));
        mainMenu.add(new MenuItem(getString("navigation.crm.link.label"), InvestProductPage.class));

		mainMenu.add(createSettingsItem());
		mainMenu.add(new MenuItem(getString("navigation.logout.link.label"), LogoutPage.class));

		addToBorder(new MultiLevelCssMenu("menu", mainMenu));
	}

	private MenuItem createSettingsItem() {
		return new MenuItem(getString("navigation.settings.link.label"))
				.add(new MenuItem(getString("asset.settings.page.link"), AssetsPage.class))
				.add(new MenuItem(getString("product.settings.page.link"), ProductPage.class))
				.add(new MenuItem(getString("currency.settings.page.link"), CurrencyPage.class))
				.add(new MenuItem(getString("auto.call.settings.page.link"), AutoCallSettings.class))
				.add(new MenuItem(getString("navigation.admin.users.link.label"), AdminPage.class));
	}
}
