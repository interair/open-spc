package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.event.Broadcast;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.PricingResultManager;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.model.enums.Direction;
import ru.open.spc.model.result.PricingResult;
import ru.open.spc.web.componenets.renders.ChoiceRendererImpl;
import ru.open.spc.web.componenets.renders.DirectionRender;
import ru.open.spc.web.util.UpdateEvent;

import java.util.Arrays;
import java.util.List;

import static ru.open.spc.web.util.DefaultDialogButtons.getDialogButtons;
import static ru.open.spc.web.util.DefaultDialogButtons.getSaveButton;
import static ru.open.spc.web.util.WicketUtil.createDefaultDatePicker;

@Deprecated
public class PricingResultDialog extends ResizableDialog<PricingResult> {

	@SpringBean private PricingResultManager pricingResultManager;
	@SpringBean private ProductManager productManager;
	@SpringBean private AssetManager assetManager;

	private final Form<PricingResult> pricingResultForm;

	public static final String PRICING_RESULT_EVENT = "dialogEvent";

	public PricingResultDialog(String id) {
		super(id, new ResourceModel("result.title").getObject(), true);
        pricingResultForm = new Form<>("pricingResultForm");
        pricingResultForm.add(new JQueryFeedbackPanel("feedbackPanel"));
        pricingResultForm.setOutputMarkupId(true);
		pricingResultForm.add(createDefaultDatePicker("productStartDate").setRequired(true));
		pricingResultForm.add(createDefaultDatePicker("productStopDate").setRequired(true));
		pricingResultForm.add(new TextField<>("kzk").setType(Double.class));
		pricingResultForm.add(new RequiredTextField<>("strikePrice").setType(Double.class).setRequired(true));
		pricingResultForm.add(new TextField<>("coefficient").setType(Double.class));
		pricingResultForm.add(new RequiredTextField<>("interestRate").setType(Double.class).setRequired(true));
		pricingResultForm.add(new RequiredTextField<>("interval").setType(Integer.class).setRequired(true));
		pricingResultForm.add(new DropDownChoice<>("direction",
			Arrays.asList(Direction.values()),
			new DirectionRender()).setRequired(true));
		pricingResultForm.add(new DropDownChoice<>("asset",
			assetManager.all(),
			new ChoiceRendererImpl<>()).setRequired(true));
		pricingResultForm.add(new DropDownChoice<>("product",
			productManager.all(),
			new ChoiceRendererImpl<>()).setRequired(true));
		add(pricingResultForm);
		pricingResultForm.add(new Spinner<Long>("timeoutHours").setRequired(true));
		pricingResultForm.add(new Spinner<Long>("timeoutMinutes").setRequired(true));
	}

	@Override
	protected DialogButton getSubmitButton() {
		return getSaveButton();
	}

	@Override
	protected List<DialogButton> getButtons() {
		return getDialogButtons();
	}

	@Override
	public Form<?> getForm() {
		return pricingResultForm;
	}

	@Override
	protected void onError(AjaxRequestTarget target) {
		target.add(get("pricingResultForm").get("feedbackPanel"));
	}

	@Override
	protected void onSubmit(AjaxRequestTarget target) {
		PricingResult pricingResult = pricingResultForm.getModel().getObject();
		if (pricingResult != null) {
			pricingResultManager.save(pricingResult);
		}
		send(getParent(), Broadcast.BREADTH,
			new UpdateEvent(target, pricingResult, PRICING_RESULT_EVENT));
	}

	public void setDefaultModelObject(PricingResult pricingResult) {
		pricingResultForm.setDefaultModel(new CompoundPropertyModel<>(pricingResult));
	}

	@Override
	protected void onOpen(AjaxRequestTarget target) {
		target.add(pricingResultForm);
		super.onOpen(target);
	}
}
