package ru.open.spc.web.price.parameters.panel;

import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.web.price.parameters.PriceParametersPanel;

public class PriceExchangeParametersPanel extends PriceParametersPanel {

    @SpringBean(name = "exchangeFactory") TabFactory tabFactory;

    public PriceExchangeParametersPanel(String id) {
        super(id);
    }

    public TabFactory getTabFactory() {
        return tabFactory;
    }
}
