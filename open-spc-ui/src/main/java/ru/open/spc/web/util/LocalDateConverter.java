package ru.open.spc.web.util;

import org.apache.wicket.util.convert.ConversionException;
import org.apache.wicket.util.convert.IConverter;
import org.apache.wicket.util.lang.Args;
import ru.open.spc.dao.util.DateUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class LocalDateConverter implements IConverter<LocalDate> {

    private final String pattern;

    public LocalDateConverter() {
        this(UIUtil.DATE_FORMAT);
    }

    public LocalDateConverter(String pattern) {
        this.pattern = Args.notNull(pattern, "Pattern");
    }

    private DateTimeFormatter getFormatter() {
        return DateTimeFormatter.ofPattern(pattern);
    }

    @Override
    public LocalDate convertToObject(String value, Locale locale) {
        try {
            return DateUtil.toLocalDate(DateFormat.getDateInstance(DateFormat.SHORT, locale).parse(value));
        } catch (final ParseException e) {
            throw new ConversionException(e.getMessage(), e);
        }
    }

    @Override
    public String convertToString(LocalDate value, Locale locale) {
        return value == null ? "" : getFormatter().format(value);
    }

}