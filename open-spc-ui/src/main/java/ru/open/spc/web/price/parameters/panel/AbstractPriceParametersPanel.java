package ru.open.spc.web.price.parameters.panel;

import com.googlecode.wicket.kendo.ui.form.dropdown.DropDownList;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.open.spc.calc.model.AbstractCalculationParameters;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Underlying;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.services.rest.ETCIntegration;
import ru.open.spc.web.componenets.renders.AssetUnityRender;
import ru.open.spc.web.componenets.renders.ChoiceRendererImpl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ru.open.spc.web.price.parameters.PriceParametersPanel.MAIN_FORM_ID;
import static ru.open.spc.web.util.WicketUtil.findComponentBy;

//TODO: cleanup this class!
public abstract class AbstractPriceParametersPanel<T extends AbstractCalculationParameters> extends BaseParametersPanel<T> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractPriceParametersPanel.class);

    @SpringBean private ETCIntegration restIntegration;
	@SpringBean private CalculationManager calculationManager;

    private final TextField<Double> underlyingAssetPrice = new TextField<>("underlyingAssetPrice");
	private final TextField<Integer> underlyingAssetMultiplicator = new TextField<>("underlyingAssetMultiplicator");
	private final DropDownList<Asset> assetChoice;
	private final DropDownList<AssetUnity> assetUnity;
	private final TextField<Double> itemCost = new TextField<>("itemCost");
	private final TextField<String> baseTicker = new TextField<>("baseTicker");
    private final TextField<Double> dollarExchangeRate = new TextField<>("dollarExchangeRate");
    private final List<Component> inputFields = new ArrayList<>();

    public AbstractPriceParametersPanel(String id, final T model) {
		super(id, model);
		assetChoice = new DropDownList<>("asset", Collections.<Asset>emptyList(), new ChoiceRendererImpl<>());
		assetChoice.add(new AjaxFormComponentUpdatingBehavior("onchange") {

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                assetUpdated(model, target);
            }
        });
        addToForm(assetChoice.setRequired(true));
		assetUnity = new DropDownList<>("assetUnity", Arrays.asList(AssetUnity.values()),
			new AssetUnityRender());
        addToForm(assetUnity.setRequired(true));
        addToForm(itemCost.setRequired(true).setEnabled(false));
        addToForm(dollarExchangeRate);
        addToForm(baseTicker.setRequired(true).setType(String.class).add(StringValidator.minimumLength(2)).setEnabled(false));
        addToForm(underlyingAssetPrice.setRequired(true));
        addToForm(underlyingAssetMultiplicator.setRequired(true).setEnabled(false));

	}

	@Override
	protected void onBeforeRender() {
        assetChoice.setChoices(getAssetManager().allFor(getCurrentParams().getProduct().getId()));
		dollarExchangeRate.setModelObject(getUSDExcPrice());
		super.onBeforeRender();
	}

	protected void assetSelected(AbstractCalculationParameters object) {
		LOG.debug("assetSelected");
		if (object != null) {
			object.cleanAssetFields();
			Asset asset = object.getAsset();
			if (asset == null) { asset = new Asset(); }

				LOG.debug("Selected PointPrice = {}, Multiplier = {}, assetUnity = {}, "
					+ "strikeStep = {}, MinimumPrice = {}",
					asset.getPointPrice(), asset.getMultiplier(), asset.getUnity(),
					asset.getStrikeStep(), asset.getMinimumPrice());
				LOG.debug("underlyingAssetPrice = {}, underlyingAssetMultiplicator = {}",
					underlyingAssetPrice, underlyingAssetMultiplicator);
				underlyingAssetMultiplicator.setModelObject(asset.getMultiplier());
				assetUnity.setModelObject(asset.getUnity());
				itemCost.setModelObject(asset.getPointPrice());
				getInvestingSum().setModelObject(asset.getMinimumPrice());
				baseTicker.setModelObject(asset.getBaseTicker());
				underlyingAssetPrice.setModelObject(getAssetPrice(asset));
				dollarExchangeRate.setModelObject(getUSDExcPrice());
		}
	}

	private void assetUpdated(T model, AjaxRequestTarget target) {
		assetSelected(model);
		if (target != null) {
			target.add(underlyingAssetPrice, underlyingAssetMultiplicator, assetUnity,
                itemCost, getInvestingSum(), baseTicker, dollarExchangeRate);
		}
	}

    protected double getAssetPrice(Asset asset) {
        asset.setAssetPrice(updateAssetPrice(asset.getBaseTicker()));
        if (asset.getAssetPrice() == null) {
            return 0d;
        }
        return asset.getAssetPrice().getRealPrice();
    }

    private Underlying updateAssetPrice(String ticker) {
        return calculationManager.getAssetPriceBy(ticker);
    }

    protected Double getUSDExcPrice() {
        return restIntegration.getUSDPrice();
    }

	protected List<FormComponent<? extends Serializable>> getAssetComponents() {
		return Arrays.asList(assetChoice, assetUnity, itemCost, getInvestingSum(), underlyingAssetPrice);
	}

	protected FeedbackPanel getFeedBack() {
		return (FeedbackPanel) findComponentBy(MAIN_FORM_ID, getParent()).get("feedbackPanel");
	}

    public List<Component> getInputFields() {
        return Collections.unmodifiableList(inputFields);
    }

}
