package ru.open.spc.web.componenets.model;

public class BooleanModel extends Model<String> {

	private final boolean yesNo;

	public BooleanModel(boolean yesNo) {
		this.yesNo = yesNo;
	}

	@Override
	public String getObject() {
		return yesNo ? get("yes.label") : get("no.label");
	}
}
