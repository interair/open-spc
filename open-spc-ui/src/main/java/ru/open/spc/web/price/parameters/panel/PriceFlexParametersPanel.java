package ru.open.spc.web.price.parameters.panel;

import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.web.price.parameters.PriceParametersPanel;

public class PriceFlexParametersPanel extends PriceParametersPanel {

    @SpringBean(name = "flexFactory") TabFactory tabFactory;

    public PriceFlexParametersPanel(String id) {
        super(id);
    }

    public TabFactory getTabFactory() {
        return tabFactory;
    }
}
