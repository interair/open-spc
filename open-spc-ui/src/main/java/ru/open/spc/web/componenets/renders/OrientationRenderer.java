package ru.open.spc.web.componenets.renders;

import ru.open.spc.calc.model.Orientation;

public class OrientationRenderer extends StringChoiceRenderer<Orientation> {

    @Override
    public String getKey(Orientation object) {
        switch (object) {
            case INNER:
                return "orientation.inner";
            case OUTER:
                return "orientation.outer";
            default:
                return null;
        }
    }
}
