package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.ui.widget.dialog.AbstractDialog;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

@SuppressWarnings("serial")
public class MultiLinesMessageDialog extends AbstractDialog<String> {

	private final MultiLineLabel label;

	public MultiLinesMessageDialog(final String id, final String title) {
		this(id, title, new Model<>(""));
	}

	public MultiLinesMessageDialog(final String id, final String title, final IModel<String> message) {
		super(id, title, message, true);
		final WebMarkupContainer container = new WebMarkupContainer("container");
		this.add(container);
		this.label = new MultiLineLabel("message", this.getModel());
		container.add(this.label.setOutputMarkupId(true));
	}

	@Override
	protected void onOpen(final AjaxRequestTarget target) {
		target.add(this.label);
	}

	@Override
	public void onClose(final AjaxRequestTarget target, DialogButton button) {
	}

}
