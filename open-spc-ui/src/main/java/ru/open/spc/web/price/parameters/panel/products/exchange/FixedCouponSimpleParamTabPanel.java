package ru.open.spc.web.price.parameters.panel.products.exchange;

import org.apache.wicket.markup.html.form.RequiredTextField;
import ru.open.spc.calc.model.simple.exchange.FixedCouponSimpleParameters;
import ru.open.spc.web.price.parameters.panel.SimplePriceParameterTabPanel;

public class FixedCouponSimpleParamTabPanel extends SimplePriceParameterTabPanel<FixedCouponSimpleParameters> {

	public FixedCouponSimpleParamTabPanel(String id) {
        super(id, new FixedCouponSimpleParameters());
		addToForm(new RequiredTextField<Double>("fixStrike1"));
	}
}