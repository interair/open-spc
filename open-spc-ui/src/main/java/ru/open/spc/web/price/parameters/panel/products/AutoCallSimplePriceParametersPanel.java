package ru.open.spc.web.price.parameters.panel.products;

import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import com.googlecode.wicket.kendo.ui.form.dropdown.DropDownList;
import com.vaynberg.wicket.select2.DragAndDropBehavior;
import com.vaynberg.wicket.select2.Select2MultiChoice;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.util.CollectionModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.RangeValidator;
import ru.open.spc.calc.model.simple.AutoCallParameters;
import ru.open.spc.dao.manager.AutoCallAssetsManager;
import ru.open.spc.dao.manager.AutoCallAssetsRelationManager;
import ru.open.spc.model.AutoCallAsset;
import ru.open.spc.model.AutoCallAssetsRelation;
import ru.open.spc.web.componenets.model.NamedChoiceProvider;
import ru.open.spc.web.price.parameters.panel.BaseParametersPanel;
import ru.open.spc.web.price.parameters.panel.annotation.PricingTabComponent;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@PricingTabComponent(isSimple = true, nameResourceKey = "simple.calculation.tab.label")
public class AutoCallSimplePriceParametersPanel extends BaseParametersPanel<AutoCallParameters> {

	private final Select2MultiChoice<AutoCallAsset> multiSelector;
	private final List<Component> components = new ArrayList<>();

	@SpringBean
	private AutoCallAssetsManager autoCallAssetsManager;
	@SpringBean
	private AutoCallAssetsRelationManager autoCallAssetsRelationManager;
	private final DropDownList<Integer> countDropDown;

	public AutoCallSimplePriceParametersPanel(String id) {

		super(id, new AutoCallParameters());

		countDropDown = new DropDownList<>(
				"countAssets", IntStream.range(1, 5).boxed().collect(Collectors.toList()));

		countDropDown.add(new AjaxFormComponentUpdatingBehavior("onchange") {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				Integer modelObject = countDropDown.getModelObject();
				components.forEach(a -> {
					a.setEnabled(true);
					IntStream.range(modelObject, 5).boxed().forEach(b -> {
						if (a.getId().contains(b.toString())) {
							a.setEnabled(false);
						}
					});
				});
				target.add(components.toArray(new Component[components.size()]));
			}
		});

		addToForm(countDropDown);
		addToForm(new Spinner<Double>("interestRate"));
		FormComponent<Long> countOfDays = new Spinner<Long>("countOfDays").setRequired(true);
		addToForm(countOfDays);

		addToForm(new Spinner<Integer>("timePassed").setMax(1000).setMin(0).add(new RangeValidator<Integer>(0, 1000)));

		addToForm(setVConstraints("clientIncome", 1000d));
		addToForm(setSpinnerWithConstraints("topBarrierRate"));
		addToForm(setSpinnerWithConstraints("bottomBarrierRate"));
		addToForm(setSpinnerWithConstraints("shipmentBarrierRate"));
		addToForm(setSpinnerWithConstraints("additionalCoupon"));
		addToForm(new Spinner<Integer>("coupon").setMax(100).setMin(0).add(new RangeValidator<Integer>(0, 100)));

		NamedChoiceProvider provider = new NamedChoiceProvider();
		provider.setList(autoCallAssetsManager.all());
		multiSelector = new Select2MultiChoice<>("autoCallAssets", new CollectionModel<AutoCallAsset>(), provider);
		multiSelector.setOutputMarkupId(true);
		multiSelector.add(new DragAndDropBehavior());
		addToForm(multiSelector);

		multiSelector.add(new AjaxFormComponentUpdatingBehavior("onchange") {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				List<AutoCallAsset> modelObject = new ArrayList<AutoCallAsset>(multiSelector.getModelObject());
				AutoCallParameters object = getCalculationParamForm().getModel().getObject();
				getCalculationParamForm().getModel().getObject().setAutoCallAssets(new ArrayList<>(modelObject));

				object.reset();
				for (int i = 0; i < modelObject.size(); i++) {
					object.getAutoCallAssets().set(i, modelObject.get(i));
				}

				for (int i = 0; i < object.getAutoCallAssets().size(); i++) {
					for (int j = 0; j < object.getAutoCallAssets().size(); j++) {
						AutoCallAsset ai = object.getAutoCallAssets().get(i);
						AutoCallAsset aj = object.getAutoCallAssets().get(j);

						if (ai.getId() != null && aj.getId() != null) {
							AutoCallAssetsRelation relation = autoCallAssetsRelationManager.getRelation(ai.getId(), aj.getId());
							if (relation != null) {
								object.getCorrelations()[i][j] = relation.getCoefficient();
							}
						}
					}
				}
				target.add(components.toArray(new Component[components.size()]));
			}
		});

		addToFormAC(setVConstraints("autoCallAssets0.initRelativePrice", 1000d));
		addToFormAC(setVConstraints("autoCallAssets1.initRelativePrice", 1000d));
		addToFormAC(setVConstraints("autoCallAssets2.initRelativePrice", 1000d));
		addToFormAC(setVConstraints("autoCallAssets3.initRelativePrice", 1000d));

		addToFormAC(setVConstraints("autoCallAssets0.relativePrice", 1000d));
		addToFormAC(setVConstraints("autoCallAssets1.relativePrice", 1000d));
		addToFormAC(setVConstraints("autoCallAssets2.relativePrice", 1000d));
		addToFormAC(setVConstraints("autoCallAssets3.relativePrice", 1000d));

		addToFormAC(setVConstraints("autoCallAssets0.sigma"));
		addToFormAC(setVConstraints("autoCallAssets1.sigma"));
		addToFormAC(setVConstraints("autoCallAssets2.sigma"));
		addToFormAC(setVConstraints("autoCallAssets3.sigma"));

		addToFormAC(setVConstraints("correlation00"));
		addToFormAC(setVConstraints("correlation01"));
		addToFormAC(setVConstraints("correlation02"));
		addToFormAC(setVConstraints("correlation03"));

		addToFormAC(setVConstraints("correlation11"));
		addToFormAC(setVConstraints("correlation12"));
		addToFormAC(setVConstraints("correlation13"));

		addToFormAC(setVConstraints("correlation22"));
		addToFormAC(setVConstraints("correlation23"));

		addToFormAC(setVConstraints("correlation33"));

	}

	private void addToFormAC(Component component) {
		components.add(component);
		addToForm(component);

	}

	private FormComponent setSpinnerWithConstraints(String name) {
		return new Spinner<Double>(name).setMax(1000).setMin(0).add(new RangeValidator<Double>(0d, 1000d));
	}

	private Component setVConstraints(String name) {
		return setVConstraints(name, 1d);
	}

	private Component setVConstraints(String name, Double max) {
		return new TextField<>(name).add(new RangeValidator<Double>(0d, max));
	}

}