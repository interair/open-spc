package ru.open.spc.web.ajaxhelps;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.attributes.IAjaxCallListener;

public class AjaxCallListener implements IAjaxCallListener {

	@Override
	public CharSequence getSuccessHandler(Component component) {
		return "setButtonsEnable(true);";
	}

	@Override
	public CharSequence getFailureHandler(Component component) {
		return "setButtonsEnable(true);";
	}

	@Override
	public CharSequence getBeforeHandler(Component component) {
		return "setButtonsEnable(false);";
	}

	@Override
	public CharSequence getBeforeSendHandler(Component component) {
		return null;
	}

	@Override
	public CharSequence getAfterHandler(Component component) {
		return null;
	}

	@Override
	public CharSequence getCompleteHandler(Component component) {
		return "setButtonsEnable(true);";
	}

	@Override
	public CharSequence getPrecondition(Component component) {
		return null;
	}

}
