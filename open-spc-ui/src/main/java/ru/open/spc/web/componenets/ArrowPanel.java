package ru.open.spc.web.componenets;

import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import ru.open.spc.model.enums.Direction;

public class ArrowPanel extends Panel {

	public ArrowPanel(String id, Direction direction) {
		super(id);
		switch (direction) {
			case FALLING:
				add(new Fragment("arrowDown", "arrowDown", this));
				break;
			case RISING:
				add(new Fragment("arrowUp", "arrowUp", this));
				break;
		}
	}
}
