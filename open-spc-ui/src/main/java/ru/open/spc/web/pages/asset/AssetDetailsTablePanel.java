package ru.open.spc.web.pages.asset;

import org.apache.wicket.event.IEvent;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.model.ApproximatingRate;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Dividend;
import ru.open.spc.web.util.UpdateEvent;

import java.util.ArrayList;
import java.util.List;

import static ru.open.spc.calc.kernel.SPCalculationCommon.sortApproximatingRates;
import static ru.open.spc.calc.kernel.SPCalculationCommon.sortDividends;

@SuppressWarnings("serial")
public class AssetDetailsTablePanel extends Panel {

	private final PropertyListView<Asset> list;
	@SpringBean private AssetManager assetManager;
	private static final Logger LOG = LoggerFactory.getLogger(AssetDetailsTablePanel.class);

	public AssetDetailsTablePanel(String id) {
		super(id);
		list = new PropertyListView<Asset>("coefficentList", new ArrayList<>()) {

			@Override
			protected void populateItem(final ListItem<Asset> item) {
				item.add(new Label("id"));
				item.add(new Label("name"));
				item.add(new Label("coefficients.fromDate"));
				PropertyListView<ApproximatingRate> rates = new PropertyListView<ApproximatingRate>("coefficients.approximatingRates",
					sortApproximatingRates(item.getModelObject().getCoefficients().getApproximatingRates(), true)) {
					@Override
					protected void populateItem(ListItem<ApproximatingRate> item) {
						item.add(new Label("rate"));
						item.add(new Label("fromDate"));
					}
				};
				rates.setOutputMarkupId(true);
				item.add(rates);
				PropertyListView<Dividend> dividends = new PropertyListView<Dividend>("coefficients.dividends",
					sortDividends(new ArrayList<>(item.getModelObject().getCoefficients().getDividends()), true)) {
					@Override
					protected void populateItem(ListItem<Dividend> item) {
						item.add(new Label("amount"));
						item.add(new Label("payDate"));
					}
				};
				dividends.setOutputMarkupId(true);
				item.add(dividends);
			}
		};
		add(list.setOutputMarkupId(true));
		setOutputMarkupId(true);
	}

	@Override
	public void onEvent(IEvent<?> event) {
		Object payload = event.getPayload();
		if (payload instanceof UpdateEvent) {
			UpdateEvent uevent = (UpdateEvent) payload;
			List<Asset> alist = uevent.getPayload();
			list.setList(alist);
			if (uevent.getTarget() != null) {
				uevent.getTarget().add(this);
			}
		} else {
			LOG.warn("unknown event {}", payload);
		}
		super.onEvent(event);
	}

	@Override
	protected void onBeforeRender() {
		if (list.getList().isEmpty()) {
			list.setList(assetManager.all());
		}
		super.onBeforeRender();
	}
}
