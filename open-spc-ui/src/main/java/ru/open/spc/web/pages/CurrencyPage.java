package ru.open.spc.web.pages;

import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.HeaderlessColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.dao.manager.CurrencyManager;
import ru.open.spc.model.Currency;
import ru.open.spc.web.AbstractWebPage;
import ru.open.spc.web.componenets.DeleteLink;
import ru.open.spc.web.componenets.EditLink;
import ru.open.spc.web.componenets.model.SortableDataProviderImpl;
import ru.open.spc.web.dialog.CurrencyDialog;
import ru.open.spc.web.dialog.DeleteDialog;

import java.util.List;

import static ru.open.spc.web.util.WicketUtil.createSortableColumnsWithPrefix;

@SuppressWarnings("serial")
public class CurrencyPage extends AbstractWebPage {

	@SpringBean private CurrencyManager currencyManager;
    private final SortableDataProviderImpl<Currency> dataProvider = new SortableDataProviderImpl<>();

	public CurrencyPage() {
		final CurrencyDialog currencyDialog = new CurrencyDialog("currencyDialog");
		final DeleteDialog<Currency> deleteMessageDialog = new DeleteDialog("deleteConfirmation",
			getString("delete.dialog.title"),
			getString("currency.delete.message"), DialogButtons.YES_NO);
		deleteMessageDialog.setGenericManager(currencyManager);
		add(new AjaxLink<String>("newCurrencyLink") {
			@Override
			public void onClick(AjaxRequestTarget target) {
				currencyDialog.setDefaultModelObject(new Currency());
				currencyDialog.open(target);
			}
		});
        List<IColumn<Currency, String>> columns = createSortableColumnsWithPrefix("currency", "id", "code", "number", "name");

        columns.add(0, new HeaderlessColumn<Currency, String>() {
            public void populateItem(Item<ICellPopulator<Currency>> cellItem, String componentId,
                                     final IModel<Currency> model) {
                cellItem.add(new EditLink(componentId).add(new AjaxLink<String>("editLink") {

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        currencyDialog.setModelObject(model.getObject());
                        currencyDialog.open(target);
                    }
                }));
            }
        });

        columns.add(1, new HeaderlessColumn<Currency, String>() {
            public void populateItem(Item<ICellPopulator<Currency>> cellItem, String componentId,
                                     final IModel<Currency> model) {
                cellItem.add(new DeleteLink(componentId).add(new AjaxLink<String>("deleteLink") {

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        deleteMessageDialog.setObject(model.getObject());
                        deleteMessageDialog.open(target);
                    }
                }));
            }
        });
        final DataTable<Currency, Class> resultListView = new AjaxFallbackDefaultDataTable("currencyList", columns, dataProvider, 100);
        add(resultListView);
		add(currencyDialog);
		add(deleteMessageDialog);
		setOutputMarkupId(true);
	}

	@Override
	protected void onBeforeRender() {
        dataProvider.setList(currencyManager.all());
		super.onBeforeRender();
	}
}