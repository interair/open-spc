package ru.open.spc.web.excel;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.stereotype.Service;
import ru.open.spc.web.ui.model.PricingResultUIWrapper;

import java.util.List;
import java.util.Map;

@Service
public class PricingResultExcelExport extends AbstractExcelExport<PricingResultUIWrapper> {

	@Override
	protected String getName() {
		return "pricing result for";
	}

	protected static String[] getReportHeaderColumns() {
		return get("id", "product", "asset", "baseTicker", "priceDate",
			"productStartDate", "productStopDate", "currency.code",
			"kzk", "strikePrice", "strike2Price", "direction", "coefficient", "totalReward");
	}

	protected void createRow(int r, Sheet sheet, Map<ExcelStyles, CellStyle> styles, List<PricingResultUIWrapper> data) {
		int i = 0;
		PricingResultUIWrapper result = data.get(r - 1);
		Row row = sheet.createRow(r);
		addCellTo(row, i++, result.getId(), styles);
		addCellTo(row, i++, result.getProduct().getName(), styles);
		addCellTo(row, i++, result.getAsset().getName(), styles);
		addCellTo(row, i++, result.getAsset().getBaseTicker(), styles);
		addCellTo(row, i++, result.getPriceDate(), styles);
		addCellTo(row, i++, result.getProductStartDate(), styles);
		addCellTo(row, i++, result.getProductStopDate(), styles);
		addCellTo(row, i++, result.getCurrency().getCode(), styles);
		addCellTo(row, i++, result.getKzk(), styles);
		addCellTo(row, i++, result.getStrikePrice(), styles);
		addCellTo(row, i++, result.getStrike2Price(), styles);
		addCellTo(row, i++, result.getDirection(), styles);
		addCellTo(row, i++, result.getCoefficient(), styles);
		addCellTo(row, i, result.getMaximumEarning(), styles);
	}
}
