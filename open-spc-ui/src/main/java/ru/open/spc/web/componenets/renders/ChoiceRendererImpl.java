package ru.open.spc.web.componenets.renders;

import org.apache.wicket.markup.html.form.IChoiceRenderer;
import ru.open.spc.model.common.Named;

public class ChoiceRendererImpl<T extends Named> implements IChoiceRenderer<T> {

	@Override
	public Object getDisplayValue(T object) {
		return object.getName();
	}

	@Override
	public String getIdValue(T object, int index) {
		return object.getId().toString();
	}
}
