package ru.open.spc.web.price.parameters.panel.products;

import com.vaynberg.wicket.select2.DragAndDropBehavior;
import com.vaynberg.wicket.select2.Select2MultiChoice;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.model.util.CollectionModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.spc.calc.model.complex.AutoCallComplexCalculationParameters;
import ru.open.spc.dao.manager.AutoCallAssetsManager;
import ru.open.spc.model.AutoCallAsset;
import ru.open.spc.web.componenets.model.NamedChoiceProvider;
import ru.open.spc.web.price.parameters.panel.BaseParametersPanel;
import ru.open.spc.web.price.parameters.panel.annotation.PricingTabComponent;

import java.util.ArrayList;

import static ru.open.spc.web.util.WicketUtil.createSteppedDoubleComp;
import static ru.open.spc.web.util.WicketUtil.createSteppedLongComp;

@PricingTabComponent(isSimple = false, nameResourceKey = "complex.calculation.tab.label")
public class AutoCallComplexPriceParametersPanel extends BaseParametersPanel {

    @SpringBean
    AutoCallAssetsManager autoCallAssetsManager;
    private final Select2MultiChoice<AutoCallAsset> multiSelector;

    public AutoCallComplexPriceParametersPanel(String id) {
        super(id, new AutoCallComplexCalculationParameters());

        addToForm(createSteppedDoubleComp("interestRate"));
        addToForm(createSteppedDoubleComp("clientIncome"));
        addToForm(createSteppedDoubleComp("topBarrierRate"));
        addToForm(createSteppedDoubleComp("bottomBarrierRate"));
        addToForm(createSteppedDoubleComp("shipmentBarrierRate"));
        addToForm(createSteppedLongComp("coupon"));
        addToForm(createSteppedDoubleComp("additionalCoupon"));

        NamedChoiceProvider provider = new NamedChoiceProvider();
        provider.setList(autoCallAssetsManager.all());
        multiSelector = new Select2MultiChoice<>("autoCallAssets", new CollectionModel<AutoCallAsset>(), provider);
        multiSelector.setOutputMarkupId(true);
        multiSelector.setRequired(true);
        multiSelector.add(new DragAndDropBehavior());

        multiSelector.add(new AjaxFormComponentUpdatingBehavior("onchange") {
            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                final AutoCallComplexCalculationParameters modelObject = (AutoCallComplexCalculationParameters)
                    getCalculationParamForm().getModelObject();
                modelObject.setAutoCallAssets(new ArrayList<>(multiSelector.getModelObject()));
            }
        });

        addToForm(multiSelector);
    }
}
