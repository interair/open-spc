package ru.open.spc.web.dialog.tabs;


import com.googlecode.wicket.jquery.core.renderer.TextRenderer;
import com.googlecode.wicket.jquery.ui.form.autocomplete.AutoCompleteTextField;
import com.googlecode.wicket.jquery.ui.form.spinner.Spinner;
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import com.googlecode.wicket.kendo.ui.form.combobox.ComboBox;
import com.googlecode.wicket.kendo.ui.form.dropdown.DropDownList;
import com.vaynberg.wicket.select2.DragAndDropBehavior;
import com.vaynberg.wicket.select2.Select2MultiChoice;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.util.CollectionModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.RangeValidator;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.CalculationManager;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Product;
import ru.open.spc.model.StockAsset;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.web.componenets.model.NamedChoiceProvider;
import ru.open.spc.web.componenets.renders.AssetUnityRender;
import ru.open.spc.web.dialog.AssetDialog;
import ru.open.spc.web.util.UpdateEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.apache.wicket.event.Broadcast.DEPTH;
import static ru.open.spc.web.pages.asset.AssetMainTablePanel.TABBED_PANEL_PAGE;
import static ru.open.spc.web.util.WicketUtil.createDefaultDatePicker;
import static ru.open.spc.web.util.WicketUtil.findComponentBy;

public class AssetTabPanel extends ModelObjectAwarePanel {

    private final Form<Asset> assetForm = new Form<>("assetTabForm");
    private final FeedbackPanel feedbackPanel = new JQueryFeedbackPanel("assetFeedbackPanel");
    private final Select2MultiChoice<Product> productMultiSelector;
    public static final String DIALOG_ASSET_EVENT = "dialogAssetEvent";
    private static final int MAX_VALUE = 1_000_000;
    private static final double MAX_VALUE_D = 10_000_000d;

    @SpringBean private AssetManager assetManager;
    @SpringBean private ProductManager productManager;
    @SpringBean private CalculationManager calculationManager;

    public AssetTabPanel(String id) {
        super(id);
        Injector.get().inject(this);
        final NamedChoiceProvider<Product> productProvider = new NamedChoiceProvider<>();
        productProvider.setList(productManager.all());
        productMultiSelector = new Select2MultiChoice<>("products", new CollectionModel<Product>(), productProvider);
        productMultiSelector.setOutputMarkupId(true);
        productMultiSelector.setRequired(true);
        productMultiSelector.add(new DragAndDropBehavior());
        assetForm.add(productMultiSelector);

        final RequiredTextField<String> name = new RequiredTextField<>("name");

        final NamedChoiceProvider<StockAsset> tickerProvider = new NamedChoiceProvider<StockAsset>() {
            @Override
            protected String getName(StockAsset named) {
                return named.getTicker();
            }
        };
        tickerProvider.setList(calculationManager.getAvailableTickers());
        final AutoCompleteTextField<StockAsset> tickerSelector = new AutoCompleteTextField<StockAsset>("stockAsset", new TextRenderer<StockAsset>() {
            @Override
            public String getText(StockAsset object) {
                if (object == null || object.getTicker() == null) return null;
                return (object.getTicker() + ", " + object.getName()).replace("\"", "'");
            }

        }, StockAsset.class) {
            @Override
            protected List<StockAsset> getChoices(String input) {
                return tickerProvider.matchingName(input, 0);
            }

            @Override
            protected void onSelected(AjaxRequestTarget target) {
                StockAsset stockAsset = this.getModelObject();
                name.setDefaultModelObject(stockAsset.getName());
                target.add(name);
            }

        };
        assetForm.add(feedbackPanel);
        assetForm.add(name.setOutputMarkupId(true));
        assetForm.add(tickerSelector);
        assetForm.add(new ComboBox<>("externalContractTicker", assetManager.getExternalTickerCodes()).setRequired(true).setOutputMarkupId(true));

        assetForm.add(new DropDownList<>("unity", Arrays.asList(AssetUnity.values()), new AssetUnityRender()).setRequired(true));
        assetForm.add(new Spinner<>("multiplier").setRequired(true).setType(Integer.class).add(new RangeValidator<>(0, MAX_VALUE)));
        assetForm.add(new Spinner<>("pointPrice").setRequired(true).setType(Double.class).add(new RangeValidator<>(0d, MAX_VALUE_D)));
        assetForm.add(createDefaultDatePicker("coefficients.fromDate"));

        assetForm.add(new CheckBox("exchange"));
        assetForm.add(new CheckBox("basePriceVariative"));
        assetForm.add(new Spinner<>("strikeStep").setRequired(true).setType(Integer.class).add(new RangeValidator<>(0, MAX_VALUE)));
        assetForm.add(new Spinner("minimumPrice").setRequired(true).setType(Double.class).add(new RangeValidator<>(0d, MAX_VALUE_D)));

        add(assetForm);

        assetForm.add(new AjaxButton("saveButton") {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                Asset asset = assetForm.getModel().getObject();
                if (asset != null) {
                    asset.setProducts(new ArrayList<>(productMultiSelector.getModelObject()));
                    assetManager.save(asset);
                    send(findComponentBy(TABBED_PANEL_PAGE, getParent()).getParent(), DEPTH, new UpdateEvent(target, Collections.singletonList(asset), AssetDialog.DIALOG_EVENT));
                }
            }

            @Override
            public void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }
        });
    }

    public void setModelObject(Asset asset) {
        assetForm.setDefaultModel(new CompoundPropertyModel<>(asset));
        List<Product> products;
        if (asset != null && asset.getProducts() != null) {
            products = asset.getProducts();
        } else {
            products = Collections.emptyList();
        }
        productMultiSelector.setModelObject(new ArrayList<>(products));
    }

}
