package ru.open.spc.web.price.parameters.panel.products.flex;

import ru.open.spc.calc.model.simple.flex.FixedCouponSimpleFlexParameters;

public class FixedCouponFlexSimpleParameterTabPanel extends SimplePriceParameterFlexTabPanel<FixedCouponSimpleFlexParameters> {

	public FixedCouponFlexSimpleParameterTabPanel(String id) {
		super(id, new FixedCouponSimpleFlexParameters());
	}
}
