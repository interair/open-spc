package ru.open.spc.web.price.parameters.panel;

import org.apache.wicket.extensions.markup.html.tabs.ITab;
import ru.open.spc.model.Product;

import java.util.List;

public interface TabFactory {

    List<ITab> createTabsList(Product product);
}
