package ru.open.spc.web.price.parameters.panel;

import org.apache.wicket.markup.html.form.FormComponent;
import ru.open.spc.calc.model.complex.AbstractComplexCalculationParameters;
import ru.open.spc.calc.model.complex.ComplexCalculationParameters;
import ru.open.spc.web.price.parameters.panel.annotation.PricingTabComponent;

import java.io.Serializable;
import java.util.List;

import static ru.open.spc.web.util.WicketUtil.createDefaultDatePicker;
import static ru.open.spc.web.util.WicketUtil.createSteppedDoubleComp;

@PricingTabComponent(isSimple = false, nameResourceKey = "complex.calculation.tab.label")
public class ComplexPriceParameterTabPanel extends AbstractPriceParametersPanel<AbstractComplexCalculationParameters> {

	public ComplexPriceParameterTabPanel(String id, AbstractComplexCalculationParameters model) {
		super(id, model);
		addToForm(createDefaultDatePicker("productStartDate"));
		addToForm(createDefaultDatePicker("productEndDate").setRequired(true));
		addToForm(createSteppedDoubleComp("interestRate"));
		addToForm(createSteppedDoubleComp("strike1"));

		List<FormComponent<? extends Serializable>> assetComponents = getAssetComponents();
		for (FormComponent<? extends Serializable> formComponent : assetComponents) {
			formComponent.setRequired(false);
		}
	}

	public ComplexPriceParameterTabPanel(String id) {
		this(id, new ComplexCalculationParameters());
	}
}
