package ru.open.spc.web.componenets.model;

import org.apache.wicket.model.AbstractReadOnlyModel;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.model.Product;

import java.util.List;

public class ProductsModel extends AbstractReadOnlyModel<List<Product>> {

	private final ProductManager productManager;

	public ProductsModel(ProductManager productManager) {
		this.productManager = productManager;
	}

	@Override
	public List<Product> getObject() {
		return productManager.all();
	}
}
