package ru.open.spc.web.pages;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.open.commons.templates.TemplateBuilder;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.model.Product;
import ru.open.spc.services.dto.InvestProduct;
import ru.open.spc.services.mapper.InvestProductMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CrmIntegrationInvestPage extends WebPage {

    @SpringBean
    private InvestProductMapper productMapper;

    @SpringBean
    private ProductManager productManager;

    @SpringBean
    private TemplateBuilder builder;

    public CrmIntegrationInvestPage() throws Exception {
        final List<InvestProduct> allActive = new ArrayList<>();
        final List<Product> all = productManager.all();
        for (Product product : all) {
            allActive.addAll(productMapper.getPreparedInvestIds(product.getId()).stream().map(productMapper::getById).collect(Collectors.toList()));
        }
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("investProducts", allActive);
        final String result = builder.buildTemplate(objectMap, "crm-integration-page.ftl");
        Label label = new Label("output",result);
        label.setEscapeModelStrings(false);
        add(label);
    }
}
