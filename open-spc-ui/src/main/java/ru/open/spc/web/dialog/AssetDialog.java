package ru.open.spc.web.dialog;

import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import com.vaynberg.wicket.select2.DragAndDropBehavior;
import com.vaynberg.wicket.select2.Select2MultiChoice;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.event.Broadcast;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.util.CollectionModel;
import org.apache.wicket.validation.validator.RangeValidator;
import ru.open.spc.dao.manager.AssetManager;
import ru.open.spc.dao.manager.ProductManager;
import ru.open.spc.model.ApproximatingRate;
import ru.open.spc.model.Asset;
import ru.open.spc.model.Dividend;
import ru.open.spc.model.Product;
import ru.open.spc.model.enums.AssetUnity;
import ru.open.spc.web.componenets.model.NamedChoiceProvider;
import ru.open.spc.web.componenets.renders.AssetUnityRender;
import ru.open.spc.web.util.UpdateEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ru.open.spc.calc.kernel.SPCalculationCommon.sortApproximatingRates;
import static ru.open.spc.calc.kernel.SPCalculationCommon.sortDividends;
import static ru.open.spc.calc.util.MathUtil.HUNDRED;
import static ru.open.spc.calc.util.Util.merge;
import static ru.open.spc.calc.util.Util.setTempId;
import static ru.open.spc.web.util.DefaultDialogButtons.getDialogButtons;
import static ru.open.spc.web.util.DefaultDialogButtons.getSaveButton;
import static ru.open.spc.web.util.WicketUtil.createDefaultDatePicker;

@Deprecated    //TODO: remove it!
public class AssetDialog extends ResizableDialog<Asset> {

	public static final String DIALOG_EVENT = "dialogEvent";

	private ProductManager productManager;
	private AssetManager assetManager;

	private PropertyListView<Dividend> dividendList;
	private PropertyListView<ApproximatingRate> rateList;
	private final Form<Asset> assetForm = new Form<>("assetForm");
    private final Select2MultiChoice<Product> multiSelector;
	private final DividendDialog divDialog;
	private final RateCoeffDialog rateCoeffDialog;
	private final DeleteDialog<Dividend> deleteDividendDialog;
	private final DeleteDialog<ApproximatingRate> deleteRateDialog;

	public AssetDialog(String id) {
		super(id, new ResourceModel("asset.title").getObject(), true);
        NamedChoiceProvider provider = new NamedChoiceProvider();
        provider.setList(productManager.all());
        multiSelector = new Select2MultiChoice<>("products", new CollectionModel<Product>(), provider);
        multiSelector.setOutputMarkupId(true);
        multiSelector.add(new DragAndDropBehavior());
        assetForm.add(multiSelector);
		assetForm.setOutputMarkupId(true);
		divDialog = new DividendDialog("dividendDialog");
		rateCoeffDialog = new RateCoeffDialog("rateCoeffDialog");
		deleteDividendDialog = new DeleteDialog<>("deleteDividendDialog", new ResourceModel("delete.dialog.title").getObject(),
			new ResourceModel("delete.dividend.dialog.title").getObject(), DialogButtons.YES_NO);
		deleteRateDialog = new DeleteDialog<>("deleteRateDialog", new ResourceModel("delete.dialog.title").getObject(),
			new ResourceModel("delete.rate.dialog.title").getObject(), DialogButtons.YES_NO);
		assetForm.add(new JQueryFeedbackPanel("feedbackPanel"));
		assetForm.add(new RequiredTextField<String>("name"));
		assetForm.add(new RequiredTextField<String>("baseTicker"));
		assetForm.add(new DropDownChoice<>("unity",
			Arrays.asList(AssetUnity.values()),
			new AssetUnityRender()).setRequired(true));
		assetForm.add(new RequiredTextField<>("multiplier").setType(Integer.class).add(new RangeValidator<>(0, Integer.MAX_VALUE)));
		assetForm.add(new RequiredTextField<>("pointPrice").setType(Double.class).add(new RangeValidator<>(0d, Double.MAX_VALUE)));
		assetForm.add(createDefaultDatePicker("coefficients.fromDate"));

		assetForm.add(createDefaultDatePicker("spreadVolatility.minVolatilityDate").setRequired(true));
		assetForm.add(new RequiredTextField<>("spreadVolatility.minVolatility").setType(Double.class).add(new RangeValidator<>(-HUNDRED, HUNDRED)));
		assetForm.add(createDefaultDatePicker("spreadVolatility.maxVolatilityDate").setRequired(true));
		assetForm.add(new RequiredTextField<>("spreadVolatility.maxVolatility").setType(Double.class).add(new RangeValidator<>(-HUNDRED, HUNDRED)));

		assetForm.add(new CheckBox("exchange"));
		assetForm.add(new CheckBox("basePriceVariative"));
		assetForm.add(new RequiredTextField<>("strikeStep").setType(Integer.class).add(new RangeValidator<>(0, Integer.MAX_VALUE)));
		assetForm.add(new RequiredTextField<>("minimumPrice").setType(Double.class).add(new RangeValidator<>(0d, Double.MAX_VALUE)));
		addDividends();
		addCoeffRates();
		setOutputMarkupId(true);
		add(assetForm);
		///deleteMessageDialog.setGenericManager(productManager);
	}

	@Override
	protected DialogButton getSubmitButton() {
		return getSaveButton();
	}

	@Override
	protected List<DialogButton> getButtons() {
		return getDialogButtons();
	}

	public void setDefaultModelObject(Asset asset) {
		assetForm.setDefaultModel(new CompoundPropertyModel<>(asset));
		dividendList.setList(sortDividends(asset.getCoefficients().getDividends(), true));
		rateList.setList(sortApproximatingRates(asset.getCoefficients().getApproximatingRates(), true));
        multiSelector.setModelObject(new ArrayList<>(asset.getProducts()));
	}

	@Override
	public Form<?> getForm() {
		return assetForm;
	}

	@Override
	protected void onOpen(AjaxRequestTarget target) {
		target.add(assetForm);
		updateCoeffVisible();
	}

	private void updateCoeffVisible() {
		rateList.setVisible(!rateList.getList().isEmpty());
		dividendList.setVisible(!dividendList.getList().isEmpty());
	}

	@Override
	protected void onError(AjaxRequestTarget target) {
		target.add(get("assetForm").get("feedbackPanel"));
	}

	@Override
	protected void onSubmit(AjaxRequestTarget target) {
		Asset asset = assetForm.getModel().getObject();
		if (asset != null) {
			assetManager.save(asset);
		}
		send(getParent(), Broadcast.BREADTH,
			new UpdateEvent(target, Collections.singletonList(asset), DIALOG_EVENT));
	}

	@Override
	public void onEvent(IEvent<?> event) {
		Object payload = event.getPayload();
		if (payload instanceof UpdateEvent) {
			UpdateEvent uevent = (UpdateEvent) payload;
			if (assetForm.getModelObject() == null || uevent.getTarget() == null) {
				super.onEvent(event);
				return;
			}
			List<Dividend> dividends = assetForm.getModelObject().getCoefficients().getDividends();
			List<ApproximatingRate> rates = assetForm.getModelObject().getCoefficients().getApproximatingRates();
			if (uevent.isAccepted(DividendDialog.DIVIDEND_ADD_EVENT)) {
				dividendList.setList(merge(dividends, uevent.getPayload()));
			} else if (uevent.isAccepted(DeleteDialog.DELETE_EVENT)) {
				if (uevent.getPayload() instanceof Dividend) {
					dividends.remove(uevent.getPayload());
					dividendList.setList(dividends);
				} else if (uevent.getPayload() instanceof ApproximatingRate) {
					rates.remove(uevent.getPayload());
					rateList.setList(rates);
				}
			}
			if (uevent.isAccepted(RateCoeffDialog.RATE_COEF_ADD_EVENT)) {
				rateList.setList(merge(rates, uevent.getPayload()));

			}
			uevent.getTarget().add(assetForm);
			updateCoeffVisible();
		}
		super.onEvent(event);
	}

	private void addDividends() {
		assetForm.add(new AjaxLink<String>("newDivLink") {
			@Override
			public void onClick(AjaxRequestTarget target) {
				divDialog.setDefaultModelObject(setTempId(new Dividend()));
				divDialog.open(target);
			}
		});
		final List<Dividend> dividends = new ArrayList<>();
		if (assetForm.getModelObject() != null) {
			dividends.addAll(getModelObject().getCoefficients().getDividends());
		}
		sortDividends(dividends, true);
		dividendList = new PropertyListView<Dividend>("dividendList", dividends) {
			@Override
			protected void populateItem(final ListItem<Dividend> item) {
				item.add(new AjaxLink<String>("dividend.editLink") {

					@Override
					public void onClick(AjaxRequestTarget target) {
						divDialog.setDefaultModelObject(item.getModelObject());
						divDialog.open(target);
					}
				});
				item.add(new AjaxLink<String>("dividend.deleteLink") {
					@Override
					public void onClick(AjaxRequestTarget target) {
						deleteDividendDialog.setObject(item.getModelObject());
						deleteDividendDialog.open(target);
					}
				});
				item.add(new Label("payDate"));
				item.add(new Label("amount"));
			}
		};
		assetForm.add(dividendList.setOutputMarkupId(true));
		add(divDialog);
		add(deleteDividendDialog);
	}

	private void addCoeffRates() {
		assetForm.add(new AjaxLink<String>("newRateLink") {
			@Override
			public void onClick(AjaxRequestTarget target) {
				rateCoeffDialog.setDefaultModelObject(setTempId(new ApproximatingRate()));
				rateCoeffDialog.open(target);
			}
		});
		final List<ApproximatingRate> rates = new ArrayList<>();
		if (assetForm.getModelObject() != null) {
			rates.addAll(getModelObject().getCoefficients().getApproximatingRates());
		}
		sortApproximatingRates(rates, true);
		rateList = new PropertyListView<ApproximatingRate>("rateList", rates) {
			@Override
			protected void populateItem(final ListItem<ApproximatingRate> item) {
				item.add(new AjaxLink<String>("rate.editLink") {
					@Override
					public void onClick(AjaxRequestTarget target) {
						rateCoeffDialog.setDefaultModelObject(item.getModelObject());
						rateCoeffDialog.open(target);
					}
				});
				item.add(new AjaxLink<String>("rate.deleteLink") {
					@Override
					public void onClick(AjaxRequestTarget target) {
						deleteRateDialog.setObject(item.getModelObject());
						deleteRateDialog.open(target);
					}
				});
				item.add(new Label("fromDate"));
				item.add(new Label("rate"));
			}
		};
		assetForm.add(rateList.setOutputMarkupId(true));
		add(rateCoeffDialog);
		add(deleteRateDialog);
	}

}
