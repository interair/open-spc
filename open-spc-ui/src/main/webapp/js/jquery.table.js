(function ($) {
    $.fn.styleTable = function (options) {
        var defaults = {
            css: 'ui-styled-table'
        };
        options = $.extend(defaults, options);

        return this.each(function () {
            $this = $(this);
            $this.addClass(options.css);
            $this.addClass("table");
            $this.addClass("table-hover");
            $this.addClass("table-striped");

        });
    };
})(jQuery);